<?php


$config['numerals'] = array(
   'głos' => array(
        '1' => 'głos',
        '2' => 'głosy',
        '3' => 'głosy',
        '4' => 'głosy',
        'default' => 'głosów'
    ),
    
 );

$config['variety'] = array(
   'dzień' => array(
        'default' => array(
            '1' => 'dzień',
            'default' => 'dni'
        ),
       'tomorrow' => 'jutro'
   ),
   'godzina' => array(
        'default' => array(
            '1' => 'godzinę',
            '2' => 'godziny',
            '3' => 'godziny',
            '4' => 'godziny',
            '22' => 'godziny',
            '23' => 'godziny',
            '24' => 'godziny',
            'default' => 'godzin'
        ),
   ),
   'minuta' => array(
        'default' => array(
            '1' => 'minutę',
            '2' => 'minuty',
            '3' => 'minuty',
            '4' => 'minuty',
            '22' => 'minuty',
            '23' => 'minuty',
            '24' => 'minuty',
            '32' => 'minuty',
            '33' => 'minuty',
            '34' => 'minuty',
            '42' => 'minuty',
            '43' => 'minuty',
            '44' => 'minuty',
            '52' => 'minuty',
            '53' => 'minuty',
            '54' => 'minuty',
            'default' => 'minut'
        ),
   ),
 );

$config['month'] = array(
    '1' => array(
        'M' => 'Styczeń',
        'D' => 'stycznia'
    ),
    '2' => array(
        'M' => 'Luty',
        'D' => 'lutego'
    ),
    '3' => array(
        'M' => 'Marzec',
        'D' => 'marca'
    ),
    '4' => array(
        'M' => 'Kwiecień',
        'D' => 'kwietnia'
    ),
    '5' => array(
        'M' => 'Maj',
        'D' => 'maja'
    ),
    '6' => array(
        'M' => 'Czerwiec',
        'D' => 'czerwca'
    ),
    '7' => array(
        'M' => 'Lipiec',
        'D' => 'lipca'
    ),
    '8' => array(
        'M' => 'Sierpień',
        'D' => 'sierpnia'
    ),
    '9' => array(
        'M' => 'Wrzesień',
        'D' => 'września'
    ),
    '10' => array(
        'M' => 'Październik',
        'D' => 'października'
    ),
    '11' => array(
        'M' => 'Listopad',
        'D' => 'listopada'
    ),
    '12' => array(
        'M' => 'Grudzień',
        'D' => 'grudnia'
    ),
);