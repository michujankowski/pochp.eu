<?php

/**
  * Module dedicated for EE on ptpz
  * 
**/

/**
 * Ptpz
 * 
 * @package Firma
 * @author admin
 * @copyright 2012
 * @version $Id$
 * @access public
 */
class Ptpz {
  

  
  var $version = '0.1';
  var $settings = array();
  var $name = "ptpz";
  var $numerals = array();
  var $variety = array();
  
  public $return_data = '';
  
  
  function Ptpz()
  {
    $this->EE =& get_instance();
    $this->EE->load->helper(array('url'));
    @session_start();
    if(!isset($_SESSION['courses_filter_tags'])){$_SESSION['courses_filter_tags']='';}
    if(!isset($_SESSION['courses_filter_category'])){$_SESSION['courses_filter_category']='';}
    if(!isset($_SESSION['ekp_logged'])){$_SESSION['ekp_logged']='';}
    if(!isset($_SESSION['ekp_pesel'])){$_SESSION['ekp_pesel']='';}
    
    require PATH_THIRD.'ptpz/lib/config.php';
    $this->settings = $config;
    $this->numerals = $config['numerals'];
    $this->variety = $config['variety'];
  }
  
  function remove_item_by_value($array, $val = '', $preserve_keys = true) {
	if (empty($array) || !is_array($array)) return false;
	if (!in_array($val, $array)) return $array;

	foreach($array as $key => $value) {
		if ($value == $val) unset($array[$key]);
	}

	return ($preserve_keys === true) ? $array : array_values($array);
  }
  
  /**
   * function catch_premium_activation()
   * Catch session varaible after MedpassPremium code is used in activation.
   * EE tag {exp:ptpz:catch_premium_activation}
   * 
   * return: Propoer text depanding on bool(true/false) varaible 
   * 
   */
  function catch_contact_form_submit(){
    if(isset($_POST['send'])){
        $return = false;
        if(isset($_POST['handle'])){
            if($_POST['handle']==''){ $return[] = 'Podaj imię i nazwisko'; }
        }
        else { $return[] = 'Podaj imię i nazwisko.';}
        
        if(isset($_POST['email'])){
            if($_POST['email']==''){ $return[] = 'Podaj adres email'; }
            elseif(!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/',$_POST['email'])){ $return[] = 'To nie jest poprawny adres e-mail'; }
        }
        else { $return[] = 'Podaj adres email.';}
        
        if(isset($_POST['content'])){
            if($_POST['content']==''){ $return[] = 'Wpisz treść'; }
        }
        else { $return[] = 'Wpisz treść.';}
        
        if($return){ return $return; }
        $content = '
        Ptpz POChP - Pomoc<br />
        Imię i Nazwisko: '.$_POST['handle'].'<br />
        Email: '.$_POST['email'].'<br />
        Treść: '.$_POST['content'].'<br />
        ';
        $this->sendEmail('no-reply@ptpz-pochp.test.activeweb.pl','helpdesk@activeweb.pl',$content,'Ptpz POChP - Pomoc');
    }
    else{
        return false;
    }
  }
  
  function create_contact_form(){
    $message = $this->catch_contact_form_submit();
    $return = '';
    
    if($message){ $return .= implode('<br />',$message).'<br />'; }
    
    $return .= '
    <form method="POST">
        <label for="handle">Imię i nazwisko</label><input type="text" name="handle" id="handle" /><br />
        <label for="email">Email</label><input type="text" name="email" id="email" /><br />
        <label for="content">Treść</label><textarea name="content" id="content"></textarea><br />
        <input type="submit" value="wyślij" name="send" />
    </form>
    ';
    return $return;
  }
  
  /**
   * function current_uri()
   * EE tag {exp:ptpz:current_uri}
   * 
   * return: returns current uri string
   * 
   */
   
    function sendEmail($from,$to,$content,$subject){
        
        $mail_type = 'html';//plain';
        $mail_to = $to;//'Krzysztof.Skupinski@connectmedica.com';
        $mail_from = $from;//'Krzysztof.Skupinski@connectmedica.com';
        $mail_replayto = $from;//'Krzysztof.Skupinski@connectmedica.com';
        $mail_subject = $subject;//'Testowe zgłoszenie';
        $mail_message = $content;
        
        
        $this->EE->load->library('email');
        $this->EE->email->wordwrap = true;
        $this->EE->email->mailtype = $mail_type;
        $this->EE->email->priority = '3';
        
        $this->EE->email->EE_initialize();
        $this->EE->email->to($mail_to);
        
        $this->EE->email->from($mail_from, $mail_from);
        $this->EE->email->reply_to($mail_from, $mail_replayto);
        
        $this->EE->email->subject($mail_subject);
        $this->EE->email->message($mail_message);
        $this->EE->email->send();
                
    }
    
    function ekp_log_in(){
        if(isset($_POST['LogIn'])){
            $return = false;
            if(isset($_POST['pesel'])){
                if($_POST['pesel']==''){ $return[] = 'Podaj numer pesel'; }
                elseif((int)$_POST['pesel']==0 || strlen($_POST['pesel'])<11){$return[] = 'To nie jest poprawny numer pesel!';}
            }
            else { $return[] = 'Podaj numer pesel';}
            
            if(isset($_POST['pin'])){
                if($_POST['pin']==''){ $return[] = 'Podaj pin'; }
            }
            else { $return[] = 'Podaj pin.';}
            
            if($return){ return implode('<br />',$return); }
            
            if($_POST['pin']==$this->render_pin($_POST['pesel'])){
                $_SESSION['ekp_logged'] = true;
                $_SESSION['ekp_pesel'] = $_POST['pesel'];
                redirect('http://'.$_SERVER['SERVER_NAME'].'/elektroniczna_karta_pacjenta');
            }
            else {return "Podane dane nie są poprawne.";}
        }
        else{
            session_destroy();
            return false;
        }
    }
    
    
    function render_pin($pesel){
        $pin = '';
        $mymd5 = md5($pesel."admin1", true);
        $size = strlen($mymd5);
        for($i=0;$i<$size;$i++){
            $pin .= ''.ord($mymd5[$i])%10;
        }
        return substr($pin,0,8);
    }
    
    function ekp_data(){
        if(!$_SESSION['ekp_logged']){redirect('http://'.$_SERVER['SERVER_NAME'].'/elektroniczna_karta_pacjenta_logowanie');}
        else{
            
            $connect = mssql_connect('mssql1','pochp_reader', 'ThinkCentreEdge')or die(mssql_get_last_message());
            mssql_select_db('SciencePOChP',$connect)or die(mssql_get_last_message());
            $q = mssql_query("SELECT Id, Doctor_id, CAST(Firstname AS TEXT) AS Firstname, CAST(Lastname AS TEXT) AS Lastname FROM [Patients] WHERE Pesel = '".$_SESSION['ekp_pesel']."'",$connect);

            if(mssql_num_rows($q)!=1){
                session_destroy();
                mssql_close($connect);
                redirect('http://'.$_SERVER['SERVER_NAME'].'/elektroniczna_karta_pacjenta_logowanie/blad_danych');
                return false;
            }

            $user_data = mssql_fetch_assoc($q);
            
            $q = mssql_query("SELECT CAST(Firstname AS VARCHAR) AS Firstname, CAST(Lastname AS TEXT) AS Lastname, Hospital_id FROM [Doctors] WHERE Id = '".$user_data['Doctor_id']."' AND (Role_Id = '2' OR Role_Id = '1')",$connect);
            $doctor_data = mssql_fetch_assoc($q);
            
            $q = mssql_query("SELECT CAST(Firstname AS VARCHAR) AS Firstname, CAST(Lastname AS TEXT) AS Lastname, Hospital_id FROM [Doctors] WHERE Id = '".$user_data['Doctor_id']."' AND Role_Id = '4'",$connect);
            $nurse_data = mssql_fetch_assoc($q);
            
            $q = mssql_query("SELECT CAST(Name AS TEXT) AS Name, CAST(Province AS TEXT) AS Province, CAST(City AS TEXT) AS City FROM [Hospitals] WHERE Id = '".$doctor_data['Hospital_id']."'",$connect);
            $hospital_data = mssql_fetch_assoc($q);
            
            $q=mssql_query("SELECT Id,Treatment_Id,Disease_Id FROM [Procedures] WHERE Patient_Id = '".$user_data['Id']."' AND ProcedureStatus_Id = '1' ORDER BY ProcedureStart ASC");
            $procedure_data = mssql_fetch_assoc($q);
            
            $doctors_visits = array(array(),array(),array());
            $i=0;
            $q=mssql_query("SELECT ProcedureActivityStatus_Id ,ActivityDate ,TypeId FROM [ProcedureActivities] JOIN [ProcedureActivityRules] ON ProcedureActivityRule_Id = ProcedureActivityRules.Id WHERE Procedure_Id = '".$procedure_data['Id']."' AND (TypeId = 2 OR TypeId = 4) ORDER BY ActivityDate ASC");
            while($data = mssql_fetch_assoc($q)){
                if($i==0 && strtotime($data['ActivityDate'])>time()){$i++;}
                elseif($i==1 && strtotime($data['ActivityDate'])>time()){$i++;}
                $doctors_visits[$i][] = array('status' => $data['ProcedureActivityStatus_Id'],'visit_type' => $data['TypeId'],'date' => $data['ActivityDate']);    
            }
            
            $phone_visits = array(array(),array(),array());
            $i=0;
            $activity = 0;
            $q=mssql_query("SELECT ProcedureActivityStatus_Id ,ActivityDate ,TypeId FROM [ProcedureActivities] JOIN [ProcedureActivityRules] ON ProcedureActivityRule_Id = ProcedureActivityRules.Id WHERE Procedure_Id = '".$procedure_data['Id']."' AND TypeId = 3 ORDER BY ActivityDate ASC");
            while($data = mssql_fetch_assoc($q)){
                if($i==0 && strtotime($data['ActivityDate'])>time()){$i++;}
                elseif($i==1 && strtotime($data['ActivityDate'])>time()){$i++;}
                $phone_visits[$i][] = array('status' => $data['ProcedureActivityStatus_Id'],'visit_type' => $data['TypeId'],'date' => $data['ActivityDate']);
            }
            
            $drugs = mssql_query("SELECT Name, DrugForm, Dosage FROM [PatientDrugs] WHERE Patient_Id = ".$user_data['Id']);
            $diseases = mssql_query("SELECT Name, YearStart FROM [PatientConditions] WHERE Patient_Id = ".$user_data['Id']);
              

            $q=mssql_query("SELECT Id FROM [ProcedureActivities] WHERE Procedure_Id = '".$procedure_data['Id']."' AND ProcedureActivityRule_Id = 1 ORDER BY created_at DESC");
            $data = mssql_fetch_assoc($q);
            $id = $data['Id'];
            
            // Pobranie planu zaostrzenia
            $q=mssql_query("SELECT Session_Id FROM [ActivityTasks] WHERE ProcedureActivity_Id = '$id' AND ActivityTaskRule_Id = 7");
            $data = mssql_fetch_assoc($q);
            
            $session_id = $data['Session_Id'];
            
            $q=mssql_query("SELECT TextAnswer FROM [Results] WHERE Session_Id = '$session_id' AND Question_Id = 6002");
            $data = mssql_fetch_assoc($q);
            $zaostrzenie = $data['TextAnswer'];
            
            $q=mssql_query("SELECT Session_Id FROM [ActivityTasks] WHERE ProcedureActivity_Id = '$id' AND ActivityTaskRule_Id = 1");
            $data = mssql_fetch_assoc($q);

            $session_id = $data['Session_Id'];

            
            $q=mssql_query("SELECT IntAnswer FROM [Results] WHERE Session_Id = '$session_id' AND Question_Id = 1002");
            $data = mssql_fetch_assoc($q);
            $zdiagnozowano = $data['IntAnswer'];
            //foreach($user_data as $k => $v){$user_data[$k] = iconv("CP1250","utf-8",$v);}
            //foreach($doctor_data as $k => $v){$doctor_data[$k] = iconv("CP1250","utf-8",$v);}
            //foreach($hospital_data as $k => $v){$hospital_data[$k] = iconv("CP1250","utf-8",$v);}
            
            
            $return = '
            <div id="ca">
                <ul class="ekp_user_data">
	                <li>Imię: <span class="ekp_data">'.$user_data['Firstname'].'</span></li>
	                <li>Nazwisko: <span class="ekp_data">'.$user_data['Lastname'].'</span></li>
	                <li>Pesel: <span class="ekp_data">'.$_SESSION['ekp_pesel'].'</span></li>
	                <li>Lekarz prowadzący: <span class="ekp_data">'.$doctor_data['Lastname'].', '.$doctor_data['Firstname'].'</span></li>
	                <li>Szpital: <span class="ekp_data">"'.$hospital_data['Name'].'" <br /> '.$hospital_data['City'].' ('.$hospital_data['Province'].')</span></li>
	                <li>Rozpoznanie: <span class="ekp_data">J44 POChP</span></li>
	                <li>Rok rozpoznania: <span class="ekp_data">'.$zdiagnozowano.'</span></li>';
            if(mssql_num_rows($diseases)!=0){ 
                $return .='<li>Choroby współistniejące:<span class="ekp_data">';
                while($data = mssql_fetch_assoc($diseases)){
                    $return .= $data['Name'].' - '.$data['YearStart'].' ';
                }
				$return .= '</span></li>';
            }
            $return .='<li>Zalecane leczenie:<span class="ekp_data">';
            if(mssql_num_rows($drugs)==0){ $return .='Brak zaleceń.<br />'; }
            else{
                while($data = mssql_fetch_assoc($drugs)){
                    $return .= $data['Name'].' - '.$data['DrugForm'].' - '.$data['Dosage']. ' ';
                }
            }
            $return .='</span></li>';
            $return .='<li>Plan zaostrzenia:<span class="ekp_data">'.$zaostrzenie.'</span></li>
            
            </ul></div>
            
            <div id="cl">
              <ul class="ekp_user_data">
                <li>Imię: <span class="ekp_data">'.$user_data['Firstname'].'</span></li>
                <li>Nazwisko: <span class="ekp_data">'.$user_data['Lastname'].'</span></li>
                <li>Lekarz prowadzący: <span class="ekp_data">'.$doctor_data['Lastname'].', '.$doctor_data['Firstname'].'</span></li>
                <li>Szpital: <span class="ekp_data">"'.$hospital_data['Name'].'" <br /> '.$hospital_data['City'].' ('.$hospital_data['Province'].')</span></li>
              </ul>
            </div>
            <div id="cr">
              <div class="ekp_actions">
                <a href="/elektroniczna_karta_pacjenta_logowanie/" class="btn logout"><i>Wyloguj</i><em>&gt;</em></a>
                <a href="#" class="btn toggle_test"><i>Test</i><em>&gt;</em></a>
              </div>
              <div class="ekp_visit_list">
                <h3>Lista wizyt:</h3>
                <ul>
                  <li class="f em">
                    <em class="red_gt">&gt;</em>
                    <i class="ekp_data">Lekarskich</i>
                    
                    <div class="ekp_visit_list_content">
                      <span class="visit_nearest_main">Najbliższa wizyta: <span>'.date("d-m-Y H:i",strtotime($doctors_visits[1][0]['date'])).'</span></span>
                      <a class="toggle_doctors_visits btn">Pozostałe wizyty</a>
                      <ul class="doctors_visits">';
                      $return.='<li class="visit_title">Nadchodące wyzyty:</li>';
                      foreach($doctors_visits[2] as $visit){
                          $return.= '<li class="visit_status_future"><em>&bull;</em>'.date("d-m-Y H:i",strtotime($visit['date'])).'</li>
                          ';
                      }
                      
                      $return.='<li class="visit_title">Wizyty odbyte:</li>';
                      foreach($doctors_visits[0] as $visit){
                          $return.= '<li clas<li class="visit_status_done"><em>&bull;</em>'.date("d-m-Y H:i",strtotime($visit['date'])).'</li>
                          ';
                      }
                      $return.='</ul>
                    </div>
                  </li>
                  <li class="em">
                    <em class="red_gt">&gt;</em>
                    <i class="ekp_data">Telefonicznych</i>
                    
                    <div class="ekp_visit_list_content">
                      <span class="visit_nearest_main">Najbliższa wizyta: <span>'.date("d-m-Y H:i",strtotime($phone_visits[1][0]['date'])).'</span></span>
                      <a class="toggle_phone_visits btn">Pozostałe wizyty</a>
                      <ul class="phone_visits">';
                      $return.='<li class="visit_title">Nadchodące wyzyty:</li>';
                      foreach($phone_visits[2] as $visit){
                          $return.= '<li class="visit_status_future"><em>&bull;</em>'.date("d-m-Y H:i",strtotime($visit['date'])).'</li>
                          ';
                      }
                      
                      $return.='<li class="visit_title">Wizyty odbyte:</li>';
                      
                      foreach($phone_visits[0] as $visit){
                          $return.= '<li class="visit_status_done"><em>&bull;</em>'.date("d-m-Y H:i",strtotime($visit['date'])).'</li>
                          ';
                      }
                      $return.='</ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            
            ';
            
            mssql_close($connect);
            return $return;
        }
    }
    
  
}



