<div id="standardContent">
    <h2 class="contentTitle"></h2>

<!-- ========== START ========== ZamowBiuletyn ========== -->
<h2 class="contentTitle"></h2>




	<div class="newsletterRegistration dialog newsletterRegistration-dialog">
	    <?php
//        <form method="post" action="#" name="guiForm_1" id="guiForm_1">
        echo form_open($url, array( 'id' => 'guiForm_0', 'name' => 'guiForm_0'), ''); 
        
        ?>
	          
<div class="first">
	<div class="section">
    <div class="required">
      <div class="label">
         <label for="emailAddress">Adres email</label>:<sup class="fieldsRequiredFootnoteReference">*</sup>
      </div>	    	
    </div>
    <div class="content">
        <?php echo form_input(array('name'=>"emailAddress", 
                                  'value'=>($emailAddress)?$emailAddress:'', 
                                  'size'=>48, 
                                  'maxlength' => 128, 
                                  'class' => 'emailAddress', 
                                  'id'=>'emailAddress',
                                  'type' => 'text',
                                  )
                  ); 
          if(!$emailCorrect): ?>
            <div class="fieldError">Proszę podać adres email.</div>
       <?php
          endif;
        ?>
      <!-- #komunikat_1# -->
    </div>	    	
		<div class="clearFloat">&nbsp;</div>
	</div>	    	
  <div class="clearFloat">&nbsp;</div>
</div>

            
	<div class="section">
		
    <table class="confirmation">
      <tbody>
        <tr>
          <td valign="top"><span style="white-space: nowrap">
               <?php 
               
                  $options = array(
                        'name'=>"confirmation",
                        'class' => 'control', 
                        'id'=>'confirmation',
                        'type' => 'checkbox',
                        'value' => '1'
                  );
                  if($confirmation)
                  {
                    $options['checked'] = 'checked';
                  }
                  echo form_input($options); 
              ?>
 
              <sup class="fieldsRequiredFootnoteReference">*</sup>&nbsp;</span>
          </td>
          <td class="table-label-form">Wyrażam zgodę na przetwarzanie moich danych osobowych, zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (Dz. U. Nr 133, poz. 883 z późn. zm.). Mam prawo do wglądu oraz poprawienia moich danych.
            <?php if(!$confirmation): ?>
              <div class="fieldErrors">
                <div class="fieldError">Wyrażenie zgody jest konieczne.</div>
              </div>
            <?php endif; ?>
          </td>
        </tr>
      </tbody>
    </table>  
            
		<div class="clearFloat">&nbsp;</div>
	</div>	    	





	          
	        <div class="actionButtons">

	

				<div class="button">
            <a class="actionSend" title="dodaj" id="ButtonLinkSubmit_0" href="javascript: document.guiForm_0.submit();">&nbsp;</a>
        </div>		
		    			            
	            
	        
	<div class="clearFloat">&nbsp;</div>
</div>	    	

	      
		
	
	
</form>

		
        <div class="clearFloat">&nbsp;</div>
        <p class="footnote">znakiem <span class="fieldsRequiredFootnoteReferenceMark">*</span> zostały wyróżnione pola obowiązkowe</p>

        <div class="clearFloat">&nbsp;</div>
        
	</div>		    	


	

   
<!-- ========== STOP ========== ZamowBiuletyn ========== --> 
</div>



<?php
/*
<div class="newsletterRegistration shortcut newsletterRegistration-shortcut" jwcid="$remove$">
  <h2 class="heading" title="Biuletyn informacyjny">&nbsp;</h2>
  <div class="dialog">
      <?php
//        <form method="post" action="#" name="guiForm_1" id="guiForm_1">
        echo form_open($url, array( 'id' => 'guiForm_1', 'name' => 'guiForm_1'), ''); 
        
        ?>
      <div class="first">
        <div class="section">
          <div class="label">
            <label for="emailAddress">Wpisz swój adres email, aby otrzymywać najnowsze informacje i aktualności.</label>
          </div>
          <div class="content">
 
            <?php echo form_input(array('name'=>"emailAddress", 
                                        'value'=>'', 
                                        'size'=>32, 
                                        'maxlength' => 128, 
                                        'class' => 'emailAddress', 
                                        'id'=>'emailAddress',
                                        'type' => 'text',
                                        'onkeydown' => 'this.style.backgroundImage = \'none\''
                                        )
                        ); 
            ?>
            
          </div>
        </div>
      </div>

      <div class="actionButtons">
        <div class="button">
            <?php //<a href="#" id="ButtonLinkSubmit_1" title="Dodaj"><span>&nbsp;</span>dodaj</a> ?>
            <?php  echo form_submit('bulletin_submit', lang('dodaj'), 'class="submit"'); ?>  
        </div>
        <div class="clearFloat">&#160;</div>
      </div>
    <?php echo form_close(); ?>
  </div>
</div>
    




<?php
/*
<div class="section login_form">
  <div class="header">
    <h2>Formularz logowania</h2>
  </div>
  <div class="content">
  <div class="frm">
      <?php 
    if(isset($message) && $message):?>
      <p><?php echo $message; ?></p>
    <?php
    endif; ?>
    
   <div class="input string">
      <?php    
            //$urlAction = $url?'http://'.$url:'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'.html';
            echo form_open('#', '', ''); ?>
      <?php echo form_input(array('name'=>"medpass_identifier", 'value'=>'', 'size'=>40)); ?>
    </div>
    <div class="btn">
      <?php echo form_submit('quicklinks_update', lang('zaloguj sie'), 'class="submit"'); ?>  
    </div>
  
    <?php  echo form_close(); ?>
  </p>
  </div>  
  </div>
</div>
  */
?>
  