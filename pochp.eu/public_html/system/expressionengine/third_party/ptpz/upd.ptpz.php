<?php
class Ptpz_upd 
{

  var $version = '0.1';
  
  function __construct()
  {
    $this->EE =& get_instance();

  }
  
  
  
  function install() 
  {
    $this->EE->load->dbforge();
    $data = array(
      'module_name' => 'Ptpz' ,
      'module_version' => $this->version,
      'has_cp_backend' => 'n',
      'has_publish_fields' => 'n'
    );
    $this->EE->db->insert('modules', $data);
    
    
    $data = array(
      'class'		=> 'Ptpz' ,
      'method'	=> 'index'
    );
    $this->EE->db->insert('actions', $data);

    
    return TRUE;
  }
  
  function uninstall()
  {
    $this->EE->load->dbforge();

    $this->EE->db->select('module_id');
    $query = $this->EE->db->get_where('modules', array('module_name' => 'Ptpz'));

    $this->EE->db->where('module_id', $query->row('module_id'));
    $this->EE->db->delete('module_member_groups');

    $this->EE->db->where('module_name', 'Ptpz');
    $this->EE->db->delete('modules');

    $this->EE->db->where('class', 'Ptpz');
    $this->EE->db->delete('actions');

    
    
    return TRUE;
  }
  
  
  function update($current = '')
  {
    return FALSE;
  }
  
}
