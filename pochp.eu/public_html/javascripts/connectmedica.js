var banners_interval = true;
$(document).ready(function() {
  $('input[type=text]').focus(function() {
    if ($(this).val() == $(this).attr('title')) {
      clearMePrevious = $(this).val();
      $(this).val('');
    }
  });
  
  $('input[type=text]').blur(function() {
    if ($(this).val() == '') {
      $(this).val(clearMePrevious);
    }
  });
  
  
  /* animacja na stronie glownej */
  if($(".carousel ul li").length > 1){
      $(".carousel").jCarouselLite({
          btnNext: ".next_big, .next",
          btnPrev: ".prev_big, .prev",
          speed: 2400
      }); 
      banners_interval = setInterval("rotateBanners()",7000);
  }
  else{
    $(".carousel ul").css("margin-top","-44px")
  }
  
  
  $(".carousel ul li").mouseover(function(){
     clearInterval(banners_interval);
  }).mouseout(function(){
     banners_interval = setInterval("rotateBanners()",7000);
  });
    
    
    if($("#ft ol li").length > 3){
        $("#ft ol li").slice(0, 3).clone().appendTo($('#ft ol'));
        var steps = ($("#ft ol li").length-3);
        $("#ft ol").css('width',(steps+3)*250+'px')
        //$("#ft ol").animate({'margin-left':(6*(-235))+'px'}, 5000*6,'linear', function(){$("#ft ol").css('margin-left','0px'); });
        partner_slider()
    }
    
    $('a.toggle_doctors_visits').click(function(){
        $('ul.doctors_visits').toggle();
        return false;
    });
    
    $('a.toggle_phone_visits').click(function(){
        $('ul.phone_visits').toggle();
        return false;
    });
    
    $('a.toggle_test').click(function(){$('#cl,#ca').toggle(); return false;})
}); 

function rotateBanners(){
    $('span.next').click();
    return false;
}

function partner_slider(){
var steps = ($("#ft ol li").length-3);
$("#ft ol").animate({'margin-left':(steps*(-250))+'px'}, 5000*steps,'linear', function(){$("#ft ol").css('margin-left','0px'); partner_slider();});
}

$(document).ready(function(){
    if ( $.cookie('popup') != 'true'){
        $('body').append('<div id="pop-up" class="cookie-alert"><p>Strona Polskiego Towarzystwa Programów Zdrowotnych generuje pliki cookie, które są przechowywane na Twoim komputerze.</p><p>Obsługę plików cookie możesz wyłączyć w ustawieniach swojej przeglądarki.</p><p><a href="/strony/polityka-prywatnoci-i-wykorzystywania-plikow-cookies-w-serwisach-internetow">Kliknij</a> aby przeczytać pełen tekst Polityki prywatności i wykorzystywania plików cookies w serwisach PTPZ.</p><p><input class="btn" type="button" value="Rozumiem" /></p></div>');
        var popup = $('#pop-up');
        popup.fadeIn('500');

        popup.find('input[type=button]').on('click', function(){
            popup.fadeOut('500');
            setTimeout(function(){
                popup.remove();
                $.cookie('popup', 'true', {expires: 999999});
            }, 500);
        });
    }
});