-- MySQL dump 10.14  Distrib 10.0.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: pochp.eu    Database: pochp_dbforum
-- ------------------------------------------------------
-- Server version	5.0.67-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `phpbb_acl_groups`
--

DROP TABLE IF EXISTS `phpbb_acl_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_groups` (
  `group_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_role_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_setting` tinyint(2) NOT NULL default '0',
  KEY `group_id` (`group_id`),
  KEY `auth_opt_id` (`auth_option_id`),
  KEY `auth_role_id` (`auth_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_groups`
--

LOCK TABLES `phpbb_acl_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_groups` DISABLE KEYS */;
INSERT INTO `phpbb_acl_groups` VALUES (1,0,85,0,1),(1,0,93,0,1),(1,0,111,0,1),(5,0,0,5,0),(5,0,0,1,0),(2,0,0,6,0),(3,0,0,6,0),(4,0,0,5,0),(4,0,0,10,0),(1,1,0,17,0),(2,1,0,17,0),(3,1,0,17,0),(6,1,0,17,0),(1,2,0,17,0),(2,2,0,15,0),(3,2,0,15,0),(4,2,0,21,0),(5,2,0,14,0),(5,2,0,10,0),(6,2,0,19,0),(7,0,0,23,0),(7,2,0,24,0);
/*!40000 ALTER TABLE `phpbb_acl_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_options`
--

DROP TABLE IF EXISTS `phpbb_acl_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_options` (
  `auth_option_id` mediumint(8) unsigned NOT NULL auto_increment,
  `auth_option` varchar(50) collate utf8_bin NOT NULL default '',
  `is_global` tinyint(1) unsigned NOT NULL default '0',
  `is_local` tinyint(1) unsigned NOT NULL default '0',
  `founder_only` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`auth_option_id`),
  UNIQUE KEY `auth_option` (`auth_option`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_options`
--

LOCK TABLES `phpbb_acl_options` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_options` DISABLE KEYS */;
INSERT INTO `phpbb_acl_options` VALUES (1,'f_',0,1,0),(2,'f_announce',0,1,0),(3,'f_attach',0,1,0),(4,'f_bbcode',0,1,0),(5,'f_bump',0,1,0),(6,'f_delete',0,1,0),(7,'f_download',0,1,0),(8,'f_edit',0,1,0),(9,'f_email',0,1,0),(10,'f_flash',0,1,0),(11,'f_icons',0,1,0),(12,'f_ignoreflood',0,1,0),(13,'f_img',0,1,0),(14,'f_list',0,1,0),(15,'f_noapprove',0,1,0),(16,'f_poll',0,1,0),(17,'f_post',0,1,0),(18,'f_postcount',0,1,0),(19,'f_print',0,1,0),(20,'f_read',0,1,0),(21,'f_reply',0,1,0),(22,'f_report',0,1,0),(23,'f_search',0,1,0),(24,'f_sigs',0,1,0),(25,'f_smilies',0,1,0),(26,'f_sticky',0,1,0),(27,'f_subscribe',0,1,0),(28,'f_user_lock',0,1,0),(29,'f_vote',0,1,0),(30,'f_votechg',0,1,0),(31,'m_',1,1,0),(32,'m_approve',1,1,0),(33,'m_chgposter',1,1,0),(34,'m_delete',1,1,0),(35,'m_edit',1,1,0),(36,'m_info',1,1,0),(37,'m_lock',1,1,0),(38,'m_merge',1,1,0),(39,'m_move',1,1,0),(40,'m_report',1,1,0),(41,'m_split',1,1,0),(42,'m_ban',1,0,0),(43,'m_warn',1,0,0),(44,'a_',1,0,0),(45,'a_aauth',1,0,0),(46,'a_attach',1,0,0),(47,'a_authgroups',1,0,0),(48,'a_authusers',1,0,0),(49,'a_backup',1,0,0),(50,'a_ban',1,0,0),(51,'a_bbcode',1,0,0),(52,'a_board',1,0,0),(53,'a_bots',1,0,0),(54,'a_clearlogs',1,0,0),(55,'a_email',1,0,0),(56,'a_fauth',1,0,0),(57,'a_forum',1,0,0),(58,'a_forumadd',1,0,0),(59,'a_forumdel',1,0,0),(60,'a_group',1,0,0),(61,'a_groupadd',1,0,0),(62,'a_groupdel',1,0,0),(63,'a_icons',1,0,0),(64,'a_jabber',1,0,0),(65,'a_language',1,0,0),(66,'a_mauth',1,0,0),(67,'a_modules',1,0,0),(68,'a_names',1,0,0),(69,'a_phpinfo',1,0,0),(70,'a_profile',1,0,0),(71,'a_prune',1,0,0),(72,'a_ranks',1,0,0),(73,'a_reasons',1,0,0),(74,'a_roles',1,0,0),(75,'a_search',1,0,0),(76,'a_server',1,0,0),(77,'a_styles',1,0,0),(78,'a_switchperm',1,0,0),(79,'a_uauth',1,0,0),(80,'a_user',1,0,0),(81,'a_userdel',1,0,0),(82,'a_viewauth',1,0,0),(83,'a_viewlogs',1,0,0),(84,'a_words',1,0,0),(85,'u_',1,0,0),(86,'u_attach',1,0,0),(87,'u_chgavatar',1,0,0),(88,'u_chgcensors',1,0,0),(89,'u_chgemail',1,0,0),(90,'u_chggrp',1,0,0),(91,'u_chgname',1,0,0),(92,'u_chgpasswd',1,0,0),(93,'u_download',1,0,0),(94,'u_hideonline',1,0,0),(95,'u_ignoreflood',1,0,0),(96,'u_masspm',1,0,0),(97,'u_masspm_group',1,0,0),(98,'u_pm_attach',1,0,0),(99,'u_pm_bbcode',1,0,0),(100,'u_pm_delete',1,0,0),(101,'u_pm_download',1,0,0),(102,'u_pm_edit',1,0,0),(103,'u_pm_emailpm',1,0,0),(104,'u_pm_flash',1,0,0),(105,'u_pm_forward',1,0,0),(106,'u_pm_img',1,0,0),(107,'u_pm_printpm',1,0,0),(108,'u_pm_smilies',1,0,0),(109,'u_readpm',1,0,0),(110,'u_savedrafts',1,0,0),(111,'u_search',1,0,0),(112,'u_sendemail',1,0,0),(113,'u_sendim',1,0,0),(114,'u_sendpm',1,0,0),(115,'u_sig',1,0,0),(116,'u_viewonline',1,0,0),(117,'u_viewprofile',1,0,0);
/*!40000 ALTER TABLE `phpbb_acl_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_roles`
--

DROP TABLE IF EXISTS `phpbb_acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_roles` (
  `role_id` mediumint(8) unsigned NOT NULL auto_increment,
  `role_name` varchar(255) collate utf8_bin NOT NULL default '',
  `role_description` text collate utf8_bin NOT NULL,
  `role_type` varchar(10) collate utf8_bin NOT NULL default '',
  `role_order` smallint(4) unsigned NOT NULL default '0',
  PRIMARY KEY  (`role_id`),
  KEY `role_type` (`role_type`),
  KEY `role_order` (`role_order`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_roles`
--

LOCK TABLES `phpbb_acl_roles` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_roles` DISABLE KEYS */;
INSERT INTO `phpbb_acl_roles` VALUES (1,'ROLE_ADMIN_STANDARD','ROLE_DESCRIPTION_ADMIN_STANDARD','a_',1),(2,'ROLE_ADMIN_FORUM','ROLE_DESCRIPTION_ADMIN_FORUM','a_',3),(3,'ROLE_ADMIN_USERGROUP','ROLE_DESCRIPTION_ADMIN_USERGROUP','a_',4),(4,'ROLE_ADMIN_FULL','ROLE_DESCRIPTION_ADMIN_FULL','a_',2),(5,'ROLE_USER_FULL','ROLE_DESCRIPTION_USER_FULL','u_',3),(6,'ROLE_USER_STANDARD','ROLE_DESCRIPTION_USER_STANDARD','u_',1),(7,'ROLE_USER_LIMITED','ROLE_DESCRIPTION_USER_LIMITED','u_',2),(8,'ROLE_USER_NOPM','ROLE_DESCRIPTION_USER_NOPM','u_',4),(9,'ROLE_USER_NOAVATAR','ROLE_DESCRIPTION_USER_NOAVATAR','u_',5),(10,'ROLE_MOD_FULL','ROLE_DESCRIPTION_MOD_FULL','m_',3),(11,'ROLE_MOD_STANDARD','ROLE_DESCRIPTION_MOD_STANDARD','m_',1),(12,'ROLE_MOD_SIMPLE','ROLE_DESCRIPTION_MOD_SIMPLE','m_',2),(13,'ROLE_MOD_QUEUE','ROLE_DESCRIPTION_MOD_QUEUE','m_',4),(14,'ROLE_FORUM_FULL','ROLE_DESCRIPTION_FORUM_FULL','f_',7),(15,'ROLE_FORUM_STANDARD','ROLE_DESCRIPTION_FORUM_STANDARD','f_',5),(16,'ROLE_FORUM_NOACCESS','ROLE_DESCRIPTION_FORUM_NOACCESS','f_',1),(17,'ROLE_FORUM_READONLY','ROLE_DESCRIPTION_FORUM_READONLY','f_',2),(18,'ROLE_FORUM_LIMITED','ROLE_DESCRIPTION_FORUM_LIMITED','f_',3),(19,'ROLE_FORUM_BOT','ROLE_DESCRIPTION_FORUM_BOT','f_',9),(20,'ROLE_FORUM_ONQUEUE','ROLE_DESCRIPTION_FORUM_ONQUEUE','f_',8),(21,'ROLE_FORUM_POLLS','ROLE_DESCRIPTION_FORUM_POLLS','f_',6),(22,'ROLE_FORUM_LIMITED_POLLS','ROLE_DESCRIPTION_FORUM_LIMITED_POLLS','f_',4),(23,'ROLE_USER_NEW_MEMBER','ROLE_DESCRIPTION_USER_NEW_MEMBER','u_',6),(24,'ROLE_FORUM_NEW_MEMBER','ROLE_DESCRIPTION_FORUM_NEW_MEMBER','f_',10);
/*!40000 ALTER TABLE `phpbb_acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_roles_data`
--

DROP TABLE IF EXISTS `phpbb_acl_roles_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_roles_data` (
  `role_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_setting` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`role_id`,`auth_option_id`),
  KEY `ath_op_id` (`auth_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_roles_data`
--

LOCK TABLES `phpbb_acl_roles_data` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_roles_data` DISABLE KEYS */;
INSERT INTO `phpbb_acl_roles_data` VALUES (1,44,1),(1,46,1),(1,47,1),(1,48,1),(1,50,1),(1,51,1),(1,52,1),(1,56,1),(1,57,1),(1,58,1),(1,59,1),(1,60,1),(1,61,1),(1,62,1),(1,63,1),(1,66,1),(1,68,1),(1,70,1),(1,71,1),(1,72,1),(1,73,1),(1,79,1),(1,80,1),(1,81,1),(1,82,1),(1,83,1),(1,84,1),(2,44,1),(2,47,1),(2,48,1),(2,56,1),(2,57,1),(2,58,1),(2,59,1),(2,66,1),(2,71,1),(2,79,1),(2,82,1),(2,83,1),(3,44,1),(3,47,1),(3,48,1),(3,50,1),(3,60,1),(3,61,1),(3,62,1),(3,72,1),(3,79,1),(3,80,1),(3,82,1),(3,83,1),(4,44,1),(4,45,1),(4,46,1),(4,47,1),(4,48,1),(4,49,1),(4,50,1),(4,51,1),(4,52,1),(4,53,1),(4,54,1),(4,55,1),(4,56,1),(4,57,1),(4,58,1),(4,59,1),(4,60,1),(4,61,1),(4,62,1),(4,63,1),(4,64,1),(4,65,1),(4,66,1),(4,67,1),(4,68,1),(4,69,1),(4,70,1),(4,71,1),(4,72,1),(4,73,1),(4,74,1),(4,75,1),(4,76,1),(4,77,1),(4,78,1),(4,79,1),(4,80,1),(4,81,1),(4,82,1),(4,83,1),(4,84,1),(5,85,1),(5,86,1),(5,87,1),(5,88,1),(5,89,1),(5,90,1),(5,91,1),(5,92,1),(5,93,1),(5,94,1),(5,95,1),(5,96,1),(5,97,1),(5,98,1),(5,99,1),(5,100,1),(5,101,1),(5,102,1),(5,103,1),(5,104,1),(5,105,1),(5,106,1),(5,107,1),(5,108,1),(5,109,1),(5,110,1),(5,111,1),(5,112,1),(5,113,1),(5,114,1),(5,115,1),(5,116,1),(5,117,1),(6,85,1),(6,86,1),(6,87,1),(6,88,1),(6,89,1),(6,92,1),(6,93,1),(6,94,1),(6,96,1),(6,97,1),(6,98,1),(6,99,1),(6,100,1),(6,101,1),(6,102,1),(6,103,1),(6,106,1),(6,107,1),(6,108,1),(6,109,1),(6,110,1),(6,111,1),(6,112,1),(6,113,1),(6,114,1),(6,115,1),(6,117,1),(7,85,1),(7,87,1),(7,88,1),(7,89,1),(7,92,1),(7,93,1),(7,94,1),(7,99,1),(7,100,1),(7,101,1),(7,102,1),(7,105,1),(7,106,1),(7,107,1),(7,108,1),(7,109,1),(7,114,1),(7,115,1),(7,117,1),(8,85,1),(8,87,1),(8,88,1),(8,89,1),(8,92,1),(8,93,1),(8,94,1),(8,115,1),(8,117,1),(8,96,0),(8,97,0),(8,109,0),(8,114,0),(9,85,1),(9,88,1),(9,89,1),(9,92,1),(9,93,1),(9,94,1),(9,99,1),(9,100,1),(9,101,1),(9,102,1),(9,105,1),(9,106,1),(9,107,1),(9,108,1),(9,109,1),(9,114,1),(9,115,1),(9,117,1),(9,87,0),(10,31,1),(10,32,1),(10,42,1),(10,33,1),(10,34,1),(10,35,1),(10,36,1),(10,37,1),(10,38,1),(10,39,1),(10,40,1),(10,41,1),(10,43,1),(11,31,1),(11,32,1),(11,34,1),(11,35,1),(11,36,1),(11,37,1),(11,38,1),(11,39,1),(11,40,1),(11,41,1),(11,43,1),(12,31,1),(12,34,1),(12,35,1),(12,36,1),(12,40,1),(13,31,1),(13,32,1),(13,35,1),(14,1,1),(14,2,1),(14,3,1),(14,4,1),(14,5,1),(14,6,1),(14,7,1),(14,8,1),(14,9,1),(14,10,1),(14,11,1),(14,12,1),(14,13,1),(14,14,1),(14,15,1),(14,16,1),(14,17,1),(14,18,1),(14,19,1),(14,20,1),(14,21,1),(14,22,1),(14,23,1),(14,24,1),(14,25,1),(14,26,1),(14,27,1),(14,28,1),(14,29,1),(14,30,1),(15,1,1),(15,3,1),(15,4,1),(15,5,1),(15,6,1),(15,7,1),(15,8,1),(15,9,1),(15,11,1),(15,13,1),(15,14,1),(15,15,1),(15,17,1),(15,18,1),(15,19,1),(15,20,1),(15,21,1),(15,22,1),(15,23,1),(15,24,1),(15,25,1),(15,27,1),(15,29,1),(15,30,1),(16,1,0),(17,1,1),(17,7,1),(17,14,1),(17,19,1),(17,20,1),(17,23,1),(17,27,1),(18,1,1),(18,4,1),(18,7,1),(18,8,1),(18,9,1),(18,13,1),(18,14,1),(18,15,1),(18,17,1),(18,18,1),(18,19,1),(18,20,1),(18,21,1),(18,22,1),(18,23,1),(18,24,1),(18,25,1),(18,27,1),(18,29,1),(19,1,1),(19,7,1),(19,14,1),(19,19,1),(19,20,1),(20,1,1),(20,3,1),(20,4,1),(20,7,1),(20,8,1),(20,9,1),(20,13,1),(20,14,1),(20,17,1),(20,18,1),(20,19,1),(20,20,1),(20,21,1),(20,22,1),(20,23,1),(20,24,1),(20,25,1),(20,27,1),(20,29,1),(20,15,0),(21,1,1),(21,3,1),(21,4,1),(21,5,1),(21,6,1),(21,7,1),(21,8,1),(21,9,1),(21,11,1),(21,13,1),(21,14,1),(21,15,1),(21,16,1),(21,17,1),(21,18,1),(21,19,1),(21,20,1),(21,21,1),(21,22,1),(21,23,1),(21,24,1),(21,25,1),(21,27,1),(21,29,1),(21,30,1),(22,1,1),(22,4,1),(22,7,1),(22,8,1),(22,9,1),(22,13,1),(22,14,1),(22,15,1),(22,16,1),(22,17,1),(22,18,1),(22,19,1),(22,20,1),(22,21,1),(22,22,1),(22,23,1),(22,24,1),(22,25,1),(22,27,1),(22,29,1),(23,96,0),(23,97,0),(23,114,0),(24,15,0);
/*!40000 ALTER TABLE `phpbb_acl_roles_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_users`
--

DROP TABLE IF EXISTS `phpbb_acl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_users` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_role_id` mediumint(8) unsigned NOT NULL default '0',
  `auth_setting` tinyint(2) NOT NULL default '0',
  KEY `user_id` (`user_id`),
  KEY `auth_option_id` (`auth_option_id`),
  KEY `auth_role_id` (`auth_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_users`
--

LOCK TABLES `phpbb_acl_users` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_users` DISABLE KEYS */;
INSERT INTO `phpbb_acl_users` VALUES (2,0,0,5,0),(58,0,0,4,0),(68,0,0,4,0),(70,0,0,4,0);
/*!40000 ALTER TABLE `phpbb_acl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_attachments`
--

DROP TABLE IF EXISTS `phpbb_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_attachments` (
  `attach_id` mediumint(8) unsigned NOT NULL auto_increment,
  `post_msg_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `in_message` tinyint(1) unsigned NOT NULL default '0',
  `poster_id` mediumint(8) unsigned NOT NULL default '0',
  `is_orphan` tinyint(1) unsigned NOT NULL default '1',
  `physical_filename` varchar(255) collate utf8_bin NOT NULL default '',
  `real_filename` varchar(255) collate utf8_bin NOT NULL default '',
  `download_count` mediumint(8) unsigned NOT NULL default '0',
  `attach_comment` text collate utf8_bin NOT NULL,
  `extension` varchar(100) collate utf8_bin NOT NULL default '',
  `mimetype` varchar(100) collate utf8_bin NOT NULL default '',
  `filesize` int(20) unsigned NOT NULL default '0',
  `filetime` int(11) unsigned NOT NULL default '0',
  `thumbnail` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`attach_id`),
  KEY `filetime` (`filetime`),
  KEY `post_msg_id` (`post_msg_id`),
  KEY `topic_id` (`topic_id`),
  KEY `poster_id` (`poster_id`),
  KEY `is_orphan` (`is_orphan`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_attachments`
--

LOCK TABLES `phpbb_attachments` WRITE;
/*!40000 ALTER TABLE `phpbb_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_banlist`
--

DROP TABLE IF EXISTS `phpbb_banlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_banlist` (
  `ban_id` mediumint(8) unsigned NOT NULL auto_increment,
  `ban_userid` mediumint(8) unsigned NOT NULL default '0',
  `ban_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `ban_email` varchar(100) collate utf8_bin NOT NULL default '',
  `ban_start` int(11) unsigned NOT NULL default '0',
  `ban_end` int(11) unsigned NOT NULL default '0',
  `ban_exclude` tinyint(1) unsigned NOT NULL default '0',
  `ban_reason` varchar(255) collate utf8_bin NOT NULL default '',
  `ban_give_reason` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`ban_id`),
  KEY `ban_end` (`ban_end`),
  KEY `ban_user` (`ban_userid`,`ban_exclude`),
  KEY `ban_email` (`ban_email`,`ban_exclude`),
  KEY `ban_ip` (`ban_ip`,`ban_exclude`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_banlist`
--

LOCK TABLES `phpbb_banlist` WRITE;
/*!40000 ALTER TABLE `phpbb_banlist` DISABLE KEYS */;
INSERT INTO `phpbb_banlist` VALUES (1,0,'','mail.ru',1364499370,0,0,'',''),(2,0,'','*@mail.ru',1364499388,0,0,'','');
/*!40000 ALTER TABLE `phpbb_banlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bbcodes`
--

DROP TABLE IF EXISTS `phpbb_bbcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bbcodes` (
  `bbcode_id` smallint(4) unsigned NOT NULL default '0',
  `bbcode_tag` varchar(16) collate utf8_bin NOT NULL default '',
  `bbcode_helpline` varchar(255) collate utf8_bin NOT NULL default '',
  `display_on_posting` tinyint(1) unsigned NOT NULL default '0',
  `bbcode_match` text collate utf8_bin NOT NULL,
  `bbcode_tpl` mediumtext collate utf8_bin NOT NULL,
  `first_pass_match` mediumtext collate utf8_bin NOT NULL,
  `first_pass_replace` mediumtext collate utf8_bin NOT NULL,
  `second_pass_match` mediumtext collate utf8_bin NOT NULL,
  `second_pass_replace` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`bbcode_id`),
  KEY `display_on_post` (`display_on_posting`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bbcodes`
--

LOCK TABLES `phpbb_bbcodes` WRITE;
/*!40000 ALTER TABLE `phpbb_bbcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_bbcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bookmarks`
--

DROP TABLE IF EXISTS `phpbb_bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bookmarks` (
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`topic_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bookmarks`
--

LOCK TABLES `phpbb_bookmarks` WRITE;
/*!40000 ALTER TABLE `phpbb_bookmarks` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_bookmarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bots`
--

DROP TABLE IF EXISTS `phpbb_bots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bots` (
  `bot_id` mediumint(8) unsigned NOT NULL auto_increment,
  `bot_active` tinyint(1) unsigned NOT NULL default '1',
  `bot_name` varchar(255) collate utf8_bin NOT NULL default '',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `bot_agent` varchar(255) collate utf8_bin NOT NULL default '',
  `bot_ip` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`bot_id`),
  KEY `bot_active` (`bot_active`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bots`
--

LOCK TABLES `phpbb_bots` WRITE;
/*!40000 ALTER TABLE `phpbb_bots` DISABLE KEYS */;
INSERT INTO `phpbb_bots` VALUES (1,1,'AdsBot [Google]',3,'AdsBot-Google',''),(2,1,'Alexa [Bot]',4,'ia_archiver',''),(3,1,'Alta Vista [Bot]',5,'Scooter/',''),(4,1,'Ask Jeeves [Bot]',6,'Ask Jeeves',''),(5,1,'Baidu [Spider]',7,'Baiduspider+(',''),(6,1,'Bing [Bot]',8,'bingbot/',''),(7,1,'Exabot [Bot]',9,'Exabot/',''),(8,1,'FAST Enterprise [Crawler]',10,'FAST Enterprise Crawler',''),(9,1,'FAST WebCrawler [Crawler]',11,'FAST-WebCrawler/',''),(10,1,'Francis [Bot]',12,'http://www.neomo.de/',''),(11,1,'Gigabot [Bot]',13,'Gigabot/',''),(12,1,'Google Adsense [Bot]',14,'Mediapartners-Google',''),(13,1,'Google Desktop',15,'Google Desktop',''),(14,1,'Google Feedfetcher',16,'Feedfetcher-Google',''),(15,1,'Google [Bot]',17,'Googlebot',''),(16,1,'Heise IT-Markt [Crawler]',18,'heise-IT-Markt-Crawler',''),(17,1,'Heritrix [Crawler]',19,'heritrix/1.',''),(18,1,'IBM Research [Bot]',20,'ibm.com/cs/crawler',''),(19,1,'ICCrawler - ICjobs',21,'ICCrawler - ICjobs',''),(20,1,'ichiro [Crawler]',22,'ichiro/',''),(21,1,'Majestic-12 [Bot]',23,'MJ12bot/',''),(22,1,'Metager [Bot]',24,'MetagerBot/',''),(23,1,'MSN NewsBlogs',25,'msnbot-NewsBlogs/',''),(24,1,'MSN [Bot]',26,'msnbot/',''),(25,1,'MSNbot Media',27,'msnbot-media/',''),(26,1,'NG-Search [Bot]',28,'NG-Search/',''),(27,1,'Nutch [Bot]',29,'http://lucene.apache.org/nutch/',''),(28,1,'Nutch/CVS [Bot]',30,'NutchCVS/',''),(29,1,'OmniExplorer [Bot]',31,'OmniExplorer_Bot/',''),(30,1,'Online link [Validator]',32,'online link validator',''),(31,1,'psbot [Picsearch]',33,'psbot/0',''),(32,1,'Seekport [Bot]',34,'Seekbot/',''),(33,1,'Sensis [Crawler]',35,'Sensis Web Crawler',''),(34,1,'SEO Crawler',36,'SEO search Crawler/',''),(35,1,'Seoma [Crawler]',37,'Seoma [SEO Crawler]',''),(36,1,'SEOSearch [Crawler]',38,'SEOsearch/',''),(37,1,'Snappy [Bot]',39,'Snappy/1.1 ( http://www.urltrends.com/ )',''),(38,1,'Steeler [Crawler]',40,'http://www.tkl.iis.u-tokyo.ac.jp/~crawler/',''),(39,1,'Synoo [Bot]',41,'SynooBot/',''),(40,1,'Telekom [Bot]',42,'crawleradmin.t-info@telekom.de',''),(41,1,'TurnitinBot [Bot]',43,'TurnitinBot/',''),(42,1,'Voyager [Bot]',44,'voyager/1.0',''),(43,1,'W3 [Sitesearch]',45,'W3 SiteSearch Crawler',''),(44,1,'W3C [Linkcheck]',46,'W3C-checklink/',''),(45,1,'W3C [Validator]',47,'W3C_*Validator',''),(46,1,'WiseNut [Bot]',48,'http://www.WISEnutbot.com',''),(47,1,'YaCy [Bot]',49,'yacybot',''),(48,1,'Yahoo MMCrawler [Bot]',50,'Yahoo-MMCrawler/',''),(49,1,'Yahoo Slurp [Bot]',51,'Yahoo! DE Slurp',''),(50,1,'Yahoo [Bot]',52,'Yahoo! Slurp',''),(51,1,'YahooSeeker [Bot]',53,'YahooSeeker/','');
/*!40000 ALTER TABLE `phpbb_bots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_config`
--

DROP TABLE IF EXISTS `phpbb_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_config` (
  `config_name` varchar(255) collate utf8_bin NOT NULL default '',
  `config_value` varchar(255) collate utf8_bin NOT NULL default '',
  `is_dynamic` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`config_name`),
  KEY `is_dynamic` (`is_dynamic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_config`
--

LOCK TABLES `phpbb_config` WRITE;
/*!40000 ALTER TABLE `phpbb_config` DISABLE KEYS */;
INSERT INTO `phpbb_config` VALUES ('active_sessions','0',0),('allow_attachments','1',0),('allow_autologin','1',0),('allow_avatar','1',0),('allow_avatar_local','0',0),('allow_avatar_remote','0',0),('allow_avatar_upload','1',0),('allow_avatar_remote_upload','0',0),('allow_bbcode','1',0),('allow_birthdays','1',0),('allow_bookmarks','1',0),('allow_emailreuse','0',0),('allow_forum_notify','1',0),('allow_mass_pm','1',0),('allow_name_chars','USERNAME_CHARS_ANY',0),('allow_namechange','0',0),('allow_nocensors','0',0),('allow_pm_attach','0',0),('allow_pm_report','1',0),('allow_post_flash','1',0),('allow_post_links','1',0),('allow_privmsg','1',0),('allow_quick_reply','1',0),('allow_sig','1',0),('allow_sig_bbcode','1',0),('allow_sig_flash','0',0),('allow_sig_img','1',0),('allow_sig_links','1',0),('allow_sig_pm','1',0),('allow_sig_smilies','1',0),('allow_smilies','1',0),('allow_topic_notify','1',0),('attachment_quota','52428800',0),('auth_bbcode_pm','1',0),('auth_flash_pm','0',0),('auth_img_pm','1',0),('auth_method','db',0),('auth_smilies_pm','1',0),('avatar_filesize','6144',0),('avatar_gallery_path','images/avatars/gallery',0),('avatar_max_height','90',0),('avatar_max_width','90',0),('avatar_min_height','20',0),('avatar_min_width','20',0),('avatar_path','images/avatars/upload',0),('avatar_salt','1d29e461d2bb6efcf965b39819cd6453',0),('board_contact','m.jankowski@miart.pl',0),('board_disable','0',0),('board_disable_msg','',0),('board_dst','0',0),('board_email','m.jankowski@miart.pl',0),('board_email_form','0',0),('board_email_sig','Dziękujemy, Ekipa',0),('board_hide_emails','1',0),('board_timezone','0',0),('browser_check','1',0),('bump_interval','10',0),('bump_type','d',0),('cache_gc','7200',0),('captcha_plugin','phpbb_recaptcha',0),('captcha_gd','1',0),('captcha_gd_foreground_noise','0',0),('captcha_gd_x_grid','25',0),('captcha_gd_y_grid','25',0),('captcha_gd_wave','0',0),('captcha_gd_3d_noise','1',0),('captcha_gd_fonts','1',0),('confirm_refresh','1',0),('check_attachment_content','1',0),('check_dnsbl','0',0),('chg_passforce','0',0),('cookie_domain','pochp.eu',0),('cookie_name','phpbb3_boufb',0),('cookie_path','/',0),('cookie_secure','0',0),('coppa_enable','0',0),('coppa_fax','',0),('coppa_mail','',0),('database_gc','604800',0),('dbms_version','5.0.67-community-log',0),('default_dateformat','|j M Y|, \\o H:i',0),('default_style','1',0),('display_last_edited','1',0),('display_order','0',0),('edit_time','0',0),('delete_time','0',0),('email_check_mx','1',0),('email_enable','1',0),('email_function_name','mail',0),('email_max_chunk_size','50',0),('email_package_size','20',0),('enable_confirm','1',0),('enable_pm_icons','1',0),('enable_post_confirm','1',0),('feed_enable','1',0),('feed_http_auth','0',0),('feed_limit_post','15',0),('feed_limit_topic','10',0),('feed_overall_forums','0',0),('feed_overall','1',0),('feed_forum','1',0),('feed_topic','1',0),('feed_topics_new','1',0),('feed_topics_active','0',0),('feed_item_statistics','1',0),('flood_interval','15',0),('force_server_vars','0',0),('form_token_lifetime','7200',0),('form_token_mintime','0',0),('form_token_sid_guests','1',0),('forward_pm','1',0),('forwarded_for_check','0',0),('full_folder_action','2',0),('fulltext_mysql_max_word_len','254',0),('fulltext_mysql_min_word_len','4',0),('fulltext_native_common_thres','5',0),('fulltext_native_load_upd','1',0),('fulltext_native_max_chars','14',0),('fulltext_native_min_chars','3',0),('gzip_compress','0',0),('hot_threshold','25',0),('icons_path','images/icons',0),('img_create_thumbnail','0',0),('img_display_inlined','1',0),('img_imagick','',0),('img_link_height','0',0),('img_link_width','0',0),('img_max_height','0',0),('img_max_thumb_width','400',0),('img_max_width','0',0),('img_min_thumb_filesize','12000',0),('ip_check','3',0),('ip_login_limit_max','50',0),('ip_login_limit_time','21600',0),('ip_login_limit_use_forwarded','0',0),('jab_enable','0',0),('jab_host','',0),('jab_password','',0),('jab_package_size','20',0),('jab_port','5222',0),('jab_use_ssl','0',0),('jab_username','',0),('ldap_base_dn','',0),('ldap_email','',0),('ldap_password','',0),('ldap_port','',0),('ldap_server','',0),('ldap_uid','',0),('ldap_user','',0),('ldap_user_filter','',0),('limit_load','0',0),('limit_search_load','0',0),('load_anon_lastread','0',0),('load_birthdays','1',0),('load_cpf_memberlist','0',0),('load_cpf_viewprofile','1',0),('load_cpf_viewtopic','0',0),('load_db_lastread','1',0),('load_db_track','1',0),('load_jumpbox','1',0),('load_moderators','1',0),('load_online','1',0),('load_online_guests','1',0),('load_online_time','5',0),('load_onlinetrack','1',0),('load_search','1',0),('load_tplcompile','0',0),('load_unreads_search','1',0),('load_user_activity','1',0),('max_attachments','3',0),('max_attachments_pm','1',0),('max_autologin_time','0',0),('max_filesize','262144',0),('max_filesize_pm','262144',0),('max_login_attempts','3',0),('max_name_chars','20',0),('max_num_search_keywords','10',0),('max_pass_chars','100',0),('max_poll_options','10',0),('max_post_chars','60000',0),('max_post_font_size','200',0),('max_post_img_height','0',0),('max_post_img_width','0',0),('max_post_smilies','0',0),('max_post_urls','0',0),('max_quote_depth','3',0),('max_reg_attempts','5',0),('max_sig_chars','255',0),('max_sig_font_size','200',0),('max_sig_img_height','0',0),('max_sig_img_width','0',0),('max_sig_smilies','0',0),('max_sig_urls','5',0),('min_name_chars','3',0),('min_pass_chars','6',0),('min_post_chars','1',0),('min_search_author_chars','3',0),('mime_triggers','body|head|html|img|plaintext|a href|pre|script|table|title',0),('new_member_post_limit','3',0),('new_member_group_default','0',0),('override_user_style','0',0),('pass_complex','PASS_TYPE_ANY',0),('pm_edit_time','0',0),('pm_max_boxes','4',0),('pm_max_msgs','50',0),('pm_max_recipients','0',0),('posts_per_page','10',0),('print_pm','1',0),('queue_interval','60',0),('ranks_path','images/ranks',0),('require_activation','1',0),('referer_validation','1',0),('script_path','/forum',0),('search_block_size','250',0),('search_gc','7200',0),('search_interval','0',0),('search_anonymous_interval','0',0),('search_type','fulltext_native',0),('search_store_results','1800',0),('secure_allow_deny','1',0),('secure_allow_empty_referer','1',0),('secure_downloads','0',0),('server_name','pochp.eu',0),('server_port','80',0),('server_protocol','http://',0),('session_gc','3600',0),('session_length','3600',0),('site_desc','',0),('sitename','FORUM POCHP',0),('smilies_path','images/smilies',0),('smilies_per_page','50',0),('smtp_auth_method','LOGIN',0),('smtp_delivery','1',0),('smtp_host','mail.pochp.eu',0),('smtp_password','veVQ9UwU',0),('smtp_port','25',0),('smtp_username','noreplay@pochp.eu',0),('topics_per_page','25',0),('tpl_allow_php','0',0),('upload_icons_path','images/upload_icons',0),('upload_path','files',0),('version','3.0.11',0),('warnings_expire_days','90',0),('warnings_gc','14400',0),('cache_last_gc','1372331329',1),('cron_lock','0',1),('database_last_gc','1370590379',1),('last_queue_run','0',1),('newest_user_colour','',1),('newest_user_id','81',1),('newest_username','AaAntiqueimigue',1),('num_files','0',1),('num_posts','0',1),('num_topics','0',1),('num_users','9',1),('rand_seed','b4c5da1976341990e7b4bfd5f92fc9f0',1),('rand_seed_last_update','1372491594',1),('record_online_date','1361334559',1),('record_online_users','3',1),('search_indexing_state','',1),('search_last_gc','1371142643',1),('session_last_gc','1368882617',1),('upload_dir_size','0',1),('warnings_last_gc','1371481871',1),('board_startdate','1360014818',0),('default_lang','pl',0),('recaptcha_pubkey','6LdUj9wSAAAAAPO4ZeOxGIdS3HxYVgUYWAmud02b',0),('recaptcha_privkey','6LdUj9wSAAAAAEXxPx2p5ND6FaLkwA-dQWbjPU5L',0),('questionnaire_unique_id','8d64347a35b1db7a',0);
/*!40000 ALTER TABLE `phpbb_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_confirm`
--

DROP TABLE IF EXISTS `phpbb_confirm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_confirm` (
  `confirm_id` char(32) collate utf8_bin NOT NULL default '',
  `session_id` char(32) collate utf8_bin NOT NULL default '',
  `confirm_type` tinyint(3) NOT NULL default '0',
  `code` varchar(8) collate utf8_bin NOT NULL default '',
  `seed` int(10) unsigned NOT NULL default '0',
  `attempts` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`session_id`,`confirm_id`),
  KEY `confirm_type` (`confirm_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_confirm`
--

LOCK TABLES `phpbb_confirm` WRITE;
/*!40000 ALTER TABLE `phpbb_confirm` DISABLE KEYS */;
INSERT INTO `phpbb_confirm` VALUES ('5bc66bd3716f6a5787b4f5278ceea13b','c3f80f9ae2af758e8fe1575c7fe86fa9',1,'1HNQ3',1251576409,0),('5b20253908504b308d5681c1c54f28ea','5509de453285ea225dde822d44b7d1bc',3,'1IV5NS',1042629332,0),('bdc9be77cc708b40d42f43a21f431e96','5509de453285ea225dde822d44b7d1bc',3,'44J6M',443915022,0),('cec63dc3571a962e4b8c1023d036676b','5509de453285ea225dde822d44b7d1bc',3,'64FPSN',2087958660,0),('e93fec8aa94ca4e914b2fd858d6709c1','f1f263400d1caa8263ef351e4c8ab762',3,'2T426U',2054320301,0),('9804f0e4594cdbe66df8ed2e479cdf0d','f1f263400d1caa8263ef351e4c8ab762',3,'3SVNUX',207521896,0),('6c0505cf8b7d4a6ed7918caf572b3562','1e952cc47091a6e502a495322c0cdb50',3,'5FGBU',1016916774,0),('edab8402725f0fead102f987e6a83a37','ba6f8dfb26058eda3d97b4f74e313b7d',1,'5G2GJ',761978623,4),('d1718b7290f29149e6ee1b93b9b4c93d','224baaf4f789c53ff5699ece2afaf180',1,'65WJ8',2000874151,9),('10ea295ab1b028744aa12924eda59d2c','997856f75403d1671c8d73484141b673',3,'1CYS',1240744386,0),('1f92f43f0feaef0fe504f1ea15aeb807','0d55f9dd58bb143427f664214ddd313b',3,'3D9TF',530903831,0),('1fdbd6af987d5579d3941d4a5aea5793','0d55f9dd58bb143427f664214ddd313b',3,'65DN',412672092,0),('a8ed0ee690f05f42a66364629e5890fd','18931655877a72eb79b2552af6c73f85',3,'78V8XGB',1247176375,0),('88fadb2349df94a4187808a54b3dcd1d','ad7ce51f3ef99c20d210c60fe56a073a',3,'4HYYH6',293239635,0),('2f099f4c124023a1c3e279820b4b951e','b8cabdd979a1c067292539abbb50a344',1,'F6D27FB',297405703,0),('b8856cfc979de63602e9376a2b7ba988','35716ee1502538df0b8ae2fd961f2b13',3,'P22L',1513619891,0),('ce366323e029a48a61197c3a3145e3a2','028d983d84d87fbac648cd1b9daea8a5',3,'64HFV',72080033,0),('ac5a5a38878fb6025a978112f06d9ad3','deb48b34538777726ccbb9ecd90a4b9d',1,'64TIS1',464548280,26),('c4291f1b4e2ce875cce89304aa4ab2d7','37f3fa54998cd92a6a12de1a2ba7de8b',1,'7ZUC',1625183969,0),('0733001c0924da194cbf89fcb9f9bbe0','3770260ac4270000fb414c041bf26c87',3,'3I6I4',1636637856,0),('fcc48fd971752ae23bb71d71bb023fa7','3770260ac4270000fb414c041bf26c87',3,'3AC82HR',1077990210,0),('7b96002c92e8cf6bc1e5ca3d377748f2','6b8ccea2f69ed4ee6cf80e5081394f15',3,'3USWMH',1779045521,0),('3988ecf330bbdc3027c7c2ca8f6d3de7','6b8ccea2f69ed4ee6cf80e5081394f15',3,'7B13',1276200027,0),('0c45e0804c95d1a6b867f2763c140bc1','3770260ac4270000fb414c041bf26c87',3,'59WM',1592712259,0),('84332954cbba20409a35f791d7bdc76b','534e59c220a000c71d6b8e330b3aebc5',3,'5M7P',864834036,0),('0ae8d295e7c11dee55a1e6f12713308f','534e59c220a000c71d6b8e330b3aebc5',3,'73MY983',1173650930,0),('b13727cd3e449234016816eba441f41d','534e59c220a000c71d6b8e330b3aebc5',3,'Q9LJ7IJ',517340572,0),('76c60e06abd4e81cd8b615fb0bb45e19','d5cbfc8602f4dbb0a814a21879911855',3,'1MI6JQ',233368381,0),('ea4e3c3a0849e8c23fbbd657af3e0911','d5cbfc8602f4dbb0a814a21879911855',3,'5JB7',1762219878,0),('bc29d1f24fdeba42cdc70eb6a9cf9afd','d5cbfc8602f4dbb0a814a21879911855',3,'216ULLS',872087572,0),('67df9f4dc112c3c56d1df762c58ffb96','caae8effdf318bf523eadc45074ca7f1',3,'4J27',954458199,0),('c5f16c30f0ec12676d005999aeb5769d','91748f21347999f18583a4fac4c205ed',1,'3ATSEL',1796211151,0),('79679b14b1131e4719f66df1a847c77e','2e21d51d5a00c11682bc4e472e627d3f',3,'4FYE4',196387223,0),('babba4eb565281e09b70b8b1c1bba210','6d58ffa85cf8adb483ef38eac927675b',3,'19A3N45',1510785838,0),('d4757620806c9c7bcffad9d97ea83641','e351f7f108d2eba5b8eeb32d2985b145',3,'3BN6IC',703087463,0),('3fc6f548c50875ee2170df06b86e061f','9802d2643e07c199a727ff0661c063bd',3,'7AK64BP',1134367082,0),('a3853a9304a4c1f88a4433e4cc198887','e6c3248fbdfa63b369c6a83abb134cae',3,'1I243',788605235,0),('7fe0aaea2cd19ef5987c78679ea6edf9','8128cdb0b22b69a9afaba91eec5078a7',3,'4PS9R9',1553582946,0),('f9d24f370992adcc9745181b53f07b74','fb637052f5c6116cebdbdf1af15005d9',3,'4GKD5',1314285127,0),('7078de5730d5a2e3c9fedb21ac55bb2c','d184cbcd83af6aff4a353f343f9cd5bf',3,'4JGJC',1653739358,0),('0fc730d2368f053c67e01df7d8cd9710','9728d6f8eb9e5a76773572db6c400a82',3,'7TR8J36',455354164,0),('f51036ca97489cf7b5a2b04f30de0167','ff57ea209fde0198a4d7066c7bf6abed',3,'3S39RQS',7444934,0),('6c3822c7a516398867b0ebf44b6cfa94','005be1aec15d5b9a19fe5b473f03114b',3,'6LZSMB',1447313044,0),('4a30e24404171e50b687916ca5aae662','ba276a79b037a5ef08f762a32efd0303',3,'3UMLTL',78535385,0),('8f03b4c7cdf5a5bd28ad728770f2b9ab','5a111f87a2ffccb93eefcf396a49b5b2',3,'13Q2N',1305445866,0),('e823f796de6384a255b55dc2d3e2f3be','61416528c3cca05da69a75aa73460cac',3,'5APLYJB',1675608694,0),('2f09718cae5b33df4a5e853e7b4f236c','b74bdcfe9d46693368c9ef5914fbb0c5',3,'H5JH',1321337275,0),('d255ba9de5cf6ab2e2cc4921d5acb52a','49aca5d9cf0ddb461f469ba802598eb4',3,'51MP',478049344,0),('f7bda4ea31bbab1b0eb1b5581ece1e9b','e9b86dfb85c8e3c64680fcfe2fbb5169',3,'PDHQ1',1921627143,0),('2f031379a30eb6cc6d77505a9e91afdc','f8481c18799b984797c34651c5edf6b6',3,'616AGT',781023862,0),('9007211fc58a7a5f6394b2c36e7816d8','d4fd7a70d5562750f24cbbff3f7851cb',3,'49CB99I',1579848607,0),('fb5a8f35a80120132806dda3d8809212','0613b93706e9a22e244de5f1c00901ac',3,'1VFYEH',523945878,0),('0c873ca780f8b290567fd114fcb27a26','4abaade003b961fe870daa846a23884a',3,'WH26NDP',1766584182,0),('e590ff20fa9b51f5aff2b5416ddd6790','76bfbffaa57c444e4336f5e70d272368',3,'SKTXMMM',1065836325,0),('d9efa65df0bcb6b9d1ca38637c69948d','76bfbffaa57c444e4336f5e70d272368',3,'4U9L',1249018989,0),('79679f17dead022eab1d5b96533e100a','76bfbffaa57c444e4336f5e70d272368',3,'3X5F',457651774,0),('462ba2bf0f4542bb87116c667a6b2edd','a63991d0d879a5f48117f4155d922eda',3,'1WUF',1348974626,0),('839a9cd2763ac847927cc49047e16efb','d75499559a91c8fba87080cf5aae462c',3,'1S56LH',437152598,0),('9f4a684e28ee4e3a2fd639c4d6f1fb4e','161f4125991692f445cc5642fdc69d08',3,'1AN6CN',493595833,0),('0a6c7be1f89e16495bc958de95310cfe','f4aba109248fd1ba5509198785997e3d',3,'2D7N',435296830,0),('3e431e2a47941eab2053a5d43339d86a','1f10f78b1a85281a160641e22e1de03d',3,'13EL2',1859256706,0),('a42dc02325ae5f5580bf6859d9f101ad','520044ca7fb5c1e1a53037c791caaf9a',3,'2SRLRZ',572171588,0),('7db61c1b4b779be962a6ee66bf740d0d','3056d05b0fb51125ca2d28e659f1f9b4',3,'NMGS',644513791,0),('3d7ed1b44783f3ad4d76653df65a6bac','6168030375e2d5ecc41eeae2a2a199d9',3,'77HHG',788031797,0),('13c38cd9a1dc8b1f61cf9d2f2f2c69a2','41ed4375ccad543882e550ee393eff96',3,'6V1Y',1072829188,0),('dcb91e22b06c30b588a28ecafa6ec853','b35b200b1f70bc14be66eadff5e37688',3,'J1AXH',730787367,0),('b88bf54a6acc5ffbea093285efe84522','9a72b4bed805b23248a999945d12eef8',3,'65B8',1181219177,0),('f3f188d6a82e92e4610779b03fb6861e','73d939d86fd8d7be2dde81c198d6fe43',3,'4TIR',871759098,0),('2c99521f0e802e458e417fbd444d81d0','8df83b7ebf79afba0741a2b0403c0d4d',3,'2PSAVH',44597755,0),('c74d99f793a57d25f9b7389d3fb2b723','a17a0d391ce40f959270b41622da71b5',3,'17T2R',936469189,0),('b4abaf64657f0aea7e60d4b9fbd992a7','07a8f5b8ee6f313dca19a47c7d9b8461',3,'6SU2EC',579572740,0),('3f257d5aba958bbcfb0364e2fe67dbc8','3e869d19f54d274b5005739608d41abb',3,'TEEFRKV',1140227227,0),('02dd7b9b8e00bc553dc20608c4b15a67','7e4e0213d8e9568ba976bc9a011555ad',3,'3PUS',400507712,0),('9652a24a60342628e2102c5ed777e496','e307b8439d75df25366da1414524a818',3,'265BCL',1375934972,0),('90a6b743c704a825e4042956813c2bff','1c34659b503a2ff5ddefb7c0f6cc78c9',3,'6PP6Q',771613501,0),('db026fa55a9bbb3154601bf112f795cc','1c34659b503a2ff5ddefb7c0f6cc78c9',3,'23UYZWU',1087226286,0),('591867861d389c51391f41a37f9de7b9','1c34659b503a2ff5ddefb7c0f6cc78c9',3,'4VPDYJZ',1164997956,0),('cd86783d0474990745fbdabb642f614f','399da78cc3e9258d1ee8117d61e29299',3,'8NPG2V',1250319001,0),('ceb80be71c5220044ae6ce39abfa790e','ecb1704af72509af709b152c3d1143ca',3,'4XFH',1835097179,0),('32f755a3240fbb8226e01dd35a328c61','ec7f47929d4731f34d1a19323b187681',3,'5MVGZ7J',1337241233,0),('62a7cb0c41b8baa245731d7de59df97b','317fcde2382df71c8fcd61f1b543e0d0',3,'5FAV',2018054868,0),('9f2c18058b086a9122da1d372a005639','f4e84d5f1958b0e21c3ff3312c0ba7d1',3,'33QCR',1098896249,0),('5d821d42b581b1e34c77e1e50c7859b7','5c000f8e4e058f7fd995b4a448491a86',3,'4Q6BMJ',1135121930,0),('6ef15402d837d3a0c01ef7d9d321266c','b81f1ac789c62f88d0bf0b68e3be2fb5',3,'2428',65876986,0),('8f6395c92bad6c15a5ed667669ae9bc4','1e777833a258a3ebac356461fea6bce5',3,'7ESENWD',1662871727,0),('1e0155edf152a04ceb8192eccf6b8d4e','b6e3355732723a018a95d7b710e67433',3,'2V95',1750999503,0),('0992dd7bc8f2bf92c6c360eeb94c9372','8b3236a485030d9f8218f40a6953c292',3,'478Q',827318992,0),('c669b7910bc46ae5e37983e79b98e1dd','4e6dd3a6ddda5247c402cd49aa21b931',3,'45HLDC',183566127,0),('b657981bcda4a4b33644724c2f805f71','784307da492472fa12d5850627ef1b9f',3,'2IQKF',309426612,0),('2d32e591343472fece1644d3014c88b3','452599118f65d58f90203124429bb787',3,'3UAFIIC',1125271465,0),('ba948a8ad669d048b29fa9b484797867','0fd022d9dd5f4cc319e9666a58b56332',3,'3Z5I',889115594,0),('9cfb0ce36fd2929ae2870e325ed53dd4','3ecaff05443dd1dfa3b75633cff14354',3,'1FB1IL',859081115,0),('442cd5013696b3f3d2b84fdb0d59cf27','f02692f26ed7e3ee9802b871ede8def7',3,'QGPC',1793263661,0),('46b771d81319aa46d97beedc0c9f6392','381f02a65696750c0f35ad49fd1b55ed',3,'3LY1DM',925645089,0),('1b4aebe19db5e0dc66885aff6fba6a27','a7de98e1b4916f8ed86920d2d2e95238',3,'2F59',1152034295,0),('faedb1aa566ae93599d850310d785203','d3a19227808921a2dbed5759c98e232c',3,'6PBE3B1',1002276557,0),('bb0ed4100d2eeada5f0495c7d64901d6','4cee69ff439e8f6ad2938a00c4f31f2a',3,'3VVE',166482285,0),('bf2f12d74770cbb640d79ef68f10593e','fe7ec2eadf4c301cc17608b8204aaeb9',3,'6HBRYCW',1792054136,0),('35e5c329e857a719b282e3217433f6e6','20e54e260c63fd89b2fbb0c74bbc2cb7',3,'45UFJD',1704625609,0),('0e958c7ba8e41a2c8b82e30dd38111b3','5df869c9bff07cb72d6c64a559944d18',3,'61YZ',1583511620,0),('1f0d0ab668d36230f59b3cb65e46fbea','482cf9093f8feced7cb26f09c6ca796b',3,'G3QTKTH',589897790,0),('406ac6e9d182146539380574778c5080','d3c443d3c9ec5ddeb2af376a103b3db4',3,'5Q5Q6',1292240915,0),('d370f52871a15db11fcdc4fa022fb006','b8c65a288edd015d5a379c101fd83175',3,'J3ZB5VC',2114679589,0),('95f184c20463dbf0dc2940e428e89ddc','52f54c5d93d6443a6beb9dabaa399f28',3,'1984S7I',1048370255,0),('df0814bcfb6e19bb035f2d15a209f4f1','90d944c6bc6c09de68d0f7ae651dd49d',3,'6S2B1QU',2128133346,0),('b7cf66e17ba1a08cde5206521eec57bc','497b7440364b998ad68fe367ccfbe193',3,'6WV8K4T',341474657,0),('2debed95cecf0049061eda73cfcd02bc','a8abe6a0eb1660249fe6968d3b0c50ff',3,'NJL9Z63',664785839,0),('5313da70386e2cfcb60aad36ade5a620','dc8418b1839665f0fe69b8cd33224dc5',3,'6TLDSRE',2130581354,0),('0e0f509c5c0a1e64119e1b1c19daa447','64ca998a2db479abb65b786bc1faeb3b',3,'543I4',1653717524,0),('ab5fd097fdb1570c3b4e0be9e5696a8c','3d96271aecbe7371a8a088852cd51cba',3,'2J3S49',612439322,0),('11ba163848f2dac942497417a341b101','c2cb69048b596b964b8adb5e23d0feb9',3,'4377',360060942,0),('7729065bfa5aca5f6bfe0c15ca751957','0261084074e65a9e867913e66e212366',3,'4MUNPV',779866160,0),('11226fe87adf118245ab0cecd13c7d2f','e7050d6ab80bc7331193300044b33e6b',3,'274T14',344244527,0),('ec02c17dfcb0b49fabae3acfe7a7760e','96ebf0ed7a5bc2e1b6641fa4b1bab630',3,'5XJDW',851661662,0),('e389b7c2f0947a5f7838ba0320d26b03','1d701630e2371b641546fcfc8d870898',3,'63BK1',383625477,0),('e1c13ecc52be984438b5d584ddc71757','d039ca13522cb7de1765bb75532340d2',3,'56VFYI',754140137,0),('f5be762dbfe691b544402820d4ebded9','4fc9d1351a9f4bcd9e46257d2a812e24',3,'5KHAT4',181305472,0),('0dc4d9e5d0a94aa8b889e93479302c1a','ec72763d7e44ab254aa613fabe286eb0',3,'6KLGCRP',1322213158,0),('a632abb00982608edd12c73f1860314e','5059eeba5da9623a7d3319ac035180ec',3,'5Z3Q14M',1312113555,0),('bb03962391ada783c356bf9606b8823c','cca4b39b9258f5713a5b8cd03914aadc',3,'26RNE',1243858355,0),('5bff1bb36fb714efc90b10ac210766ee','a280a10840450186273557228ceead51',3,'5RB9VMF',937098729,0),('72b4afe5488ebd47b55871dba02f534b','cb1fb0fcab03c003dd6fd581879107f3',3,'3N1W',534753417,0),('5f803dab91f7e01776b6fe0ced56c97d','8b4152026defa8bda2c44bbfde47a167',3,'2DUBX',1820323286,0),('e090d3b8f29b74029d0e1014a135a828','ddd3cbcae684ebdcfc882682f9bead4c',3,'4L5RWM',631855301,0),('5b6e426f776cb85058d005a52b286f65','f667f89999098ca57696c1f1df05b2f6',3,'1CUC1ME',802161210,0),('8c34e1349abfc48b7fe9d88c38adabcc','8a2707ba7a0596b3c4d8636647bc98e0',3,'79A8V',1442503173,0),('bada882eedd78766bef0fe7720474f17','660c194105e9ee455919aaaebabc1980',3,'47XGE2',1153772803,0),('8068c005af1bcb087c4f21d92bb0fb04','b7ba27d5c662c3a4d9d40d5b09857533',3,'2F4M4',1028061906,0),('7785c9237a95f84d8d05eb6a31fd3ac9','f159ccb2ecf11a0e07a8651a592ef5fa',3,'4RCNVEU',236875152,0),('0f15216864a62db4ee169dd1182f3d61','b56ba6de371405a61557906fe3e2752b',3,'FG5CE2F',1029057722,0),('99b0219c70d69104c1f11e74fea475dd','2c16a05600756e6ca1197b6d4fa0f962',3,'4UE77',65109743,0),('9c3e890d072b880f41359a29a66c3e7b','25003f7e717de2b8041ef4e984b72c26',3,'25D2RIX',1579729038,0),('12412485dcd61ec302f01af0ced7b203','4297da062d910432477529f8780f7be6',3,'66PK6',1885721515,0),('a0fc8f33b4161a513df23cce3c5e313c','1beeae6b86d9879fb84195dc2d9aca4f',3,'3XQMK',1338236768,0),('36f2253d36e9e7a8a3248b2dd158d536','d730167f32261130beba574ae0f599a5',3,'3657YZ9',1616193376,0),('43be9e78eca1df50f6b29a17817261c4','77967bbf22fc10462a1b241671136b61',3,'4AMZ959',119321728,0),('6fcb0a5ad4a70ba7577c52c1797fb717','9e29cf88fc925a2a019988756a1d329d',3,'4LGLFN',128819473,0),('08057a9afa146f0f1db97b4c884c5ae3','ac13b72f784e592e4bb575e4d8f690f6',3,'65FF9',1049300913,0),('67a31a7d8fb505bd2a025f30912c233e','08e33e1c67a607a654e6e291fbabb80b',3,'57LU',1496992691,0),('fe87e245dd5a9e9a5905dbd83b597276','8e6bb65de9825a376bd7f3c14f60f069',3,'7DN1U',1733298594,0),('b30b29ca96aaaa7fa17bf3b8058f02ef','c20813707b3f23b628e393366d5f6fd8',3,'156CU',837954059,0),('dbf80562536b110c989dde2f1cfc0353','5cf5175c3b19db44d0e2c481885f8a16',3,'3M6I6ZV',1960943582,0),('f3e456167c5c0dace2fddcf7698901e0','acea9abab7b7fa3fdeae9d4cf036b672',3,'3PN6',1287383344,0),('2a4c89dc68dd41b50339136ec75367e9','55688cc0029ca52713fb62b52c006538',3,'6ZME2L6',1826358756,0),('a16ebbd007156749f7832d1ee4b2c494','9d30d3546d5d9ca26bfba9c7435c111b',3,'PLPB',763269778,0),('c2fa39a7ddc6d807fa96453f10b69e60','a1322fad6590a481d07f5b33144263e3',3,'15DHM29',1820987457,0),('b9e8e40c6ba8923eeb749f5c661ee6c9','76e89266369107b69f8c4c8ec8505ed9',3,'1KCRB8',656067376,0),('a9bd190e8dcea3bbf40d9711fcb5a733','adb796fb687c701be6224d458337e900',3,'7EX5',1951579704,0),('131ed3682e2ed1ceb0e7e15a43d1f4d1','0a53a2eb5decfc915632d60ccc809c0d',3,'E5QXS',1830852504,0),('8d0c602dd97d908a28df606e65903363','7014e0555843362f18c2211519a010c7',3,'6U4M',1399047022,0),('830244b000664a9d8694b906153efe8a','cd42ee363f61ed0ceadf7d171d0462bc',3,'53FBL',649128105,0),('c3ae8c92c685d423f18c0d1a4643b1cc','35404fd341de11116d6bfbfa0b6e6e6e',3,'6JR3I',1725313767,0),('e3b1c5150968d501d8d83b56e506c9cb','e61b0b2880485b37ba73384fc3894b56',3,'4ADJ',1340468366,0),('cb518a35239e6b382c7648bb3db0d605','b7734ada65d591b1c9c6e727a4d3921a',3,'6GYQT',1762434033,0),('68a3675bcbf81cd5a90e80bc14d947d8','c6523f0dbee3a84e989d0610b8ffcfa1',3,'49XW',799564028,0),('0c18e165d3ba2186019d91a214073e00','a1aa6ef1c5c9ceaccd7c46da6e989e33',3,'3EGULMI',700759903,0),('070cc194940f49b85c09e00668d172e2','d56627fbabbb860312b1a917f63fd7ef',3,'6L14',1649416885,0),('fe821d997e06fea3c5aef96315a70b62','fd051239853dc7c977e9a465e3b58e7f',3,'3QPBL',1518125908,0),('0ae099c0558c433a80f2a3d33303c998','84d053680c2a147af5b0cd3de905a29c',3,'3Q77N',1537393098,0),('87a4ee0f439f8964a78ea02b6f813c5f','607441431420f605807abbe170fa1d57',3,'3RL9',715566603,0),('a4c6de50deae693056bd55e7a7ad33f4','92d09fbd7ecadf9149621b331de6be88',3,'3PCV1',363747451,0),('587050aceaad6649e7e89dcc46b3f963','c7b1819e4578e8573b3eb1092ba49aad',3,'5P2BU',666331663,0),('0b7f5dd378dfbe0b2211e7e7b88b45e8','58b46abfe7880f716dcaaab6797d7f1a',3,'463XI',1890745553,0),('bbf9c973a605656ac267dbd00dda38ac','b25425e459f86bdc0d292bafa595e249',3,'F7S2DA',913031589,0),('9671029ea4da35b2514d29c881a42c93','20e31a82380d63ffbe9a8dcef6cd1da7',3,'PM2APG',1351907105,0),('248bd5314b8e29a591e1f9bf1e5659ae','ba731545db1bae4b8cd7aa2677189d71',3,'2ALDFN',1805053127,0),('ef279a980db7d8f8fef5b5f648c07b26','262102c9a8cd92569c9a96cf0e42ed66',3,'7CNJV',1322058268,0),('b21a6e1f43dd16651b9e916717be074b','2defdefd56ae211fcec5d79dce3623cd',3,'3I6B',203182400,0),('58027d5c6d48c5a53ee4980c2f3f5d1a','8dafd9b8cf1f4e496d4652cd5057d708',3,'3PML',206770592,0),('ed5c515d4eca4cf8221521808c708200','d976312461d0e080c8d35be0b49fae1c',3,'1B91',1729783492,0),('b769b04fe4658e834452adba075ee5a9','506ac75b173594a3fdc5446f5571c449',3,'66EK',305954648,0),('d4f518a4a13c26d0c391c176251f7f3b','ebb9046432e8407defd9a3bd32fff2eb',3,'7NH6Z',1799591617,0),('7a360f42b30a1ac251b17fd55828d60f','108344b65283f718130d2927047af31a',3,'3RC6',436780056,0),('158e3a21f9454aeb182f795a88ffb4fd','cb80b2fdf37b1cfa3e50d0e1ca2bcfe8',3,'2LFWD',634247624,0),('af14bc05c6bdb5ad5c797197224636ec','1b48acccca315c5b668dbced05b28d29',3,'3WKH99C',978269143,0),('f67f7a5bcb459388af0cd668f8b6a34e','2f037b87bcdea4a70dc337ad104440d5',3,'49T1P8',1059612065,0),('404656606fc10fadae816e92dbceea4c','5fe41fe060c5176b54a54814a369be76',3,'5JVZ6Z',498180375,0),('fc11346d7db2d38278cccec5c7debb6a','86d421be88b6d84ff3ef714b58ab28e8',3,'4V3B',86424523,0),('be3cc2ed95f45cf7f980018a1e5a6e21','6904ce8e14e68ca8f1b8c12d2597b515',3,'NLN7HX',1284018890,0),('6b4dc2d50df31660eeb6116da8eff14d','5733e90a94d3747b96e83702a72aff4d',3,'4KMAK',329221403,0),('5ee5a63499f22e9e1d70057838309516','e452d48afef37ba3848465fcf9dab587',3,'2VCCD',1759909118,0),('80ce1302efe47357aefe1faac5e3d0a8','e052e83eaf7425666a947ed029020ec8',1,'3BI59',1470481596,24),('8c79db7d82208591cb07d7ce0f041293','74098e79a24c0c5935d0cd9d6cf18b9f',3,'7Y14XWH',265045516,0),('e6905aeccaf2e7bc254e561a4292841c','74098e79a24c0c5935d0cd9d6cf18b9f',3,'2MTB',628428968,0),('6f7e5e401d715ea495447166e0a3961e','96aa28b3370c194cbc1e6fa7af084740',3,'KFIB1',1986605420,0),('c263044c152e8cde0222ad73167303ae','96aa28b3370c194cbc1e6fa7af084740',3,'5HGZ',2073238915,0),('8378dc2462d647848c301b3b17e0b522','461dff357d4afe9945b652c21e17f5e5',3,'526ASCM',1192988189,0),('1d6346b5c1c3cfbced2de26e82ddca49','461dff357d4afe9945b652c21e17f5e5',3,'2X1L',2099193320,0),('01dcc3117a9ba69e557264f23580594a','e58eb3693113a031b0429c8248ae880c',3,'28DVJH',796233284,0),('1b221d5b8f6b2acce80b8f681df4ca81','e58eb3693113a031b0429c8248ae880c',3,'56AZW3U',83634855,0);
/*!40000 ALTER TABLE `phpbb_confirm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_disallow`
--

DROP TABLE IF EXISTS `phpbb_disallow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_disallow` (
  `disallow_id` mediumint(8) unsigned NOT NULL auto_increment,
  `disallow_username` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`disallow_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_disallow`
--

LOCK TABLES `phpbb_disallow` WRITE;
/*!40000 ALTER TABLE `phpbb_disallow` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_disallow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_drafts`
--

DROP TABLE IF EXISTS `phpbb_drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_drafts` (
  `draft_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `save_time` int(11) unsigned NOT NULL default '0',
  `draft_subject` varchar(255) collate utf8_bin NOT NULL default '',
  `draft_message` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`draft_id`),
  KEY `save_time` (`save_time`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_drafts`
--

LOCK TABLES `phpbb_drafts` WRITE;
/*!40000 ALTER TABLE `phpbb_drafts` DISABLE KEYS */;
INSERT INTO `phpbb_drafts` VALUES (1,73,0,2,1365508534,'What to do','Hello. Czy ktoś wie co to jest wszystko o tej sprawie akceptacji plików cookie ? Czy to bezpieczne?   Dzięki za odpowiedź');
/*!40000 ALTER TABLE `phpbb_drafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_extension_groups`
--

DROP TABLE IF EXISTS `phpbb_extension_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_extension_groups` (
  `group_id` mediumint(8) unsigned NOT NULL auto_increment,
  `group_name` varchar(255) collate utf8_bin NOT NULL default '',
  `cat_id` tinyint(2) NOT NULL default '0',
  `allow_group` tinyint(1) unsigned NOT NULL default '0',
  `download_mode` tinyint(1) unsigned NOT NULL default '1',
  `upload_icon` varchar(255) collate utf8_bin NOT NULL default '',
  `max_filesize` int(20) unsigned NOT NULL default '0',
  `allowed_forums` text collate utf8_bin NOT NULL,
  `allow_in_pm` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_extension_groups`
--

LOCK TABLES `phpbb_extension_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_extension_groups` DISABLE KEYS */;
INSERT INTO `phpbb_extension_groups` VALUES (1,'IMAGES',1,1,1,'',0,'',0),(2,'ARCHIVES',0,1,1,'',0,'',0),(3,'PLAIN_TEXT',0,0,1,'',0,'',0),(4,'DOCUMENTS',0,0,1,'',0,'',0),(5,'REAL_MEDIA',3,0,1,'',0,'',0),(6,'WINDOWS_MEDIA',2,0,1,'',0,'',0),(7,'FLASH_FILES',5,0,1,'',0,'',0),(8,'QUICKTIME_MEDIA',6,0,1,'',0,'',0),(9,'DOWNLOADABLE_FILES',0,0,1,'',0,'',0);
/*!40000 ALTER TABLE `phpbb_extension_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_extensions`
--

DROP TABLE IF EXISTS `phpbb_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_extensions` (
  `extension_id` mediumint(8) unsigned NOT NULL auto_increment,
  `group_id` mediumint(8) unsigned NOT NULL default '0',
  `extension` varchar(100) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_extensions`
--

LOCK TABLES `phpbb_extensions` WRITE;
/*!40000 ALTER TABLE `phpbb_extensions` DISABLE KEYS */;
INSERT INTO `phpbb_extensions` VALUES (1,1,'gif'),(2,1,'png'),(3,1,'jpeg'),(4,1,'jpg'),(5,1,'tif'),(6,1,'tiff'),(7,1,'tga'),(8,2,'gtar'),(9,2,'gz'),(10,2,'tar'),(11,2,'zip'),(12,2,'rar'),(13,2,'ace'),(14,2,'torrent'),(15,2,'tgz'),(16,2,'bz2'),(17,2,'7z'),(18,3,'txt'),(19,3,'c'),(20,3,'h'),(21,3,'cpp'),(22,3,'hpp'),(23,3,'diz'),(24,3,'csv'),(25,3,'ini'),(26,3,'log'),(27,3,'js'),(28,3,'xml'),(29,4,'xls'),(30,4,'xlsx'),(31,4,'xlsm'),(32,4,'xlsb'),(33,4,'doc'),(34,4,'docx'),(35,4,'docm'),(36,4,'dot'),(37,4,'dotx'),(38,4,'dotm'),(39,4,'pdf'),(40,4,'ai'),(41,4,'ps'),(42,4,'ppt'),(43,4,'pptx'),(44,4,'pptm'),(45,4,'odg'),(46,4,'odp'),(47,4,'ods'),(48,4,'odt'),(49,4,'rtf'),(50,5,'rm'),(51,5,'ram'),(52,6,'wma'),(53,6,'wmv'),(54,7,'swf'),(55,8,'mov'),(56,8,'m4v'),(57,8,'m4a'),(58,8,'mp4'),(59,8,'3gp'),(60,8,'3g2'),(61,8,'qt'),(62,9,'mpeg'),(63,9,'mpg'),(64,9,'mp3'),(65,9,'ogg'),(66,9,'ogm');
/*!40000 ALTER TABLE `phpbb_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums`
--

DROP TABLE IF EXISTS `phpbb_forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums` (
  `forum_id` mediumint(8) unsigned NOT NULL auto_increment,
  `parent_id` mediumint(8) unsigned NOT NULL default '0',
  `left_id` mediumint(8) unsigned NOT NULL default '0',
  `right_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_parents` mediumtext collate utf8_bin NOT NULL,
  `forum_name` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_desc` text collate utf8_bin NOT NULL,
  `forum_desc_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_desc_options` int(11) unsigned NOT NULL default '7',
  `forum_desc_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `forum_link` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_password` varchar(40) collate utf8_bin NOT NULL default '',
  `forum_style` mediumint(8) unsigned NOT NULL default '0',
  `forum_image` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_rules` text collate utf8_bin NOT NULL,
  `forum_rules_link` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_rules_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_rules_options` int(11) unsigned NOT NULL default '7',
  `forum_rules_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `forum_topics_per_page` tinyint(4) NOT NULL default '0',
  `forum_type` tinyint(4) NOT NULL default '0',
  `forum_status` tinyint(4) NOT NULL default '0',
  `forum_posts` mediumint(8) unsigned NOT NULL default '0',
  `forum_topics` mediumint(8) unsigned NOT NULL default '0',
  `forum_topics_real` mediumint(8) unsigned NOT NULL default '0',
  `forum_last_post_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_last_poster_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_last_post_subject` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_last_post_time` int(11) unsigned NOT NULL default '0',
  `forum_last_poster_name` varchar(255) collate utf8_bin NOT NULL default '',
  `forum_last_poster_colour` varchar(6) collate utf8_bin NOT NULL default '',
  `forum_flags` tinyint(4) NOT NULL default '32',
  `forum_options` int(20) unsigned NOT NULL default '0',
  `display_subforum_list` tinyint(1) unsigned NOT NULL default '1',
  `display_on_index` tinyint(1) unsigned NOT NULL default '1',
  `enable_indexing` tinyint(1) unsigned NOT NULL default '1',
  `enable_icons` tinyint(1) unsigned NOT NULL default '1',
  `enable_prune` tinyint(1) unsigned NOT NULL default '0',
  `prune_next` int(11) unsigned NOT NULL default '0',
  `prune_days` mediumint(8) unsigned NOT NULL default '0',
  `prune_viewed` mediumint(8) unsigned NOT NULL default '0',
  `prune_freq` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`forum_id`),
  KEY `left_right_id` (`left_id`,`right_id`),
  KEY `forum_lastpost_id` (`forum_last_post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums`
--

LOCK TABLES `phpbb_forums` WRITE;
/*!40000 ALTER TABLE `phpbb_forums` DISABLE KEYS */;
INSERT INTO `phpbb_forums` VALUES (1,0,1,4,'','Informacje ogólne','','',7,'','','',0,'','','','',7,'',0,0,0,1,1,1,1,2,'',1360014818,'administrator','AA0000',32,0,1,1,1,1,0,0,0,0,0),(2,1,2,3,'a:1:{i:1;a:2:{i:0;s:18:\"Informacje ogólne\";i:1;i:0;}}','POCHP','Pytania dotyczące choroby','',7,'','','',0,'','','','',7,'',0,1,0,0,0,2,0,0,'',0,'','',48,0,1,1,1,1,0,0,0,0,0);
/*!40000 ALTER TABLE `phpbb_forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_access`
--

DROP TABLE IF EXISTS `phpbb_forums_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_access` (
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `session_id` char(32) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`forum_id`,`user_id`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_access`
--

LOCK TABLES `phpbb_forums_access` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_forums_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_track`
--

DROP TABLE IF EXISTS `phpbb_forums_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_track` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `mark_time` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_track`
--

LOCK TABLES `phpbb_forums_track` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_track` DISABLE KEYS */;
INSERT INTO `phpbb_forums_track` VALUES (2,2,1364499092);
/*!40000 ALTER TABLE `phpbb_forums_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_watch`
--

DROP TABLE IF EXISTS `phpbb_forums_watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_watch` (
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `notify_status` tinyint(1) unsigned NOT NULL default '0',
  KEY `forum_id` (`forum_id`),
  KEY `user_id` (`user_id`),
  KEY `notify_stat` (`notify_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_watch`
--

LOCK TABLES `phpbb_forums_watch` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_watch` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_forums_watch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_groups`
--

DROP TABLE IF EXISTS `phpbb_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_groups` (
  `group_id` mediumint(8) unsigned NOT NULL auto_increment,
  `group_type` tinyint(4) NOT NULL default '1',
  `group_founder_manage` tinyint(1) unsigned NOT NULL default '0',
  `group_skip_auth` tinyint(1) unsigned NOT NULL default '0',
  `group_name` varchar(255) collate utf8_bin NOT NULL default '',
  `group_desc` text collate utf8_bin NOT NULL,
  `group_desc_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `group_desc_options` int(11) unsigned NOT NULL default '7',
  `group_desc_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `group_display` tinyint(1) unsigned NOT NULL default '0',
  `group_avatar` varchar(255) collate utf8_bin NOT NULL default '',
  `group_avatar_type` tinyint(2) NOT NULL default '0',
  `group_avatar_width` smallint(4) unsigned NOT NULL default '0',
  `group_avatar_height` smallint(4) unsigned NOT NULL default '0',
  `group_rank` mediumint(8) unsigned NOT NULL default '0',
  `group_colour` varchar(6) collate utf8_bin NOT NULL default '',
  `group_sig_chars` mediumint(8) unsigned NOT NULL default '0',
  `group_receive_pm` tinyint(1) unsigned NOT NULL default '0',
  `group_message_limit` mediumint(8) unsigned NOT NULL default '0',
  `group_max_recipients` mediumint(8) unsigned NOT NULL default '0',
  `group_legend` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`group_id`),
  KEY `group_legend_name` (`group_legend`,`group_name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_groups`
--

LOCK TABLES `phpbb_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_groups` DISABLE KEYS */;
INSERT INTO `phpbb_groups` VALUES (1,3,0,0,'GUESTS','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(2,3,0,0,'REGISTERED','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(3,3,0,0,'REGISTERED_COPPA','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(4,3,0,0,'GLOBAL_MODERATORS','','',7,'',0,'',0,0,0,0,'00AA00',0,0,0,0,1),(5,3,1,0,'ADMINISTRATORS','','',7,'',0,'',0,0,0,0,'AA0000',0,0,0,0,1),(6,3,0,0,'BOTS','','',7,'',0,'',0,0,0,0,'9E8DA7',0,0,0,5,0),(7,3,0,0,'NEWLY_REGISTERED','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0);
/*!40000 ALTER TABLE `phpbb_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_icons`
--

DROP TABLE IF EXISTS `phpbb_icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_icons` (
  `icons_id` mediumint(8) unsigned NOT NULL auto_increment,
  `icons_url` varchar(255) collate utf8_bin NOT NULL default '',
  `icons_width` tinyint(4) NOT NULL default '0',
  `icons_height` tinyint(4) NOT NULL default '0',
  `icons_order` mediumint(8) unsigned NOT NULL default '0',
  `display_on_posting` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`icons_id`),
  KEY `display_on_posting` (`display_on_posting`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_icons`
--

LOCK TABLES `phpbb_icons` WRITE;
/*!40000 ALTER TABLE `phpbb_icons` DISABLE KEYS */;
INSERT INTO `phpbb_icons` VALUES (1,'misc/fire.gif',16,16,1,1),(2,'smile/redface.gif',16,16,9,1),(3,'smile/mrgreen.gif',16,16,10,1),(4,'misc/heart.gif',16,16,4,1),(5,'misc/star.gif',16,16,2,1),(6,'misc/radioactive.gif',16,16,3,1),(7,'misc/thinking.gif',16,16,5,1),(8,'smile/info.gif',16,16,8,1),(9,'smile/question.gif',16,16,6,1),(10,'smile/alert.gif',16,16,7,1);
/*!40000 ALTER TABLE `phpbb_icons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_lang`
--

DROP TABLE IF EXISTS `phpbb_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_lang` (
  `lang_id` tinyint(4) NOT NULL auto_increment,
  `lang_iso` varchar(30) collate utf8_bin NOT NULL default '',
  `lang_dir` varchar(30) collate utf8_bin NOT NULL default '',
  `lang_english_name` varchar(100) collate utf8_bin NOT NULL default '',
  `lang_local_name` varchar(255) collate utf8_bin NOT NULL default '',
  `lang_author` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`lang_id`),
  KEY `lang_iso` (`lang_iso`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_lang`
--

LOCK TABLES `phpbb_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_lang` DISABLE KEYS */;
INSERT INTO `phpbb_lang` VALUES (1,'en','en','British English','British English','phpBB Group'),(2,'pl','pl','Polish','Polski','phpBB3.PL Group');
/*!40000 ALTER TABLE `phpbb_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_log`
--

DROP TABLE IF EXISTS `phpbb_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_log` (
  `log_id` mediumint(8) unsigned NOT NULL auto_increment,
  `log_type` tinyint(4) NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `reportee_id` mediumint(8) unsigned NOT NULL default '0',
  `log_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `log_time` int(11) unsigned NOT NULL default '0',
  `log_operation` text collate utf8_bin NOT NULL,
  `log_data` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`log_id`),
  KEY `log_type` (`log_type`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `reportee_id` (`reportee_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_log`
--

LOCK TABLES `phpbb_log` WRITE;
/*!40000 ALTER TABLE `phpbb_log` DISABLE KEYS */;
INSERT INTO `phpbb_log` VALUES (1,2,1,0,0,0,'127.0.0.1',1360014869,'LOG_ERROR_EMAIL','a:1:{i:0;s:1138:\"<strong>EMAIL/SMTP</strong><br /><em>/forum/install/index.php</em><br /><br />Ran into problems sending Mail at <strong>Line 1054</strong>. Response: 550 \"Unknown User\"\r\n<br /><br /><strong>&lt;m.jankowski@miart.pl&gt;</strong> possibly an invalid email address?<h1>Backtrace</h1><p>Connecting to mail.pochp.eu:25<br />LINE: 1018 &lt;- 220 vps.miart.pl ESMTP Exim 4.72 Mon, 04 Feb 2013 22:54:29 +0100\r\n<br /># EHLO mac-mini-michal.local<br />LINE: 1290 &lt;- 250-vps.miart.pl Hello 89-69-184-11.dynamic.chello.pl [89.69.184.11]\r\n<br />LINE: 1290 &lt;- 250-SIZE 52428800\r\n<br />LINE: 1290 &lt;- 250-PIPELINING\r\n<br />LINE: 1290 &lt;- 250-AUTH PLAIN LOGIN\r\n<br />LINE: 1290 &lt;- 250-STARTTLS\r\n<br />LINE: 1290 &lt;- 250 HELP\r\n<br /># AUTH LOGIN<br />LINE: 1419 &lt;- 334 VXNlcm5hbWU6\r\n<br /># Omitting sensitive information<br />LINE: 1425 &lt;- 334 UGFzc3dvcmQ6\r\n<br /># Omitting sensitive information<br />LINE: 1431 &lt;- 235 Authentication succeeded\r\n<br /># MAIL FROM:&lt;m.jankowski@miart.pl&gt;<br />LINE: 1034 &lt;- 250 OK\r\n<br /># RCPT TO:&lt;m.jankowski@miart.pl&gt;<br />LINE: 1054 &lt;- 550 &quot;Unknown User&quot;\r\n</p><br />\";}'),(2,0,1,0,0,0,'127.0.0.1',1360014869,'LOG_INSTALL_INSTALLED','a:1:{i:0;s:6:\"3.0.11\";}'),(3,0,2,0,0,0,'127.0.0.1',1360015400,'LOG_ADMIN_AUTH_SUCCESS',''),(4,0,2,0,0,0,'127.0.0.1',1360015501,'LOG_PURGE_CACHE',''),(5,0,2,0,0,0,'127.0.0.1',1360015685,'LOG_TEMPLATE_CACHE_CLEARED','a:2:{i:0;s:9:\"prosilver\";i:1;s:15:\"Wszystkie pliki\";}'),(6,0,2,0,0,0,'127.0.0.1',1360015726,'LOG_TEMPLATE_CACHE_CLEARED','a:2:{i:0;s:9:\"prosilver\";i:1;s:58:\"forumlist_body, index_body, overall_footer, overall_header\";}'),(7,0,2,0,0,0,'127.0.0.1',1360015749,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(8,0,2,0,0,0,'127.0.0.1',1360015790,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(9,0,2,0,0,0,'127.0.0.1',1360015866,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(10,0,2,0,0,0,'127.0.0.1',1360015894,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(11,0,2,0,0,0,'127.0.0.1',1360016085,'LOG_CONFIG_SETTINGS',''),(12,0,2,0,0,0,'127.0.0.1',1360016104,'LOG_CONFIG_SETTINGS',''),(13,0,2,0,0,0,'127.0.0.1',1360016220,'LOG_CONFIG_VISUAL',''),(14,0,2,0,0,0,'127.0.0.1',1360016230,'LOG_CONFIG_VISUAL',''),(15,0,2,0,0,0,'127.0.0.1',1360016487,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(16,0,2,0,0,0,'127.0.0.1',1360017508,'LOG_ADMIN_AUTH_SUCCESS',''),(17,0,2,0,0,0,'127.0.0.1',1360017523,'LOG_TEMPLATE_CACHE_CLEARED','a:2:{i:0;s:9:\"prosilver\";i:1;s:15:\"Wszystkie pliki\";}'),(18,0,2,0,0,0,'127.0.0.1',1360017542,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(19,0,2,0,0,0,'127.0.0.1',1360017553,'LOG_IMAGESET_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(20,0,2,0,0,0,'127.0.0.1',1360017583,'LOG_IMAGESET_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(21,0,2,0,0,0,'127.0.0.1',1360017643,'LOG_CONFIG_SETTINGS',''),(22,0,2,0,0,0,'127.0.0.1',1360018202,'LOG_CONFIG_SETTINGS',''),(23,0,2,0,0,0,'127.0.0.1',1360018209,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(24,0,2,0,0,0,'127.0.0.1',1360018217,'LOG_IMAGESET_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(25,0,2,0,0,0,'127.0.0.1',1360018270,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(26,0,2,0,0,0,'127.0.0.1',1360018307,'LOG_THEME_REFRESHED','a:1:{i:0;s:9:\"prosilver\";}'),(27,0,2,0,0,0,'127.0.0.1',1360018372,'LOG_CONFIG_SETTINGS',''),(28,1,2,2,0,0,'89.67.201.68',1364498523,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:6:\"cialis\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(29,1,2,2,0,0,'89.67.201.68',1364498523,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:41:\"Effortless buy cialis Secrets - The Facts\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(30,1,2,2,0,0,'89.67.201.68',1364498523,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:23:\"скачать shareman\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(31,1,2,2,0,0,'89.67.201.68',1364498523,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:102:\"SeoUpTime - компания по продвижению сайтов в Санкт-Петербург\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(32,1,2,2,0,0,'89.67.201.68',1364498523,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:102:\"SeoUpTime - компания по продвижению сайтов в Санкт-Петербург\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(33,1,2,2,0,0,'89.67.201.68',1364498538,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:37:\"how to get Demadex cheap alternatives\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(34,1,2,2,0,0,'89.67.201.68',1364498538,'LOG_TOPIC_DISAPPROVED','a:2:{i:0;s:49:\"sales of Kamagra Effervescent 100mg buy online uk\";i:1;s:25:\"Wiadomość zawiera spam.\";}'),(35,0,2,0,0,0,'89.67.201.68',1364498581,'LOG_ADMIN_AUTH_SUCCESS',''),(36,0,2,0,0,0,'89.67.201.68',1364498722,'LOG_USER_DELETED','a:1:{i:0;s:6:\"Diamad\";}'),(37,0,2,0,0,0,'89.67.201.68',1364498780,'LOG_CONFIG_REGISTRATION',''),(38,0,2,0,0,0,'89.67.201.68',1364498848,'LOG_USER_DELETED','a:1:{i:0;s:11:\"swesseliemA\";}'),(39,0,2,0,0,0,'89.67.201.68',1364498869,'LOG_USER_DELETED','a:1:{i:0;s:6:\"Eagerg\";}'),(40,0,2,0,0,0,'89.67.201.68',1364498886,'LOG_USER_DELETED','a:1:{i:0;s:11:\"HeemnaTneks\";}'),(41,0,2,0,0,0,'89.67.201.68',1364498903,'LOG_USER_DELETED','a:1:{i:0;s:13:\"halpunnyunina\";}'),(42,0,2,0,0,0,'89.67.201.68',1364498915,'LOG_USER_DELETED','a:1:{i:0;s:6:\"Shekss\";}'),(43,0,2,0,0,0,'89.67.201.68',1364498921,'LOG_USER_DELETED','a:1:{i:0;s:6:\"arolfe\";}'),(44,0,2,0,0,0,'89.67.201.68',1364498934,'LOG_USER_DELETED','a:1:{i:0;s:9:\"BefeLetty\";}'),(45,0,2,0,0,0,'89.67.201.68',1364498945,'LOG_USER_DELETED','a:1:{i:0;s:9:\"selryCedy\";}'),(46,0,2,0,0,0,'89.67.201.68',1364498957,'LOG_USER_DELETED','a:1:{i:0;s:6:\"injury\";}'),(47,0,2,0,0,0,'89.67.201.68',1364498968,'LOG_USER_DELETED','a:1:{i:0;s:6:\"Whotje\";}'),(48,0,2,0,0,0,'89.67.201.68',1364498980,'LOG_USER_DELETED','a:1:{i:0;s:13:\"layemncyboame\";}'),(49,0,2,0,0,0,'89.67.201.68',1364499049,'LOG_ACL_TRANSFER_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(50,0,2,0,0,0,'89.67.201.68',1364499143,'LOG_ACL_RESTORE_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(51,1,2,2,1,0,'89.67.201.68',1364499236,'LOG_DELETE_POST','a:2:{i:0;s:16:\"Witamy w phpBB3!\";i:1;s:13:\"administrator\";}'),(52,0,2,0,0,0,'89.67.201.68',1364499290,'LOG_ADMIN_AUTH_SUCCESS',''),(53,0,2,0,0,0,'89.67.201.68',1364499360,'LOG_USER_DELETED','a:1:{i:0;s:11:\"Claceoccawn\";}'),(54,0,2,0,0,0,'89.67.201.68',1364499370,'LOG_BAN_EMAIL','a:2:{i:0;s:0:\"\";i:1;s:7:\"mail.ru\";}'),(55,1,2,0,0,0,'89.67.201.68',1364499370,'LOG_BAN_EMAIL','a:2:{i:0;s:0:\"\";i:1;s:7:\"mail.ru\";}'),(56,0,2,0,0,0,'89.67.201.68',1364499388,'LOG_BAN_EMAIL','a:2:{i:0;s:0:\"\";i:1;s:9:\"*@mail.ru\";}'),(57,1,2,0,0,0,'89.67.201.68',1364499388,'LOG_BAN_EMAIL','a:2:{i:0;s:0:\"\";i:1;s:9:\"*@mail.ru\";}'),(58,0,2,0,0,0,'89.67.201.68',1364499433,'LOG_USER_USER_UPDATE','a:1:{i:0;s:10:\"lulkiewicz\";}'),(59,0,2,0,0,0,'89.67.201.68',1364499446,'LOG_USER_USER_UPDATE','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(60,0,2,0,0,0,'89.67.201.68',1364499452,'LOG_ACL_TRANSFER_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(61,0,2,0,0,0,'89.67.201.68',1364499493,'LOG_ACL_RESTORE_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(62,0,2,0,0,0,'89.67.201.68',1364499576,'LOG_ACL_ADD_ADMIN_GLOBAL_A_','a:1:{i:0;s:10:\"lulkiewicz\";}'),(63,0,2,0,0,0,'89.67.201.68',1364499600,'LOG_ACL_ADD_ADMIN_GLOBAL_A_','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(64,0,2,0,0,0,'89.67.201.68',1364499614,'LOG_ACL_TRANSFER_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(65,0,2,0,0,0,'89.67.201.68',1364499637,'LOG_ACL_RESTORE_PERMISSIONS','a:1:{i:0;s:17:\"Jacek Nasiłowski\";}'),(66,2,1,0,0,0,'89.67.201.68',1364499733,'LOG_ERROR_EMAIL','a:1:{i:0;s:1126:\"<strong>EMAIL/SMTP</strong><br /><em>/forum/ucp.php</em><br /><br />Natrafiono na problemy przy wysyłaniu e-maila w <strong>linii 1054</strong>. Odpowiedź: 550 \"Unknown User\"\r\n.<br /><br /><strong>&lt;biuro@miart.pl&gt;</strong> prawdopodobnie nieprawidłowy adres e-mail?<h1>Backtrace</h1><p>Connecting to mail.pochp.eu:25<br />LINE: 1018 &lt;- 220 vps.miart.pl ESMTP Exim 4.72 Thu, 28 Mar 2013 20:42:12 +0100\r\n<br /># EHLO vps.miart.pl<br />LINE: 1290 &lt;- 250-vps.miart.pl Hello 80.82.17.228.ionic.pl [80.82.17.228]\r\n<br />LINE: 1290 &lt;- 250-SIZE 20971520\r\n<br />LINE: 1290 &lt;- 250-PIPELINING\r\n<br />LINE: 1290 &lt;- 250-AUTH PLAIN LOGIN\r\n<br />LINE: 1290 &lt;- 250-STARTTLS\r\n<br />LINE: 1290 &lt;- 250 HELP\r\n<br /># AUTH LOGIN<br />LINE: 1419 &lt;- 334 VXNlcm5hbWU6\r\n<br /># Omitting sensitive information<br />LINE: 1425 &lt;- 334 UGFzc3dvcmQ6\r\n<br /># Omitting sensitive information<br />LINE: 1431 &lt;- 235 Authentication succeeded\r\n<br /># MAIL FROM:&lt;m.jankowski@miart.pl&gt;<br />LINE: 1034 &lt;- 250 OK\r\n<br /># RCPT TO:&lt;biuro@miart.pl&gt;<br />LINE: 1054 &lt;- 550 &quot;Unknown User&quot;\r\n</p><br />\";}'),(67,0,2,0,0,0,'89.67.201.68',1364499859,'LOG_ADMIN_AUTH_SUCCESS',''),(68,0,2,0,0,0,'89.67.201.68',1364499898,'LOG_USER_ACTIVE','a:1:{i:0;s:9:\"test_user\";}'),(69,3,2,0,0,70,'89.67.201.68',1364499898,'LOG_USER_ACTIVE_USER',''),(70,0,2,0,0,0,'89.67.201.68',1364499902,'LOG_USER_USER_UPDATE','a:1:{i:0;s:9:\"test_user\";}'),(71,0,2,0,0,0,'89.67.201.68',1364499988,'LOG_ADMIN_AUTH_SUCCESS',''),(72,0,2,0,0,0,'89.67.201.68',1364500025,'LOG_ACL_ADD_ADMIN_GLOBAL_A_','a:1:{i:0;s:9:\"test_user\";}'),(73,0,2,0,0,0,'89.67.201.68',1364500187,'LOG_FORUM_EDIT','a:1:{i:0;s:18:\"Informacje ogólne\";}'),(74,0,2,0,0,0,'89.67.201.68',1364500315,'LOG_FORUM_EDIT','a:1:{i:0;s:5:\"POCHP\";}'),(75,0,2,0,0,0,'89.67.201.68',1364500483,'LOG_USERS_ADDED','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:9:\"test_user\";}'),(76,0,2,0,0,0,'89.67.201.68',1364500488,'LOG_GROUP_DEFAULTS','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:9:\"test_user\";}'),(77,0,2,0,0,0,'89.67.201.68',1364500530,'LOG_USERS_ADDED','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:17:\"Jacek Nasiłowski\";}'),(78,0,2,0,0,0,'89.67.201.68',1364500532,'LOG_GROUP_DEFAULTS','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:17:\"Jacek Nasiłowski\";}'),(79,0,2,0,0,0,'89.67.201.68',1364500555,'LOG_USERS_ADDED','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:10:\"lulkiewicz\";}'),(80,0,2,0,0,0,'89.67.201.68',1364500556,'LOG_GROUP_DEFAULTS','a:2:{i:0;s:15:\"Administratorzy\";i:1;s:10:\"lulkiewicz\";}'),(81,0,2,0,0,0,'89.67.201.68',1364500596,'LOG_USERS_ADDED','a:2:{i:0;s:20:\"Moderatorzy globalni\";i:1;s:9:\"test_user\";}'),(82,0,70,0,0,0,'89.67.201.68',1364500753,'LOG_ADMIN_AUTH_SUCCESS',''),(83,0,70,0,0,0,'89.67.201.68',1364501056,'LOG_ADMIN_AUTH_SUCCESS',''),(84,3,1,0,0,73,'46.246.38.200',1365508482,'LOG_USER_ACTIVE_USER',''),(85,3,1,0,0,77,'178.32.217.217',1367518225,'LOG_USER_ACTIVE_USER',''),(86,3,1,0,0,78,'46.118.125.111',1368408893,'LOG_USER_ACTIVE_USER',''),(87,3,1,0,0,81,'217.132.30.60',1370850147,'LOG_USER_ACTIVE_USER','');
/*!40000 ALTER TABLE `phpbb_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_login_attempts`
--

DROP TABLE IF EXISTS `phpbb_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_login_attempts` (
  `attempt_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `attempt_browser` varchar(150) collate utf8_bin NOT NULL default '',
  `attempt_forwarded_for` varchar(255) collate utf8_bin NOT NULL default '',
  `attempt_time` int(11) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `username` varchar(255) collate utf8_bin NOT NULL default '0',
  `username_clean` varchar(255) collate utf8_bin NOT NULL default '0',
  KEY `att_ip` (`attempt_ip`,`attempt_time`),
  KEY `att_for` (`attempt_forwarded_for`,`attempt_time`),
  KEY `att_time` (`attempt_time`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_login_attempts`
--

LOCK TABLES `phpbb_login_attempts` WRITE;
/*!40000 ALTER TABLE `phpbb_login_attempts` DISABLE KEYS */;
INSERT INTO `phpbb_login_attempts` VALUES ('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:13.0) Gecko/20100101 Firefox/13.0.1','',1371275437,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0','',1371272524,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0','',1371269654,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:11.0) Gecko/20120313 Firefox/11.0','',1371250881,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','',1371247679,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0) Gecko/20100101 Firefox/7.0','',1371244469,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','',1371241182,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25','',1371234498,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','',1371237864,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1','',1371231164,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.50','',1371227630,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.10','',1371224243,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','',1371220847,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/17.0 Firefox/17.0','',1371217460,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:16.0) Gecko/20100101 Firefox/16.0','',1371213844,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','',1371210362,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/17.0 Firefox/17.0','',1371203562,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','',1371206980,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1','',1371200055,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1','',1371196575,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','',1371193170,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16','',1371189855,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 5970)) Presto/2.12.388 Version/12.10','',1371186461,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','',1371183154,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; ru) Presto/2.10.289 Version/12.02','',1371179267,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','',1371175067,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0; rv:16.0) Gecko/20100101 Firefox/16.0','',1371171074,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1371167170,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.10.289 Version/12.02','',1371163219,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.11','',1371159365,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Iron/5.0.381.0 Chrome/5.0.381 Safari/533.4','',1371155372,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28 sputnik 2.5.2.8','',1371151388,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YE','',1371147457,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0','',1371143452,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.11','',1371139379,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371134123,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2','',1371127976,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1','',1371119089,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0','',1371095519,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; Edition Yx) Presto/2.12.388 Version/12.11','',1371083472,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.10','',1371027986,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.11','',1371055945,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; MRA 8.0 (build 5880); ru) Presto/2.10.289 Version/12.02','',1370796722,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','',1370792802,0,'selryCedy','selrycedy'),('94.185.85.171','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; MRA 6.0 (build 5993); MRA 8.0 (build 5784); InfoPath.2)','',1370780188,0,'tabaknowak','tabaknowak'),('91.238.134.14','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','',1370774053,0,'mmndrapiasta','mmndrapiasta'),('91.238.134.14','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0','',1370697691,0,'Roneamome','roneamome'),('91.238.134.14','Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1370651303,0,'Roneamome','roneamome'),('46.246.90.219','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1369289386,0,'selryCedy','selrycedy'),('46.246.90.219','Opera/9.80 (Windows NT 6.1; WOW64; U; Edition Next; Edition Yx; ru) Presto/2.11.310 Version/12.50','',1369401445,0,'selryCedy','selrycedy'),('178.210.216.140','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2','',1369525890,0,'rahdee','rahdee'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1','',1371278411,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.11','',1371281621,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)','',1371284885,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.10','',1371288248,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.11','',1371291502,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0','',1371294876,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; MRA 6.0 (build 5970); ru) Presto/2.7.62 Version/11.00','',1371298634,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; U; Edition Yx; ru) Presto/2.10.289 Version/12.02','',1371302153,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.22) Gecko/20110902 Firefox/3.6.22','',1371305806,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.2; U; en) Presto/2.10.289 Version/12.02','',1371309362,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.10','',1371312745,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; U; YB/3.5.1; ru) Presto/2.6.30 Version/10.63','',1371316201,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','',1371319503,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','',1371322717,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; MRA 6.0 (build 5976)) Presto/2.12.388 Version/12.11','',1371325996,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25','',1371329285,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 6001)) Presto/2.12.388 Version/12.10','',1371332373,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2','',1371335509,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','',1371338590,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','',1371341727,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2','',1371349991,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','',1371353359,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.19) Gecko/2010031422 Firefox/3.0.19 YB/3.5.1 (.NET CLR 3.5.30729)','',1371356669,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; MRA 6.0 (build 5998)) Presto/2.12.388 Version/12.11','',1371359830,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','',1371363155,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1371366554,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1371369878,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371373535,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.7.39 Version/11.00','',1371378712,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371382232,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4','',1371385681,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.10','',1371389149,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; GTB7.4; MRA 6.0 (build 6005); User-agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows','',1371392713,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','',1371396417,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','',1371400118,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:16.0) Gecko/20121026 Firefox/16.0 SeaMonkey/2.13.2','',1371403636,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.10.229 Version/11.64','',1371407192,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.10','',1371410687,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)','',1371414200,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)','',1371417607,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2','',1371420990,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:16.0) Gecko/20100101 Firefox/16.0','',1371424326,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:12.0) Gecko/20100101 Firefox/12.0','',1371427762,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322)','',1371431433,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/17.0 Firefox/17.0','',1371435179,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:16.0) Gecko/20100101 Firefox/16.0','',1371438831,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.00','',1371442239,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.10','',1371445665,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1','',1371449093,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Ukraine Local) Presto/2.12.388 Version/12.10','',1371452594,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)','',1371456035,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371459605,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','',1371463610,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; MRA 6.0 (build 5993); MRA 8.0 (build 5784); InfoPath.2)','',1371467200,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/17.0 Firefox/17.0','',1371470834,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','',1371474474,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25','',1371478174,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0','',1371481886,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1371485373,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; MRA 8.0 (build 5880); ru) Presto/2.10.289 Version/12.02','',1371488903,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','',1371492508,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/17.0 Firefox/17.0','',1371496486,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28 sputnik 2.5.2.8','',1371501058,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371505104,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.2; U; en) Presto/2.10.289 Version/12.02','',1371594663,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','',1371598758,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11','',1371602811,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0','',1371606914,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','',1371611186,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0; rv:16.0) Gecko/20100101 Firefox/16.0','',1371615578,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; MRA 6.0 (build 5993); MRA 8.0 (build 5784); InfoPath.2)','',1371619462,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YI','',1371623462,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','',1371627387,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','',1371631427,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','',1371635318,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','',1371639283,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4','',1371643289,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0','',1371647446,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1','',1371651972,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0','',1371656291,0,'selryCedy','selrycedy'),('46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/20100101 Firefox/17.0','',1371660588,0,'selryCedy','selrycedy'),('46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Yx) Presto/2.12.388 Version/12.10','',1371665015,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0','',1371669309,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','',1371672401,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 6.0; rv:17.0) Gecko/17.0 Firefox/17.0','',1371675616,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','',1371679200,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','',1371682708,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)','',1371686098,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','',1371689436,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2','',1371693601,0,'selryCedy','selrycedy'),('5.9.124.103','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.11','',1371698451,0,'selryCedy','selrycedy'),('5.9.124.103','Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Yx) Presto/2.12.388 Version/12.11','',1371703105,0,'selryCedy','selrycedy'),('5.9.124.103','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 6001)) Presto/2.12.388 Version/12.10','',1371707006,0,'selryCedy','selrycedy'),('5.9.124.103','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','',1371710630,0,'selryCedy','selrycedy'),('5.9.124.103','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.10','',1371713933,0,'selryCedy','selrycedy'),('188.126.69.253','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YI','',1371717325,0,'selryCedy','selrycedy'),('188.126.69.253','Opera/9.80 (Windows NT 6.1; U; Edition Yx; ru) Presto/2.10.289 Version/12.02','',1371722742,0,'selryCedy','selrycedy'),('188.126.69.253','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.92 Safari/537.4','',1371727174,0,'selryCedy','selrycedy'),('188.126.69.253','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0','',1371732069,0,'selryCedy','selrycedy'),('188.126.69.253','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/20100101 Firefox/17.0','',1371737042,0,'selryCedy','selrycedy');
/*!40000 ALTER TABLE `phpbb_login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_moderator_cache`
--

DROP TABLE IF EXISTS `phpbb_moderator_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_moderator_cache` (
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `username` varchar(255) collate utf8_bin NOT NULL default '',
  `group_id` mediumint(8) unsigned NOT NULL default '0',
  `group_name` varchar(255) collate utf8_bin NOT NULL default '',
  `display_on_index` tinyint(1) unsigned NOT NULL default '1',
  KEY `disp_idx` (`display_on_index`),
  KEY `forum_id` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_moderator_cache`
--

LOCK TABLES `phpbb_moderator_cache` WRITE;
/*!40000 ALTER TABLE `phpbb_moderator_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_moderator_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_modules`
--

DROP TABLE IF EXISTS `phpbb_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_modules` (
  `module_id` mediumint(8) unsigned NOT NULL auto_increment,
  `module_enabled` tinyint(1) unsigned NOT NULL default '1',
  `module_display` tinyint(1) unsigned NOT NULL default '1',
  `module_basename` varchar(255) collate utf8_bin NOT NULL default '',
  `module_class` varchar(10) collate utf8_bin NOT NULL default '',
  `parent_id` mediumint(8) unsigned NOT NULL default '0',
  `left_id` mediumint(8) unsigned NOT NULL default '0',
  `right_id` mediumint(8) unsigned NOT NULL default '0',
  `module_langname` varchar(255) collate utf8_bin NOT NULL default '',
  `module_mode` varchar(255) collate utf8_bin NOT NULL default '',
  `module_auth` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`module_id`),
  KEY `left_right_id` (`left_id`,`right_id`),
  KEY `module_enabled` (`module_enabled`),
  KEY `class_left_id` (`module_class`,`left_id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_modules`
--

LOCK TABLES `phpbb_modules` WRITE;
/*!40000 ALTER TABLE `phpbb_modules` DISABLE KEYS */;
INSERT INTO `phpbb_modules` VALUES (1,1,1,'','acp',0,1,64,'ACP_CAT_GENERAL','',''),(2,1,1,'','acp',1,4,17,'ACP_QUICK_ACCESS','',''),(3,1,1,'','acp',1,18,41,'ACP_BOARD_CONFIGURATION','',''),(4,1,1,'','acp',1,42,49,'ACP_CLIENT_COMMUNICATION','',''),(5,1,1,'','acp',1,50,63,'ACP_SERVER_CONFIGURATION','',''),(6,1,1,'','acp',0,65,84,'ACP_CAT_FORUMS','',''),(7,1,1,'','acp',6,66,71,'ACP_MANAGE_FORUMS','',''),(8,1,1,'','acp',6,72,83,'ACP_FORUM_BASED_PERMISSIONS','',''),(9,1,1,'','acp',0,85,110,'ACP_CAT_POSTING','',''),(10,1,1,'','acp',9,86,99,'ACP_MESSAGES','',''),(11,1,1,'','acp',9,100,109,'ACP_ATTACHMENTS','',''),(12,1,1,'','acp',0,111,166,'ACP_CAT_USERGROUP','',''),(13,1,1,'','acp',12,112,145,'ACP_CAT_USERS','',''),(14,1,1,'','acp',12,146,153,'ACP_GROUPS','',''),(15,1,1,'','acp',12,154,165,'ACP_USER_SECURITY','',''),(16,1,1,'','acp',0,167,216,'ACP_CAT_PERMISSIONS','',''),(17,1,1,'','acp',16,170,179,'ACP_GLOBAL_PERMISSIONS','',''),(18,1,1,'','acp',16,180,191,'ACP_FORUM_BASED_PERMISSIONS','',''),(19,1,1,'','acp',16,192,201,'ACP_PERMISSION_ROLES','',''),(20,1,1,'','acp',16,202,215,'ACP_PERMISSION_MASKS','',''),(21,1,1,'','acp',0,217,230,'ACP_CAT_STYLES','',''),(22,1,1,'','acp',21,218,221,'ACP_STYLE_MANAGEMENT','',''),(23,1,1,'','acp',21,222,229,'ACP_STYLE_COMPONENTS','',''),(24,1,1,'','acp',0,231,250,'ACP_CAT_MAINTENANCE','',''),(25,1,1,'','acp',24,232,241,'ACP_FORUM_LOGS','',''),(26,1,1,'','acp',24,242,249,'ACP_CAT_DATABASE','',''),(27,1,1,'','acp',0,251,276,'ACP_CAT_SYSTEM','',''),(28,1,1,'','acp',27,252,255,'ACP_AUTOMATION','',''),(29,1,1,'','acp',27,256,267,'ACP_GENERAL_TASKS','',''),(30,1,1,'','acp',27,268,275,'ACP_MODULE_MANAGEMENT','',''),(31,1,1,'','acp',0,277,278,'ACP_CAT_DOT_MODS','',''),(32,1,1,'attachments','acp',3,19,20,'ACP_ATTACHMENT_SETTINGS','attach','acl_a_attach'),(33,1,1,'attachments','acp',11,101,102,'ACP_ATTACHMENT_SETTINGS','attach','acl_a_attach'),(34,1,1,'attachments','acp',11,103,104,'ACP_MANAGE_EXTENSIONS','extensions','acl_a_attach'),(35,1,1,'attachments','acp',11,105,106,'ACP_EXTENSION_GROUPS','ext_groups','acl_a_attach'),(36,1,1,'attachments','acp',11,107,108,'ACP_ORPHAN_ATTACHMENTS','orphan','acl_a_attach'),(37,1,1,'ban','acp',15,155,156,'ACP_BAN_EMAILS','email','acl_a_ban'),(38,1,1,'ban','acp',15,157,158,'ACP_BAN_IPS','ip','acl_a_ban'),(39,1,1,'ban','acp',15,159,160,'ACP_BAN_USERNAMES','user','acl_a_ban'),(40,1,1,'bbcodes','acp',10,87,88,'ACP_BBCODES','bbcodes','acl_a_bbcode'),(41,1,1,'board','acp',3,21,22,'ACP_BOARD_SETTINGS','settings','acl_a_board'),(42,1,1,'board','acp',3,23,24,'ACP_BOARD_FEATURES','features','acl_a_board'),(43,1,1,'board','acp',3,25,26,'ACP_AVATAR_SETTINGS','avatar','acl_a_board'),(44,1,1,'board','acp',3,27,28,'ACP_MESSAGE_SETTINGS','message','acl_a_board'),(45,1,1,'board','acp',10,89,90,'ACP_MESSAGE_SETTINGS','message','acl_a_board'),(46,1,1,'board','acp',3,29,30,'ACP_POST_SETTINGS','post','acl_a_board'),(47,1,1,'board','acp',10,91,92,'ACP_POST_SETTINGS','post','acl_a_board'),(48,1,1,'board','acp',3,31,32,'ACP_SIGNATURE_SETTINGS','signature','acl_a_board'),(49,1,1,'board','acp',3,33,34,'ACP_FEED_SETTINGS','feed','acl_a_board'),(50,1,1,'board','acp',3,35,36,'ACP_REGISTER_SETTINGS','registration','acl_a_board'),(51,1,1,'board','acp',4,43,44,'ACP_AUTH_SETTINGS','auth','acl_a_server'),(52,1,1,'board','acp',4,45,46,'ACP_EMAIL_SETTINGS','email','acl_a_server'),(53,1,1,'board','acp',5,51,52,'ACP_COOKIE_SETTINGS','cookie','acl_a_server'),(54,1,1,'board','acp',5,53,54,'ACP_SERVER_SETTINGS','server','acl_a_server'),(55,1,1,'board','acp',5,55,56,'ACP_SECURITY_SETTINGS','security','acl_a_server'),(56,1,1,'board','acp',5,57,58,'ACP_LOAD_SETTINGS','load','acl_a_server'),(57,1,1,'bots','acp',29,257,258,'ACP_BOTS','bots','acl_a_bots'),(58,1,1,'captcha','acp',3,37,38,'ACP_VC_SETTINGS','visual','acl_a_board'),(59,1,0,'captcha','acp',3,39,40,'ACP_VC_CAPTCHA_DISPLAY','img','acl_a_board'),(60,1,1,'database','acp',26,243,244,'ACP_BACKUP','backup','acl_a_backup'),(61,1,1,'database','acp',26,245,246,'ACP_RESTORE','restore','acl_a_backup'),(62,1,1,'disallow','acp',15,161,162,'ACP_DISALLOW_USERNAMES','usernames','acl_a_names'),(63,1,1,'email','acp',29,259,260,'ACP_MASS_EMAIL','email','acl_a_email && cfg_email_enable'),(64,1,1,'forums','acp',7,67,68,'ACP_MANAGE_FORUMS','manage','acl_a_forum'),(65,1,1,'groups','acp',14,147,148,'ACP_GROUPS_MANAGE','manage','acl_a_group'),(66,1,1,'icons','acp',10,93,94,'ACP_ICONS','icons','acl_a_icons'),(67,1,1,'icons','acp',10,95,96,'ACP_SMILIES','smilies','acl_a_icons'),(68,1,1,'inactive','acp',13,115,116,'ACP_INACTIVE_USERS','list','acl_a_user'),(69,1,1,'jabber','acp',4,47,48,'ACP_JABBER_SETTINGS','settings','acl_a_jabber'),(70,1,1,'language','acp',29,261,262,'ACP_LANGUAGE_PACKS','lang_packs','acl_a_language'),(71,1,1,'logs','acp',25,233,234,'ACP_ADMIN_LOGS','admin','acl_a_viewlogs'),(72,1,1,'logs','acp',25,235,236,'ACP_MOD_LOGS','mod','acl_a_viewlogs'),(73,1,1,'logs','acp',25,237,238,'ACP_USERS_LOGS','users','acl_a_viewlogs'),(74,1,1,'logs','acp',25,239,240,'ACP_CRITICAL_LOGS','critical','acl_a_viewlogs'),(75,1,1,'main','acp',1,2,3,'ACP_INDEX','main',''),(76,1,1,'modules','acp',30,269,270,'ACP','acp','acl_a_modules'),(77,1,1,'modules','acp',30,271,272,'UCP','ucp','acl_a_modules'),(78,1,1,'modules','acp',30,273,274,'MCP','mcp','acl_a_modules'),(79,1,1,'permission_roles','acp',19,193,194,'ACP_ADMIN_ROLES','admin_roles','acl_a_roles && acl_a_aauth'),(80,1,1,'permission_roles','acp',19,195,196,'ACP_USER_ROLES','user_roles','acl_a_roles && acl_a_uauth'),(81,1,1,'permission_roles','acp',19,197,198,'ACP_MOD_ROLES','mod_roles','acl_a_roles && acl_a_mauth'),(82,1,1,'permission_roles','acp',19,199,200,'ACP_FORUM_ROLES','forum_roles','acl_a_roles && acl_a_fauth'),(83,1,1,'permissions','acp',16,168,169,'ACP_PERMISSIONS','intro','acl_a_authusers || acl_a_authgroups || acl_a_viewauth'),(84,1,0,'permissions','acp',20,203,204,'ACP_PERMISSION_TRACE','trace','acl_a_viewauth'),(85,1,1,'permissions','acp',18,181,182,'ACP_FORUM_PERMISSIONS','setting_forum_local','acl_a_fauth && (acl_a_authusers || acl_a_authgroups)'),(86,1,1,'permissions','acp',18,183,184,'ACP_FORUM_PERMISSIONS_COPY','setting_forum_copy','acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth'),(87,1,1,'permissions','acp',18,185,186,'ACP_FORUM_MODERATORS','setting_mod_local','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(88,1,1,'permissions','acp',17,171,172,'ACP_USERS_PERMISSIONS','setting_user_global','acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(89,1,1,'permissions','acp',13,117,118,'ACP_USERS_PERMISSIONS','setting_user_global','acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(90,1,1,'permissions','acp',18,187,188,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(91,1,1,'permissions','acp',13,119,120,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(92,1,1,'permissions','acp',17,173,174,'ACP_GROUPS_PERMISSIONS','setting_group_global','acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(93,1,1,'permissions','acp',14,149,150,'ACP_GROUPS_PERMISSIONS','setting_group_global','acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(94,1,1,'permissions','acp',18,189,190,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(95,1,1,'permissions','acp',14,151,152,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(96,1,1,'permissions','acp',17,175,176,'ACP_ADMINISTRATORS','setting_admin_global','acl_a_aauth && (acl_a_authusers || acl_a_authgroups)'),(97,1,1,'permissions','acp',17,177,178,'ACP_GLOBAL_MODERATORS','setting_mod_global','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(98,1,1,'permissions','acp',20,205,206,'ACP_VIEW_ADMIN_PERMISSIONS','view_admin_global','acl_a_viewauth'),(99,1,1,'permissions','acp',20,207,208,'ACP_VIEW_USER_PERMISSIONS','view_user_global','acl_a_viewauth'),(100,1,1,'permissions','acp',20,209,210,'ACP_VIEW_GLOBAL_MOD_PERMISSIONS','view_mod_global','acl_a_viewauth'),(101,1,1,'permissions','acp',20,211,212,'ACP_VIEW_FORUM_MOD_PERMISSIONS','view_mod_local','acl_a_viewauth'),(102,1,1,'permissions','acp',20,213,214,'ACP_VIEW_FORUM_PERMISSIONS','view_forum_local','acl_a_viewauth'),(103,1,1,'php_info','acp',29,263,264,'ACP_PHP_INFO','info','acl_a_phpinfo'),(104,1,1,'profile','acp',13,121,122,'ACP_CUSTOM_PROFILE_FIELDS','profile','acl_a_profile'),(105,1,1,'prune','acp',7,69,70,'ACP_PRUNE_FORUMS','forums','acl_a_prune'),(106,1,1,'prune','acp',15,163,164,'ACP_PRUNE_USERS','users','acl_a_userdel'),(107,1,1,'ranks','acp',13,123,124,'ACP_MANAGE_RANKS','ranks','acl_a_ranks'),(108,1,1,'reasons','acp',29,265,266,'ACP_MANAGE_REASONS','main','acl_a_reasons'),(109,1,1,'search','acp',5,59,60,'ACP_SEARCH_SETTINGS','settings','acl_a_search'),(110,1,1,'search','acp',26,247,248,'ACP_SEARCH_INDEX','index','acl_a_search'),(111,1,1,'send_statistics','acp',5,61,62,'ACP_SEND_STATISTICS','send_statistics','acl_a_server'),(112,1,1,'styles','acp',22,219,220,'ACP_STYLES','style','acl_a_styles'),(113,1,1,'styles','acp',23,223,224,'ACP_TEMPLATES','template','acl_a_styles'),(114,1,1,'styles','acp',23,225,226,'ACP_THEMES','theme','acl_a_styles'),(115,1,1,'styles','acp',23,227,228,'ACP_IMAGESETS','imageset','acl_a_styles'),(116,1,1,'update','acp',28,253,254,'ACP_VERSION_CHECK','version_check','acl_a_board'),(117,1,1,'users','acp',13,113,114,'ACP_MANAGE_USERS','overview','acl_a_user'),(118,1,0,'users','acp',13,125,126,'ACP_USER_FEEDBACK','feedback','acl_a_user'),(119,1,0,'users','acp',13,127,128,'ACP_USER_WARNINGS','warnings','acl_a_user'),(120,1,0,'users','acp',13,129,130,'ACP_USER_PROFILE','profile','acl_a_user'),(121,1,0,'users','acp',13,131,132,'ACP_USER_PREFS','prefs','acl_a_user'),(122,1,0,'users','acp',13,133,134,'ACP_USER_AVATAR','avatar','acl_a_user'),(123,1,0,'users','acp',13,135,136,'ACP_USER_RANK','rank','acl_a_user'),(124,1,0,'users','acp',13,137,138,'ACP_USER_SIG','sig','acl_a_user'),(125,1,0,'users','acp',13,139,140,'ACP_USER_GROUPS','groups','acl_a_user && acl_a_group'),(126,1,0,'users','acp',13,141,142,'ACP_USER_PERM','perm','acl_a_user && acl_a_viewauth'),(127,1,0,'users','acp',13,143,144,'ACP_USER_ATTACH','attach','acl_a_user'),(128,1,1,'words','acp',10,97,98,'ACP_WORDS','words','acl_a_words'),(129,1,1,'users','acp',2,5,6,'ACP_MANAGE_USERS','overview','acl_a_user'),(130,1,1,'groups','acp',2,7,8,'ACP_GROUPS_MANAGE','manage','acl_a_group'),(131,1,1,'forums','acp',2,9,10,'ACP_MANAGE_FORUMS','manage','acl_a_forum'),(132,1,1,'logs','acp',2,11,12,'ACP_MOD_LOGS','mod','acl_a_viewlogs'),(133,1,1,'bots','acp',2,13,14,'ACP_BOTS','bots','acl_a_bots'),(134,1,1,'php_info','acp',2,15,16,'ACP_PHP_INFO','info','acl_a_phpinfo'),(135,1,1,'permissions','acp',8,73,74,'ACP_FORUM_PERMISSIONS','setting_forum_local','acl_a_fauth && (acl_a_authusers || acl_a_authgroups)'),(136,1,1,'permissions','acp',8,75,76,'ACP_FORUM_PERMISSIONS_COPY','setting_forum_copy','acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth'),(137,1,1,'permissions','acp',8,77,78,'ACP_FORUM_MODERATORS','setting_mod_local','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(138,1,1,'permissions','acp',8,79,80,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(139,1,1,'permissions','acp',8,81,82,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(140,1,1,'','mcp',0,1,10,'MCP_MAIN','',''),(141,1,1,'','mcp',0,11,18,'MCP_QUEUE','',''),(142,1,1,'','mcp',0,19,32,'MCP_REPORTS','',''),(143,1,1,'','mcp',0,33,38,'MCP_NOTES','',''),(144,1,1,'','mcp',0,39,48,'MCP_WARN','',''),(145,1,1,'','mcp',0,49,56,'MCP_LOGS','',''),(146,1,1,'','mcp',0,57,64,'MCP_BAN','',''),(147,1,1,'ban','mcp',146,58,59,'MCP_BAN_USERNAMES','user','acl_m_ban'),(148,1,1,'ban','mcp',146,60,61,'MCP_BAN_IPS','ip','acl_m_ban'),(149,1,1,'ban','mcp',146,62,63,'MCP_BAN_EMAILS','email','acl_m_ban'),(150,1,1,'logs','mcp',145,50,51,'MCP_LOGS_FRONT','front','acl_m_ || aclf_m_'),(151,1,1,'logs','mcp',145,52,53,'MCP_LOGS_FORUM_VIEW','forum_logs','acl_m_,$id'),(152,1,1,'logs','mcp',145,54,55,'MCP_LOGS_TOPIC_VIEW','topic_logs','acl_m_,$id'),(153,1,1,'main','mcp',140,2,3,'MCP_MAIN_FRONT','front',''),(154,1,1,'main','mcp',140,4,5,'MCP_MAIN_FORUM_VIEW','forum_view','acl_m_,$id'),(155,1,1,'main','mcp',140,6,7,'MCP_MAIN_TOPIC_VIEW','topic_view','acl_m_,$id'),(156,1,1,'main','mcp',140,8,9,'MCP_MAIN_POST_DETAILS','post_details','acl_m_,$id || (!$id && aclf_m_)'),(157,1,1,'notes','mcp',143,34,35,'MCP_NOTES_FRONT','front',''),(158,1,1,'notes','mcp',143,36,37,'MCP_NOTES_USER','user_notes',''),(159,1,1,'pm_reports','mcp',142,20,21,'MCP_PM_REPORTS_OPEN','pm_reports','aclf_m_report'),(160,1,1,'pm_reports','mcp',142,22,23,'MCP_PM_REPORTS_CLOSED','pm_reports_closed','aclf_m_report'),(161,1,1,'pm_reports','mcp',142,24,25,'MCP_PM_REPORT_DETAILS','pm_report_details','aclf_m_report'),(162,1,1,'queue','mcp',141,12,13,'MCP_QUEUE_UNAPPROVED_TOPICS','unapproved_topics','aclf_m_approve'),(163,1,1,'queue','mcp',141,14,15,'MCP_QUEUE_UNAPPROVED_POSTS','unapproved_posts','aclf_m_approve'),(164,1,1,'queue','mcp',141,16,17,'MCP_QUEUE_APPROVE_DETAILS','approve_details','acl_m_approve,$id || (!$id && aclf_m_approve)'),(165,1,1,'reports','mcp',142,26,27,'MCP_REPORTS_OPEN','reports','aclf_m_report'),(166,1,1,'reports','mcp',142,28,29,'MCP_REPORTS_CLOSED','reports_closed','aclf_m_report'),(167,1,1,'reports','mcp',142,30,31,'MCP_REPORT_DETAILS','report_details','acl_m_report,$id || (!$id && aclf_m_report)'),(168,1,1,'warn','mcp',144,40,41,'MCP_WARN_FRONT','front','aclf_m_warn'),(169,1,1,'warn','mcp',144,42,43,'MCP_WARN_LIST','list','aclf_m_warn'),(170,1,1,'warn','mcp',144,44,45,'MCP_WARN_USER','warn_user','aclf_m_warn'),(171,1,1,'warn','mcp',144,46,47,'MCP_WARN_POST','warn_post','acl_m_warn && acl_f_read,$id'),(172,1,1,'','ucp',0,1,12,'UCP_MAIN','',''),(173,1,1,'','ucp',0,13,22,'UCP_PROFILE','',''),(174,1,1,'','ucp',0,23,30,'UCP_PREFS','',''),(175,1,1,'','ucp',0,31,42,'UCP_PM','',''),(176,1,1,'','ucp',0,43,48,'UCP_USERGROUPS','',''),(177,1,1,'','ucp',0,49,54,'UCP_ZEBRA','',''),(178,1,1,'attachments','ucp',172,10,11,'UCP_MAIN_ATTACHMENTS','attachments','acl_u_attach'),(179,1,1,'groups','ucp',176,44,45,'UCP_USERGROUPS_MEMBER','membership',''),(180,1,1,'groups','ucp',176,46,47,'UCP_USERGROUPS_MANAGE','manage',''),(181,1,1,'main','ucp',172,2,3,'UCP_MAIN_FRONT','front',''),(182,1,1,'main','ucp',172,4,5,'UCP_MAIN_SUBSCRIBED','subscribed',''),(183,1,1,'main','ucp',172,6,7,'UCP_MAIN_BOOKMARKS','bookmarks','cfg_allow_bookmarks'),(184,1,1,'main','ucp',172,8,9,'UCP_MAIN_DRAFTS','drafts',''),(185,1,0,'pm','ucp',175,32,33,'UCP_PM_VIEW','view','cfg_allow_privmsg'),(186,1,1,'pm','ucp',175,34,35,'UCP_PM_COMPOSE','compose','cfg_allow_privmsg'),(187,1,1,'pm','ucp',175,36,37,'UCP_PM_DRAFTS','drafts','cfg_allow_privmsg'),(188,1,1,'pm','ucp',175,38,39,'UCP_PM_OPTIONS','options','cfg_allow_privmsg'),(189,1,0,'pm','ucp',175,40,41,'UCP_PM_POPUP_TITLE','popup','cfg_allow_privmsg'),(190,1,1,'prefs','ucp',174,24,25,'UCP_PREFS_PERSONAL','personal',''),(191,1,1,'prefs','ucp',174,26,27,'UCP_PREFS_POST','post',''),(192,1,1,'prefs','ucp',174,28,29,'UCP_PREFS_VIEW','view',''),(193,1,1,'profile','ucp',173,14,15,'UCP_PROFILE_PROFILE_INFO','profile_info',''),(194,1,1,'profile','ucp',173,16,17,'UCP_PROFILE_SIGNATURE','signature',''),(195,1,1,'profile','ucp',173,18,19,'UCP_PROFILE_AVATAR','avatar','cfg_allow_avatar && (cfg_allow_avatar_local || cfg_allow_avatar_remote || cfg_allow_avatar_upload || cfg_allow_avatar_remote_upload)'),(196,1,1,'profile','ucp',173,20,21,'UCP_PROFILE_REG_DETAILS','reg_details',''),(197,1,1,'zebra','ucp',177,50,51,'UCP_ZEBRA_FRIENDS','friends',''),(198,1,1,'zebra','ucp',177,52,53,'UCP_ZEBRA_FOES','foes','');
/*!40000 ALTER TABLE `phpbb_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_poll_options`
--

DROP TABLE IF EXISTS `phpbb_poll_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_poll_options` (
  `poll_option_id` tinyint(4) NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `poll_option_text` text collate utf8_bin NOT NULL,
  `poll_option_total` mediumint(8) unsigned NOT NULL default '0',
  KEY `poll_opt_id` (`poll_option_id`),
  KEY `topic_id` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_poll_options`
--

LOCK TABLES `phpbb_poll_options` WRITE;
/*!40000 ALTER TABLE `phpbb_poll_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_poll_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_poll_votes`
--

DROP TABLE IF EXISTS `phpbb_poll_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_poll_votes` (
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `poll_option_id` tinyint(4) NOT NULL default '0',
  `vote_user_id` mediumint(8) unsigned NOT NULL default '0',
  `vote_user_ip` varchar(40) collate utf8_bin NOT NULL default '',
  KEY `topic_id` (`topic_id`),
  KEY `vote_user_id` (`vote_user_id`),
  KEY `vote_user_ip` (`vote_user_ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_poll_votes`
--

LOCK TABLES `phpbb_poll_votes` WRITE;
/*!40000 ALTER TABLE `phpbb_poll_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_poll_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_posts`
--

DROP TABLE IF EXISTS `phpbb_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_posts` (
  `post_id` mediumint(8) unsigned NOT NULL auto_increment,
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `poster_id` mediumint(8) unsigned NOT NULL default '0',
  `icon_id` mediumint(8) unsigned NOT NULL default '0',
  `poster_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `post_time` int(11) unsigned NOT NULL default '0',
  `post_approved` tinyint(1) unsigned NOT NULL default '1',
  `post_reported` tinyint(1) unsigned NOT NULL default '0',
  `enable_bbcode` tinyint(1) unsigned NOT NULL default '1',
  `enable_smilies` tinyint(1) unsigned NOT NULL default '1',
  `enable_magic_url` tinyint(1) unsigned NOT NULL default '1',
  `enable_sig` tinyint(1) unsigned NOT NULL default '1',
  `post_username` varchar(255) collate utf8_bin NOT NULL default '',
  `post_subject` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `post_text` mediumtext collate utf8_bin NOT NULL,
  `post_checksum` varchar(32) collate utf8_bin NOT NULL default '',
  `post_attachment` tinyint(1) unsigned NOT NULL default '0',
  `bbcode_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `bbcode_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `post_postcount` tinyint(1) unsigned NOT NULL default '1',
  `post_edit_time` int(11) unsigned NOT NULL default '0',
  `post_edit_reason` varchar(255) collate utf8_bin NOT NULL default '',
  `post_edit_user` mediumint(8) unsigned NOT NULL default '0',
  `post_edit_count` smallint(4) unsigned NOT NULL default '0',
  `post_edit_locked` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`post_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `poster_ip` (`poster_ip`),
  KEY `poster_id` (`poster_id`),
  KEY `post_approved` (`post_approved`),
  KEY `post_username` (`post_username`),
  KEY `tid_post_time` (`topic_id`,`post_time`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_posts`
--

LOCK TABLES `phpbb_posts` WRITE;
/*!40000 ALTER TABLE `phpbb_posts` DISABLE KEYS */;
INSERT INTO `phpbb_posts` VALUES (9,9,2,77,0,'178.32.217.217',1367840964,0,0,1,1,1,1,'','medication risperdal tardive dyskinesia','how long does it take for prevacid to write working  \n[url=http://6may2013.com&gt;diabetes age health statistics [/url]','5c675cd2121dbd9a0f84d78d4f3723f7',0,'','3besq8nc',1,0,'',0,0,0),(10,10,2,78,0,'46.118.125.111',1368632641,0,0,1,1,1,1,'','hi all man','hi all man soory no <!-- m --><a class=\"postlink\" href=\"http://rasselmajoo.com\">http://rasselmajoo.com</a><!-- m -->','ebe5f5a2abb22c2e8917e71842f9953b',0,'','1694ybtz',1,0,'',0,0,0);
/*!40000 ALTER TABLE `phpbb_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs`
--

DROP TABLE IF EXISTS `phpbb_privmsgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs` (
  `msg_id` mediumint(8) unsigned NOT NULL auto_increment,
  `root_level` mediumint(8) unsigned NOT NULL default '0',
  `author_id` mediumint(8) unsigned NOT NULL default '0',
  `icon_id` mediumint(8) unsigned NOT NULL default '0',
  `author_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `message_time` int(11) unsigned NOT NULL default '0',
  `enable_bbcode` tinyint(1) unsigned NOT NULL default '1',
  `enable_smilies` tinyint(1) unsigned NOT NULL default '1',
  `enable_magic_url` tinyint(1) unsigned NOT NULL default '1',
  `enable_sig` tinyint(1) unsigned NOT NULL default '1',
  `message_subject` varchar(255) collate utf8_bin NOT NULL default '',
  `message_text` mediumtext collate utf8_bin NOT NULL,
  `message_edit_reason` varchar(255) collate utf8_bin NOT NULL default '',
  `message_edit_user` mediumint(8) unsigned NOT NULL default '0',
  `message_attachment` tinyint(1) unsigned NOT NULL default '0',
  `bbcode_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `bbcode_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `message_edit_time` int(11) unsigned NOT NULL default '0',
  `message_edit_count` smallint(4) unsigned NOT NULL default '0',
  `to_address` text collate utf8_bin NOT NULL,
  `bcc_address` text collate utf8_bin NOT NULL,
  `message_reported` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`msg_id`),
  KEY `author_ip` (`author_ip`),
  KEY `message_time` (`message_time`),
  KEY `author_id` (`author_id`),
  KEY `root_level` (`root_level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs`
--

LOCK TABLES `phpbb_privmsgs` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_folder`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_folder` (
  `folder_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `folder_name` varchar(255) collate utf8_bin NOT NULL default '',
  `pm_count` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`folder_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_folder`
--

LOCK TABLES `phpbb_privmsgs_folder` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_rules`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_rules` (
  `rule_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `rule_check` mediumint(8) unsigned NOT NULL default '0',
  `rule_connection` mediumint(8) unsigned NOT NULL default '0',
  `rule_string` varchar(255) collate utf8_bin NOT NULL default '',
  `rule_user_id` mediumint(8) unsigned NOT NULL default '0',
  `rule_group_id` mediumint(8) unsigned NOT NULL default '0',
  `rule_action` mediumint(8) unsigned NOT NULL default '0',
  `rule_folder_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`rule_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_rules`
--

LOCK TABLES `phpbb_privmsgs_rules` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_to`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_to` (
  `msg_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `author_id` mediumint(8) unsigned NOT NULL default '0',
  `pm_deleted` tinyint(1) unsigned NOT NULL default '0',
  `pm_new` tinyint(1) unsigned NOT NULL default '1',
  `pm_unread` tinyint(1) unsigned NOT NULL default '1',
  `pm_replied` tinyint(1) unsigned NOT NULL default '0',
  `pm_marked` tinyint(1) unsigned NOT NULL default '0',
  `pm_forwarded` tinyint(1) unsigned NOT NULL default '0',
  `folder_id` int(11) NOT NULL default '0',
  KEY `msg_id` (`msg_id`),
  KEY `author_id` (`author_id`),
  KEY `usr_flder_id` (`user_id`,`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_to`
--

LOCK TABLES `phpbb_privmsgs_to` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_to` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_to` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields`
--

DROP TABLE IF EXISTS `phpbb_profile_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields` (
  `field_id` mediumint(8) unsigned NOT NULL auto_increment,
  `field_name` varchar(255) collate utf8_bin NOT NULL default '',
  `field_type` tinyint(4) NOT NULL default '0',
  `field_ident` varchar(20) collate utf8_bin NOT NULL default '',
  `field_length` varchar(20) collate utf8_bin NOT NULL default '',
  `field_minlen` varchar(255) collate utf8_bin NOT NULL default '',
  `field_maxlen` varchar(255) collate utf8_bin NOT NULL default '',
  `field_novalue` varchar(255) collate utf8_bin NOT NULL default '',
  `field_default_value` varchar(255) collate utf8_bin NOT NULL default '',
  `field_validation` varchar(20) collate utf8_bin NOT NULL default '',
  `field_required` tinyint(1) unsigned NOT NULL default '0',
  `field_show_novalue` tinyint(1) unsigned NOT NULL default '0',
  `field_show_on_reg` tinyint(1) unsigned NOT NULL default '0',
  `field_show_on_vt` tinyint(1) unsigned NOT NULL default '0',
  `field_show_profile` tinyint(1) unsigned NOT NULL default '0',
  `field_hide` tinyint(1) unsigned NOT NULL default '0',
  `field_no_view` tinyint(1) unsigned NOT NULL default '0',
  `field_active` tinyint(1) unsigned NOT NULL default '0',
  `field_order` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`field_id`),
  KEY `fld_type` (`field_type`),
  KEY `fld_ordr` (`field_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields`
--

LOCK TABLES `phpbb_profile_fields` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields_data`
--

DROP TABLE IF EXISTS `phpbb_profile_fields_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields_data` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields_data`
--

LOCK TABLES `phpbb_profile_fields_data` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields_lang`
--

DROP TABLE IF EXISTS `phpbb_profile_fields_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields_lang` (
  `field_id` mediumint(8) unsigned NOT NULL default '0',
  `lang_id` mediumint(8) unsigned NOT NULL default '0',
  `option_id` mediumint(8) unsigned NOT NULL default '0',
  `field_type` tinyint(4) NOT NULL default '0',
  `lang_value` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`field_id`,`lang_id`,`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields_lang`
--

LOCK TABLES `phpbb_profile_fields_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_lang`
--

DROP TABLE IF EXISTS `phpbb_profile_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_lang` (
  `field_id` mediumint(8) unsigned NOT NULL default '0',
  `lang_id` mediumint(8) unsigned NOT NULL default '0',
  `lang_name` varchar(255) collate utf8_bin NOT NULL default '',
  `lang_explain` text collate utf8_bin NOT NULL,
  `lang_default_value` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`field_id`,`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_lang`
--

LOCK TABLES `phpbb_profile_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_ranks`
--

DROP TABLE IF EXISTS `phpbb_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_ranks` (
  `rank_id` mediumint(8) unsigned NOT NULL auto_increment,
  `rank_title` varchar(255) collate utf8_bin NOT NULL default '',
  `rank_min` mediumint(8) unsigned NOT NULL default '0',
  `rank_special` tinyint(1) unsigned NOT NULL default '0',
  `rank_image` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`rank_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_ranks`
--

LOCK TABLES `phpbb_ranks` WRITE;
/*!40000 ALTER TABLE `phpbb_ranks` DISABLE KEYS */;
INSERT INTO `phpbb_ranks` VALUES (1,'Administrator',0,1,'');
/*!40000 ALTER TABLE `phpbb_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_reports`
--

DROP TABLE IF EXISTS `phpbb_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_reports` (
  `report_id` mediumint(8) unsigned NOT NULL auto_increment,
  `reason_id` smallint(4) unsigned NOT NULL default '0',
  `post_id` mediumint(8) unsigned NOT NULL default '0',
  `pm_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `user_notify` tinyint(1) unsigned NOT NULL default '0',
  `report_closed` tinyint(1) unsigned NOT NULL default '0',
  `report_time` int(11) unsigned NOT NULL default '0',
  `report_text` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`report_id`),
  KEY `post_id` (`post_id`),
  KEY `pm_id` (`pm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_reports`
--

LOCK TABLES `phpbb_reports` WRITE;
/*!40000 ALTER TABLE `phpbb_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_reports_reasons`
--

DROP TABLE IF EXISTS `phpbb_reports_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_reports_reasons` (
  `reason_id` smallint(4) unsigned NOT NULL auto_increment,
  `reason_title` varchar(255) collate utf8_bin NOT NULL default '',
  `reason_description` mediumtext collate utf8_bin NOT NULL,
  `reason_order` smallint(4) unsigned NOT NULL default '0',
  PRIMARY KEY  (`reason_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_reports_reasons`
--

LOCK TABLES `phpbb_reports_reasons` WRITE;
/*!40000 ALTER TABLE `phpbb_reports_reasons` DISABLE KEYS */;
INSERT INTO `phpbb_reports_reasons` VALUES (1,'warez','Post zawiera odnośniki do nielegalnego oprogramowania.',1),(2,'spam','Post zawiera spam.',2),(3,'off_topic','Treść postu nie dotyczy tematu dyskusji.',3),(4,'other','Powód zgłoszenia nie pasuje do powyższych kategorii, podaj powód w polu opis.',4);
/*!40000 ALTER TABLE `phpbb_reports_reasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_results`
--

DROP TABLE IF EXISTS `phpbb_search_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_results` (
  `search_key` varchar(32) collate utf8_bin NOT NULL default '',
  `search_time` int(11) unsigned NOT NULL default '0',
  `search_keywords` mediumtext collate utf8_bin NOT NULL,
  `search_authors` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`search_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_results`
--

LOCK TABLES `phpbb_search_results` WRITE;
/*!40000 ALTER TABLE `phpbb_search_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_search_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_wordlist`
--

DROP TABLE IF EXISTS `phpbb_search_wordlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_wordlist` (
  `word_id` mediumint(8) unsigned NOT NULL auto_increment,
  `word_text` varchar(255) collate utf8_bin NOT NULL default '',
  `word_common` tinyint(1) unsigned NOT NULL default '0',
  `word_count` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`word_id`),
  UNIQUE KEY `wrd_txt` (`word_text`),
  KEY `wrd_cnt` (`word_count`)
) ENGINE=MyISAM AUTO_INCREMENT=430 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_wordlist`
--

LOCK TABLES `phpbb_search_wordlist` WRITE;
/*!40000 ALTER TABLE `phpbb_search_wordlist` DISABLE KEYS */;
INSERT INTO `phpbb_search_wordlist` VALUES (1,'oto',0,0),(2,'przykładowy',0,0),(3,'post',0,0),(4,'twojej',0,0),(5,'instalacji',0,0),(6,'phpbb3',0,0),(7,'wygląda',0,0),(8,'wszystko',0,0),(9,'działa',0,0),(10,'jeśli',0,0),(11,'chcesz',0,0),(12,'możesz',0,0),(13,'usunąć',0,0),(14,'ten',0,0),(15,'kontynuować',0,0),(16,'konfigurację',0,0),(17,'twojego',0,0),(18,'forum',0,0),(19,'czasie',0,0),(20,'pierwszej',0,0),(21,'kategorii',0,0),(22,'twojemu',0,0),(23,'pierwszemu',0,0),(24,'działowi',0,0),(25,'przyznano',0,0),(26,'odpowiednie',0,0),(27,'uprawnienia',0,0),(28,'dla',0,0),(29,'predefiniowanych',0,0),(30,'grup',0,0),(31,'administratorzy',0,0),(32,'boty',0,0),(33,'goście',0,0),(34,'moderatorzy',0,0),(35,'globalni',0,0),(36,'zarejestrowani',0,0),(37,'użytkownicy',0,0),(38,'coppa',0,0),(39,'postanowisz',0,0),(40,'twoją',0,0),(41,'pierwszą',0,0),(42,'kategorię',0,0),(43,'twój',0,0),(44,'pierwszy',0,0),(45,'dział',0,0),(46,'nie',0,0),(47,'zapomnij',0,0),(48,'później',0,0),(49,'przyznać',0,0),(50,'uprawnień',0,0),(51,'dostępu',0,0),(52,'wszystkich',0,0),(53,'nowych',0,0),(54,'działów',0,0),(55,'wszystkim',0,0),(56,'tym',0,0),(57,'grupom',0,0),(58,'zalecamy',0,0),(59,'prostu',0,0),(60,'zmienić',0,0),(61,'nazwę',0,0),(62,'pierwszego',0,0),(63,'działu',0,0),(64,'potem',0,0),(65,'kopiować',0,0),(66,'nich',0,0),(67,'przy',0,0),(68,'tworzeniu',0,0),(69,'dobrej',0,0),(70,'zabawy',0,0),(71,'witamy',0,0),(425,'dyskinesia',0,1),(424,'tardive',0,1),(423,'risperdal',0,1),(422,'statistics',0,1),(421,'health',0,1),(420,'age',0,1),(419,'diabetes',0,1),(418,'6may2013',0,1),(417,'url',0,1),(416,'working',0,1),(415,'write',0,1),(414,'prevacid',0,1),(413,'take',0,1),(412,'long',0,1),(72,'buy',0,1),(73,'demadex',0,0),(74,'online',0,1),(75,'torsemide',0,0),(76,'click',0,1),(77,'price',0,1),(78,'below',0,1),(79,'and',0,3),(80,'you',0,1),(81,'will',0,1),(82,'redirected',0,1),(83,'our',0,1),(84,'pharmacy',0,1),(85,'http',0,3),(86,'orderrxstore',0,1),(87,'com',0,4),(88,'jpg',0,1),(89,'tags',0,1),(90,'low',0,1),(91,'cost',0,1),(92,'generic',0,1),(93,'antioch',0,0),(94,'discount',0,1),(95,'canadian',0,1),(96,'purchase',0,1),(97,'usa',0,1),(98,'london',0,0),(99,'buying',0,1),(100,'cheap',0,1),(101,'medication',0,2),(102,'interactions',0,0),(103,'www',0,1),(104,'newtzgames',0,0),(105,'app',0,0),(106,'webroot',0,0),(107,'5120',0,0),(108,'vancouver',0,0),(109,'canada',0,1),(110,'utah',0,0),(111,'cary',0,0),(112,'order',0,1),(113,'very',0,0),(114,'saguenay',0,0),(115,'need',0,1),(116,'prescription',0,1),(117,'for',0,2),(118,'united',0,0),(119,'states',0,0),(120,'where',0,1),(121,'newport',0,0),(122,'news',0,0),(123,'with',0,1),(124,'paypal',0,0),(125,'natural',0,0),(126,'without',0,1),(127,'mesquite',0,0),(128,'durham',0,0),(129,'there',0,0),(130,'inexpensive',0,0),(131,'can',0,1),(132,'donde',0,1),(133,'puedo',0,1),(134,'comprar',0,1),(135,'garland',0,0),(136,'how',0,2),(137,'get',0,1),(138,'best',0,1),(139,'non',0,1),(140,'bakersfield',0,0),(141,'10mg',0,0),(142,'necesito',0,0),(143,'over',0,0),(144,'the',0,3),(145,'counter',0,0),(146,'onqu75bcvap11j',0,0),(147,'nano',0,0),(148,'517',0,0),(149,'extra',0,1),(150,'addiction',0,1),(151,'symptoms',0,1),(152,'elizabeth',0,0),(153,'during',0,0),(154,'pregnancy',0,0),(155,'free',0,1),(156,'shipping',0,0),(157,'tyler',0,1),(158,'texas',0,1),(159,'medical',0,1),(160,'doctors',0,1),(161,'ohio',0,0),(162,'mail',0,1),(163,'legal',0,1),(164,'winnipeg',0,0),(165,'what',0,1),(166,'portland',0,0),(167,'clinesharing',0,0),(168,'index',0,0),(169,'php',0,1),(170,'new',0,0),(171,'side',0,0),(172,'effects',0,0),(173,'gainesville',0,0),(174,'prices',0,1),(175,'sheffield',0,0),(176,'overnight',0,0),(177,'delivery',0,0),(178,'pills',0,1),(179,'nchen',0,0),(180,'compare',0,1),(181,'vipps',0,0),(182,'certified',0,0),(183,'hampton',0,0),(184,'indiana',0,0),(185,'nashville',0,0),(186,'safe',0,0),(187,'petersburg',0,1),(188,'cleveland',0,0),(189,'use',0,0),(190,'alternatives',0,0),(191,'kamagra',0,0),(192,'effervescent',0,0),(193,'sildenafil',0,0),(194,'citrate',0,0),(195,'hartford',0,0),(196,'las',0,0),(197,'farmacias',0,0),(198,'simi',0,0),(199,'works',0,0),(200,'sale',0,0),(201,'india',0,0),(202,'safety',0,0),(203,'eugene',0,0),(204,'100mg',0,0),(205,'wiesbaden',0,0),(206,'cheapest',0,0),(207,'place',0,0),(208,'same',0,0),(209,'thing',0,0),(210,'jamaica',0,0),(211,'oral',0,0),(212,'jelly',0,0),(213,'visa',0,0),(214,'chesapeake',0,0),(215,'brasilia',0,0),(216,'pricing',0,0),(217,'chandler',0,0),(218,'pics',0,0),(219,'weight',0,0),(220,'loss',0,0),(221,'pictures',0,0),(222,'sseldorf',0,0),(223,'hexie799',0,0),(224,'joinbbs',0,0),(225,'net',0,0),(226,'viewthread',0,0),(227,'627',0,0),(228,'100',0,0),(229,'most',0,0),(230,'reliable',0,0),(231,'onle',0,0),(232,'phamacy',0,0),(233,'money',0,0),(234,'mobile',0,0),(235,'brand',0,0),(236,'san',0,0),(237,'antonio',0,0),(238,'drug',0,0),(239,'quierofibertel',0,0),(240,'foro',0,0),(241,'showt',0,0),(242,'post23380',0,0),(243,'arira',0,0),(244,'megasoft',0,0),(245,'showthread',0,0),(246,'tid',0,0),(247,'17786',0,0),(248,'edinburgh',0,0),(249,'does',0,1),(250,'sales',0,0),(251,'prior',0,0),(252,'perscription',0,0),(253,'minnesota',0,0),(254,'cod',0,0),(255,'vicodin',0,0),(256,'super',0,0),(257,'active',0,0),(258,'mgamers',0,0),(259,'viewtopic',0,0),(260,'890774',0,0),(261,'palm',0,0),(262,'bay',0,0),(263,'kansas',0,0),(264,'city',0,0),(265,'cnc',0,0),(266,'corp',0,0),(267,'supportcenter',0,0),(268,'viewt',0,0),(269,'p171502',0,0),(270,'savannah',0,0),(271,'brampton',0,0),(272,'from',0,0),(273,'american',0,0),(274,'pharmacies',0,0),(275,'tennessee',0,0),(276,'when',0,0),(277,'viagra',0,0),(278,'fullerton',0,0),(279,'birmingham',0,0),(280,'work',0,0),(281,'augustaa',0,0),(282,'honolulu',0,0),(429,'rasselmajoo',0,1),(428,'soory',0,1),(427,'man',0,2),(426,'all',0,2),(283,'cialis',0,2),(284,'versions',0,0),(285,'are',0,0),(286,'easily',0,0),(287,'available',0,0),(288,'your',0,0),(289,'neighborhood',0,0),(290,'even',0,0),(291,'store',0,0),(292,'however',0,0),(293,'many',0,0),(294,'fake',0,0),(295,'that',0,0),(296,'sell',0,0),(297,'them',0,0),(298,'this',0,0),(299,'consumption',0,0),(300,'should',0,0),(301,'exempted',0,0),(302,'victim',0,0),(303,'advised',0,0),(304,'routine',0,0),(305,'nitrate',0,0),(306,'drugs',0,0),(307,'men',0,0),(308,'who',0,0),(309,'not',0,0),(310,'only',0,0),(311,'have',0,0),(312,'easier',0,0),(313,'but',0,0),(314,'they',0,0),(315,'safeguard',0,0),(316,'their',0,0),(317,'privacy',0,0),(318,'too',0,0),(319,'consulting',0,0),(320,'doctor',0,0),(321,'immediately',0,0),(322,'see',0,0),(323,'withdrawal',0,0),(324,'time',0,0),(325,'erection',0,0),(326,'added',0,0),(327,'advantage',0,0),(328,'which',0,0),(329,'patients',0,0),(330,'while',0,0),(331,'treatment',0,0),(332,'fact',0,0),(333,'taken',0,0),(334,'any',0,0),(335,'food',0,0),(336,'intake',0,0),(337,'full',0,0),(338,'stomach',0,0),(339,'perfectly',0,0),(340,'fine',0,0),(341,'effortless',0,0),(342,'secrets',0,0),(343,'facts',0,0),(344,'программа',0,0),(345,'для',0,0),(346,'shareman',0,0),(347,'поможет',0,0),(348,'один',0,0),(349,'клик',0,0),(350,'скачать',0,0),(351,'интересующие',0,0),(352,'музыку',0,0),(353,'наверное',0,0),(354,'интересуетесь',0,0),(355,'про',0,0),(356,'бесплатно',0,0),(357,'это',0,0),(358,'можно',0,0),(359,'сделать',0,0),(360,'тут',0,0),(361,'seouptime',0,2),(362,'company',0,1),(363,'promote',0,1),(364,'sites',0,1),(365,'develop',0,1),(366,'affordable',0,1),(367,'effective',0,1),(368,'internet',0,1),(369,'solutions',0,1),(370,'audit',0,1),(371,'semantic',0,1),(372,'core',0,1),(373,'site',0,1),(374,'selection',0,1),(375,'key',0,1),(376,'words',0,1),(377,'promotion',0,1),(378,'social',0,1),(379,'networks',0,1),(380,'search',0,1),(381,'engine',0,1),(382,'optimization',0,1),(383,'website',0,1),(384,'russia',0,1),(385,'компания',0,2),(386,'продвижению',0,2),(387,'сайтов',0,2),(388,'санкт',0,2),(389,'петербурге',0,1),(390,'разработка',0,1),(391,'доступных',0,1),(392,'эффективных',0,1),(393,'интернет',0,1),(394,'решений',0,1),(395,'аудит',0,1),(396,'составление',0,1),(397,'семантического',0,1),(398,'ядра',0,1),(399,'сайта',0,1),(400,'подбор',0,1),(401,'ключевых',0,1),(402,'слов',0,1),(403,'продвижение',0,1),(404,'социальных',0,1),(405,'сетях',0,1),(406,'поисковая',0,1),(407,'оптимизация',0,1),(408,'россии',0,1),(409,'septimeuptr',0,1),(410,'gmail',0,1),(411,'петербург',0,1);
/*!40000 ALTER TABLE `phpbb_search_wordlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_wordmatch`
--

DROP TABLE IF EXISTS `phpbb_search_wordmatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_wordmatch` (
  `post_id` mediumint(8) unsigned NOT NULL default '0',
  `word_id` mediumint(8) unsigned NOT NULL default '0',
  `title_match` tinyint(1) unsigned NOT NULL default '0',
  UNIQUE KEY `unq_mtch` (`word_id`,`post_id`,`title_match`),
  KEY `word_id` (`word_id`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_wordmatch`
--

LOCK TABLES `phpbb_search_wordmatch` WRITE;
/*!40000 ALTER TABLE `phpbb_search_wordmatch` DISABLE KEYS */;
INSERT INTO `phpbb_search_wordmatch` VALUES (9,85,0),(10,85,0),(9,87,0),(10,87,0),(9,101,1),(9,117,0),(9,136,0),(9,249,0),(9,412,0),(9,413,0),(9,414,0),(9,415,0),(9,416,0),(9,417,0),(9,418,0),(9,419,0),(9,420,0),(9,421,0),(9,422,0),(9,423,1),(9,424,1),(9,425,1),(10,426,0),(10,426,1),(10,427,0),(10,427,1),(10,428,0),(10,429,0);
/*!40000 ALTER TABLE `phpbb_search_wordmatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sessions`
--

DROP TABLE IF EXISTS `phpbb_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sessions` (
  `session_id` char(32) collate utf8_bin NOT NULL default '',
  `session_user_id` mediumint(8) unsigned NOT NULL default '0',
  `session_forum_id` mediumint(8) unsigned NOT NULL default '0',
  `session_last_visit` int(11) unsigned NOT NULL default '0',
  `session_start` int(11) unsigned NOT NULL default '0',
  `session_time` int(11) unsigned NOT NULL default '0',
  `session_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `session_browser` varchar(150) collate utf8_bin NOT NULL default '',
  `session_forwarded_for` varchar(255) collate utf8_bin NOT NULL default '',
  `session_page` varchar(255) collate utf8_bin NOT NULL default '',
  `session_viewonline` tinyint(1) unsigned NOT NULL default '1',
  `session_autologin` tinyint(1) unsigned NOT NULL default '0',
  `session_admin` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `session_time` (`session_time`),
  KEY `session_user_id` (`session_user_id`),
  KEY `session_fid` (`session_forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sessions`
--

LOCK TABLES `phpbb_sessions` WRITE;
/*!40000 ALTER TABLE `phpbb_sessions` DISABLE KEYS */;
INSERT INTO `phpbb_sessions` VALUES ('47a5433a20d736420fd56e9a3d46e75f',1,0,1371288680,1371288680,1371288680,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('11d1f4994e3aa8790a2a267ff9a88198',1,0,1371289026,1371289026,1371289026,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('1e777833a258a3ebac356461fea6bce5',1,2,1371291501,1371291501,1371291504,'46.246.20.248','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('b6e3355732723a018a95d7b710e67433',1,2,1371294875,1371294875,1371294878,'46.246.20.248','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('b6ab26f2fb8bb81c5aea6ead9c43ffa8',1,0,1371295514,1371295514,1371295514,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=70',1,0,0),('8b3236a485030d9f8218f40a6953c292',1,2,1371298633,1371298633,1371298636,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; MRA 6.0 (build 5970); ru) Presto/2.7.62 Version/11.00','','posting.php?mode=post&f=2',1,0,0),('30b808f47a868b3dabea2a56e67227af',17,0,1372453510,1372453510,1372453510,'66.249.75.21','DoCoMo/2.0 N905i(c100;TB;W24H16) (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)','','index.php',1,0,0),('4e6dd3a6ddda5247c402cd49aa21b931',1,2,1371302152,1371302152,1371302155,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; Edition Yx; ru) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('b81f1ac789c62f88d0bf0b68e3be2fb5',1,2,1371288247,1371288247,1371288250,'46.246.20.248','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('ecb1704af72509af709b152c3d1143ca',1,2,1371272520,1371272520,1371272528,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0','','posting.php?mode=post&f=2',1,0,0),('d499863e8db1036559607b420c8f729e',1,0,1371275105,1371275105,1371275105,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ec7f47929d4731f34d1a19323b187681',1,2,1371275436,1371275436,1371275440,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:13.0) Gecko/20100101 Firefox/13.0.1','','posting.php?mode=post&f=2',1,0,0),('317fcde2382df71c8fcd61f1b543e0d0',1,2,1371278409,1371278409,1371278413,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1','','posting.php?mode=post&f=2',1,0,0),('f4e84d5f1958b0e21c3ff3312c0ba7d1',1,2,1371281618,1371281618,1371281623,'46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('5c000f8e4e058f7fd995b4a448491a86',1,2,1371284883,1371284883,1371284886,'46.246.20.248','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)','','posting.php?mode=post&f=2',1,0,0),('193b7c1bd1e8c5f4b5e3097d29ec40c7',1,0,1371256307,1371256307,1371256307,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('1c34659b503a2ff5ddefb7c0f6cc78c9',1,0,1371260865,1371260865,1371260868,'91.238.134.14','Mozilla/5.0 (Windows NT 5.2; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0','','index.php',1,0,0),('399da78cc3e9258d1ee8117d61e29299',1,2,1371269646,1371269646,1371269658,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0','','posting.php?mode=post&f=2',1,0,0),('7e4e0213d8e9568ba976bc9a011555ad',1,2,1371247678,1371247678,1371247681,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('e307b8439d75df25366da1414524a818',1,2,1371250878,1371250878,1371250882,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:11.0) Gecko/20120313 Firefox/11.0','','posting.php?mode=post&f=2',1,0,0),('a17a0d391ce40f959270b41622da71b5',1,2,1371237863,1371237863,1371237868,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','','posting.php?mode=post&f=2',1,0,0),('07a8f5b8ee6f313dca19a47c7d9b8461',1,2,1371241181,1371241181,1371241184,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','posting.php?mode=post&f=2',1,0,0),('4bec75acbf519f973a18452dfc3a940f',1,0,1371243631,1371243631,1371243631,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3e869d19f54d274b5005739608d41abb',1,2,1371244468,1371244468,1371244471,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0) Gecko/20100101 Firefox/7.0','','posting.php?mode=post&f=2',1,0,0),('c158c099c1525cda22265c8bd4ef99e9',1,0,1371245379,1371245379,1371245379,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=73',1,0,0),('a7eadf4768067b92c7efc907623dc172',1,0,1371210577,1371210577,1371210577,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('520044ca7fb5c1e1a53037c791caaf9a',1,2,1371213840,1371213840,1371213846,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('3056d05b0fb51125ca2d28e659f1f9b4',1,2,1371217459,1371217459,1371217465,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('6168030375e2d5ecc41eeae2a2a199d9',1,2,1371220846,1371220846,1371220849,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','','posting.php?mode=post&f=2',1,0,0),('9856afd7823169df4d90132e978b6053',1,0,1371223510,1371223510,1371223510,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=78',1,0,0),('503ca4fce53b4f49b69d626fc3a390be',1,0,1371223757,1371223757,1371223757,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('41ed4375ccad543882e550ee393eff96',1,2,1371224242,1371224242,1371224245,'46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('b35b200b1f70bc14be66eadff5e37688',1,2,1371227629,1371227629,1371227632,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.50','','posting.php?mode=post&f=2',1,0,0),('9a72b4bed805b23248a999945d12eef8',1,2,1371231163,1371231163,1371231166,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1','','posting.php?mode=post&f=2',1,0,0),('73d939d86fd8d7be2dde81c198d6fe43',1,2,1371234497,1371234497,1371234500,'46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25','','posting.php?mode=post&f=2',1,0,0),('8df83b7ebf79afba0741a2b0403c0d4d',1,2,1371237407,1371237407,1371237407,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','posting.php?mode=post&f=2',1,0,0),('161f4125991692f445cc5642fdc69d08',1,2,1371203560,1371203560,1371203564,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('f4aba109248fd1ba5509198785997e3d',1,2,1371206979,1371206979,1371206982,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','','posting.php?mode=post&f=2',1,0,0),('9e0154937593354303c1ca2d1aee7414',1,0,1371209016,1371209016,1371209016,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','faq.php',1,0,0),('1f10f78b1a85281a160641e22e1de03d',1,2,1371210360,1371210360,1371210363,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','','posting.php?mode=post&f=2',1,0,0),('535d80f6e89e11da2f70d5ab73fa5328',1,0,1370867139,1370867139,1370867139,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d2ba654d17578502567373c3d829fc13',1,0,1370879749,1370879749,1370879749,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.02','','faq.php',1,0,0),('5830c9825b5ed3b4f9b5396c330ffbb5',1,0,1370880423,1370880423,1370880423,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','','faq.php',1,0,0),('ab35bb64d07331eb5afbf44d1a2d5f80',1,0,1370881119,1370881119,1370881119,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)','','faq.php',1,0,0),('003e6b8e8c920d428f9a1dfc2dbeb8af',1,0,1370881789,1370881789,1370881789,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4','','faq.php',1,0,0),('7f54b716999d8014edfd3f987aacc0de',1,0,1370882478,1370882478,1370882478,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; Edition Next; Edition Yx; ru) Presto/2.11.310 Version/12.50','','faq.php',1,0,0),('79dc0c3dd3a315b7b6862c352b54058e',1,0,1370883152,1370883152,1370883152,'46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.10','','faq.php',1,0,0),('cb32d81573b5546a5ca4a9c2db56d5a9',1,0,1370883444,1370883444,1370883444,'69.58.178.56','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:14.0; ips-agent) Gecko/20100101 Firefox/14.0.1','','index.php',1,0,0),('61416528c3cca05da69a75aa73460cac',1,2,1371167169,1371167169,1371167172,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('b74bdcfe9d46693368c9ef5914fbb0c5',1,2,1371171066,1371171066,1371171078,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('12d0dd819b5068ff54a0579fb1531b4c',1,0,1371173406,1371173406,1371173406,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('49aca5d9cf0ddb461f469ba802598eb4',1,2,1371175065,1371175065,1371175069,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('8f51bdf3e82a1d2610a8a490215a2f4b',1,0,1371177573,1371177573,1371177573,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e9b86dfb85c8e3c64680fcfe2fbb5169',1,2,1371179263,1371179263,1371179272,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; ru) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('a059e7fe580a8106d3a6dd081bfb5fe6',1,2,1370603882,1370603882,1370603882,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2&t=1',1,0,0),('4fa625b76cd805155d524bb2c6e4fed2',1,0,1370608235,1370608235,1370608235,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a04dff3443b7fe85d2c04fa07dc70f27',1,0,1370612374,1370612374,1370612374,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=unanswered',1,0,0),('ad7ce51f3ef99c20d210c60fe56a073a',1,2,1370128688,1370128688,1370128688,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','posting.php?mode=post&f=2',1,0,0),('de72d7d97b898504fd7febe29c5c3c5e',1,0,1370136957,1370136957,1370136957,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php',1,0,0),('06007ff47bcaced0620794e74a947b72',1,0,1370140493,1370140493,1370140493,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b282840a07a5e50d12400e254f169fbb',1,0,1370152038,1370152038,1370152038,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('1ebd75ed96ce798332e126480870d842',1,0,1370154233,1370154233,1370154233,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('61b91e6aa7e0720414cd7c66365e0f1a',1,2,1370157960,1370157960,1370157960,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=2',1,0,0),('48272808a30ba99dd4e7ecfbb3b88955',1,0,1370160709,1370160709,1370160709,'188.124.183.79','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('3b8b085f515a03bd0445ace2732b2f88',1,0,1370173125,1370173125,1370173125,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5a235dff6e77f4dbd70e6ea6e255635f',9,2,1372444505,1370175170,1372444505,'178.255.215.71','Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)','','viewforum.php?f=2',1,0,0),('49f740044db0bf9af1c7c6c5b27835bf',1,0,1370186665,1370186665,1370186665,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('89d21dd8e187c2870b77576effef0f06',1,0,1370194522,1370194522,1370194522,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php',1,0,0),('1498980a7d12daa3cd85a8bb0f75b957',1,0,1370194528,1370194528,1370194528,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('1629602aacd20289fe3ce922492afc4a',1,0,1370206270,1370206270,1370206270,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('aee3a40fcbd7b72607d37a573d4497a8',1,0,1370218899,1370218899,1370218899,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5a44102d36f9f858ba273cb144ca574d',1,0,1370237889,1370237889,1370237889,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d7ce37b1aaf6b025183cb8602fd9074f',1,0,1370239449,1370239449,1370239449,'173.199.116.187','Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)','','index.php',1,0,0),('7fe580bc868316b47269371bc0cf89aa',1,0,1370239738,1370239738,1370239738,'173.199.116.187','Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)','','index.php',1,0,0),('213be5ccfdb6acf161c2bd1a7d88cee4',1,0,1370247216,1370247216,1370247216,'153.19.102.13','Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0','','index.php',1,0,0),('78a09529bdeb223e20160598746edea1',1,0,1370251543,1370251543,1370251543,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('95a7509f26cbd9119f08be8f5997671c',1,0,1370252425,1370252425,1370252425,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=leaders',1,0,0),('cde41cdd6cd93232af833bbde0cffbb4',1,0,1370271275,1370271275,1370271275,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7fbf59031d04421ec61ef57b0f5496a9',1,2,1370280649,1370280649,1370280669,'83.4.241.158','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0','','viewforum.php?f=2',1,0,0),('c159c1ac8d8e4a2ad3f2d7195beeac7d',1,0,1370284223,1370284223,1370284223,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a27e0e24c19918105d2278ce283e3845',1,0,1370287544,1370287544,1370287544,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=63',1,0,0),('018cff7f714d2ec9d6372ab89304960c',1,0,1370303330,1370303330,1370303330,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('323f0d90ca4e3613e56dfb0b50b0b66f',1,0,1370316407,1370316407,1370316407,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2a54a631248d930230d96d61bb375557',1,0,1370330666,1370330666,1370330666,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('3d67b5c04c6dd3bced19e04b45e0388f',1,0,1370335875,1370335875,1370335875,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('0f7b5b8f4e991df46b430464eb996b9f',1,0,1370349039,1370349039,1370349039,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2cbf95dcb4a2a5afd6de7c3f796a5d4b',1,0,1370368700,1370368700,1370368700,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('bd02d17f835f4cc317b27192ffe9d2f2',1,0,1370381356,1370381356,1370381356,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2be739da0a1473b32c771b3e2709991e',1,0,1370382077,1370382077,1370382077,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('64ca998a2db479abb65b786bc1faeb3b',1,2,1371378711,1371378711,1371378714,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.7.39 Version/11.00','','posting.php?mode=post&f=2',1,0,0),('31c2ff1f4a2db3e1ac8d66454288c790',1,0,1370879088,1370879088,1370879088,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0','','faq.php',1,0,0),('a3bdc0f1d2e0e2bb6e9f4cd078c06276',1,0,1370497618,1370497618,1370497618,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7330282271c440656e260bfe42789822',1,0,1370401037,1370401037,1370401037,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9f2b54910fd8e88be762f3cacceea0cc',1,0,1370413735,1370413735,1370413735,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('73360c552ee789b24c1ca91e86ebb003',1,0,1370433121,1370433121,1370433121,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2c16a05600756e6ca1197b6d4fa0f962',1,2,1371452593,1371452593,1371452597,'46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Ukraine Local) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('3109206b567746458b3816c7fb14ca8c',1,0,1370447250,1370447250,1370447250,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9e4374c2b168f1bfd3af96dca207e001',1,0,1370465618,1370465618,1370465618,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('557b1133e9cca2b7e7318762df94e3ad',1,0,1370478237,1370478237,1370478237,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('33f33c0cecc6e9daece469581dd0d914',1,0,1370482783,1370482783,1370482783,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('f3670803d88515d0977909d658a14837',1,0,1370488813,1370488813,1370488813,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('88c1504fe8a68f52ce3d813ad37c55a0',1,0,1370510888,1370510888,1370510888,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b8cabdd979a1c067292539abbb50a344',1,0,1370527678,1370527678,1370527681,'178.210.216.140','Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0','','ucp.php?mode=register',1,0,0),('d5982fa74ae0f795efe9892e459046d8',1,0,1370531363,1370531363,1370531363,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a71ead26c182c75235a65465cf341852',1,0,1370540949,1370540949,1370540949,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('0e37f66c670639410aca177762b93866',1,0,1370543330,1370543330,1370543330,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5e79b5cc420b2f17abebedf2987aec2e',1,0,1370560573,1370560573,1370560573,'95.49.104.120','Mozilla/5.0 (Windows NT 6.2; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('9df3e9170f08d59fa6380f6026ce5983',1,0,1370562258,1370562258,1370562258,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7a7a242ed31c161c93fdb0b61d181bc6',1,0,1370575739,1370575739,1370575739,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8f6498f754bb9fc93c3e61eb2ddd0abd',1,0,1370590168,1370590168,1370590378,'31.11.196.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36','','faq.php',1,0,0),('e148a5ff2c1e6da15d338b9e24d410ce',1,0,1370590206,1370590206,1370590206,'54.226.9.252','Mozilla/5.0 (compatible; proximic; +http://www.proximic.com/info/spider.php)','','index.php',1,0,0),('c9a2f72024baaddeac5876bfa19937de',1,0,1370590232,1370590232,1370590232,'50.19.170.222','Mozilla/5.0 (compatible; proximic; +http://www.proximic.com/info/spider.php)','','faq.php',1,0,0),('35330fd5e37222eed62d3bd449ba627e',1,0,1371202572,1371202572,1371202572,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=leaders',1,0,0),('d75499559a91c8fba87080cf5aae462c',1,2,1371200054,1371200054,1371200057,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1','','posting.php?mode=post&f=2',1,0,0),('a63991d0d879a5f48117f4155d922eda',1,2,1371196573,1371196573,1371196577,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1','','posting.php?mode=post&f=2',1,0,0),('76bfbffaa57c444e4336f5e70d272368',1,0,1371194973,1371194973,1371194977,'46.165.208.105','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','','index.php',1,0,0),('c588cd18a6f538c100d22359c4c3855e',1,0,1371194690,1371194690,1371194690,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=privacy',1,0,0),('4abaade003b961fe870daa846a23884a',1,2,1371193169,1371193169,1371193172,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','','posting.php?mode=post&f=2',1,0,0),('abb363be98cadd970477b9a219064f9b',1,0,1371191147,1371191147,1371191147,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d4fd7a70d5562750f24cbbff3f7851cb',1,2,1371186456,1371186456,1371186465,'46.246.20.248','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 5970)) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('0613b93706e9a22e244de5f1c00901ac',1,2,1371189853,1371189853,1371189860,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16','','posting.php?mode=post&f=2',1,0,0),('f8481c18799b984797c34651c5edf6b6',1,2,1371183150,1371183150,1371183162,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','','posting.php?mode=post&f=2',1,0,0),('21833fea7db5e169f592d4e65079b269',1,0,1371165731,1371165731,1371165731,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('7295e920dc89193dab20206d7a3e5b05',1,0,1371165725,1371165725,1371165725,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php',1,0,0),('5a111f87a2ffccb93eefcf396a49b5b2',1,2,1371163217,1371163217,1371163221,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('ba276a79b037a5ef08f762a32efd0303',1,2,1371159364,1371159364,1371159367,'46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('3e1266424472daf668bf51c33c48132b',1,0,1371158573,1371158573,1371158573,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('005be1aec15d5b9a19fe5b473f03114b',1,2,1371155371,1371155371,1371155374,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Iron/5.0.381.0 Chrome/5.0.381 Safari/533.4','','posting.php?mode=post&f=2',1,0,0),('ff57ea209fde0198a4d7066c7bf6abed',1,2,1371151387,1371151387,1371151390,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28 sputnik 2.5.2.8','','posting.php?mode=post&f=2',1,0,0),('9728d6f8eb9e5a76773572db6c400a82',1,2,1371147456,1371147456,1371147459,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YE','','posting.php?mode=post&f=2',1,0,0),('0b51ed61df6fcc058fc1d9e6137ffad6',1,0,1371146552,1371146552,1371146552,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d184cbcd83af6aff4a353f343f9cd5bf',1,2,1371143451,1371143451,1371143454,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('f55ef4ec48858cdd1306d069480484ec',1,0,1371142606,1371142606,1371142606,'91.221.122.209','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; GTB7.4; InfoPath.2)','','index.php',1,0,0),('e1ebb627df4ec1ee30d6211401cf43af',1,0,1370626887,1370626887,1370626887,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b4de225efd59c8fafefd4b84d990f92d',1,0,1370640413,1370640413,1370640413,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('6460df28bdee31244e01972e03eb323c',1,0,1370647274,1370647274,1370647274,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('18307f9eb9aef66c9f5ae6d83a5ef3fb',1,0,1370651302,1370651302,1370651305,'91.238.134.14','Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','index.php',1,0,0),('2910e1d600bff13376656030db982c3d',1,0,1370659763,1370659763,1370659763,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('95fd5a6d4ff853347a9177954b4ed16e',1,0,1370661548,1370661548,1370661548,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=78',1,0,0),('397b2d98e05e7291e25cc76ef1d87882',1,2,1370670522,1370670522,1370670522,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('e7fae67c362a3be4c9ead29289a0f4cd',1,0,1370670535,1370670535,1370670535,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=resend_act',1,0,0),('55f7f6e79025fbfef4c8a7b145d2ec6b',1,0,1370672822,1370672822,1370672822,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('251c73fc26c9865e67d54c41b37fba3e',1,0,1370691861,1370691861,1370691861,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f4995c5c09c0946cde36a223175542fc',1,0,1370843121,1370843121,1370843121,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=73',1,0,0),('35716ee1502538df0b8ae2fd961f2b13',1,0,1370697680,1370697680,1370697707,'91.238.134.14','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0','','index.php',1,0,0),('9a2ccefb40a587929b94282202c6c1b4',1,0,1370704951,1370704951,1370704951,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=privacy',1,0,0),('a87fff33ad8a8976ec623345d4e5fe65',1,0,1370704967,1370704967,1370704967,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c07e4fa2c5a52e2e30df606a58a7707d',1,0,1370723940,1370723940,1370723940,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('86e16ad3e6788176f8dbf3a57d3d04d3',1,0,1370737378,1370737378,1370737378,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('fbef607c8c2f951b291e1844785547b9',1,0,1370756369,1370756369,1370756369,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('837531e5dba113152bb9326e2b6acd93',1,0,1370762551,1370762551,1370762551,'31.11.191.208','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36','','index.php',1,0,0),('6ecae6f589d5c1c5a5c2a7329264c1af',1,0,1370769878,1370769878,1370769878,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('028d983d84d87fbac648cd1b9daea8a5',1,0,1370774048,1370774048,1370774058,'91.238.134.14','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','','index.php',1,0,0),('91748f21347999f18583a4fac4c205ed',1,0,1371038305,1371038305,1371038305,'178.248.30.132','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)','','ucp.php?mode=register',1,0,0),('6195497c2d7825c8ef68ee808d6d7cac',1,0,1371048501,1371048501,1371048501,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('0382feabdfcb4ef0b5818f4330c073b5',1,0,1371037538,1371037538,1371037538,'81.163.8.183','Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.15','','index.php',1,0,0),('576020fed13d6e9063b4458fbf2729ad',1,0,1370788683,1370788683,1370788683,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c9e4de2d69ec3b2f9bf4391b271a1556',1,0,1370792800,1370792800,1370792803,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','index.php',1,0,0),('563c925e4cb3b3e78c6ce59879e498a4',1,0,1370796720,1370796720,1370796723,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; MRA 8.0 (build 5880); ru) Presto/2.10.289 Version/12.02','','index.php',1,0,0),('5770fc53bee1c182f8a78b2008595ee4',1,2,1370799349,1370799349,1370799349,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=2',1,0,0),('c96c59dace7a229b92846f83fac987de',1,0,1370802248,1370802248,1370802248,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ef1af0acc9ed69a5898d82d3458d3708',1,1,1370820080,1370820080,1370820080,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=1',1,0,0),('7e25059ce93f937295f76a643eea6249',1,0,1370820101,1370820101,1370820101,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('be40eb9f6d1c806bc4e12a1854c96adf',1,0,1370820851,1370820851,1370820851,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('06e7e023d393ff1168cae96609b8a508',1,0,1370834578,1370834578,1370834578,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('49667089702143fdfab429c206c49281',1,0,1370834591,1370834591,1370834591,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('71565a8f4c3791a15d80809efca45f6a',1,0,1370843129,1370843129,1370843129,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=delete_cookies',1,0,0),('37f3fa54998cd92a6a12de1a2ba7de8b',1,0,1370849135,1370849135,1370849140,'217.132.30.60','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','','ucp.php?mode=register',1,0,0),('fb637052f5c6116cebdbdf1af15005d9',1,2,1371139378,1371139378,1371139381,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('8128cdb0b22b69a9afaba91eec5078a7',1,2,1371134121,1371134121,1371134125,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('e6c3248fbdfa63b369c6a83abb134cae',1,2,1371127974,1371127974,1371127979,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2','','posting.php?mode=post&f=2',1,0,0),('09bdb77f1be0a66a7dd968db3dd46d16',1,0,1371126466,1371126466,1371126466,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('14797f1bc7026189fd8a41e244656acb',1,0,1371119349,1371119349,1371119349,'94.254.211.156','Mozilla/5.0 (Linux; U; Android 4.0.3; pl-pl; LG-E610 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30','','index.php',1,0,0),('9802d2643e07c199a727ff0661c063bd',1,2,1371119082,1371119082,1371119092,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1','','posting.php?mode=post&f=2',1,0,0),('ca5af8ed6b8e19d0ca75b00675b08674',1,0,1371113223,1371113223,1371113223,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e351f7f108d2eba5b8eeb32d2985b145',1,2,1371095517,1371095517,1371095522,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('6815e9a36a8b50bab9227c3224d5d5b7',1,0,1371093856,1371093856,1371093856,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('251c82827ad71786fe14c5da4ab7fc8d',1,0,1371087527,1371087527,1371087527,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('6d58ffa85cf8adb483ef38eac927675b',1,2,1371083468,1371083468,1371083478,'46.246.20.248','Opera/9.80 (Windows NT 6.1; Edition Yx) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('4e380aeb8d9bb79a05ad345630f79362',1,0,1371080102,1371080102,1371080102,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('352ee587a0f7106eed2dc5fca9d29400',1,0,1371061542,1371061542,1371061542,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('110af9f07ff60feecac560d27a559100',1,0,1371064623,1371064623,1371064623,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('2e21d51d5a00c11682bc4e472e627d3f',1,2,1371055943,1371055943,1371055948,'46.246.20.248','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('5139826a011dfc6b03a2aa689c0a5a83',1,0,1371035703,1371035703,1371035703,'31.6.141.200','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1; .NET CLR 3.0.4506.2152; .NET CLR 3','','index.php',1,0,0),('7bf08f057fb3ff9b1730119d3c53477b',1,0,1371029200,1371029200,1371029200,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('caae8effdf318bf523eadc45074ca7f1',1,2,1371027984,1371027984,1371027989,'46.246.20.248','Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('cbf836940d499d93e9f6f20751e63f13',1,0,1371015358,1371015358,1371015358,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a931a66989ebc0c0e026832cc12e4696',1,0,1371014975,1371014975,1371014975,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('d5cbfc8602f4dbb0a814a21879911855',1,0,1371008204,1371008204,1371008213,'91.238.134.14','Opera/9.80 (Windows NT 5.1; Edition Yx) Presto/2.12.388 Version/12.10','','index.php',1,0,0),('13bce0224e7515f2885f0b190c776d5d',1,0,1370996549,1370996549,1370996549,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b4ea1b84c86563fac5230fe063ccb06f',1,0,1370992941,1370992941,1370992941,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('c7ffdd6b8b0465e2175edaba957d13de',1,0,1370983121,1370983121,1370983121,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b59afd0298834df4bc922f29d83d07d6',1,0,1370964370,1370964370,1370964370,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('edba83c40b8625649a71467b3e35fb10',1,0,1370950945,1370950945,1370950945,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('534e59c220a000c71d6b8e330b3aebc5',1,0,1370944619,1370944619,1370944624,'91.238.134.14','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','index.php',1,0,0),('e50c8718c26c628f93bdfcebdc36baf0',1,0,1370931793,1370931793,1370931793,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5422588a24b87368aad7c1492833c2c4',1,0,1370918351,1370918351,1370918351,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('907c47394d77df851b41bfba29dd4976',1,0,1370913855,1370913855,1370913855,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.00','','faq.php',1,0,0),('76f569e881ba760ba663a9293eaf186e',1,0,1370913161,1370913161,1370913161,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','','faq.php',1,0,0),('7583b3549fac9a829ad50a2dab643064',1,0,1370912436,1370912436,1370912436,'46.246.20.248','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 5970)) Presto/2.12.388 Version/12.10','','faq.php',1,0,0),('0e6460e3670261dd4846d06e1cd9b04c',1,0,1370911728,1370911728,1370911728,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; MRA 8.0 (build 5880)) Presto/2.12.388 Version/12.11','','faq.php',1,0,0),('6cae0e7a1d3bfb05d8b95ffcf5069201',1,0,1370911025,1370911025,1370911025,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.7.39 Version/11.00','','faq.php',1,0,0),('20c9bf9231f734d8615676d818c291a3',1,0,1370910217,1370910217,1370910217,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','','faq.php',1,0,0),('c6b8003726110d64caf7ba7ab4c65659',1,0,1370909393,1370909393,1370909393,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0','','faq.php',1,0,0),('b470b1d4138ef4683641919815767957',1,0,1370908655,1370908655,1370908655,'46.246.20.248','Opera/9.80 (Windows NT 6.0; U; ru) Presto/2.10.289 Version/12.02','','faq.php',1,0,0),('ef13b2b0c897821ca938dcb1e406f23b',1,0,1370907971,1370907971,1370907971,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.50','','faq.php',1,0,0),('b26e798efac48ed604e369c1f5cbac20',1,0,1370907283,1370907283,1370907283,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; ru) Presto/2.10.289 Version/12.02','','faq.php',1,0,0),('60c9da84e188f24f0958a0f5282f95bc',1,0,1370906631,1370906631,1370906631,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','','faq.php',1,0,0),('7058cd91f2d09689f70cbcec49128bb2',1,0,1370906531,1370906531,1370906531,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('f9aabb0333dffa16a42923ecf48a878f',1,0,1370905951,1370905951,1370905951,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','','faq.php',1,0,0),('3529d87f6ddf5c7d119bc62bfe164e8b',1,0,1370905239,1370905239,1370905239,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0; rv:16.0) Gecko/20100101 Firefox/16.0','','faq.php',1,0,0),('2800d08615883eff374cd32b0b1e2d0a',1,0,1370904559,1370904559,1370904559,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','faq.php',1,0,0),('f1517c7b140fbfd97bfce9e4cce26004',1,0,1370903900,1370903900,1370903900,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; YB/3.5.1; ru) Presto/2.10.229 Version/11.64','','faq.php',1,0,0),('ab9979c88b23ba51dba097e73cd4bde8',1,0,1370903237,1370903237,1370903237,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','faq.php',1,0,0),('e346371d6e1b9615855a5680df527f9c',1,0,1370902558,1370902558,1370902558,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','','faq.php',1,0,0),('5830ce5cdefa7f18e09ce079170c8858',1,0,1370901879,1370901879,1370901879,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; ru) Presto/2.10.289 Version/12.02','','faq.php',1,0,0),('2a11dae3cc12709324fe1df102895272',1,0,1370901174,1370901174,1370901174,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)','','faq.php',1,0,0),('38b82e7c2b5dd60aaffcf12d27472704',1,0,1370900498,1370900498,1370900498,'46.246.20.248','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','faq.php',1,0,0),('81cc0fe3f249295d2091fc8faf2f3409',1,0,1370900103,1370900103,1370900103,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('33c6b58a5b0e73b730992ea6f41350a9',1,0,1370899836,1370899836,1370899836,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; YB/3.5.1; ru) Presto/2.6.30 Version/10.63','','faq.php',1,0,0),('14c92a27bc91fde6ec28b64f23e82199',1,0,1370899178,1370899178,1370899178,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:11.0) Gecko/20120313 Firefox/11.0','','faq.php',1,0,0),('46c62e91ea412c37e54b358ba77949b5',1,0,1370898628,1370898628,1370898628,'173.199.115.147','Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)','','index.php',1,0,0),('c70d6aeaf0f5d7a9bbe59146d5a0aaf1',1,0,1370898502,1370898502,1370898502,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','','faq.php',1,0,0),('03d1c4b4a0ce28759b504754990a04c8',1,0,1370897836,1370897836,1370897836,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0','','faq.php',1,0,0),('1931f351a40885b4797696ae439791fb',1,0,1370897156,1370897156,1370897156,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/17.0 Firefox/17.0','','faq.php',1,0,0),('cfaedbe6421208ae6fab192e034e8900',1,0,1370896460,1370896460,1370896460,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1','','faq.php',1,0,0),('3c0f1e4df818e981abc6ccf6a706f3c2',1,0,1370895773,1370895773,1370895773,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','faq.php',1,0,0),('953638d47c73353f715890b5794272b4',1,0,1370895116,1370895116,1370895116,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:16.0) Gecko/20100101 Firefox/16.0','','faq.php',1,0,0),('467551ec5f9a8b5987f1fdb96d3900f5',1,0,1370894412,1370894412,1370894412,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4','','faq.php',1,0,0),('bbf7b930295c9346d3a0af0b2d69ec08',1,0,1370893736,1370893736,1370893736,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; MRA 6.0 (build 6001)) Presto/2.12.388 Version/12.11','','faq.php',1,0,0),('5cff90cb17970c9d0b9d947159765132',1,2,1370863018,1370863018,1370863018,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('3770260ac4270000fb414c041bf26c87',1,0,1370865601,1370865601,1370865624,'91.238.134.188','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YI','','feed.php?mode=topics',1,0,0),('6b8ccea2f69ed4ee6cf80e5081394f15',1,0,1370865605,1370865605,1370865608,'91.238.134.188','Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','index.php',1,0,0),('97111543183bb03e272c84aaa4453496',1,0,1370893079,1370893079,1370893079,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/20100101 Firefox/17.0','','faq.php',1,0,0),('c8720b6e0b01816328d543a68fd0f683',1,0,1370892421,1370892421,1370892421,'46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.10','','faq.php',1,0,0),('42685fa4ea7a6fbca6a416d7e49fb404',1,0,1370891770,1370891770,1370891770,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0','','faq.php',1,0,0),('23c57c0aaa0dc0d17a33ddaf4e9f8f81',1,0,1370891118,1370891118,1370891118,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','','faq.php',1,0,0),('8038fd4f9542aa6a060cda6cd11ebb08',1,0,1370890464,1370890464,1370890464,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','','faq.php',1,0,0),('953541f56e0ae6b13a7f430984f98b05',1,0,1370889818,1370889818,1370889818,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','','faq.php',1,0,0),('ae9b646e9e188d140ed8be8da056ece5',1,0,1370889156,1370889156,1370889156,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Iron/5.0.381.0 Chrome/5.0.381 Safari/533.4','','faq.php',1,0,0),('b05fcfb35cd2b56d2c8c3d2cba7aa9cf',1,0,1370888479,1370888479,1370888479,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.96 Safari/537.4','','faq.php',1,0,0),('0b48937ea86857a40cba89758fdc0b1c',1,0,1370887809,1370887809,1370887809,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/17.0 Firefox/17.0','','faq.php',1,0,0),('70353a48e1c2ca272315602e13d3a03b',1,0,1370887127,1370887127,1370887127,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16','','faq.php',1,0,0),('4b06760751837a295d19551715e182d9',1,0,1370886453,1370886453,1370886453,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','','faq.php',1,0,0),('485752fe4c91a325ba5afca0f6921e7e',1,0,1370885939,1370885939,1370885939,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f3d36ba87fe4d86ef5d16953b2b5980f',1,0,1370885821,1370885821,1370885821,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/17.0 Firefox/17.0','','faq.php',1,0,0),('1b0202b53ebd3ca6d72b16ef7fc89f08',1,0,1370885156,1370885156,1370885156,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/17.0 Firefox/17.0','','faq.php',1,0,0),('a912b9ffbf382aceef41bc7568e86e21',1,0,1370884493,1370884493,1370884493,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11','','faq.php',1,0,0),('07b4eaadeb08c7768955da5dec68a49c',1,0,1370883809,1370883809,1370883809,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0','','faq.php',1,0,0),('fa37d1f5ca41355791f0244690660270',1,0,1370857721,1370857721,1370857721,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('47a574dcb7e42850205f3a464f1194f2',1,0,1371891158,1371891158,1371891158,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('cbb0b3149c10be4700d6449cb03d4b9b',1,0,1370854353,1370854353,1370854353,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7c6a47071877e0ec202bbf1ee701df5a',1,0,1370857712,1370857712,1370857712,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=77',1,0,0),('c17752c0af7a2cb71121a34690c97b25',81,0,1370850149,1370850148,1370850158,'217.132.30.60','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0','','memberlist.php?mode=viewprofile&u=81',1,0,0),('7f54dc15d6d2d9aa2e5bf0ace0a1be07',1,0,1370853895,1370853895,1370853895,'217.132.30.60','Opera/9.80 (Windows NT 6.1; WOW64; Edition Yx) Presto/2.12.388 Version/12.11','','memberlist.php?mode=viewprofile&u=81',1,0,0),('2afadbd44cf2e224426658cf34c18b7c',1,0,1370853895,1370853895,1370853895,'217.132.30.60','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','memberlist.php?mode=viewprofile&u=81index.php',1,0,0),('c995835fe4e55625dff9b361c25b11e4',1,0,1369017280,1369017280,1369017280,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('882180c2ec146ab2b30ef30743b01e74',1,0,1368986687,1368986687,1368986687,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9941c2f933fdfd729789ec0abc4ca58e',1,0,1368990868,1368990868,1368990868,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('723ba48a1c28806fa263d8091d2d53b0',1,0,1368977369,1368977369,1368977369,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=63',1,0,0),('103d38e4e9db1d206c45527009e91be7',1,0,1368958461,1368958461,1368958461,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('34beaf2474d5bb49b0c67d6f0527ac38',1,0,1368957091,1368957091,1368957091,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=leaders',1,0,0),('e7560c6e943e5ffe26a0268bb8760e00',1,0,1370595572,1370595572,1370595572,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('de90ed6b2a79638d852ab21cd2fd7fd9',1,0,1369877834,1369877834,1369877834,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('dc591f311b82fd511434f753fd4553e8',1,0,1369748885,1369748885,1369748885,'89.70.160.63','Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.2) Gecko/20100115 Firefox/3.6','','index.php',1,0,0),('56a3b9ea36969be27f8be99ab4bc9291',1,0,1368894549,1368894549,1368894549,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('02b3bcb12a75ee0bfc10eac24ee347b9',1,0,1368894987,1368894987,1368894987,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('49fba789bb6c0a1f1f5dd98dfb27e471',1,0,1368898954,1368898954,1368898954,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php',1,0,0),('af1be316ac9eb547fde3bf161f43b12a',1,0,1368898967,1368898967,1368898967,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('48c8e3465abb8949c84977ab8f7471b3',1,0,1368925562,1368925562,1368925562,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a861cd9cccb2d46c4e5502f65fae5ea9',1,0,1368926395,1368926395,1368926395,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('057b905804c3f27ed3775335567f00aa',1,0,1368934984,1368934984,1368934984,'131.253.24.94','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  .NET CLR 3.5.30707;  InfoPath','','index.php',1,0,0),('be0ab09ed140fc85206dc14307d405e3',8,0,1372338979,1372338979,1372338979,'157.56.93.63','Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)','','index.php',1,0,0),('deb33fcddce0a22a21ae977c70894100',1,0,1368955938,1368955938,1368955938,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('adfe933d957760645f55293e2c0a6269',1,0,1368882581,1368882581,1368882581,'153.19.191.226','Mozilla/5.0 (Windows NT 5.1; rv:20.0) Gecko/20100101 Firefox/20.0','','index.php',1,0,0),('5082892e2b040cd7f6787637e9b32418',1,0,1369020311,1369020311,1369020311,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=58',1,0,0),('96118fb62f880671480b96501bfb089d',1,0,1369020367,1369020367,1369020367,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('dd59a734688a1f69c4c4022fa0a8db96',1,0,1369023193,1369023193,1369023193,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e025291662c320ad5b5846202100688e',1,2,1369028694,1369028694,1369028694,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewtopic.php?f=2&t=1&p=1',1,0,0),('b09ada2c89974a4f8200e2ff24b9cbfc',1,0,1369049514,1369049514,1369049514,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a51773a34b5bcee2727b2032c944604f',1,0,1369053654,1369053654,1369053654,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b0a11e63089e30b104be89ad847fe0f3',1,0,1369082186,1369082186,1369082186,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('215fff016454462e9fae0a0d9b277c66',1,0,1369084834,1369084834,1369084834,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f0bef9c40ffc4965322df3b54c34c3e4',1,0,1369085505,1369085505,1369085505,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('bd30a76484e510f3d72c544119741116',1,0,1369106611,1369106611,1369106611,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=2',1,0,0),('e38b1eccc09b26b69ddcdc4badfa8d07',1,0,1369114084,1369114084,1369114084,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8255bc067b32586581d5df75f04cca3c',1,0,1369114380,1369114380,1369114380,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('18931655877a72eb79b2552af6c73f85',1,2,1369129317,1369129317,1369129422,'81.144.138.34','Wotbox/2.01 (+http://www.wotbox.com/bot/)','','posting.php?mode=post&f=2',1,0,0),('8f1aa639de7816b879c6957d247a49d9',1,0,1369145301,1369145301,1369145301,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('565ac3f39bc4b24893ee83c91665c9d1',1,0,1369147301,1369147301,1369147301,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d6d5edc028de86de51e1bd596d21e818',1,0,1369176909,1369176909,1369176909,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f95e4a76f7f69ccdd4a9b173fb742c7a',1,0,1369178307,1369178307,1369178307,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('43052d786d1aa0cb8134e80262441d22',1,0,1369180890,1369180890,1369180890,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('0d55f9dd58bb143427f664214ddd313b',1,0,1369180907,1369180907,1369180909,'46.119.114.17','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','','index.php',1,0,0),('cf5437bb22b0641f8a6c885f7e887e7e',1,2,1369187091,1369187091,1369187091,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewtopic.php?f=2&p=1',1,0,0),('557eb58c936cba920bf01cec80541308',1,0,1369187099,1369187099,1369187099,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('f737d600be7d1a1b6b582f271ce9b52a',1,0,1369206646,1369206646,1369206646,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('72ff77fe046718debf87d291d05fabfd',1,0,1369210382,1369210382,1369210382,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('098e7582e96f516f6962646e01466ef0',1,0,1369238440,1369238440,1369238440,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('90b7bb369a2cf687e448dd2c26919ab9',1,0,1369239362,1369239362,1369239362,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('6ea1d97821bee07906b6316e0715a626',1,0,1369244765,1369244765,1369244765,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('ebecb69f04c51f6c97aa5ff92f9c9137',1,0,1369269884,1369269884,1369269884,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('56b39c65dc972ce891a18e5aba407eb6',1,0,1369271939,1369271939,1369271939,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e192e9a92d1195404571781138cd81aa',1,0,1369279307,1369279307,1369279307,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewtopic.php?p=1',1,0,0),('4eeff67c0bbf5dcab97756ce0c4b7818',1,0,1369289383,1369289383,1369289387,'46.246.90.219','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','index.php',1,0,0),('cda02274c7ba8db56b8f458f2b33f469',1,2,1369293796,1369293796,1369293796,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2&t=1',1,0,0),('fac09383836f7371a7b24d2d07d6c6a9',1,0,1369301321,1369301321,1369301321,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5a03e5bdb152db4c1d8f404d49dd3e87',1,0,1369304562,1369304562,1369304562,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2a05b93c317bcd53b09a8fe1caadfe18',1,0,1369308122,1369308122,1369308122,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=70',1,0,0),('997856f75403d1671c8d73484141b673',1,2,1369314722,1369314722,1369314727,'192.114.71.13','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0','','posting.php?mode=post&f=2',1,0,0),('470bb50391c51e953506d763f8db1538',1,0,1369317178,1369317178,1369317178,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=unanswered',1,0,0),('44709dbbcb3f2068a4b24b68e9a92cfb',1,0,1369320336,1369320336,1369320362,'94.42.177.36','Mozilla/5.0 (Windows NT 5.1; rv:20.0) Gecko/20100101 Firefox/20.0','','ucp.php?mode=sendpassword',1,0,0),('a0b3bf414d25d939f327227b1a64da85',1,0,1369332895,1369332895,1369332895,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('164d3022af53b915587a70ae62d85187',1,0,1369336813,1369336813,1369336813,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('37790224b33651b7663d20225098da1e',1,0,1369337142,1369337142,1369337142,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('be58bfb68abd4d5099a2a11946549d8e',1,0,1369337789,1369337789,1369337789,'87.205.214.25','Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0; MALNJS)','','index.php',1,0,0),('4889589d9addadd8e7afd63c7f0092af',1,0,1369363731,1369363731,1369363731,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9eeee7a262761a500b104e50e00678a1',1,0,1369369509,1369369509,1369369509,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2084571f394f1c27d440ceb60227bf67',1,2,1369374256,1369374256,1369374256,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('53c9064496f900bdf36568e7de5ea91a',1,0,1369374261,1369374261,1369374261,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=resend_act',1,0,0),('391f7db86d31c6c9b8df03e2e5c2246f',1,0,1369392825,1369392825,1369392825,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5a7ad057f709d8afcd5831196678d61f',1,0,1369394451,1369394451,1369394451,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=privacy',1,0,0),('afc06122f6896fa9f61ad8874a2a9468',1,0,1369399706,1369399706,1369399706,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ba5d974a807e9065a9c7cb07c5db657b',1,0,1369401443,1369401443,1369401446,'46.246.90.219','Opera/9.80 (Windows NT 6.1; WOW64; U; Edition Next; Edition Yx; ru) Presto/2.11.310 Version/12.50','','index.php',1,0,0),('0b8abeb1da659e9af930b55b59392079',1,0,1369424430,1369424430,1369424430,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('93fd8fe7ef52d52a0cfbad7114c4c780',1,0,1369428827,1369428827,1369428827,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('38b9dff3ff5af324b025af3e7f800694',1,0,1369456086,1369456086,1369456086,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a080f18cc0734cb07702bfea5b528a38',1,0,1369461460,1369461460,1369461460,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('0d35586291c5b76ab343fa3cf8eeec5c',1,0,1369487683,1369487683,1369487683,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('510b09c596dec1eda615c61a3c6135c4',1,0,1369494016,1369494016,1369494016,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c397852ff4e47ed82918d67fa968c792',1,2,1369504136,1369504136,1369504136,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=2',1,0,0),('3a60eec08897b14209807a5e27d00be9',1,1,1369510232,1369510232,1369510232,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=1',1,0,0),('3e06da8dc9e9ff6b88741b69b89dd3c4',1,0,1369510238,1369510238,1369510238,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('2b1c70abb15dda79dbe79153fc005d06',1,0,1369518801,1369518801,1369518801,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3c860b054ae2b49ddb53e87cecc0c37c',1,0,1369524105,1369524105,1369524105,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('224baaf4f789c53ff5699ece2afaf180',1,0,1369525681,1369525681,1369525893,'178.210.216.140','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2','','index.php',1,0,0),('0a267aa03553a718f2cd25a5feeec7e1',1,0,1369525946,1369525946,1369525946,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('245c4252e95cce228a75c310f6fdd9a9',1,0,1369547166,1369547166,1369547166,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=delete_cookies',1,0,0),('cb01a12fbcd436fdaf2d093ca26918fd',1,0,1369547166,1369547166,1369547166,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=73',1,0,0),('42e621b6026a9a99698047b795ee63b4',1,0,1369549673,1369549673,1369549673,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8221f6400c04a88d067fe2579db0486f',1,2,1369553003,1369553003,1369553003,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('9f7e54c7f2f3c5394be67590c971392e',1,0,1369558158,1369558158,1369558158,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a1dc25a699c2f56ac2b93773040cf1eb',1,0,1369561389,1369561389,1369561389,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('c10b5377f615d1064a2a5940de0e5144',1,0,1369575572,1369575572,1369575572,'178.43.130.84','Mozilla/5.0 (Windows NT 6.0; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('db5dbe629ea5e1d173d016d0f25f05ac',1,0,1369580783,1369580783,1369580783,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('72328b33106dd57910b61145e8222178',1,0,1369589986,1369589986,1369589986,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3dcaf730bec327dd8801debc5073d3a7',1,0,1369592247,1369592247,1369592247,'89.74.140.244','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31','','faq.php',1,0,0),('4063a0728eb1c8d7584189d0053e3df2',1,0,1369595895,1369595895,1369595895,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('5bfddae374e194ff3bd04ab14858aba2',1,0,1369611528,1369611528,1369611528,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ba6ce4c16675d2d07c122b4d7bb29755',1,2,1369619117,1369619117,1369619117,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewtopic.php?f=2&t=1',1,0,0),('f79d857546fcd98fa80ace85fdbdc1ef',1,0,1369621810,1369621810,1369621810,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9e030492965d98e70d80f2d13ae6537c',1,0,1369641968,1369641968,1369641968,'173.199.117.195','Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)','','index.php',1,0,0),('49803218b02f7423acdec745a882a6b0',1,0,1369642608,1369642608,1369642608,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('56e77dc275c3b0627cb0d07af37f51c4',1,2,1369653838,1369653838,1369653838,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewtopic.php?t=1&f=2',1,0,0),('673fcb8c270889a69e139ef689e95388',1,0,1369654541,1369654541,1369654541,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8d35d96ccb18276096191a218888119a',1,0,1369674202,1369674202,1369674202,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('756bc970b68a17c0ad625df88feba56b',1,0,1369682465,1369682465,1369682465,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('4680f089137fb154c8c7ce6fb7375647',1,0,1369687159,1369687159,1369687159,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e0ed504504cb55a86a9088f52a51154a',1,0,1369689998,1369689998,1369689998,'85.202.47.58','Mozilla/5.0 (X11; Linux i686; rv:18.0) Gecko/20100101 Firefox/18.0','','faq.php',1,0,0),('319e288cb95404fe1da03bf995fa3d74',1,0,1369705397,1369705397,1369705397,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2af15328f3232b8ae9ba5d3b2d047f68',1,0,1369719971,1369719971,1369719971,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a76d5a94c25f36e9341b747349082f9d',1,0,1369719981,1369719981,1369719981,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('675c8eae610608b1579d1398ba895d63',1,0,1369736777,1369736777,1369736777,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('bcab0ca1f2fa026462c25c43057ebb31',1,0,1369752776,1369752776,1369752776,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('68a122a51a6f459f06da61be2ad6ac36',1,0,1369754452,1369754452,1369754452,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('d852f130a7bc8703fe429934df48e809',1,0,1369768598,1369768598,1369768598,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9320080fd4eae6c239c255d17216dd1e',1,0,1369785310,1369785310,1369785310,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ba6f8dfb26058eda3d97b4f74e313b7d',1,0,1369787077,1369787077,1369787225,'178.210.216.140','Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Yx) Presto/2.12.388 Version/12.11','','ucp.php?mode=register',1,0,0),('b51af6a9f8d7af2119b0371d8d56e2d1',1,0,1369791789,1369791789,1369791789,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('4fae80c018e929190554c1d91c5a0b19',1,0,1369800428,1369800428,1369800428,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c6822441870072580ba4465d50601922',1,0,1369817515,1369817515,1369817515,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a2dd7177c2aa2d569d8c08da24d22e66',1,0,1369830539,1369830539,1369830539,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2c883f15cd43832bdfb9beaf2ccf43b7',1,0,1369849430,1369849430,1369849430,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3c76b5bc15908a2abae1d6578a8ac776',1,0,1369855354,1369855354,1369855354,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php',1,0,0),('f9e084d955189f12b4e8eddc8450ea2b',1,0,1369855360,1369855360,1369855360,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('52050cdad39ae2cb3e6e11276e27bb42',1,2,1369855365,1369855365,1369855365,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','posting.php?mode=reply&f=2&t=1',1,0,0),('bb0b7aa4b94ca8611069e598efc2435e',1,0,1369862641,1369862641,1369862641,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('db8342df58bdffb78064d078087d4001',1,0,1369881679,1369881679,1369881679,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('18f2958ca4fd8fbfdc8ac22fe1acf8dd',1,0,1369884123,1369884123,1369884123,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=privacy',1,0,0),('8d7fcc08f1f6e7204678277569ac8ae4',1,0,1369895019,1369895019,1369895019,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3240e861dc0f9d3b34fb46c11518c926',1,0,1369898763,1369898763,1369898763,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','faq.php',1,0,0),('d2a9a0a737d14d7feaaea8a0144a7a7c',1,0,1369906939,1369906939,1369906939,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=leaders',1,0,0),('f0ea1dfb1aac0ebc33492fc52573cc98',1,0,1369907429,1369907429,1369907429,'178.159.166.112','Mozilla/5.0 (Windows NT 6.0; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('03f2399dcb834360bfce413e0d224601',1,0,1369914213,1369914213,1369914213,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('65cff539a5c56a831eccf62e8ea6aacf',1,0,1370108267,1370108267,1370108267,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('1e952cc47091a6e502a495322c0cdb50',1,2,1369927308,1369927308,1369927308,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','posting.php?mode=post&f=2',1,0,0),('1ca351f008ac74e3b691e85edd714fe0',1,0,1369927534,1369927534,1369927534,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3c6449771dbe6113d3a4c46d35fb4dac',1,0,1369935009,1369935009,1369935009,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','index.php',1,0,0),('d514ce69fcf0156d4b9d3da58119ce84',1,2,1369935026,1369935026,1369935026,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','feed.php?f=2',1,0,0),('9682760bf6ec171dda47bdca88dac012',1,2,1369935026,1369935026,1369935026,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','feed.php?f=2',1,0,0),('a8a640e934dd0c4c3b0481062eeca8c2',1,2,1369935026,1369935026,1369935026,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','feed.php?f=2',1,0,0),('3d4b7302563b376c52a811ceebd5af82',1,2,1369935026,1369935026,1369935026,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','feed.php?f=2',1,0,0),('e6cc9f361c726aea3ae12ac20cdfce05',1,2,1369935026,1369935026,1369935026,'46.105.161.27','Mozilla/5.0 (Windows NT 6.0; rv:18.0) Gecko/20100101 Firefox/18.0','','feed.php?f=2',1,0,0),('1393f9051cf1087090e06093b9f5f42b',1,0,1369946363,1369946363,1369946363,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d1fedc70b6de7db85acb92aa1feff5b3',1,0,1369949810,1369949810,1369949810,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=73',1,0,0),('052efaa7395bf81269abc86295f0a855',1,0,1369959708,1369959708,1369959708,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('17d92c823f386198441741331df918d4',1,0,1369978762,1369978762,1369978762,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('dbe66a46e4fc89693068e3344af39b52',1,0,1369984872,1369984872,1369984872,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=70',1,0,0),('25ab9eb51267405f28f8b549f4ba58c5',1,0,1369993032,1369993032,1369993032,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7610aa01e75cfdf1755203f6a7b1ee75',1,0,1369993543,1369993543,1369993543,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=63',1,0,0),('4ee17ff421660a5fb0ed7b7e7e20ced3',1,0,1369993559,1369993559,1369993559,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('f1f263400d1caa8263ef351e4c8ab762',1,0,1370001009,1370001009,1370001015,'136.169.255.221','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','index.php',1,0,0),('2c56b97ecc2bc6f556d64e2db31fd37f',1,0,1370007419,1370007419,1370007419,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('bc7946dc6b6d31e30495927e3699a1cc',1,0,1370012735,1370012735,1370012735,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('81bcab30649f8e24cf3c65a3a9d9218b',1,0,1370013920,1370013920,1370013920,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=resend_act',1,0,0),('ada224bdf9620c9b03dc23380cf52fe6',1,0,1370025552,1370025552,1370025552,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c533fe27146ffa7ec2edaf539c8af306',1,0,1370044545,1370044545,1370044545,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('fefa82cf3a26270f853d06ea5e294de8',1,0,1370053228,1370053228,1370053230,'91.238.134.14','Opera/9.80 (Windows NT 5.1) Presto/2.12.388 Version/12.10','','index.php',1,0,0),('513bcd0f58c765883bcc99938dbb44dd',1,0,1370056994,1370056994,1370056994,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('74e1b689b4fe6862a86a7047b240a375',1,0,1370057505,1370057505,1370057505,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=unanswered',1,0,0),('07ea763c8ad9024d87f74b8af7910727',1,0,1370076138,1370076138,1370076138,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('eab6303967c68171ed949cfbabeb0a58',1,0,1370079608,1370079608,1370079608,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php',1,0,0),('a0790b2f9142db507c45a3836e0c206c',1,1,1370079613,1370079613,1370079613,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=1',1,0,0),('6955d8e9f6803c7827cc08e8183cb830',1,0,1370081618,1370081618,1370081628,'91.238.134.14','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.00','','index.php',1,0,0),('7b4b5bb12811900f80bb707ed2cb23c7',1,0,1370089297,1370089297,1370089297,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8c3ef005e1dd50471013afbb2c0a3bb6',1,0,1370095182,1370095182,1370095182,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=delete_cookies',1,0,0),('02d67e2635119efc6a3eafb6535e7838',1,0,1370095186,1370095186,1370095186,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','faq.php',1,0,0),('5509de453285ea225dde822d44b7d1bc',1,0,1370098120,1370098120,1370098126,'46.165.208.108','Opera/9.80 (Windows NT 6.1; WOW64; U; ru) Presto/2.10.289 Version/12.00','','index.php',1,0,0),('7c88304a0128164c95b7ad91f13b2fd1',1,0,1370121601,1370121601,1370121601,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c3f80f9ae2af758e8fe1575c7fe86fa9',1,0,1370122357,1370122357,1370122360,'178.210.216.140','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)','','ucp.php?mode=register',1,0,0),('4a1777624fb04136693e0b35423d829c',1,0,1371304340,1371304340,1371304340,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('784307da492472fa12d5850627ef1b9f',1,2,1371305805,1371305805,1371305808,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.22) Gecko/20110902 Firefox/3.6.22','','posting.php?mode=post&f=2',1,0,0),('3834208ac4b9469da0cc233e3985933b',1,0,1371308050,1371308050,1371308050,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('452599118f65d58f90203124429bb787',1,2,1371309361,1371309361,1371309364,'46.246.20.248','Opera/9.80 (Windows NT 6.2; U; en) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('0fd022d9dd5f4cc319e9666a58b56332',1,2,1371312744,1371312744,1371312747,'46.246.20.248','Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('3ecaff05443dd1dfa3b75633cff14354',1,2,1371316200,1371316200,1371316203,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; YB/3.5.1; ru) Presto/2.6.30 Version/10.63','','posting.php?mode=post&f=2',1,0,0),('f02692f26ed7e3ee9802b871ede8def7',1,2,1371319502,1371319502,1371319505,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','','posting.php?mode=post&f=2',1,0,0),('5e134dde110fedb559930f24709eb052',1,0,1371320933,1371320933,1371320933,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('381f02a65696750c0f35ad49fd1b55ed',1,2,1371322716,1371322716,1371322719,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0','','posting.php?mode=post&f=2',1,0,0),('0d7b520263042e64625679680bc62adb',1,0,1371324286,1371324286,1371324286,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=resend_act',1,0,0),('a7de98e1b4916f8ed86920d2d2e95238',1,2,1371325995,1371325995,1371325998,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; MRA 6.0 (build 5976)) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('d3a19227808921a2dbed5759c98e232c',1,2,1371329284,1371329284,1371329287,'46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25','','posting.php?mode=post&f=2',1,0,0),('4cee69ff439e8f6ad2938a00c4f31f2a',1,2,1371332371,1371332371,1371332374,'46.246.20.248','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 6001)) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('fe7ec2eadf4c301cc17608b8204aaeb9',1,2,1371335508,1371335508,1371335511,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2','','posting.php?mode=post&f=2',1,0,0),('20e54e260c63fd89b2fbb0c74bbc2cb7',1,2,1371338584,1371338584,1371338592,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','','posting.php?mode=post&f=2',1,0,0),('2c88f64d78f5601f13221e4dca7a71bd',1,0,1371339694,1371339694,1371339694,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5df869c9bff07cb72d6c64a559944d18',1,2,1371341726,1371341726,1371341729,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','','posting.php?mode=post&f=2',1,0,0),('482cf9093f8feced7cb26f09c6ca796b',1,2,1371349984,1371349984,1371349994,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2','','posting.php?mode=post&f=2',1,0,0),('4640f32c9e2aef03c046292f6e25a1aa',1,0,1371353127,1371353127,1371353127,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d3c443d3c9ec5ddeb2af376a103b3db4',1,2,1371353352,1371353352,1371353363,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','','posting.php?mode=post&f=2',1,0,0),('b8c65a288edd015d5a379c101fd83175',1,2,1371356659,1371356659,1371356674,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.19) Gecko/2010031422 Firefox/3.0.19 YB/3.5.1 (.NET CLR 3.5.30729)','','posting.php?mode=post&f=2',1,0,0),('52f54c5d93d6443a6beb9dabaa399f28',1,2,1371359825,1371359825,1371359834,'46.246.20.248','Opera/9.80 (Windows NT 5.1; MRA 6.0 (build 5998)) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('90d944c6bc6c09de68d0f7ae651dd49d',1,2,1371363150,1371363150,1371363159,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.152 Safari/535.19 CoolNovo/2.0.3.55','','posting.php?mode=post&f=2',1,0,0),('497b7440364b998ad68fe367ccfbe193',1,2,1371366550,1371366550,1371366558,'46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('529a7dd1521cfee7b590d86b90eb8ce7',1,0,1371366748,1371366748,1371366748,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=2',1,0,0),('e19f910851b2274b05b6d79980255012',1,0,1371366752,1371366752,1371366752,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=unanswered',1,0,0),('a8abe6a0eb1660249fe6968d3b0c50ff',1,2,1371369877,1371369877,1371369881,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('290f1b680c497c3febd1ba7ac0f97036',1,0,1371372427,1371372427,1371372427,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('dc8418b1839665f0fe69b8cd33224dc5',1,2,1371373534,1371373534,1371373537,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('fff705d1fbac1bb1b79bc2867beb747d',1,0,1371375717,1371375717,1371375717,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php',1,0,0),('0f453e71424388165d34d99c451d803e',1,1,1371375721,1371375721,1371375721,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=1',1,0,0),('3d96271aecbe7371a8a088852cd51cba',1,2,1371382231,1371382231,1371382234,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('b3507b62171aec32c8d348f1374f78b1',1,0,1371385500,1371385500,1371385500,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c2cb69048b596b964b8adb5e23d0feb9',1,2,1371385680,1371385680,1371385683,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4','','posting.php?mode=post&f=2',1,0,0),('0261084074e65a9e867913e66e212366',1,2,1371389147,1371389147,1371389150,'46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('2ab59a4591048855a54ef33859a148e7',1,0,1371390510,1371390510,1371390510,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','faq.php',1,0,0),('12642297f094f4ab43deed84e90e30d1',1,0,1371390521,1371390521,1371390521,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=delete_cookies',1,0,0),('e7050d6ab80bc7331193300044b33e6b',1,2,1371392712,1371392712,1371392715,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; GTB7.4; MRA 6.0 (build 6005); User-agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows','','posting.php?mode=post&f=2',1,0,0),('b3931360020f47c1ed9cefc6ade3da3e',1,0,1371396134,1371396134,1371396134,'46.204.32.4','Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('96ebf0ed7a5bc2e1b6641fa4b1bab630',1,2,1371396416,1371396416,1371396419,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','','posting.php?mode=post&f=2',1,0,0),('1d701630e2371b641546fcfc8d870898',1,2,1371400117,1371400117,1371400121,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1.0 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('d039ca13522cb7de1765bb75532340d2',1,2,1371403635,1371403635,1371403638,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:16.0) Gecko/20121026 Firefox/16.0 SeaMonkey/2.13.2','','posting.php?mode=post&f=2',1,0,0),('99afafde9be078feb311de2d1af3008a',1,0,1371404541,1371404541,1371404541,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('4fc9d1351a9f4bcd9e46257d2a812e24',1,2,1371407190,1371407190,1371407194,'46.246.20.248','Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.10.229 Version/11.64','','posting.php?mode=post&f=2',1,0,0),('ec72763d7e44ab254aa613fabe286eb0',1,2,1371410686,1371410686,1371410691,'46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('5059eeba5da9623a7d3319ac035180ec',1,2,1371414198,1371414198,1371414202,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)','','posting.php?mode=post&f=2',1,0,0),('cca4b39b9258f5713a5b8cd03914aadc',1,2,1371417606,1371417606,1371417609,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)','','posting.php?mode=post&f=2',1,0,0),('849a7c7db5911dc69739ab531c3fd434',1,0,1371417861,1371417861,1371417861,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a280a10840450186273557228ceead51',1,2,1371420989,1371420989,1371420992,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2','','posting.php?mode=post&f=2',1,0,0),('cb1fb0fcab03c003dd6fd581879107f3',1,2,1371424325,1371424325,1371424328,'46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('8b4152026defa8bda2c44bbfde47a167',1,2,1371427761,1371427761,1371427764,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:12.0) Gecko/20100101 Firefox/12.0','','posting.php?mode=post&f=2',1,0,0),('ddd3cbcae684ebdcfc882682f9bead4c',1,2,1371431428,1371431428,1371431436,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322)','','posting.php?mode=post&f=2',1,0,0),('917917b16ebabcc618cb77c440d0ccb8',1,0,1371433105,1371433105,1371433105,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php',1,0,0),('f667f89999098ca57696c1f1df05b2f6',1,2,1371435170,1371435170,1371435184,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('a1882914ddde9bb867416ed9324eba6c',1,0,1371437350,1371437350,1371437350,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8a2707ba7a0596b3c4d8636647bc98e0',1,2,1371438827,1371438827,1371438837,'46.246.20.248','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('660c194105e9ee455919aaaebabc1980',1,2,1371439359,1371439359,1371439359,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','posting.php?mode=post&f=2',1,0,0),('b7ba27d5c662c3a4d9d40d5b09857533',1,2,1371442235,1371442235,1371442243,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.00','','posting.php?mode=post&f=2',1,0,0),('f159ccb2ecf11a0e07a8651a592ef5fa',1,2,1371445659,1371445659,1371445668,'46.246.20.248','Opera/9.80 (Windows NT 6.1) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('9741b51b8a7138b4eec517f2a616b281',1,0,1371448085,1371448085,1371448085,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('b56ba6de371405a61557906fe3e2752b',1,2,1371449091,1371449091,1371449096,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1','','posting.php?mode=post&f=2',1,0,0),('eee53d32363930c4eb36fd4597bcee0d',1,0,1371450204,1371450204,1371450204,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('25003f7e717de2b8041ef4e984b72c26',1,2,1371456034,1371456034,1371456037,'46.246.20.248','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)','','posting.php?mode=post&f=2',1,0,0),('4297da062d910432477529f8780f7be6',1,2,1371459602,1371459602,1371459608,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('1beeae6b86d9879fb84195dc2d9aca4f',1,2,1371463609,1371463609,1371463615,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1','','posting.php?mode=post&f=2',1,0,0),('6ab672730ec593a994d41311c5857d7f',1,0,1371464601,1371464601,1371464626,'62.233.246.100','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.','','index.php',1,0,0),('d730167f32261130beba574ae0f599a5',1,2,1371467199,1371467199,1371467202,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; MRA 6.0 (build 5993); MRA 8.0 (build 5784); InfoPath.2)','','posting.php?mode=post&f=2',1,0,0),('36a84ff78822b91d98ec782fa53f4cd3',1,2,1371467671,1371467671,1371467671,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=2',1,0,0),('20a47fe0ea06633664f46a8699c6566e',1,0,1371470077,1371470077,1371470077,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('77967bbf22fc10462a1b241671136b61',1,2,1371470833,1371470833,1371470836,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('9e29cf88fc925a2a019988756a1d329d',1,2,1371474473,1371474473,1371474476,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('ac13b72f784e592e4bb575e4d8f690f6',1,2,1371478173,1371478173,1371478179,'46.246.20.248','Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25','','posting.php?mode=post&f=2',1,0,0),('8a9f5c37d5be8f04e2b05e59c04d0e46',1,0,1371481862,1371481862,1371481862,'193.107.213.164','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0','','index.php',1,0,0),('08e33e1c67a607a654e6e291fbabb80b',1,2,1371481885,1371481885,1371481888,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0','','posting.php?mode=post&f=2',1,0,0),('f48160b82e9dac4f0dd902dfa83b32d4',1,0,1371483011,1371483011,1371483011,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8e6bb65de9825a376bd7f3c14f60f069',1,2,1371485372,1371485372,1371485375,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('c20813707b3f23b628e393366d5f6fd8',1,2,1371488901,1371488901,1371488905,'46.246.20.248','Opera/9.80 (Windows NT 6.1; WOW64; U; MRA 8.0 (build 5880); ru) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('e5f72ebb58b0a9639cedc003bfaee7a8',1,0,1371490833,1371490833,1371490833,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('1b0ba4bc6d9dbfa6a20752043dabe5d1',1,0,1371490843,1371490843,1371490843,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php',1,0,0),('5cf5175c3b19db44d0e2c481885f8a16',1,2,1371492507,1371492507,1371492512,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','','posting.php?mode=post&f=2',1,0,0),('acea9abab7b7fa3fdeae9d4cf036b672',1,2,1371496482,1371496482,1371496489,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('55688cc0029ca52713fb62b52c006538',1,2,1371501056,1371501056,1371501063,'46.246.20.248','Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28 sputnik 2.5.2.8','','posting.php?mode=post&f=2',1,0,0),('8017e9148b57da8601802b33d60acf1d',1,0,1371501804,1371501804,1371501804,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('9d30d3546d5d9ca26bfba9c7435c111b',1,2,1371505101,1371505101,1371505105,'46.246.20.248','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('506ac75b173594a3fdc5446f5571c449',1,2,1371693596,1371693596,1371693612,'5.9.124.103','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2','','posting.php?mode=post&f=2',1,0,0),('a1322fad6590a481d07f5b33144263e3',1,2,1371594660,1371594660,1371594673,'46.246.20.248','Opera/9.80 (Windows NT 6.2; U; en) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('bf3e1bc075370f0d585c5cb3750b1a35',1,0,1371597250,1371597250,1371597250,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=63',1,0,0),('76e89266369107b69f8c4c8ec8505ed9',1,2,1371598753,1371598753,1371598761,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','','posting.php?mode=post&f=2',1,0,0),('7b38efbc9296e6892623323510e2b723',1,0,1371599436,1371599436,1371599436,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('adb796fb687c701be6224d458337e900',1,2,1371602809,1371602809,1371602813,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11','','posting.php?mode=post&f=2',1,0,0),('0a53a2eb5decfc915632d60ccc809c0d',1,2,1371606911,1371606911,1371606917,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('7014e0555843362f18c2211519a010c7',1,2,1371611180,1371611180,1371611194,'46.246.20.248','Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.01','','posting.php?mode=post&f=2',1,0,0),('24fb1b556b49b0cef66c600f3f60efb2',1,0,1371612316,1371612316,1371612316,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('cd42ee363f61ed0ceadf7d171d0462bc',1,2,1371615575,1371615575,1371615581,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0; rv:16.0) Gecko/20100101 Firefox/16.0','','posting.php?mode=post&f=2',1,0,0),('35404fd341de11116d6bfbfa0b6e6e6e',1,2,1371619461,1371619461,1371619466,'46.246.20.248','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; MRA 6.0 (build 5993); MRA 8.0 (build 5784); InfoPath.2)','','posting.php?mode=post&f=2',1,0,0),('e61b0b2880485b37ba73384fc3894b56',1,2,1371623460,1371623460,1371623464,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YI','','posting.php?mode=post&f=2',1,0,0),('b7734ada65d591b1c9c6e727a4d3921a',1,2,1371627386,1371627386,1371627389,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11','','posting.php?mode=post&f=2',1,0,0),('c6523f0dbee3a84e989d0610b8ffcfa1',1,2,1371631425,1371631425,1371631428,'46.246.20.248','Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('f257e8b706177d871ef818064516199c',1,0,1371632088,1371632088,1371632088,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('a1aa6ef1c5c9ceaccd7c46da6e989e33',1,2,1371635316,1371635316,1371635320,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','posting.php?mode=post&f=2',1,0,0),('d56627fbabbb860312b1a917f63fd7ef',1,2,1371639277,1371639277,1371639288,'46.246.20.248','Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0','','posting.php?mode=post&f=2',1,0,0),('b0a1b9e4dda5d11d572522ad19e9ee52',1,0,1371640791,1371640791,1371640791,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('fd051239853dc7c977e9a465e3b58e7f',1,2,1371643286,1371643286,1371643291,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4','','posting.php?mode=post&f=2',1,0,0),('cea060435341f0ab85882f0cd3c4e95d',1,0,1371644775,1371644775,1371644775,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('84d053680c2a147af5b0cd3de905a29c',1,2,1371647445,1371647445,1371647450,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('607441431420f605807abbe170fa1d57',1,2,1371651971,1371651971,1371651974,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1','','posting.php?mode=post&f=2',1,0,0),('92d09fbd7ecadf9149621b331de6be88',1,2,1371656290,1371656290,1371656293,'46.246.20.248','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0','','posting.php?mode=post&f=2',1,0,0),('c7b1819e4578e8573b3eb1092ba49aad',1,2,1371660584,1371660584,1371660593,'46.246.20.248','Mozilla/5.0 (Windows NT 5.2; rv:17.0) Gecko/20100101 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('13e3f9988a5a71af5d13e6078d1fa9e0',1,0,1371664705,1371664705,1371664705,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('58b46abfe7880f716dcaaab6797d7f1a',1,2,1371665011,1371665011,1371665017,'46.246.20.248','Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Yx) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('b25425e459f86bdc0d292bafa595e249',1,2,1371669309,1371669309,1371669311,'5.9.124.103','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0','','posting.php?mode=post&f=2',1,0,0),('20e31a82380d63ffbe9a8dcef6cd1da7',1,2,1371672397,1371672397,1371672403,'5.9.124.103','Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0','','posting.php?mode=post&f=2',1,0,0),('ba731545db1bae4b8cd7aa2677189d71',1,2,1371675613,1371675613,1371675617,'5.9.124.103','Mozilla/5.0 (Windows NT 6.0; rv:17.0) Gecko/17.0 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('c913fe9741f4930228f0b8ec52e0b4b3',1,0,1371677237,1371677237,1371677237,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ec51fa646c6c4de891f6e12592ecc02e',1,0,1371677977,1371677977,1371677977,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('262102c9a8cd92569c9a96cf0e42ed66',1,2,1371679194,1371679194,1371679202,'5.9.124.103','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) YaBrowser/1.1.1084.5409 Chrome/19.1.1084.5409 Safari/536.5','','posting.php?mode=post&f=2',1,0,0),('2defdefd56ae211fcec5d79dce3623cd',1,2,1371682704,1371682704,1371682712,'5.9.124.103','Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('8dafd9b8cf1f4e496d4652cd5057d708',1,2,1371686096,1371686096,1371686100,'5.9.124.103','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)','','posting.php?mode=post&f=2',1,0,0),('d976312461d0e080c8d35be0b49fae1c',1,2,1371689433,1371689433,1371689438,'5.9.124.103','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','posting.php?mode=post&f=2',1,0,0),('2aca7e4e3837c2749cc9920885fd46d7',1,0,1371696055,1371696055,1371696055,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ebb9046432e8407defd9a3bd32fff2eb',1,2,1371698438,1371698438,1371698475,'5.9.124.103','Opera/9.80 (Windows NT 6.1; Win64; x64) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('108344b65283f718130d2927047af31a',1,2,1371703101,1371703101,1371703110,'5.9.124.103','Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Yx) Presto/2.12.388 Version/12.11','','posting.php?mode=post&f=2',1,0,0),('cb80b2fdf37b1cfa3e50d0e1ca2bcfe8',1,2,1371707000,1371707000,1371707010,'5.9.124.103','Opera/9.80 (Windows NT 6.1; MRA 6.0 (build 6001)) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('1ba4b57d14e43f58e4a09199f2664c51',1,0,1371709513,1371709513,1371709513,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('1b48acccca315c5b668dbced05b28d29',1,2,1371710622,1371710622,1371710636,'5.9.124.103','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5 Nichrome/self/19','','posting.php?mode=post&f=2',1,0,0),('2f037b87bcdea4a70dc337ad104440d5',1,2,1371713933,1371713933,1371713934,'5.9.124.103','Opera/9.80 (Windows NT 6.2; WOW64; MRA 8.0 (build 5784)) Presto/2.12.388 Version/12.10','','posting.php?mode=post&f=2',1,0,0),('5fe41fe060c5176b54a54814a369be76',1,2,1371717324,1371717324,1371717332,'188.126.69.253','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YI','','posting.php?mode=post&f=2',1,0,0),('86d421be88b6d84ff3ef714b58ab28e8',1,2,1371722739,1371722739,1371722744,'188.126.69.253','Opera/9.80 (Windows NT 6.1; U; Edition Yx; ru) Presto/2.10.289 Version/12.02','','posting.php?mode=post&f=2',1,0,0),('f0e158c54287b7b02281fce7c6a8a789',1,0,1371761686,1371761686,1371761686,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('6904ce8e14e68ca8f1b8c12d2597b515',1,2,1371727172,1371727172,1371727179,'188.126.69.253','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.92 Safari/537.4','','posting.php?mode=post&f=2',1,0,0),('5b2606c39cd174ffb17376c9ff43a52d',1,0,1371729363,1371729363,1371729363,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5733e90a94d3747b96e83702a72aff4d',1,2,1371732068,1371732068,1371732073,'188.126.69.253','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0','','posting.php?mode=post&f=2',1,0,0),('e452d48afef37ba3848465fcf9dab587',1,2,1371737038,1371737038,1371737051,'188.126.69.253','Mozilla/5.0 (Windows NT 5.1; rv:17.0) Gecko/20100101 Firefox/17.0','','posting.php?mode=post&f=2',1,0,0),('7e5a1b64e7f95a4f2bbf3e8c4980fb47',1,0,1371742435,1371742435,1371742435,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('de8af5497a9810a449d6790ce9dcbca2',1,0,1371774404,1371774404,1371774404,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('56bcbfdf14cf5364103f01c759037477',1,0,1371778367,1371778367,1371778367,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('a55b2ff6ed7b20d63800f28768a18050',1,0,1371793754,1371793754,1371793754,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('86951da95c14b2c43d52ca18b04da069',1,0,1371799290,1371799290,1371799290,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('96c33c92b224d062b524d84af79ad059',1,0,1371805897,1371805897,1371805897,'194.242.62.91','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; InfoPath.2; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)','','index.php',1,0,0),('8841a56c928fa34e86f4a3f569a1a9d7',1,0,1371807548,1371807548,1371807548,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('3d146b4686af08f2f22cd4d236cd0e29',1,0,1371827190,1371827190,1371827190,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2fa68801160ff551461e4654a7f0aa34',1,0,1371836318,1371836318,1371836318,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('66783cd528183a164ebf6abcd93704ef',1,0,1371839342,1371839342,1371839342,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e4019e81f4a707920dc384877aa4f137',1,0,1371858925,1371858925,1371858925,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('5ef0dcbbf6e607e6ea0f362c82942d1e',1,0,1372305508,1372305508,1372305508,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=terms',1,0,0),('6e5cd7a32d9a40ee1ece0a156b958db7',1,0,1371871609,1371871609,1371871609,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ae427672729e807cf1262ca68ebda9c3',1,0,1372214689,1372214689,1372214689,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('894a3f0c65eef2ce6d09608d60357b22',1,0,1371879698,1371879698,1371879698,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=81',1,0,0),('737ef89ba323807cf42c29d46444544c',1,0,1371904099,1371904099,1371904099,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('90e6be2072d5a41eb7d725682e310f36',1,0,1371908812,1371908812,1371908812,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=unanswered',1,0,0),('fd6e9b6e5529f88c324620dd3776fdc1',1,0,1371923542,1371923542,1371923542,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c9afc995132a371042004d4519c99fc9',1,0,1371936344,1371936344,1371936344,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('70a6adf24489952d60536c8b278dcf00',1,0,1371956097,1371956097,1371956097,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f1ed76fa0dc2dc93e365900704a41a4a',1,0,1371957785,1371957785,1371957785,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('4a14eff3fe0d00c02212303db7592b24',1,2,1371966017,1371966017,1371966017,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('f4de4c5a44b5a30e724ea3a339926e43',1,0,1371966107,1371966107,1371966107,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=resend_act',1,0,0),('0e6cbc08f48888c7e8d9fcf89167127b',1,0,1371968719,1371968719,1371968719,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('041dd3f42fcf11c464f4826b62950515',1,0,1371972160,1371972160,1371972160,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=78',1,0,0),('bb4caf8b607363e07c736492be3cf0ec',1,0,1371988698,1371988698,1371988698,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b6a273b76f12fccd8ab01e11970f1a7a',1,0,1372001695,1372001695,1372001695,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e4dcee8c945f6d51e74f0e6b4e76550e',1,0,1372015151,1372015151,1372015151,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=privacy',1,0,0),('53cfdd15ebd2413df96b863d480d9e47',1,0,1372020918,1372020918,1372020918,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7655a1bc71957041d0ccc6286c186451',1,0,1372033206,1372033206,1372033206,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d046bef428bc80246b5f0f0905cb7621',1,0,1372052532,1372052532,1372052532,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c8f580ba8bd450979b9907a839d84c97',1,0,1372066109,1372066109,1372066109,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('982d79a16cde8a21ca73aeb1d11b8636',1,0,1372085988,1372085988,1372085988,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('1a14fcfcbadbf5ca2989274c51be2b39',1,0,1372095121,1372095121,1372095121,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=81',1,0,0),('e7c104b99d04ecc2aadb1b058519ec23',1,2,1372095143,1372095143,1372095143,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=2',1,0,0),('b83f12a32ce432a21789d5bfa8fbdcf7',1,0,1372098684,1372098684,1372098684,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('36ad3b221ef56649b0c268f0dc732893',1,0,1372101483,1372101483,1372101490,'69.84.207.246','Mozilla/4.0 (compatible; MSIE 7.0;Windows NT 5.1;.NET CLR 1.1.4322;.NET CLR 2.0.50727;.NET CLR 3.0.04506.30) Lightspeedsystems','','feed.php',1,0,0),('447e943a4117bcf7a17cc8983b85c9dc',1,0,1372118160,1372118160,1372118160,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('38331da14f310accf3a95a075a3a995e',1,1,1372130501,1372130501,1372130501,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','viewforum.php?f=1',1,0,0),('fb7470a1a95522c6d265f848137935e5',1,0,1372130510,1372130510,1372130510,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('b484fd821abf1837afb1256f67ea19fb',1,0,1372130826,1372130826,1372130826,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('81a5d0670710d924559727770ad49e5b',1,0,1372138880,1372138880,1372138880,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=73',1,0,0),('c761a90a54c0e5989fd8e5dd8991a274',1,0,1372138897,1372138897,1372138897,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=delete_cookies',1,0,0),('cc0cc118f059a6464878953e18472253',1,0,1372144981,1372144981,1372144981,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=register',1,0,0),('503760731eb4ee2306c345bc433a507c',1,0,1372150644,1372150644,1372150644,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2097f8d33274916aa453676a29cc9608',1,0,1372153605,1372153605,1372153605,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=viewprofile&u=77',1,0,0),('ed1db33c10022c820dd4135e1d9d27fb',1,0,1372153608,1372153608,1372153608,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=sendpassword',1,0,0),('e9e858412335fcd403fc7e72a1bf5711',1,0,1372163272,1372163272,1372163272,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('ac2e870f1cd36e4c65ceaacd8aba7e99',1,2,1372173712,1372173712,1372173712,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?f=2',1,0,0),('86a289304387ffee38958e9158809cba',1,0,1372183644,1372183644,1372183644,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8f3aceb857c7224c4f46a40e0cdba846',1,0,1372183670,1372183670,1372183670,'65.55.215.245','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.648)','','faq.php',1,0,0),('83e67db88d745ae462f7794da21d5dbf',1,0,1372196153,1372196153,1372196153,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f3d6d631e5f27736441698da239001a0',1,0,1372217093,1372217093,1372217093,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','index.php',1,0,0),('f659b4b3a990d6b7980b80e37bc932ca',1,0,1372228050,1372228050,1372228050,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('6883d420254050ee88af519339f5ab57',1,0,1372247464,1372247464,1372247464,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e052e83eaf7425666a947ed029020ec8',1,0,1372251659,1372251659,1372251824,'178.210.216.140','Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1','','index.php',1,0,0),('d79352c9cd8c6c253fdc8c2133622c05',1,0,1372260589,1372260589,1372260589,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7e0e78bcf33888b543be17341423367a',1,0,1372280311,1372280311,1372280311,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('7364abecd735f15e29b3e856f0f2c0cd',1,0,1372292782,1372292782,1372292782,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('86a1933ef9dd1b56594701b33e0b1cc3',1,0,1372311486,1372311486,1372311486,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','ucp.php?mode=login',1,0,0),('6cedd945492a3b56018cf84b9f5e0aca',1,0,1372312367,1372312367,1372312367,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('d9516fdc6031812cda8784ab4aa8894a',1,0,1372325516,1372325516,1372325516,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('68d0957dd0b25ff2acbbdab66706edf9',1,0,1372331322,1372331322,1372331322,'65.55.213.33','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.648)','','index.php',1,0,0),('307c6d04fe0c597b8a35ce70d7a11c6c',1,0,1372345483,1372345483,1372345483,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('22cad763f1ceca9c2bf69e2bbfa1d2c6',1,0,1372359209,1372359209,1372359209,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('61f7855f6aed51ba143a68ba7a5da43b',1,0,1372375008,1372375008,1372375008,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','search.php?search_id=active_topics',1,0,0),('5089e0eacf3eea125c741e641d4df968',1,0,1372454557,1372454557,1372454557,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('53fbbcd8d54cd42f2706c09098c944a7',1,0,1372380503,1372380503,1372380503,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('b5682acd97ef9b31f348e78f7095d04b',1,0,1372383522,1372383522,1372383522,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php?mode=topics',1,0,0),('26e452c54cc738dbaa2afb4b9643a90e',1,0,1372389923,1372389923,1372389923,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('17cc48aeddde45e6ab7dec7d828967c4',1,0,1372410431,1372410431,1372410431,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('c2de2677a7dcd4ccbae39a8199d3cef8',1,0,1372422628,1372422628,1372422628,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('e52f99b16cb429633e4316560ac7d413',1,0,1372441873,1372441873,1372441873,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('f44ae906774696751c10f6eda2d0b9b0',1,0,1372469787,1372469787,1372469787,'208.115.113.90','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=5',1,0,0),('10edf361b327971ae8fe273dbbb46653',1,0,1372473708,1372473708,1372473708,'213.186.119.141','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('2c9cb2471b50a6795f7e543adb779845',1,0,1372476087,1372476087,1372476087,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','feed.php',1,0,0),('f69d0acaac935fa4dda516c469652766',1,0,1372476123,1372476123,1372476123,'208.115.111.74','Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)','','memberlist.php?mode=group&g=4',1,0,0),('74098e79a24c0c5935d0cd9d6cf18b9f',1,0,1372482805,1372482805,1372482814,'91.232.96.6','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)','','index.php',1,0,0),('96aa28b3370c194cbc1e6fa7af084740',1,0,1372485266,1372485266,1372485271,'91.232.96.19','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','','index.php',1,0,0),('461dff357d4afe9945b652c21e17f5e5',1,0,1372485474,1372485474,1372485475,'91.232.96.34','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.1634 Safari/535.19 YE','','index.php',1,0,0),('e58eb3693113a031b0429c8248ae880c',1,0,1372486364,1372486364,1372486366,'91.232.96.27','Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11','','index.php',1,0,0),('0f17d41544d2bcd17f55979a6c29f62b',1,0,1372487087,1372487087,1372487087,'213.186.119.135','AhrefsBot.Feeds v0.1; http://ahrefs.com/','','feed.php?mode=topics',1,0,0),('8911bfdc16dcc492d12ad5a091d908cc',1,0,1372491594,1372491594,1372491594,'173.199.116.211','Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)','','index.php',1,0,0);
/*!40000 ALTER TABLE `phpbb_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sessions_keys`
--

DROP TABLE IF EXISTS `phpbb_sessions_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sessions_keys` (
  `key_id` char(32) collate utf8_bin NOT NULL default '',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `last_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `last_login` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`key_id`,`user_id`),
  KEY `last_login` (`last_login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sessions_keys`
--

LOCK TABLES `phpbb_sessions_keys` WRITE;
/*!40000 ALTER TABLE `phpbb_sessions_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_sessions_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sitelist`
--

DROP TABLE IF EXISTS `phpbb_sitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sitelist` (
  `site_id` mediumint(8) unsigned NOT NULL auto_increment,
  `site_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `site_hostname` varchar(255) collate utf8_bin NOT NULL default '',
  `ip_exclude` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sitelist`
--

LOCK TABLES `phpbb_sitelist` WRITE;
/*!40000 ALTER TABLE `phpbb_sitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_sitelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_smilies`
--

DROP TABLE IF EXISTS `phpbb_smilies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_smilies` (
  `smiley_id` mediumint(8) unsigned NOT NULL auto_increment,
  `code` varchar(50) collate utf8_bin NOT NULL default '',
  `emotion` varchar(50) collate utf8_bin NOT NULL default '',
  `smiley_url` varchar(50) collate utf8_bin NOT NULL default '',
  `smiley_width` smallint(4) unsigned NOT NULL default '0',
  `smiley_height` smallint(4) unsigned NOT NULL default '0',
  `smiley_order` mediumint(8) unsigned NOT NULL default '0',
  `display_on_posting` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`smiley_id`),
  KEY `display_on_post` (`display_on_posting`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_smilies`
--

LOCK TABLES `phpbb_smilies` WRITE;
/*!40000 ALTER TABLE `phpbb_smilies` DISABLE KEYS */;
INSERT INTO `phpbb_smilies` VALUES (1,':D','Bardzo szczęśliwy','icon_e_biggrin.gif',15,17,1,1),(2,':-D','Bardzo szczęśliwy','icon_e_biggrin.gif',15,17,2,1),(3,':grin:','Bardzo szczęśliwy','icon_e_biggrin.gif',15,17,3,1),(4,':)','Szczęśliwy','icon_e_smile.gif',15,17,4,1),(5,':-)','Szczęśliwy','icon_e_smile.gif',15,17,5,1),(6,':smile:','Szczęśliwy','icon_e_smile.gif',15,17,6,1),(7,';)','Puszcza oko','icon_e_wink.gif',15,17,7,1),(8,';-)','Puszcza oko','icon_e_wink.gif',15,17,8,1),(9,':wink:','Puszcza oko','icon_e_wink.gif',15,17,9,1),(10,':(','Smutny','icon_e_sad.gif',15,17,10,1),(11,':-(','Smutny','icon_e_sad.gif',15,17,11,1),(12,':sad:','Smutny','icon_e_sad.gif',15,17,12,1),(13,':o','Zdziwiony','icon_e_surprised.gif',15,17,13,1),(14,':-o','Zdziwiony','icon_e_surprised.gif',15,17,14,1),(15,':eek:','Zdziwiony','icon_e_surprised.gif',15,17,15,1),(16,':shock:','Zaszokowany','icon_eek.gif',15,17,16,1),(17,':?','Boi się','icon_e_confused.gif',15,17,17,1),(18,':-?','Boi się','icon_e_confused.gif',15,17,18,1),(19,':???:','Boi się','icon_e_confused.gif',15,17,19,1),(20,'8-)','Cool','icon_cool.gif',15,17,20,1),(21,':cool:','Cool','icon_cool.gif',15,17,21,1),(22,':lol:','Śmieje się','icon_lol.gif',15,17,22,1),(23,':x','Wściekły','icon_mad.gif',15,17,23,1),(24,':-x','Wściekły','icon_mad.gif',15,17,24,1),(25,':mad:','Wściekły','icon_mad.gif',15,17,25,1),(26,':P','Pokazuje język','icon_razz.gif',15,17,26,1),(27,':-P','Pokazuje język','icon_razz.gif',15,17,27,1),(28,':razz:','Pokazuje język','icon_razz.gif',15,17,28,1),(29,':oops:','Zawstydzony','icon_redface.gif',15,17,29,1),(30,':cry:','Płacze','icon_cry.gif',15,17,30,1),(31,':evil:','Zły','icon_evil.gif',15,17,31,1),(32,':twisted:','Szalony','icon_twisted.gif',15,17,32,1),(33,':roll:','Udaje, że to nie on','icon_rolleyes.gif',15,17,33,1),(34,':!:','Wykrzyknik','icon_exclaim.gif',15,17,34,1),(35,':?:','Znak zapytania','icon_question.gif',15,17,35,1),(36,':idea:','Pomysł','icon_idea.gif',15,17,36,1),(37,':arrow:','Strzałka','icon_arrow.gif',15,17,37,1),(38,':|','Średnio zadowolony','icon_neutral.gif',15,17,38,1),(39,':-|','Średnio zadowolony','icon_neutral.gif',15,17,39,1),(40,':mrgreen:','Pan Zielony','icon_mrgreen.gif',15,17,40,1),(41,':geek:','Geek','icon_e_geek.gif',17,17,41,1),(42,':ugeek:','Uber Geek','icon_e_ugeek.gif',17,18,42,1);
/*!40000 ALTER TABLE `phpbb_smilies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles`
--

DROP TABLE IF EXISTS `phpbb_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles` (
  `style_id` mediumint(8) unsigned NOT NULL auto_increment,
  `style_name` varchar(255) collate utf8_bin NOT NULL default '',
  `style_copyright` varchar(255) collate utf8_bin NOT NULL default '',
  `style_active` tinyint(1) unsigned NOT NULL default '1',
  `template_id` mediumint(8) unsigned NOT NULL default '0',
  `theme_id` mediumint(8) unsigned NOT NULL default '0',
  `imageset_id` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`style_id`),
  UNIQUE KEY `style_name` (`style_name`),
  KEY `template_id` (`template_id`),
  KEY `theme_id` (`theme_id`),
  KEY `imageset_id` (`imageset_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles`
--

LOCK TABLES `phpbb_styles` WRITE;
/*!40000 ALTER TABLE `phpbb_styles` DISABLE KEYS */;
INSERT INTO `phpbb_styles` VALUES (1,'prosilver','&copy; phpBB Group',1,1,1,1);
/*!40000 ALTER TABLE `phpbb_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_imageset`
--

DROP TABLE IF EXISTS `phpbb_styles_imageset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_imageset` (
  `imageset_id` mediumint(8) unsigned NOT NULL auto_increment,
  `imageset_name` varchar(255) collate utf8_bin NOT NULL default '',
  `imageset_copyright` varchar(255) collate utf8_bin NOT NULL default '',
  `imageset_path` varchar(100) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`imageset_id`),
  UNIQUE KEY `imgset_nm` (`imageset_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_imageset`
--

LOCK TABLES `phpbb_styles_imageset` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_imageset` DISABLE KEYS */;
INSERT INTO `phpbb_styles_imageset` VALUES (1,'prosilver','&copy; phpBB Group','prosilver');
/*!40000 ALTER TABLE `phpbb_styles_imageset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_imageset_data`
--

DROP TABLE IF EXISTS `phpbb_styles_imageset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_imageset_data` (
  `image_id` mediumint(8) unsigned NOT NULL auto_increment,
  `image_name` varchar(200) collate utf8_bin NOT NULL default '',
  `image_filename` varchar(200) collate utf8_bin NOT NULL default '',
  `image_lang` varchar(30) collate utf8_bin NOT NULL default '',
  `image_height` smallint(4) unsigned NOT NULL default '0',
  `image_width` smallint(4) unsigned NOT NULL default '0',
  `imageset_id` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`image_id`),
  KEY `i_d` (`imageset_id`)
) ENGINE=MyISAM AUTO_INCREMENT=353 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_imageset_data`
--

LOCK TABLES `phpbb_styles_imageset_data` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_imageset_data` DISABLE KEYS */;
INSERT INTO `phpbb_styles_imageset_data` VALUES (352,'button_topic_reply','button_topic_reply.gif','pl',25,104,1),(350,'button_topic_locked','button_topic_locked.gif','pl',25,101,1),(351,'button_topic_new','button_topic_new.gif','pl',25,152,1),(349,'button_pm_reply','button_pm_reply.gif','pl',25,104,1),(348,'button_pm_new','button_pm_new.gif','pl',25,95,1),(346,'icon_user_online','icon_user_online.gif','pl',58,58,1),(347,'button_pm_forward','button_pm_forward.gif','pl',25,125,1),(343,'icon_contact_pm','icon_contact_pm.gif','pl',20,28,1),(344,'icon_post_edit','icon_post_edit.gif','pl',17,56,1),(345,'icon_post_quote','icon_post_quote.gif','pl',17,58,1),(342,'button_topic_reply','button_topic_reply.gif','en',25,96,1),(341,'button_topic_new','button_topic_new.gif','en',25,96,1),(338,'button_pm_new','button_pm_new.gif','en',25,84,1),(339,'button_pm_reply','button_pm_reply.gif','en',25,96,1),(340,'button_topic_locked','button_topic_locked.gif','en',25,88,1),(335,'icon_post_quote','icon_post_quote.gif','en',20,54,1),(336,'icon_user_online','icon_user_online.gif','en',58,58,1),(337,'button_pm_forward','button_pm_forward.gif','en',25,96,1),(334,'icon_post_edit','icon_post_edit.gif','en',20,42,1),(333,'icon_contact_pm','icon_contact_pm.gif','en',20,28,1),(332,'icon_user_warn','icon_user_warn.gif','',20,20,1),(329,'icon_topic_newest','icon_topic_newest.gif','',9,11,1),(331,'icon_topic_unapproved','icon_topic_unapproved.gif','',14,16,1),(330,'icon_topic_reported','icon_topic_reported.gif','',14,16,1),(326,'icon_post_target_unread','icon_post_target_unread.gif','',9,11,1),(328,'icon_topic_latest','icon_topic_latest.gif','',9,11,1),(327,'icon_topic_attach','icon_topic_attach.gif','',10,7,1),(325,'icon_post_target','icon_post_target.gif','',9,11,1),(322,'icon_post_delete','icon_post_delete.gif','',20,20,1),(324,'icon_post_report','icon_post_report.gif','',20,20,1),(323,'icon_post_info','icon_post_info.gif','',20,20,1),(321,'icon_contact_yahoo','icon_contact_yahoo.gif','',20,20,1),(320,'icon_contact_www','icon_contact_www.gif','',20,20,1),(318,'icon_contact_jabber','icon_contact_jabber.gif','',20,20,1),(319,'icon_contact_msnm','icon_contact_msnm.gif','',20,20,1),(316,'icon_contact_email','icon_contact_email.gif','',20,20,1),(317,'icon_contact_icq','icon_contact_icq.gif','',20,20,1),(313,'pm_unread','topic_unread.gif','',27,27,1),(314,'icon_back_top','icon_back_top.gif','',11,11,1),(315,'icon_contact_aim','icon_contact_aim.gif','',20,20,1),(311,'subforum_unread','subforum_unread.gif','',9,11,1),(312,'pm_read','topic_read.gif','',27,27,1),(309,'global_unread_locked_mine','announce_unread_locked_mine.gif','',27,27,1),(310,'subforum_read','subforum_read.gif','',9,11,1),(306,'global_unread','announce_unread.gif','',27,27,1),(307,'global_unread_mine','announce_unread_mine.gif','',27,27,1),(308,'global_unread_locked','announce_unread_locked.gif','',27,27,1),(305,'global_read_locked_mine','announce_read_locked_mine.gif','',27,27,1),(304,'global_read_locked','announce_read_locked.gif','',27,27,1),(302,'global_read','announce_read.gif','',27,27,1),(303,'global_read_mine','announce_read_mine.gif','',27,27,1),(300,'announce_unread_locked','announce_unread_locked.gif','',27,27,1),(301,'announce_unread_locked_mine','announce_unread_locked_mine.gif','',27,27,1),(298,'announce_unread','announce_unread.gif','',27,27,1),(299,'announce_unread_mine','announce_unread_mine.gif','',27,27,1),(295,'announce_read_mine','announce_read_mine.gif','',27,27,1),(296,'announce_read_locked','announce_read_locked.gif','',27,27,1),(297,'announce_read_locked_mine','announce_read_locked_mine.gif','',27,27,1),(293,'sticky_unread_locked_mine','sticky_unread_locked_mine.gif','',27,27,1),(294,'announce_read','announce_read.gif','',27,27,1),(292,'sticky_unread_locked','sticky_unread_locked.gif','',27,27,1),(291,'sticky_unread_mine','sticky_unread_mine.gif','',27,27,1),(289,'sticky_read_locked_mine','sticky_read_locked_mine.gif','',27,27,1),(290,'sticky_unread','sticky_unread.gif','',27,27,1),(287,'sticky_read_mine','sticky_read_mine.gif','',27,27,1),(288,'sticky_read_locked','sticky_read_locked.gif','',27,27,1),(286,'sticky_read','sticky_read.gif','',27,27,1),(285,'topic_unread_locked_mine','topic_unread_locked_mine.gif','',27,27,1),(284,'topic_unread_locked','topic_unread_locked.gif','',27,27,1),(283,'topic_unread_hot_mine','topic_unread_hot_mine.gif','',27,27,1),(282,'topic_unread_hot','topic_unread_hot.gif','',27,27,1),(281,'topic_unread_mine','topic_unread_mine.gif','',27,27,1),(280,'topic_unread','topic_unread.gif','',27,27,1),(279,'topic_read_locked_mine','topic_read_locked_mine.gif','',27,27,1),(278,'topic_read_locked','topic_read_locked.gif','',27,27,1),(276,'topic_read_hot','topic_read_hot.gif','',27,27,1),(277,'topic_read_hot_mine','topic_read_hot_mine.gif','',27,27,1),(275,'topic_read_mine','topic_read_mine.gif','',27,27,1),(273,'topic_moved','topic_moved.gif','',27,27,1),(274,'topic_read','topic_read.gif','',27,27,1),(272,'forum_unread_subforum','forum_unread_subforum.gif','',27,27,1),(271,'forum_unread_locked','forum_unread_locked.gif','',27,27,1),(270,'forum_unread','forum_unread.gif','',27,27,1),(269,'forum_read_subforum','forum_read_subforum.gif','',27,27,1),(268,'forum_read_locked','forum_read_locked.gif','',27,27,1),(267,'forum_read','forum_read.gif','',27,27,1),(266,'forum_link','forum_link.gif','',27,27,1),(265,'site_logo','site_logo.png','',37,182,1);
/*!40000 ALTER TABLE `phpbb_styles_imageset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_template`
--

DROP TABLE IF EXISTS `phpbb_styles_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_template` (
  `template_id` mediumint(8) unsigned NOT NULL auto_increment,
  `template_name` varchar(255) collate utf8_bin NOT NULL default '',
  `template_copyright` varchar(255) collate utf8_bin NOT NULL default '',
  `template_path` varchar(100) collate utf8_bin NOT NULL default '',
  `bbcode_bitfield` varchar(255) collate utf8_bin NOT NULL default 'kNg=',
  `template_storedb` tinyint(1) unsigned NOT NULL default '0',
  `template_inherits_id` int(4) unsigned NOT NULL default '0',
  `template_inherit_path` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`template_id`),
  UNIQUE KEY `tmplte_nm` (`template_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_template`
--

LOCK TABLES `phpbb_styles_template` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_template` DISABLE KEYS */;
INSERT INTO `phpbb_styles_template` VALUES (1,'prosilver','&copy; phpBB Group','prosilver','lNg=',0,0,'');
/*!40000 ALTER TABLE `phpbb_styles_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_template_data`
--

DROP TABLE IF EXISTS `phpbb_styles_template_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_template_data` (
  `template_id` mediumint(8) unsigned NOT NULL default '0',
  `template_filename` varchar(100) collate utf8_bin NOT NULL default '',
  `template_included` text collate utf8_bin NOT NULL,
  `template_mtime` int(11) unsigned NOT NULL default '0',
  `template_data` mediumtext collate utf8_bin NOT NULL,
  KEY `tid` (`template_id`),
  KEY `tfn` (`template_filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_template_data`
--

LOCK TABLES `phpbb_styles_template_data` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_template_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_styles_template_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_theme`
--

DROP TABLE IF EXISTS `phpbb_styles_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_theme` (
  `theme_id` mediumint(8) unsigned NOT NULL auto_increment,
  `theme_name` varchar(255) collate utf8_bin NOT NULL default '',
  `theme_copyright` varchar(255) collate utf8_bin NOT NULL default '',
  `theme_path` varchar(100) collate utf8_bin NOT NULL default '',
  `theme_storedb` tinyint(1) unsigned NOT NULL default '0',
  `theme_mtime` int(11) unsigned NOT NULL default '0',
  `theme_data` mediumtext collate utf8_bin NOT NULL,
  PRIMARY KEY  (`theme_id`),
  UNIQUE KEY `theme_name` (`theme_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_theme`
--

LOCK TABLES `phpbb_styles_theme` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_theme` DISABLE KEYS */;
INSERT INTO `phpbb_styles_theme` VALUES (1,'prosilver','&copy; phpBB Group','prosilver',1,1345476754,'/*  phpBB3 Style Sheet\n    --------------------------------------------------------------\n	Style name:			prosilver (the default phpBB 3.0.x style)\n	Based on style:		\n	Original author:	Tom Beddard ( http://www.subblue.com/ )\n	Modified by:		phpBB Group ( http://www.phpbb.com/ )\n    --------------------------------------------------------------\n*/\n\n/* General Markup Styles\n---------------------------------------- */\n\n* {\n	/* Reset browsers default margin, padding and font sizes */\n	margin: 0;\n	padding: 0;\n}\n\nhtml {\n	font-size: 100%;\n	/* Always show a scrollbar for short pages - stops the jump when the scrollbar appears. non-IE browsers */\n	height: 101%;\n}\n\nbody {\n	/* Text-Sizing with ems: http://www.clagnut.com/blog/348/ */\n	font-family: Verdana, Helvetica, Arial, sans-serif;\n	color: #828282;\n	background-color: #FFFFFF;\n	/*font-size: 62.5%;			 This sets the default font size to be equivalent to 10px */\n	font-size: 10px;\n	margin: 0;\n	padding: 12px 0;\n}\n\nh1 {\n	/* Forum name */\n	font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\n	margin-right: 200px;\n	color: #FFFFFF;\n	margin-top: 10px;\n	font-weight: bold;\n	font-size: 2em;\n}\n\nh2 {\n	/* Forum header titles */\n	font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\n	font-weight: normal;\n	color: #3f3f3f;\n	font-size: 2em;\n	margin: 0.8em 0 0.2em 0;\n}\n\nh2.solo {\n	margin-bottom: 1em;\n}\n\nh3 {\n	/* Sub-headers (also used as post headers, but defined later) */\n	font-family: Arial, Helvetica, sans-serif;\n	font-weight: bold;\n	text-transform: uppercase;\n	border-bottom: 1px solid #CCCCCC;\n	margin-bottom: 3px;\n	padding-bottom: 2px;\n	font-size: 1.05em;\n	color: #989898;\n	margin-top: 20px;\n}\n\nh4 {\n	/* Forum and topic list titles */\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, Sans-serif;\n	font-size: 1.3em;\n}\n\np {\n	line-height: 1.3em;\n	font-size: 1.1em;\n	margin-bottom: 1.5em;\n}\n\nimg {\n	border-width: 0;\n}\n\nhr {\n	/* Also see tweaks.css */\n	border: 0 none #FFFFFF;\n	border-top: 1px solid #CCCCCC;\n	height: 1px;\n	margin: 5px 0;\n	display: block;\n	clear: both;\n}\n\nhr.dashed {\n	border-top: 1px dashed #CCCCCC;\n	margin: 10px 0;\n}\n\nhr.divider {\n	display: none;\n}\n\np.right {\n	text-align: right;\n}\n\n/* Main blocks\n---------------------------------------- */\n#wrap {\n	padding: 0 20px;\n	min-width: 650px;\n}\n\n#simple-wrap {\n	padding: 6px 10px;\n}\n\n#page-body {\n	margin: 4px 0;\n	clear: both;\n}\n\n#page-footer {\n	clear: both;\n}\n\n#page-footer h3 {\n	margin-top: 20px;\n}\n\n#logo {\n	float: left;\n	width: auto;\n	padding: 10px 0 0 10px;\n}\n\na#logo:hover {\n	text-decoration: none;\n}\n\n/* Search box\n--------------------------------------------- */\n#search-box {\n	color: #FFFFFF;\n	position: relative;\n	margin-top: 30px;\n	margin-right: 5px;\n	display: block;\n	float: right;\n	text-align: right;\n	white-space: nowrap; /* For Opera */\n}\n\n#search-box #keywords {\n	width: 95px;\n	background-color: #FFF;\n}\n\n#search-box input {\n	border: 1px solid #b0b0b0;\n}\n\n/* .button1 style defined later, just a few tweaks for the search button version */\n#search-box input.button1 {\n	padding: 1px 5px;\n}\n\n#search-box li {\n	text-align: right;\n	margin-top: 4px;\n}\n\n#search-box img {\n	vertical-align: middle;\n	margin-right: 3px;\n}\n\n/* Site description and logo */\n#site-description {\n	float: left;\n	width: 70%;\n}\n\n#site-description h1 {\n	margin-right: 0;\n}\n\n/* Round cornered boxes and backgrounds\n---------------------------------------- */\n.headerbar {\n	background: #ebebeb none repeat-x 0 0;\n	color: #FFFFFF;\n	margin-bottom: 4px;\n	padding: 0 5px;\n}\n\n.navbar {\n	background-color: #ebebeb;\n	padding: 0 10px;\n}\n\n.forabg {\n	background: #b1b1b1 none repeat-x 0 0;\n	margin-bottom: 4px;\n	padding: 0 5px;\n	clear: both;\n}\n\n.forumbg {\n	background: #ebebeb none repeat-x 0 0;\n	margin-bottom: 4px;\n	padding: 0 5px;\n	clear: both;\n}\n\n.panel {\n	margin-bottom: 4px;\n	padding: 0 10px;\n	background-color: #f3f3f3;\n	color: #3f3f3f;\n}\n\n.post {\n	padding: 0 10px;\n	margin-bottom: 4px;\n	background-repeat: no-repeat;\n	background-position: 100% 0;\n}\n\n.post:target .content {\n	color: #000000;\n}\n\n.post:target h3 a {\n	color: #000000;\n}\n\n.bg1	{ background-color: #f7f7f7;}\n.bg2	{ background-color: #f2f2f2; }\n.bg3	{ background-color: #ebebeb; }\n\n.rowbg {\n	margin: 5px 5px 2px 5px;\n}\n\n.ucprowbg {\n	background-color: #e2e2e2;\n}\n\n.fieldsbg {\n	/*border: 1px #DBDEE2 solid;*/\n	background-color: #eaeaea;\n}\n\nspan.corners-top, span.corners-bottom, span.corners-top span, span.corners-bottom span {\n	font-size: 1px;\n	line-height: 1px;\n	display: block;\n	height: 5px;\n	background-repeat: no-repeat;\n}\n\nspan.corners-top {\n	background-image: none;\n	background-position: 0 0;\n	margin: 0 -5px;\n}\n\nspan.corners-top span {\n	background-image: none;\n	background-position: 100% 0;\n}\n\nspan.corners-bottom {\n	background-image: none;\n	background-position: 0 100%;\n	margin: 0 -5px;\n	clear: both;\n}\n\nspan.corners-bottom span {\n	background-image: none;\n	background-position: 100% 100%;\n}\n\n.headbg span.corners-bottom {\n	margin-bottom: -1px;\n}\n\n.post span.corners-top, .post span.corners-bottom, .panel span.corners-top, .panel span.corners-bottom, .navbar span.corners-top, .navbar span.corners-bottom {\n	margin: 0 -10px;\n}\n\n.rules span.corners-top {\n	margin: 0 -10px 5px -10px;\n}\n\n.rules span.corners-bottom {\n	margin: 5px -10px 0 -10px;\n}\n\n/* Horizontal lists\n----------------------------------------*/\nul.linklist {\n	display: block;\n	margin: 0;\n}\n\nul.linklist li {\n	display: block;\n	list-style-type: none;\n	float: left;\n	width: auto;\n	margin-right: 5px;\n	font-size: 1.1em;\n	line-height: 2.2em;\n}\n\nul.linklist li.rightside, p.rightside {\n	float: right;\n	margin-right: 0;\n	margin-left: 5px;\n	text-align: right;\n}\n\nul.navlinks {\n	padding-bottom: 1px;\n	margin-bottom: 1px;\n	border-bottom: 1px solid #FFFFFF;\n	font-weight: bold;\n}\n\nul.leftside {\n	float: left;\n	margin-left: 0;\n	margin-right: 5px;\n	text-align: left;\n}\n\nul.rightside {\n	float: right;\n	margin-left: 5px;\n	margin-right: -5px;\n	text-align: right;\n}\n\n/* Table styles\n----------------------------------------*/\ntable.table1 {\n	/* See tweaks.css */\n}\n\n#ucp-main table.table1 {\n	padding: 2px;\n}\n\ntable.table1 thead th {\n	font-weight: normal;\n	text-transform: uppercase;\n	color: #FFFFFF;\n	line-height: 1.3em;\n	font-size: 1em;\n	padding: 0 0 4px 3px;\n}\n\ntable.table1 thead th span {\n	padding-left: 7px;\n}\n\ntable.table1 tbody tr {\n	border: 1px solid #cfcfcf;\n}\n\ntable.table1 tbody tr:hover, table.table1 tbody tr.hover {\n	background-color: #f6f6f6;\n	color: #000;\n}\n\ntable.table1 td {\n	color: #6a6a6a;\n	font-size: 1.1em;\n}\n\ntable.table1 tbody td {\n	padding: 5px;\n	border-top: 1px solid #FAFAFA;\n}\n\ntable.table1 tbody th {\n	padding: 5px;\n	border-bottom: 1px solid #000000;\n	text-align: left;\n	color: #333333;\n	background-color: #FFFFFF;\n}\n\n/* Specific column styles */\ntable.table1 .name		{ text-align: left; }\ntable.table1 .posts		{ text-align: center !important; width: 7%; }\ntable.table1 .joined	{ text-align: left; width: 15%; }\ntable.table1 .active	{ text-align: left; width: 15%; }\ntable.table1 .mark		{ text-align: center; width: 7%; }\ntable.table1 .info		{ text-align: left; width: 30%; }\ntable.table1 .info div	{ width: 100%; white-space: normal; overflow: hidden; }\ntable.table1 .autocol	{ line-height: 2em; white-space: nowrap; }\ntable.table1 thead .autocol { padding-left: 1em; }\n\ntable.table1 span.rank-img {\n	float: right;\n	width: auto;\n}\n\ntable.info td {\n	padding: 3px;\n}\n\ntable.info tbody th {\n	padding: 3px;\n	text-align: right;\n	vertical-align: top;\n	color: #000000;\n	font-weight: normal;\n}\n\n.forumbg table.table1 {\n	margin: 0;\n}\n\n.forumbg-table > .inner {\n	margin: 0 -1px;\n}\n\n.forumbg-table > .inner > span.corners-top {\n	margin: 0 -4px -1px -4px;\n}\n\n.forumbg-table > .inner > span.corners-bottom {\n	margin: -1px -4px 0 -4px;\n}\n\n/* Misc layout styles\n---------------------------------------- */\n/* column[1-2] styles are containers for two column layouts \n   Also see tweaks.css */\n.column1 {\n	float: left;\n	clear: left;\n	width: 49%;\n}\n\n.column2 {\n	float: right;\n	clear: right;\n	width: 49%;\n}\n\n/* General classes for placing floating blocks */\n.left-box {\n	float: left;\n	width: auto;\n	text-align: left;\n}\n\n.right-box {\n	float: right;\n	width: auto;\n	text-align: right;\n}\n\ndl.details {\n	/*font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;*/\n	font-size: 1.1em;\n}\n\ndl.details dt {\n	float: left;\n	clear: left;\n	width: 30%;\n	text-align: right;\n	color: #000000;\n	display: block;\n}\n\ndl.details dd {\n	margin-left: 0;\n	padding-left: 5px;\n	margin-bottom: 5px;\n	color: #828282;\n	float: left;\n	width: 65%;\n}\n\n/* Pagination\n---------------------------------------- */\n.pagination {\n	height: 1%; /* IE tweak (holly hack) */\n	width: auto;\n	text-align: right;\n	margin-top: 5px;\n	float: right;\n}\n\n.pagination span.page-sep {\n	display: none;\n}\n\nli.pagination {\n	margin-top: 0;\n}\n\n.pagination strong, .pagination b {\n	font-weight: normal;\n}\n\n.pagination span strong {\n	padding: 0 2px;\n	margin: 0 2px;\n	font-weight: normal;\n	color: #FFFFFF;\n	background-color: #bfbfbf;\n	border: 1px solid #bfbfbf;\n	font-size: 0.9em;\n}\n\n.pagination span a, .pagination span a:link, .pagination span a:visited, .pagination span a:active {\n	font-weight: normal;\n	text-decoration: none;\n	color: #747474;\n	margin: 0 2px;\n	padding: 0 2px;\n	background-color: #eeeeee;\n	border: 1px solid #bababa;\n	font-size: 0.9em;\n	line-height: 1.5em;\n}\n\n.pagination span a:hover {\n	border-color: #d2d2d2;\n	background-color: #d2d2d2;\n	color: #FFF;\n	text-decoration: none;\n}\n\n.pagination img {\n	vertical-align: middle;\n}\n\n/* Pagination in viewforum for multipage topics */\n.row .pagination {\n	display: block;\n	float: right;\n	width: auto;\n	margin-top: 0;\n	padding: 1px 0 1px 15px;\n	font-size: 0.9em;\n	background: none 0 50% no-repeat;\n}\n\n.row .pagination span a, li.pagination span a {\n	background-color: #FFFFFF;\n}\n\n.row .pagination span a:hover, li.pagination span a:hover {\n	background-color: #d2d2d2;\n}\n\n/* Miscellaneous styles\n---------------------------------------- */\n#forum-permissions {\n	float: right;\n	width: auto;\n	padding-left: 5px;\n	margin-left: 5px;\n	margin-top: 10px;\n	text-align: right;\n}\n\n.copyright {\n	padding: 5px;\n	text-align: center;\n	color: #555555;\n}\n\n.small {\n	font-size: 0.9em !important;\n}\n\n.titlespace {\n	margin-bottom: 15px;\n}\n\n.headerspace {\n	margin-top: 20px;\n}\n\n.error {\n	color: #bcbcbc;\n	font-weight: bold;\n	font-size: 1em;\n}\n\n.reported {\n	background-color: #f7f7f7;\n}\n\nli.reported:hover {\n	background-color: #ececec;\n}\n\ndiv.rules {\n	background-color: #ececec;\n	color: #bcbcbc;\n	padding: 0 10px;\n	margin: 10px 0;\n	font-size: 1.1em;\n}\n\ndiv.rules ul, div.rules ol {\n	margin-left: 20px;\n}\n\np.rules {\n	background-color: #ececec;\n	background-image: none;\n	padding: 5px;\n}\n\np.rules img {\n	vertical-align: middle;\n	padding-top: 5px;\n}\n\np.rules a {\n	vertical-align: middle;\n	clear: both;\n}\n\n#top {\n	position: absolute;\n	top: -20px;\n}\n\n.clear {\n	display: block;\n	clear: both;\n	font-size: 1px;\n	line-height: 1px;\n	background: transparent;\n}\n/* Link Styles\n---------------------------------------- */\n\n/* Links adjustment to correctly display an order of rtl/ltr mixed content */\na {\n	direction: ltr;\n	unicode-bidi: embed;\n}\n\na:link	{ color: #898989; text-decoration: none; }\na:visited	{ color: #898989; text-decoration: none; }\na:hover	{ color: #d3d3d3; text-decoration: underline; }\na:active	{ color: #d2d2d2; text-decoration: none; }\n\n/* Coloured usernames */\n.username-coloured {\n	font-weight: bold;\n	display: inline !important;\n	padding: 0 !important;\n}\n\n/* Links on gradient backgrounds */\n#search-box a:link, .navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {\n	color: #FFFFFF;\n	text-decoration: none;\n}\n\n#search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {\n	color: #FFFFFF;\n	text-decoration: none;\n}\n\n#search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {\n	color: #ffffff;\n	text-decoration: underline;\n}\n\n#search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {\n	color: #ffffff;\n	text-decoration: none;\n}\n\n/* Links for forum/topic lists */\na.forumtitle {\n	font-family: \"Trebuchet MS\", Helvetica, Arial, Sans-serif;\n	font-size: 1.2em;\n	font-weight: bold;\n	color: #898989;\n	text-decoration: none;\n}\n\n/* a.forumtitle:visited { color: #898989; } */\n\na.forumtitle:hover {\n	color: #bcbcbc;\n	text-decoration: underline;\n}\n\na.forumtitle:active {\n	color: #898989;\n}\n\na.topictitle {\n	font-family: \"Trebuchet MS\", Helvetica, Arial, Sans-serif;\n	font-size: 1.2em;\n	font-weight: bold;\n	color: #898989;\n	text-decoration: none;\n}\n\n/* a.topictitle:visited { color: #d2d2d2; } */\n\na.topictitle:hover {\n	color: #bcbcbc;\n	text-decoration: underline;\n}\n\na.topictitle:active {\n	color: #898989;\n}\n\n/* Post body links */\n.postlink {\n	text-decoration: none;\n	color: #d2d2d2;\n	border-bottom: 1px solid #d2d2d2;\n	padding-bottom: 0;\n}\n\n/* .postlink:visited { color: #bdbdbd; } */\n\n.postlink:active {\n	color: #d2d2d2;\n}\n\n.postlink:hover {\n	background-color: #f6f6f6;\n	text-decoration: none;\n	color: #404040;\n}\n\n.signature a, .signature a:visited, .signature a:hover, .signature a:active {\n	border: none;\n	text-decoration: underline;\n	background-color: transparent;\n}\n\n/* Profile links */\n.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {\n	font-weight: bold;\n	color: #898989;\n	text-decoration: none;\n}\n\n.postprofile a:hover, .postprofile dt.author a:hover {\n	text-decoration: underline;\n	color: #d3d3d3;\n}\n\n/* CSS spec requires a:link, a:visited, a:hover and a:active rules to be specified in this order. */\n/* See http://www.phpbb.com/bugs/phpbb3/59685 */\n.postprofile a:active {\n	font-weight: bold;\n	color: #898989;\n	text-decoration: none;\n}\n\n\n/* Profile searchresults */	\n.search .postprofile a {\n	color: #898989;\n	text-decoration: none; \n	font-weight: normal;\n}\n\n.search .postprofile a:hover {\n	color: #d3d3d3;\n	text-decoration: underline; \n}\n\n/* Back to top of page */\n.back2top {\n	clear: both;\n	height: 11px;\n	text-align: right;\n}\n\na.top {\n	background: none no-repeat top left;\n	text-decoration: none;\n	width: {IMG_ICON_BACK_TOP_WIDTH}px;\n	height: {IMG_ICON_BACK_TOP_HEIGHT}px;\n	display: block;\n	float: right;\n	overflow: hidden;\n	letter-spacing: 1000px;\n	text-indent: 11px;\n}\n\na.top2 {\n	background: none no-repeat 0 50%;\n	text-decoration: none;\n	padding-left: 15px;\n}\n\n/* Arrow links  */\na.up		{ background: none no-repeat left center; }\na.down		{ background: none no-repeat right center; }\na.left		{ background: none no-repeat 3px 60%; }\na.right		{ background: none no-repeat 95% 60%; }\n\na.up, a.up:link, a.up:active, a.up:visited {\n	padding-left: 10px;\n	text-decoration: none;\n	border-bottom-width: 0;\n}\n\na.up:hover {\n	background-position: left top;\n	background-color: transparent;\n}\n\na.down, a.down:link, a.down:active, a.down:visited {\n	padding-right: 10px;\n}\n\na.down:hover {\n	background-position: right bottom;\n	text-decoration: none;\n}\n\na.left, a.left:active, a.left:visited {\n	padding-left: 12px;\n}\n\na.left:hover {\n	color: #d2d2d2;\n	text-decoration: none;\n	background-position: 0 60%;\n}\n\na.right, a.right:active, a.right:visited {\n	padding-right: 12px;\n}\n\na.right:hover {\n	color: #d2d2d2;\n	text-decoration: none;\n	background-position: 100% 60%;\n}\n\n/* invisible skip link, used for accessibility  */\n.skiplink {\n	position: absolute;\n	left: -999px;\n	width: 990px;\n}\n\n/* Feed icon in forumlist_body.html */\na.feed-icon-forum {\n	float: right;\n	margin: 3px;\n}\n/* Content Styles\n---------------------------------------- */\n\nul.topiclist {\n	display: block;\n	list-style-type: none;\n	margin: 0;\n}\n\nul.forums {\n	background: #f9f9f9 none repeat-x 0 0;\n}\n\nul.topiclist li {\n	display: block;\n	list-style-type: none;\n	color: #777777;\n	margin: 0;\n}\n\nul.topiclist dl {\n	position: relative;\n}\n\nul.topiclist li.row dl {\n	padding: 2px 0;\n}\n\nul.topiclist dt {\n	display: block;\n	float: left;\n	width: 50%;\n	font-size: 1.1em;\n	padding-left: 5px;\n	padding-right: 5px;\n}\n\nul.topiclist dd {\n	display: block;\n	float: left;\n	border-left: 1px solid #FFFFFF;\n	padding: 4px 0;\n}\n\nul.topiclist dfn {\n	/* Labels for post/view counts */\n	position: absolute;\n	left: -999px;\n	width: 990px;\n}\n\nul.topiclist li.row dt a.subforum {\n	background-image: none;\n	background-position: 0 50%;\n	background-repeat: no-repeat;\n	position: relative;\n	white-space: nowrap;\n	padding: 0 0 0 12px;\n}\n\n.forum-image {\n	float: left;\n	padding-top: 5px;\n	margin-right: 5px;\n}\n\nli.row {\n	border-top: 1px solid #FFFFFF;\n	border-bottom: 1px solid #8f8f8f;\n}\n\nli.row strong {\n	font-weight: normal;\n	color: #000000;\n}\n\nli.row:hover {\n	background-color: #f6f6f6;\n}\n\nli.row:hover dd {\n	border-left-color: #CCCCCC;\n}\n\nli.header dt, li.header dd {\n	line-height: 1em;\n	border-left-width: 0;\n	margin: 2px 0 4px 0;\n	color: #FFFFFF;\n	padding-top: 2px;\n	padding-bottom: 2px;\n	font-size: 1em;\n	font-family: Arial, Helvetica, sans-serif;\n	text-transform: uppercase;\n}\n\nli.header dt {\n	font-weight: bold;\n}\n\nli.header dd {\n	margin-left: 1px;\n}\n\nli.header dl.icon {\n	min-height: 0;\n}\n\nli.header dl.icon dt {\n	/* Tweak for headers alignment when folder icon used */\n	padding-left: 0;\n	padding-right: 50px;\n}\n\n/* Forum list column styles */\ndl.icon {\n	min-height: 35px;\n	background-position: 10px 50%;		/* Position of folder icon */\n	background-repeat: no-repeat;\n}\n\ndl.icon dt {\n	padding-left: 45px;					/* Space for folder icon */\n	background-repeat: no-repeat;\n	background-position: 5px 95%;		/* Position of topic icon */\n}\n\ndd.posts, dd.topics, dd.views {\n	width: 8%;\n	text-align: center;\n	line-height: 2.2em;\n	font-size: 1.2em;\n}\n\n/* List in forum description */\ndl.icon dt ol,\ndl.icon dt ul {\n	list-style-position: inside;\n	margin-left: 1em;\n}\n\ndl.icon dt li {\n	display: list-item;\n	list-style-type: inherit;\n}\n\ndd.lastpost {\n	width: 25%;\n	font-size: 1.1em;\n}\n\ndd.redirect {\n	font-size: 1.1em;\n	line-height: 2.5em;\n}\n\ndd.moderation {\n	font-size: 1.1em;\n}\n\ndd.lastpost span, ul.topiclist dd.searchby span, ul.topiclist dd.info span, ul.topiclist dd.time span, dd.redirect span, dd.moderation span {\n	display: block;\n	padding-left: 5px;\n}\n\ndd.time {\n	width: auto;\n	line-height: 200%;\n	font-size: 1.1em;\n}\n\ndd.extra {\n	width: 12%;\n	line-height: 200%;\n	text-align: center;\n	font-size: 1.1em;\n}\n\ndd.mark {\n	float: right !important;\n	width: 9%;\n	text-align: center;\n	line-height: 200%;\n	font-size: 1.2em;\n}\n\ndd.info {\n	width: 30%;\n}\n\ndd.option {\n	width: 15%;\n	line-height: 200%;\n	text-align: center;\n	font-size: 1.1em;\n}\n\ndd.searchby {\n	width: 47%;\n	font-size: 1.1em;\n	line-height: 1em;\n}\n\nul.topiclist dd.searchextra {\n	margin-left: 5px;\n	padding: 0.2em 0;\n	font-size: 1.1em;\n	color: #333333;\n	border-left: none;\n	clear: both;\n	width: 98%;\n	overflow: hidden;\n}\n\n/* Container for post/reply buttons and pagination */\n.topic-actions {\n	margin-bottom: 3px;\n	font-size: 1.1em;\n	height: 28px;\n	min-height: 28px;\n}\ndiv[class].topic-actions {\n	height: auto;\n}\n\n/* Post body styles\n----------------------------------------*/\n.postbody {\n	padding: 0;\n	line-height: 1.48em;\n	color: #333333;\n	width: 76%;\n	float: left;\n	clear: both;\n}\n\n.postbody .ignore {\n	font-size: 1.1em;\n}\n\n.postbody h3.first {\n	/* The first post on the page uses this */\n	font-size: 1.7em;\n}\n\n.postbody h3 {\n	/* Postbody requires a different h3 format - so change it here */\n	font-size: 1.5em;\n	padding: 2px 0 0 0;\n	margin: 0 0 0.3em 0 !important;\n	text-transform: none;\n	border: none;\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\n	line-height: 125%;\n}\n\n.postbody h3 img {\n	/* Also see tweaks.css */\n	vertical-align: bottom;\n}\n\n.postbody .content {\n	font-size: 1.3em;\n}\n\n.search .postbody {\n	width: 68%\n}\n\n/* Topic review panel\n----------------------------------------*/\n#review {\n	margin-top: 2em;\n}\n\n#topicreview {\n	padding-right: 5px;\n	overflow: auto;\n	height: 300px;\n}\n\n#topicreview .postbody {\n	width: auto;\n	float: none;\n	margin: 0;\n	height: auto;\n}\n\n#topicreview .post {\n	height: auto;\n}\n\n#topicreview h2 {\n	border-bottom-width: 0;\n}\n\n.post-ignore .postbody {\n	display: none;\n}\n\n/* MCP Post details\n----------------------------------------*/\n#post_details\n{\n	/* This will only work in IE7+, plus the others */\n	overflow: auto;\n	max-height: 300px;\n}\n\n#expand\n{\n	clear: both;\n}\n\n/* Content container styles\n----------------------------------------*/\n.content {\n	min-height: 3em;\n	overflow: hidden;\n	line-height: 1.4em;\n	font-family: \"Lucida Grande\", \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\n	font-size: 1em;\n	color: #333333;\n	padding-bottom: 1px;\n}\n\n.content h2, .panel h2 {\n	font-weight: normal;\n	color: #989898;\n	border-bottom: 1px solid #CCCCCC;\n	font-size: 1.6em;\n	margin-top: 0.5em;\n	margin-bottom: 0.5em;\n	padding-bottom: 0.5em;\n}\n\n.panel h3 {\n	margin: 0.5em 0;\n}\n\n.panel p {\n	font-size: 1.2em;\n	margin-bottom: 1em;\n	line-height: 1.4em;\n}\n\n.content p {\n	font-family: \"Lucida Grande\", \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\n	font-size: 1.2em;\n	margin-bottom: 1em;\n	line-height: 1.4em;\n}\n\ndl.faq {\n	font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;\n	font-size: 1.1em;\n	margin-top: 1em;\n	margin-bottom: 2em;\n	line-height: 1.4em;\n}\n\ndl.faq dt {\n	font-weight: bold;\n	color: #333333;\n}\n\n.content dl.faq {\n	font-size: 1.2em;\n	margin-bottom: 0.5em;\n}\n\n.content li {\n	list-style-type: inherit;\n}\n\n.content ul, .content ol {\n	margin-bottom: 1em;\n	margin-left: 3em;\n}\n\n.posthilit {\n	background-color: #f3f3f3;\n	color: #BCBCBC;\n	padding: 0 2px 1px 2px;\n}\n\n.announce, .unreadpost {\n	/* Highlight the announcements & unread posts box */\n	border-left-color: #BCBCBC;\n	border-right-color: #BCBCBC;\n}\n\n/* Post author */\np.author {\n	margin: 0 15em 0.6em 0;\n	padding: 0 0 5px 0;\n	font-family: Verdana, Helvetica, Arial, sans-serif;\n	font-size: 1em;\n	line-height: 1.2em;\n}\n\n/* Post signature */\n.signature {\n	margin-top: 1.5em;\n	padding-top: 0.2em;\n	font-size: 1.1em;\n	border-top: 1px solid #CCCCCC;\n	clear: left;\n	line-height: 140%;\n	overflow: hidden;\n	width: 100%;\n}\n\ndd .signature {\n	margin: 0;\n	padding: 0;\n	clear: none;\n	border: none;\n}\n\n.signature li {\n	list-style-type: inherit;\n}\n\n.signature ul, .signature ol {\n	margin-bottom: 1em;\n	margin-left: 3em;\n}\n\n/* Post noticies */\n.notice {\n	font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;\n	width: auto;\n	margin-top: 1.5em;\n	padding-top: 0.2em;\n	font-size: 1em;\n	border-top: 1px dashed #CCCCCC;\n	clear: left;\n	line-height: 130%;\n}\n\n/* Jump to post link for now */\nul.searchresults {\n	list-style: none;\n	text-align: right;\n	clear: both;\n}\n\n/* BB Code styles\n----------------------------------------*/\n/* Quote block */\nblockquote {\n	background: #ebebeb none 6px 8px no-repeat;\n	border: 1px solid #dbdbdb;\n	font-size: 0.95em;\n	margin: 0.5em 1px 0 25px;\n	overflow: hidden;\n	padding: 5px;\n}\n\nblockquote blockquote {\n	/* Nested quotes */\n	background-color: #bababa;\n	font-size: 1em;\n	margin: 0.5em 1px 0 15px;	\n}\n\nblockquote blockquote blockquote {\n	/* Nested quotes */\n	background-color: #e4e4e4;\n}\n\nblockquote cite {\n	/* Username/source of quoter */\n	font-style: normal;\n	font-weight: bold;\n	margin-left: 20px;\n	display: block;\n	font-size: 0.9em;\n}\n\nblockquote cite cite {\n	font-size: 1em;\n}\n\nblockquote.uncited {\n	padding-top: 25px;\n}\n\n/* Code block */\ndl.codebox {\n	padding: 3px;\n	background-color: #FFFFFF;\n	border: 1px solid #d8d8d8;\n	font-size: 1em;\n}\n\ndl.codebox dt {\n	text-transform: uppercase;\n	border-bottom: 1px solid #CCCCCC;\n	margin-bottom: 3px;\n	font-size: 0.8em;\n	font-weight: bold;\n	display: block;\n}\n\nblockquote dl.codebox {\n	margin-left: 0;\n}\n\ndl.codebox code {\n	/* Also see tweaks.css */\n	overflow: auto;\n	display: block;\n	height: auto;\n	max-height: 200px;\n	white-space: normal;\n	padding-top: 5px;\n	font: 0.9em Monaco, \"Andale Mono\",\"Courier New\", Courier, mono;\n	line-height: 1.3em;\n	color: #8b8b8b;\n	margin: 2px 0;\n}\n\n.syntaxbg		{ color: #FFFFFF; }\n.syntaxcomment	{ color: #000000; }\n.syntaxdefault	{ color: #bcbcbc; }\n.syntaxhtml		{ color: #000000; }\n.syntaxkeyword	{ color: #585858; }\n.syntaxstring	{ color: #a7a7a7; }\n\n/* Attachments\n----------------------------------------*/\n.attachbox {\n	float: left;\n	width: auto; \n	margin: 5px 5px 5px 0;\n	padding: 6px;\n	background-color: #FFFFFF;\n	border: 1px dashed #d8d8d8;\n	clear: left;\n}\n\n.pm-message .attachbox {\n	background-color: #f3f3f3;\n}\n\n.attachbox dt {\n	font-family: Arial, Helvetica, sans-serif;\n	text-transform: uppercase;\n}\n\n.attachbox dd {\n	margin-top: 4px;\n	padding-top: 4px;\n	clear: left;\n	border-top: 1px solid #d8d8d8;\n}\n\n.attachbox dd dd {\n	border: none;\n}\n\n.attachbox p {\n	line-height: 110%;\n	color: #666666;\n	font-weight: normal;\n	clear: left;\n}\n\n.attachbox p.stats\n{\n	line-height: 110%;\n	color: #666666;\n	font-weight: normal;\n	clear: left;\n}\n\n.attach-image {\n	margin: 3px 0;\n	width: 100%;\n	max-height: 350px;\n	overflow: auto;\n}\n\n.attach-image img {\n	border: 1px solid #999999;\n/*	cursor: move; */\n	cursor: default;\n}\n\n/* Inline image thumbnails */\ndiv.inline-attachment dl.thumbnail, div.inline-attachment dl.file {\n	display: block;\n	margin-bottom: 4px;\n}\n\ndiv.inline-attachment p {\n	font-size: 100%;\n}\n\ndl.file {\n	font-family: Verdana, Arial, Helvetica, sans-serif;\n	display: block;\n}\n\ndl.file dt {\n	text-transform: none;\n	margin: 0;\n	padding: 0;\n	font-weight: bold;\n	font-family: Verdana, Arial, Helvetica, sans-serif;\n}\n\ndl.file dd {\n	color: #666666;\n	margin: 0;\n	padding: 0;	\n}\n\ndl.thumbnail img {\n	padding: 3px;\n	border: 1px solid #666666;\n	background-color: #FFF;\n}\n\ndl.thumbnail dd {\n	color: #666666;\n	font-style: italic;\n	font-family: Verdana, Arial, Helvetica, sans-serif;\n}\n\n.attachbox dl.thumbnail dd {\n	font-size: 100%;\n}\n\ndl.thumbnail dt a:hover {\n	background-color: #EEEEEE;\n}\n\ndl.thumbnail dt a:hover img {\n	border: 1px solid #d2d2d2;\n}\n\n/* Post poll styles\n----------------------------------------*/\nfieldset.polls {\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\n}\n\nfieldset.polls dl {\n	margin-top: 5px;\n	border-top: 1px solid #e2e2e2;\n	padding: 5px 0 0 0;\n	line-height: 120%;\n	color: #666666;\n}\n\nfieldset.polls dl.voted {\n	font-weight: bold;\n	color: #000000;\n}\n\nfieldset.polls dt {\n	text-align: left;\n	float: left;\n	display: block;\n	width: 30%;\n	border-right: none;\n	padding: 0;\n	margin: 0;\n	font-size: 1.1em;\n}\n\nfieldset.polls dd {\n	float: left;\n	width: 10%;\n	border-left: none;\n	padding: 0 5px;\n	margin-left: 0;\n	font-size: 1.1em;\n}\n\nfieldset.polls dd.resultbar {\n	width: 50%;\n}\n\nfieldset.polls dd input {\n	margin: 2px 0;\n}\n\nfieldset.polls dd div {\n	text-align: right;\n	font-family: Arial, Helvetica, sans-serif;\n	color: #FFFFFF;\n	font-weight: bold;\n	padding: 0 2px;\n	overflow: visible;\n	min-width: 2%;\n}\n\n.pollbar1 {\n	background-color: #aaaaaa;\n	border-bottom: 1px solid #747474;\n	border-right: 1px solid #747474;\n}\n\n.pollbar2 {\n	background-color: #bebebe;\n	border-bottom: 1px solid #8c8c8c;\n	border-right: 1px solid #8c8c8c;\n}\n\n.pollbar3 {\n	background-color: #D1D1D1;\n	border-bottom: 1px solid #aaaaaa;\n	border-right: 1px solid #aaaaaa;\n}\n\n.pollbar4 {\n	background-color: #e4e4e4;\n	border-bottom: 1px solid #bebebe;\n	border-right: 1px solid #bebebe;\n}\n\n.pollbar5 {\n	background-color: #f8f8f8;\n	border-bottom: 1px solid #D1D1D1;\n	border-right: 1px solid #D1D1D1;\n}\n\n/* Poster profile block\n----------------------------------------*/\n.postprofile {\n	/* Also see tweaks.css */\n	margin: 5px 0 10px 0;\n	min-height: 80px;\n	color: #666666;\n	border-left: 1px solid #FFFFFF;\n	width: 22%;\n	float: right;\n	display: inline;\n}\n.pm .postprofile {\n	border-left: 1px solid #DDDDDD;\n}\n\n.postprofile dd, .postprofile dt {\n	line-height: 1.2em;\n	margin-left: 8px;\n}\n\n.postprofile strong {\n	font-weight: normal;\n	color: #000000;\n}\n\n.avatar {\n	border: none;\n	margin-bottom: 3px;\n}\n\n.online {\n	background-image: none;\n	background-position: 100% 0;\n	background-repeat: no-repeat;\n}\n\n/* Poster profile used by search*/\n.search .postprofile {\n	width: 30%;\n}\n\n/* pm list in compose message if mass pm is enabled */\ndl.pmlist dt {\n	width: 60% !important;\n}\n\ndl.pmlist dt textarea {\n	width: 95%;\n}\n\ndl.pmlist dd {\n	margin-left: 61% !important;\n	margin-bottom: 2px;\n}\n/* Button Styles\n---------------------------------------- */\n\n/* Rollover buttons\n   Based on: http://wellstyled.com/css-nopreload-rollovers.html\n----------------------------------------*/\n.buttons {\n	float: left;\n	width: auto;\n	height: auto;\n}\n\n/* Rollover state */\n.buttons div {\n	float: left;\n	margin: 0 5px 0 0;\n	background-position: 0 100%;\n}\n\n/* Rolloff state */\n.buttons div a {\n	display: block;\n	width: 100%;\n	height: 100%;\n	background-position: 0 0;\n	position: relative;\n	overflow: hidden;\n}\n\n/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */\n/*.buttons div span		{ display: none; }*/\n/*.buttons div a:hover	{ background-image: none; }*/\n.buttons div span			{ position: absolute; width: 100%; height: 100%; cursor: pointer;}\n.buttons div a:hover span	{ background-position: 0 100%; }\n\n/* Big button images */\n.reply-icon span	{ background: transparent none 0 0 no-repeat; }\n.post-icon span		{ background: transparent none 0 0 no-repeat; }\n.locked-icon span	{ background: transparent none 0 0 no-repeat; }\n.pmreply-icon span	{ background: none 0 0 no-repeat; }\n.newpm-icon span 	{ background: none 0 0 no-repeat; }\n.forwardpm-icon span 	{ background: none 0 0 no-repeat; }\n\n/* Set big button dimensions */\n.buttons div.reply-icon		{ width: {IMG_BUTTON_TOPIC_REPLY_WIDTH}px; height: {IMG_BUTTON_TOPIC_REPLY_HEIGHT}px; }\n.buttons div.post-icon		{ width: {IMG_BUTTON_TOPIC_NEW_WIDTH}px; height: {IMG_BUTTON_TOPIC_NEW_HEIGHT}px; }\n.buttons div.locked-icon	{ width: {IMG_BUTTON_TOPIC_LOCKED_WIDTH}px; height: {IMG_BUTTON_TOPIC_LOCKED_HEIGHT}px; }\n.buttons div.pmreply-icon	{ width: {IMG_BUTTON_PM_REPLY_WIDTH}px; height: {IMG_BUTTON_PM_REPLY_HEIGHT}px; }\n.buttons div.newpm-icon		{ width: {IMG_BUTTON_PM_NEW_WIDTH}px; height: {IMG_BUTTON_PM_NEW_HEIGHT}px; }\n.buttons div.forwardpm-icon	{ width: {IMG_BUTTON_PM_FORWARD_WIDTH}px; height: {IMG_BUTTON_PM_FORWARD_HEIGHT}px; }\n\n/* Sub-header (navigation bar)\n--------------------------------------------- */\na.print, a.sendemail, a.fontsize {\n	display: block;\n	overflow: hidden;\n	height: 18px;\n	text-indent: -5000px;\n	text-align: left;\n	background-repeat: no-repeat;\n}\n\na.print {\n	background-image: none;\n	width: 22px;\n}\n\na.sendemail {\n	background-image: none;\n	width: 22px;\n}\n\na.fontsize {\n	background-image: none;\n	background-position: 0 -1px;\n	width: 29px;\n}\n\na.fontsize:hover {\n	background-position: 0 -20px;\n	text-decoration: none;\n}\n\n/* Icon images\n---------------------------------------- */\n.sitehome, .icon-faq, .icon-members, .icon-home, .icon-ucp, .icon-register, .icon-logout,\n.icon-bookmark, .icon-bump, .icon-subscribe, .icon-unsubscribe, .icon-pages, .icon-search {\n	background-position: 0 50%;\n	background-repeat: no-repeat;\n	background-image: none;\n	padding: 1px 0 0 17px;\n}\n\n/* Poster profile icons\n----------------------------------------*/\nul.profile-icons {\n	padding-top: 10px;\n	list-style: none;\n}\n\n/* Rollover state */\nul.profile-icons li {\n	float: left;\n	margin: 0 6px 3px 0;\n	background-position: 0 100%;\n}\n\n/* Rolloff state */\nul.profile-icons li a {\n	display: block;\n	width: 100%;\n	height: 100%;\n	background-position: 0 0;\n}\n\n/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */\nul.profile-icons li span { display:none; }\nul.profile-icons li a:hover { background: none; }\n\n/* Positioning of moderator icons */\n.postbody ul.profile-icons {\n	float: right;\n	width: auto;\n	padding: 0;\n}\n\n.postbody ul.profile-icons li {\n	margin: 0 3px;\n}\n\n/* Profile & navigation icons */\n.email-icon, .email-icon a		{ background: none top left no-repeat; }\n.aim-icon, .aim-icon a			{ background: none top left no-repeat; }\n.yahoo-icon, .yahoo-icon a		{ background: none top left no-repeat; }\n.web-icon, .web-icon a			{ background: none top left no-repeat; }\n.msnm-icon, .msnm-icon a			{ background: none top left no-repeat; }\n.icq-icon, .icq-icon a			{ background: none top left no-repeat; }\n.jabber-icon, .jabber-icon a		{ background: none top left no-repeat; }\n.pm-icon, .pm-icon a				{ background: none top left no-repeat; }\n.quote-icon, .quote-icon a		{ background: none top left no-repeat; }\n\n/* Moderator icons */\n.report-icon, .report-icon a		{ background: none top left no-repeat; }\n.warn-icon, .warn-icon a			{ background: none top left no-repeat; }\n.edit-icon, .edit-icon a			{ background: none top left no-repeat; }\n.delete-icon, .delete-icon a		{ background: none top left no-repeat; }\n.info-icon, .info-icon a			{ background: none top left no-repeat; }\n\n/* Set profile icon dimensions */\nul.profile-icons li.email-icon		{ width: {IMG_ICON_CONTACT_EMAIL_WIDTH}px; height: {IMG_ICON_CONTACT_EMAIL_HEIGHT}px; }\nul.profile-icons li.aim-icon	{ width: {IMG_ICON_CONTACT_AIM_WIDTH}px; height: {IMG_ICON_CONTACT_AIM_HEIGHT}px; }\nul.profile-icons li.yahoo-icon	{ width: {IMG_ICON_CONTACT_YAHOO_WIDTH}px; height: {IMG_ICON_CONTACT_YAHOO_HEIGHT}px; }\nul.profile-icons li.web-icon	{ width: {IMG_ICON_CONTACT_WWW_WIDTH}px; height: {IMG_ICON_CONTACT_WWW_HEIGHT}px; }\nul.profile-icons li.msnm-icon	{ width: {IMG_ICON_CONTACT_MSNM_WIDTH}px; height: {IMG_ICON_CONTACT_MSNM_HEIGHT}px; }\nul.profile-icons li.icq-icon	{ width: {IMG_ICON_CONTACT_ICQ_WIDTH}px; height: {IMG_ICON_CONTACT_ICQ_HEIGHT}px; }\nul.profile-icons li.jabber-icon	{ width: {IMG_ICON_CONTACT_JABBER_WIDTH}px; height: {IMG_ICON_CONTACT_JABBER_HEIGHT}px; }\nul.profile-icons li.pm-icon		{ width: {IMG_ICON_CONTACT_PM_WIDTH}px; height: {IMG_ICON_CONTACT_PM_HEIGHT}px; }\nul.profile-icons li.quote-icon	{ width: {IMG_ICON_POST_QUOTE_WIDTH}px; height: {IMG_ICON_POST_QUOTE_HEIGHT}px; }\nul.profile-icons li.report-icon	{ width: {IMG_ICON_POST_REPORT_WIDTH}px; height: {IMG_ICON_POST_REPORT_HEIGHT}px; }\nul.profile-icons li.edit-icon	{ width: {IMG_ICON_POST_EDIT_WIDTH}px; height: {IMG_ICON_POST_EDIT_HEIGHT}px; }\nul.profile-icons li.delete-icon	{ width: {IMG_ICON_POST_DELETE_WIDTH}px; height: {IMG_ICON_POST_DELETE_HEIGHT}px; }\nul.profile-icons li.info-icon	{ width: {IMG_ICON_POST_INFO_WIDTH}px; height: {IMG_ICON_POST_INFO_HEIGHT}px; }\nul.profile-icons li.warn-icon	{ width: {IMG_ICON_USER_WARN_WIDTH}px; height: {IMG_ICON_USER_WARN_HEIGHT}px; }\n\n/* Fix profile icon default margins */\nul.profile-icons li.edit-icon	{ margin: 0 0 0 3px; }\nul.profile-icons li.quote-icon	{ margin: 0 0 0 10px; }\nul.profile-icons li.info-icon, ul.profile-icons li.report-icon	{ margin: 0 3px 0 0; }\n/* Control Panel Styles\n---------------------------------------- */\n\n\n/* Main CP box\n----------------------------------------*/\n#cp-menu {\n	float:left;\n	width: 19%;\n	margin-top: 1em;\n	margin-bottom: 5px;\n}\n\n#cp-main {\n	float: left;\n	width: 81%;\n}\n\n#cp-main .content {\n	padding: 0;\n}\n\n#cp-main h3, #cp-main hr, #cp-menu hr {\n	border-color: #bfbfbf;\n}\n\n#cp-main .panel p {\n	font-size: 1.1em;\n}\n\n#cp-main .panel ol {\n	margin-left: 2em;\n	font-size: 1.1em;\n}\n\n#cp-main .panel li.row {\n	border-bottom: 1px solid #cbcbcb;\n	border-top: 1px solid #F9F9F9;\n}\n\nul.cplist {\n	margin-bottom: 5px;\n	border-top: 1px solid #cbcbcb;\n}\n\n#cp-main .panel li.header dd, #cp-main .panel li.header dt {\n	color: #000000;\n	margin-bottom: 2px;\n}\n\n#cp-main table.table1 {\n	margin-bottom: 1em;\n}\n\n#cp-main table.table1 thead th {\n	color: #333333;\n	font-weight: bold;\n	border-bottom: 1px solid #333333;\n	padding: 5px;\n}\n\n#cp-main table.table1 tbody th {\n	font-style: italic;\n	background-color: transparent !important;\n	border-bottom: none;\n}\n\n#cp-main .pagination {\n	float: right;\n	width: auto;\n	padding-top: 1px;\n}\n\n#cp-main .postbody p {\n	font-size: 1.1em;\n}\n\n#cp-main .pm-message {\n	border: 1px solid #e2e2e2;\n	margin: 10px 0;\n	background-color: #FFFFFF;\n	width: auto;\n	float: none;\n}\n\n.pm-message h2 {\n	padding-bottom: 5px;\n}\n\n#cp-main .postbody h3, #cp-main .box2 h3 {\n	margin-top: 0;\n}\n\n#cp-main .buttons {\n	margin-left: 0;\n}\n\n#cp-main ul.linklist {\n	margin: 0;\n}\n\n/* MCP Specific tweaks */\n.mcp-main .postbody {\n	width: 100%;\n}\n\n.tabs-container h2 {\n	float: left;\n	margin-bottom: 0px;\n}\n\n.tabs-container #minitabs {\n	float: right;\n	margin-top: 19px;\n}\n\n.tabs-container:after {\n	display: block;\n	clear: both;\n	content: \'\';\n}\n\n/* CP tabbed menu\n----------------------------------------*/\n#tabs {\n	line-height: normal;\n	margin: 20px 0 -1px 7px;\n	min-width: 570px;\n}\n\n#tabs ul {\n	margin:0;\n	padding: 0;\n	list-style: none;\n}\n\n#tabs li {\n	display: inline;\n	margin: 0;\n	padding: 0;\n	font-size: 1em;\n	font-weight: bold;\n}\n\n#tabs a {\n	float: left;\n	background: none no-repeat 0% -35px;\n	margin: 0 1px 0 0;\n	padding: 0 0 0 5px;\n	text-decoration: none;\n	position: relative;\n	cursor: pointer;\n}\n\n#tabs a span {\n	float: left;\n	display: block;\n	background: none no-repeat 100% -35px;\n	padding: 6px 10px 6px 5px;\n	color: #828282;\n	white-space: nowrap;\n}\n\n#tabs a:hover span {\n	color: #bcbcbc;\n}\n\n#tabs .activetab a {\n	background-position: 0 0;\n	border-bottom: 1px solid #ebebeb;\n}\n\n#tabs .activetab a span {\n	background-position: 100% 0;\n	padding-bottom: 7px;\n	color: #333333;\n}\n\n#tabs a:hover {\n	background-position: 0 -70px;\n}\n\n#tabs a:hover span {\n	background-position:100% -70px;\n}\n\n#tabs .activetab a:hover {\n	background-position: 0 0;\n}\n\n#tabs .activetab a:hover span {\n	color: #000000;\n	background-position: 100% 0;\n}\n\n/* Mini tabbed menu used in MCP\n----------------------------------------*/\n#minitabs {\n	line-height: normal;\n	margin: -20px 7px 0 0;\n}\n\n#minitabs ul {\n	margin:0;\n	padding: 0;\n	list-style: none;\n}\n\n#minitabs li {\n	display: block;\n	float: right;\n	padding: 0 10px 4px 10px;\n	font-size: 1em;\n	font-weight: bold;\n	background-color: #f2f2f2;\n	margin-left: 2px;\n}\n\n#minitabs a {\n}\n\n#minitabs a:hover {\n	text-decoration: none;\n}\n\n#minitabs li.activetab {\n	background-color: #F9F9F9;\n}\n\n#minitabs li.activetab a, #minitabs li.activetab a:hover {\n	color: #333333;\n}\n\n/* UCP navigation menu\n----------------------------------------*/\n/* Container for sub-navigation list */\n#navigation {\n	width: 100%;\n	padding-top: 36px;\n}\n\n#navigation ul {\n	list-style:none;\n}\n\n/* Default list state */\n#navigation li {\n	margin: 1px 0;\n	padding: 0;\n	font-weight: bold;\n	display: inline;\n}\n\n/* Link styles for the sub-section links */\n#navigation a {\n	display: block;\n	padding: 5px;\n	margin: 1px 0;\n	text-decoration: none;\n	font-weight: bold;\n	color: #333;\n	background: #cfcfcf none repeat-y 100% 0;\n}\n\n#navigation a:hover {\n	text-decoration: none;\n	background-color: #c6c6c6;\n	color: #bcbcbc;\n	background-image: none;\n}\n\n#navigation #active-subsection a {\n	display: block;\n	color: #d3d3d3;\n	background-color: #F9F9F9;\n	background-image: none;\n}\n\n#navigation #active-subsection a:hover {\n	color: #d3d3d3;\n}\n\n/* Preferences pane layout\n----------------------------------------*/\n#cp-main h2 {\n	border-bottom: none;\n	padding: 0;\n	margin-left: 10px;\n	color: #333333;\n}\n\n#cp-main .panel {\n	background-color: #F9F9F9;\n}\n\n#cp-main .pm {\n	background-color: #FFFFFF;\n}\n\n#cp-main span.corners-top, #cp-menu span.corners-top {\n	background-image: none;\n}\n\n#cp-main span.corners-top span, #cp-menu span.corners-top span {\n	background-image: none;\n}\n\n#cp-main span.corners-bottom, #cp-menu span.corners-bottom {\n	background-image: none;\n}\n\n#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {\n	background-image: none;\n}\n\n/* Topicreview */\n#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {\n	background-image: none;\n}\n\n#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {\n	background-image: none;\n}\n\n#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {\n	background-image: none;\n}\n\n#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {\n	background-image: none;\n}\n\n/* Friends list */\n.cp-mini {\n	background-color: #f9f9f9;\n	padding: 0 5px;\n	margin: 10px 15px 10px 5px;\n}\n\n.cp-mini span.corners-top, .cp-mini span.corners-bottom {\n	margin: 0 -5px;\n}\n\ndl.mini dt {\n	font-weight: bold;\n	color: #676767;\n}\n\ndl.mini dd {\n	padding-top: 4px;\n}\n\n.friend-online {\n	font-weight: bold;\n}\n\n.friend-offline {\n	font-style: italic;\n}\n\n/* PM Styles\n----------------------------------------*/\n#pm-menu {\n	line-height: 2.5em;\n}\n\n/* PM panel adjustments */\n.reply-all a.left {\n	background-position: 3px 60%;\n}\n\n.reply-all a.left:hover {\n	background-position: 0px 60%;\n}\n\n.reply-all {\n	font-size: 11px;\n	padding-top: 5px;\n}\n\n/* PM Message history */\n.current {\n	color: #999999;\n}\n\n/* Defined rules list for PM options */\nol.def-rules {\n	padding-left: 0;\n}\n\nol.def-rules li {\n	line-height: 180%;\n	padding: 1px;\n}\n\n/* PM marking colours */\n.pmlist li.bg1 {\n	padding: 0 3px;\n}\n\n.pmlist li.bg2 {\n	padding: 0 3px;\n}\n\n.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {\n	border-left-color: #bcbcbc;\n	border-right-color: #bcbcbc;\n}\n\n.pmlist li.pm_marked_colour, .pm_marked_colour {\n	padding: 0;\n	border: solid 3px #ffffff;\n	border-width: 0 3px;\n}\n\n.pmlist li.pm_replied_colour, .pm_replied_colour {\n	padding: 0;\n	border: solid 3px #c2c2c2;\n	border-width: 0 3px;\n}\n\n.pmlist li.pm_friend_colour, .pm_friend_colour {\n	padding: 0;\n	border: solid 3px #bdbdbd;\n	border-width: 0 3px;\n}\n\n.pmlist li.pm_foe_colour, .pm_foe_colour {\n	padding: 0;\n	border: solid 3px #000000;\n	border-width: 0 3px;\n}\n\n.pm-legend {\n	border-left-width: 10px;\n	border-left-style: solid;\n	border-right-width: 0;\n	margin-bottom: 3px;\n	padding-left: 3px;\n}\n\n/* Avatar gallery */\n#gallery label {\n	position: relative;\n	float: left;\n	margin: 10px;\n	padding: 5px;\n	width: auto;\n	background: #FFFFFF;\n	border: 1px solid #CCC;\n	text-align: center;\n}\n\n#gallery label:hover {\n	background-color: #EEE;\n}\n/* Form Styles\n---------------------------------------- */\n\n/* General form styles\n----------------------------------------*/\nfieldset {\n	border-width: 0;\n	font-family: Verdana, Helvetica, Arial, sans-serif;\n	font-size: 1.1em;\n}\n\ninput {\n	font-weight: normal;\n	cursor: pointer;\n	vertical-align: middle;\n	padding: 0 3px;\n	font-size: 1em;\n	font-family: Verdana, Helvetica, Arial, sans-serif;\n}\n\nselect {\n	font-family: Verdana, Helvetica, Arial, sans-serif;\n	font-weight: normal;\n	cursor: pointer;\n	vertical-align: middle;\n	border: 1px solid #666666;\n	padding: 1px;\n	background-color: #FAFAFA;\n	font-size: 1em;\n}\n\noption {\n	padding-right: 1em;\n}\n\noption.disabled-option {\n	color: graytext;\n}\n\ntextarea {\n	font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;\n	width: 60%;\n	padding: 2px;\n	font-size: 1em;\n	line-height: 1.4em;\n}\n\nlabel {\n	cursor: default;\n	padding-right: 5px;\n	color: #676767;\n}\n\nlabel input {\n	vertical-align: middle;\n}\n\nlabel img {\n	vertical-align: middle;\n}\n\n/* Definition list layout for forms\n---------------------------------------- */\nfieldset dl {\n	padding: 4px 0;\n}\n\nfieldset dt {\n	float: left;	\n	width: 40%;\n	text-align: left;\n	display: block;\n}\n\nfieldset dd {\n	margin-left: 41%;\n	vertical-align: top;\n	margin-bottom: 3px;\n}\n\n/* Specific layout 1 */\nfieldset.fields1 dt {\n	width: 15em;\n	border-right-width: 0;\n}\n\nfieldset.fields1 dd {\n	margin-left: 15em;\n	border-left-width: 0;\n}\n\nfieldset.fields1 {\n	background-color: transparent;\n}\n\nfieldset.fields1 div {\n	margin-bottom: 3px;\n}\n\n/* Set it back to 0px for the reCaptcha divs: PHPBB3-9587 */\nfieldset.fields1 #recaptcha_widget_div div {\n	margin-bottom: 0;\n}\n\n/* Specific layout 2 */\nfieldset.fields2 dt {\n	width: 15em;\n	border-right-width: 0;\n}\n\nfieldset.fields2 dd {\n	margin-left: 16em;\n	border-left-width: 0;\n}\n\n/* Form elements */\ndt label {\n	font-weight: bold;\n	text-align: left;\n}\n\ndd label {\n	white-space: nowrap;\n	color: #333;\n}\n\ndd input, dd textarea {\n	margin-right: 3px;\n}\n\ndd select {\n	width: auto;\n}\n\ndd textarea {\n	width: 85%;\n}\n\n/* Hover effects */\nfieldset dl:hover dt label {\n	color: #000000;\n}\n\nfieldset.fields2 dl:hover dt label {\n	color: inherit;\n}\n\n#timezone {\n	width: 95%;\n}\n\n* html #timezone {\n	width: 50%;\n}\n\n/* Quick-login on index page */\nfieldset.quick-login {\n	margin-top: 5px;\n}\n\nfieldset.quick-login input {\n	width: auto;\n}\n\nfieldset.quick-login input.inputbox {\n	width: 15%;\n	vertical-align: middle;\n	margin-right: 5px;\n	background-color: #f3f3f3;\n}\n\nfieldset.quick-login label {\n	white-space: nowrap;\n	padding-right: 2px;\n}\n\n/* Display options on viewtopic/viewforum pages  */\nfieldset.display-options {\n	text-align: center;\n	margin: 3px 0 5px 0;\n}\n\nfieldset.display-options label {\n	white-space: nowrap;\n	padding-right: 2px;\n}\n\nfieldset.display-options a {\n	margin-top: 3px;\n}\n\n/* Display actions for ucp and mcp pages */\nfieldset.display-actions {\n	text-align: right;\n	line-height: 2em;\n	white-space: nowrap;\n	padding-right: 1em;\n}\n\nfieldset.display-actions label {\n	white-space: nowrap;\n	padding-right: 2px;\n}\n\nfieldset.sort-options {\n	line-height: 2em;\n}\n\n/* MCP forum selection*/\nfieldset.forum-selection {\n	margin: 5px 0 3px 0;\n	float: right;\n}\n\nfieldset.forum-selection2 {\n	margin: 13px 0 3px 0;\n	float: right;\n}\n\n/* Jumpbox */\nfieldset.jumpbox {\n	text-align: right;\n	margin-top: 15px;\n	height: 2.5em;\n}\n\nfieldset.quickmod {\n	width: 50%;\n	float: right;\n	text-align: right;\n	height: 2.5em;\n}\n\n/* Submit button fieldset */\nfieldset.submit-buttons {\n	text-align: center;\n	vertical-align: middle;\n	margin: 5px 0;\n}\n\nfieldset.submit-buttons input {\n	vertical-align: middle;\n	padding-top: 3px;\n	padding-bottom: 3px;\n}\n\n/* Posting page styles\n----------------------------------------*/\n\n/* Buttons used in the editor */\n#format-buttons {\n	margin: 15px 0 2px 0;\n}\n\n#format-buttons input, #format-buttons select {\n	vertical-align: middle;\n}\n\n/* Main message box */\n#message-box {\n	width: 80%;\n}\n\n#message-box textarea {\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\n	width: 450px;\n	height: 270px;\n	min-width: 100%;\n	max-width: 100%;\n	font-size: 1.2em;\n	color: #333333;\n}\n\n/* Emoticons panel */\n#smiley-box {\n	width: 18%;\n	float: right;\n}\n\n#smiley-box img {\n	margin: 3px;\n}\n\n/* Input field styles\n---------------------------------------- */\n.inputbox {\n	background-color: #FFFFFF;\n	border: 1px solid #c0c0c0;\n	color: #333333;\n	padding: 2px;\n	cursor: text;\n}\n\n.inputbox:hover {\n	border: 1px solid #eaeaea;\n}\n\n.inputbox:focus {\n	border: 1px solid #eaeaea;\n	color: #4b4b4b;\n}\n\ninput.inputbox	{ width: 85%; }\ninput.medium	{ width: 50%; }\ninput.narrow	{ width: 25%; }\ninput.tiny		{ width: 125px; }\n\ntextarea.inputbox {\n	width: 85%;\n}\n\n.autowidth {\n	width: auto !important;\n}\n\n/* Form button styles\n---------------------------------------- */\ninput.button1, input.button2 {\n	font-size: 1em;\n}\n\na.button1, input.button1, input.button3, a.button2, input.button2 {\n	width: auto !important;\n	padding-top: 1px;\n	padding-bottom: 1px;\n	font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;\n	color: #000;\n	background: #FAFAFA none repeat-x top left;\n}\n\na.button1, input.button1 {\n	font-weight: bold;\n	border: 1px solid #666666;\n}\n\ninput.button3 {\n	padding: 0;\n	margin: 0;\n	line-height: 5px;\n	height: 12px;\n	background-image: none;\n	font-variant: small-caps;\n}\n\n/* Alternative button */\na.button2, input.button2, input.button3 {\n	border: 1px solid #666666;\n}\n\n/* <a> button in the style of the form buttons */\na.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {\n	text-decoration: none;\n	color: #000000;\n	padding: 2px 8px;\n	line-height: 250%;\n	vertical-align: text-bottom;\n	background-position: 0 1px;\n}\n\n/* Hover states */\na.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {\n	border: 1px solid #BCBCBC;\n	background-position: 0 100%;\n	color: #BCBCBC;\n}\n\ninput.disabled {\n	font-weight: normal;\n	color: #666666;\n}\n\n/* Topic and forum Search */\n.search-box {\n	margin-top: 3px;\n	margin-left: 5px;\n	float: left;\n}\n\n.search-box input {\n}\n\ninput.search {\n	background-image: none;\n	background-repeat: no-repeat;\n	background-position: left 1px;\n	padding-left: 17px;\n}\n\n.full { width: 95%; }\n.medium { width: 50%;}\n.narrow { width: 25%;}\n.tiny { width: 10%;}\n/* Style Sheet Tweaks\n\nThese style definitions are mainly IE specific \ntweaks required due to its poor CSS support.\n-------------------------------------------------*/\n\n* html table, * html select, * html input { font-size: 100%; }\n* html hr { margin: 0; }\n* html span.corners-top, * html span.corners-bottom { background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\"); }\n* html span.corners-top span, * html span.corners-bottom span { background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\"); }\n\ntable.table1 {\n	width: 99%;		/* IE < 6 browsers */\n	/* Tantek hack */\n	voice-family: \"\\\"}\\\"\";\n	voice-family: inherit;\n	width: 100%;\n}\nhtml>body table.table1 { width: 100%; }	/* Reset 100% for opera */\n\n* html ul.topiclist li { position: relative; }\n* html .postbody h3 img { vertical-align: middle; }\n\n/* Form styles */\nhtml>body dd label input { vertical-align: text-bottom; }	/* Align checkboxes/radio buttons nicely */\n\n* html input.button1, * html input.button2 {\n	padding-bottom: 0;\n	margin-bottom: 1px;\n}\n\n/* Misc layout styles */\n* html .column1, * html .column2 { width: 45%; }\n\n/* Nice method for clearing floated blocks without having to insert any extra markup (like spacer above)\n   From http://www.positioniseverything.net/easyclearing.html \n#tabs:after, #minitabs:after, .post:after, .navbar:after, fieldset dl:after, ul.topiclist dl:after, ul.linklist:after, dl.polls:after {\n	content: \".\"; \n	display: block; \n	height: 0; \n	clear: both; \n	visibility: hidden;\n}*/\n\n.clearfix, #tabs, #minitabs, fieldset dl, ul.topiclist dl, dl.polls {\n	height: 1%;\n	overflow: hidden;\n}\n\n/* viewtopic fix */\n* html .post {\n	height: 25%;\n	overflow: hidden;\n}\n\n/* navbar fix */\n* html .clearfix, * html .navbar, ul.linklist {\n	height: 4%;\n	overflow: hidden;\n}\n\n/* Simple fix so forum and topic lists always have a min-height set, even in IE6\n	From http://www.dustindiaz.com/min-height-fast-hack */\ndl.icon {\n	min-height: 35px;\n	height: auto !important;\n	height: 35px;\n}\n\n* html li.row dl.icon dt {\n	height: 35px;\n	overflow: visible;\n}\n\n* html #search-box {\n	width: 25%;\n}\n\n/* Correctly clear floating for details on profile view */\n*:first-child+html dl.details dd {\n	margin-left: 30%;\n	float: none;\n}\n\n* html dl.details dd {\n	margin-left: 30%;\n	float: none;\n}\n\n/* Headerbar height fix for IE7 and below */\n* html #site-description p {\n	margin-bottom: 1.0em;\n}\n\n*:first-child+html #site-description p {\n	margin-bottom: 1.0em;\n}\n\n/* #minitabs fix for IE */\n.tabs-container {\n	zoom: 1;\n}\n\n#minitabs {\n	white-space: nowrap;\n	*min-width: 50%;\n}\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for common.css\n-------------------------------------------------------------- */\n\nhtml, body {\n	color: #536482;\n	background-color: #FFFFFF;\n}\n\nh1 {\n	color: #a8c2f0;\n}\n\nh2 {\n	color: #28313F;\n}\n\nh3 {\n	border-bottom-color: #CCCCCC;\n	color: #115098;\n}\n\nhr {\n	border-color: #FFFFFF;\n	border-top-color: #CCCCCC;\n}\n\nhr.dashed {\n	border-top-color: #CCCCCC;\n}\n\n/* Search box\n--------------------------------------------- */\n\n#search-box {\n	color: #FFFFFF;\n}\n\n#search-box #keywords {\n	background-color: #FFF;\n}\n\n#search-box input {\n	border-color: #0075B0;\n}\n\n/* Round cornered boxes and backgrounds\n---------------------------------------- */\n.headerbar {\n	background-color: #fff;\n	/*background-image: url(\"{T_THEME_PATH}/images/bg_header.gif\");*/\n	color: #FFFFFF;\n}\n\n.navbar {\n	background-color: #cadceb;\n}\n\n.forabg {\n	background-color: #0076b1;\n	background-image: url(\"{T_THEME_PATH}/images/bg_list.gif\");\n}\n\n.forumbg {\n	background-color: #12A3EB;\n	background-image: url(\"{T_THEME_PATH}/images/bg_header.gif\");\n}\n\n.panel {\n	background-color: #ECF1F3;\n	color: #28313F;\n}\n\n.post:target .content {\n	color: #000000;\n}\n\n.post:target h3 a {\n	color: #000000;\n}\n\n.bg1	{ background-color: #ECF3F7; }\n.bg2	{ background-color: #e1ebf2;  }\n.bg3	{ background-color: #cadceb; }\n\n.ucprowbg {\n	background-color: #DCDEE2;\n}\n\n.fieldsbg {\n	background-color: #E7E8EA;\n}\n\nspan.corners-top {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.png\");\n}\n\nspan.corners-top span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.png\");\n}\n\nspan.corners-bottom {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.png\");\n}\n\nspan.corners-bottom span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.png\");\n}\n\n/* Horizontal lists\n----------------------------------------*/\n\nul.navlinks {\n	border-bottom-color: #FFFFFF;\n}\n\n/* Table styles\n----------------------------------------*/\ntable.table1 thead th {\n	color: #FFFFFF;\n}\n\ntable.table1 tbody tr {\n	border-color: #BFC1CF;\n}\n\ntable.table1 tbody tr:hover, table.table1 tbody tr.hover {\n	background-color: #CFE1F6;\n	color: #000;\n}\n\ntable.table1 td {\n	color: #536482;\n}\n\ntable.table1 tbody td {\n	border-top-color: #FAFAFA;\n}\n\ntable.table1 tbody th {\n	border-bottom-color: #000000;\n	color: #333333;\n	background-color: #FFFFFF;\n}\n\ntable.info tbody th {\n	color: #000000;\n}\n\n/* Misc layout styles\n---------------------------------------- */\ndl.details dt {\n	color: #000000;\n}\n\ndl.details dd {\n	color: #536482;\n}\n\n.sep {\n	color: #1198D9;\n}\n\n/* Pagination\n---------------------------------------- */\n\n.pagination span strong {\n	color: #FFFFFF;\n	background-color: #4692BF;\n	border-color: #4692BF;\n}\n\n.pagination span a, .pagination span a:link, .pagination span a:visited {\n	color: #5C758C;\n	background-color: #ECEDEE;\n	border-color: #B4BAC0;\n}\n\n.pagination span a:hover {\n	border-color: #368AD2;\n	background-color: #368AD2;\n	color: #FFF;\n}\n\n.pagination span a:active {\n	color: #5C758C;\n	background-color: #ECEDEE;\n	border-color: #B4BAC0;\n}\n\n/* Pagination in viewforum for multipage topics */\n.row .pagination {\n	background-image: url(\"{T_THEME_PATH}/images/icon_pages.gif\");\n}\n\n.row .pagination span a, li.pagination span a {\n	background-color: #FFFFFF;\n}\n\n.row .pagination span a:hover, li.pagination span a:hover {\n	background-color: #368AD2;\n}\n\n/* Miscellaneous styles\n---------------------------------------- */\n\n.copyright {\n	color: #555555;\n}\n\n.error {\n	color: #BC2A4D;\n}\n\n.reported {\n	background-color: #F7ECEF;\n}\n\nli.reported:hover {\n	background-color: #ECD5D8 !important;\n}\n.sticky, .announce {\n	/* you can add a background for stickies and announcements*/\n}\n\ndiv.rules {\n	background-color: #ECD5D8;\n	color: #BC2A4D;\n}\n\np.rules {\n	background-color: #ECD5D8;\n	background-image: none;\n}\n\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for links.css\n-------------------------------------------------------------- */\n\na:link	{ color: #105289; }\na:visited	{ color: #105289; }\na:hover	{ color: #D31141; }\na:active	{ color: #368AD2; }\n\n/* Links on gradient backgrounds */\n#search-box a:link\n{\n    color: #fff;\n}\n.navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {\n    color: #fff;\n}\n\n#search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {\n	color: #666;\n}\n\n#search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {\n	color: #3875ff;\n}\n\n#search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {\n	color: #3875ff;\n}\n\n/* Links for forum/topic lists */\na.forumtitle {\n	color: #105289;\n}\n\n/* a.forumtitle:visited { color: #105289; } */\n\na.forumtitle:hover {\n	color: #BC2A4D;\n}\n\na.forumtitle:active {\n	color: #105289;\n}\n\na.topictitle {\n	color: #105289;\n}\n\n/* a.topictitle:visited { color: #368AD2; } */\n\na.topictitle:hover {\n	color: #BC2A4D;\n}\n\na.topictitle:active {\n	color: #105289;\n}\n\n/* Post body links */\n.postlink {\n	color: #368AD2;\n	border-bottom-color: #368AD2;\n}\n\n.postlink:visited {\n	color: #5D8FBD;\n	border-bottom-color: #5D8FBD;\n}\n\n.postlink:active {\n	color: #368AD2;\n}\n\n.postlink:hover {\n	background-color: #D0E4F6;\n	color: #0D4473;\n}\n\n.signature a, .signature a:visited, .signature a:hover, .signature a:active {\n	background-color: transparent;\n}\n\n/* Profile links */\n.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {\n	color: #105289;\n}\n\n.postprofile a:hover, .postprofile dt.author a:hover {\n	color: #D31141;\n}\n\n.postprofile a:active {\n	color: #105289;\n}\n\n/* Profile searchresults */	\n.search .postprofile a {\n	color: #105289;\n}\n\n.search .postprofile a:hover {\n	color: #D31141;\n}\n\n/* Back to top of page */\na.top {\n	background-image: url(\"{IMG_ICON_BACK_TOP_SRC}\");\n}\n\na.top2 {\n	background-image: url(\"{IMG_ICON_BACK_TOP_SRC}\");\n}\n\n/* Arrow links  */\na.up		{ background-image: url(\"{T_THEME_PATH}/images/arrow_up.gif\") }\na.down		{ background-image: url(\"{T_THEME_PATH}/images/arrow_down.gif\") }\na.left		{ background-image: url(\"{T_THEME_PATH}/images/arrow_left.gif\") }\na.right		{ background-image: url(\"{T_THEME_PATH}/images/arrow_right.gif\") }\n\na.up:hover {\n	background-color: transparent;\n}\n\na.left:hover {\n	color: #368AD2;\n}\n\na.right:hover {\n	color: #368AD2;\n}\n\n\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for content.css\n-------------------------------------------------------------- */\n\nul.forums {\n	background-color: #eef5f9;\n	background-image: url(\"{T_THEME_PATH}/images/gradient.gif\");\n}\n\nul.topiclist li {\n	color: #4C5D77;\n}\n\nul.topiclist dd {\n	border-left-color: #FFFFFF;\n}\n\n.rtl ul.topiclist dd {\n	border-right-color: #fff;\n	border-left-color: transparent;\n}\n\nul.topiclist li.row dt a.subforum.read {\n	background-image: url(\"{IMG_SUBFORUM_READ_SRC}\");\n}\n\nul.topiclist li.row dt a.subforum.unread {\n	background-image: url(\"{IMG_SUBFORUM_UNREAD_SRC}\");\n}\n\nli.row {\n	border-top-color:  #FFFFFF;\n	border-bottom-color: #00608F;\n}\n\nli.row strong {\n	color: #000000;\n}\n\nli.row:hover {\n	background-color: #F6F4D0;\n}\n\nli.row:hover dd {\n	border-left-color: #CCCCCC;\n}\n\n.rtl li.row:hover dd {\n	border-right-color: #CCCCCC;\n	border-left-color: transparent;\n}\n\nli.header dt, li.header dd {\n	color: #FFFFFF;\n}\n\n/* Forum list column styles */\nul.topiclist dd.searchextra {\n	color: #333333;\n}\n\n/* Post body styles\n----------------------------------------*/\n.postbody {\n	color: #333333;\n}\n\n/* Content container styles\n----------------------------------------*/\n.content {\n	color: #333333;\n}\n\n.content h2, .panel h2 {\n	color: #115098;\n	border-bottom-color:  #CCCCCC;\n}\n\ndl.faq dt {\n	color: #333333;\n}\n\n.posthilit {\n	background-color: #F3BFCC;\n	color: #BC2A4D;\n}\n\n/* Post signature */\n.signature {\n	border-top-color: #CCCCCC;\n}\n\n/* Post noticies */\n.notice {\n	border-top-color:  #CCCCCC;\n}\n\n/* BB Code styles\n----------------------------------------*/\n/* Quote block */\nblockquote {\n	background-color: #EBEADD;\n	background-image: url(\"{T_THEME_PATH}/images/quote.gif\");\n	border-color:#DBDBCE;\n}\n\n.rtl blockquote {\n	background-image: url(\"{T_THEME_PATH}/images/quote_rtl.gif\");\n}\n\nblockquote blockquote {\n	/* Nested quotes */\n	background-color:#EFEED9;\n}\n\nblockquote blockquote blockquote {\n	/* Nested quotes */\n	background-color: #EBEADD;\n}\n\n/* Code block */\ndl.codebox {\n	background-color: #FFFFFF;\n	border-color: #C9D2D8;\n}\n\ndl.codebox dt {\n	border-bottom-color:  #CCCCCC;\n}\n\ndl.codebox code {\n	color: #2E8B57;\n}\n\n.syntaxbg		{ color: #FFFFFF; }\n.syntaxcomment	{ color: #FF8000; }\n.syntaxdefault	{ color: #0000BB; }\n.syntaxhtml		{ color: #000000; }\n.syntaxkeyword	{ color: #007700; }\n.syntaxstring	{ color: #DD0000; }\n\n/* Attachments\n----------------------------------------*/\n.attachbox {\n	background-color: #FFFFFF;\n	border-color:  #C9D2D8;\n}\n\n.pm-message .attachbox {\n	background-color: #F2F3F3;\n}\n\n.attachbox dd {\n	border-top-color: #C9D2D8;\n}\n\n.attachbox p {\n	color: #666666;\n}\n\n.attachbox p.stats {\n	color: #666666;\n}\n\n.attach-image img {\n	border-color: #999999;\n}\n\n/* Inline image thumbnails */\n\ndl.file dd {\n	color: #666666;\n}\n\ndl.thumbnail img {\n	border-color: #666666;\n	background-color: #FFFFFF;\n}\n\ndl.thumbnail dd {\n	color: #666666;\n}\n\ndl.thumbnail dt a:hover {\n	background-color: #EEEEEE;\n}\n\ndl.thumbnail dt a:hover img {\n	border-color: #368AD2;\n}\n\n/* Post poll styles\n----------------------------------------*/\n\nfieldset.polls dl {\n	border-top-color: #DCDEE2;\n	color: #666666;\n}\n\nfieldset.polls dl.voted {\n	color: #000000;\n}\n\nfieldset.polls dd div {\n	color: #FFFFFF;\n}\n\n.rtl .pollbar1, .rtl .pollbar2, .rtl .pollbar3, .rtl .pollbar4, .rtl .pollbar5 {\n	border-right-color: transparent;\n}\n\n.pollbar1 {\n	background-color: #AA2346;\n	border-bottom-color: #74162C;\n	border-right-color: #74162C;\n}\n\n.rtl .pollbar1 {\n	border-left-color: #74162C;\n}\n\n.pollbar2 {\n	background-color: #BE1E4A;\n	border-bottom-color: #8C1C38;\n	border-right-color: #8C1C38;\n}\n\n.rtl .pollbar2 {\n	border-left-color: #8C1C38;\n}\n\n.pollbar3 {\n	background-color: #D11A4E;\n	border-bottom-color: #AA2346;\n	border-right-color: #AA2346;\n}\n\n.rtl .pollbar3 {\n	border-left-color: #AA2346;\n}\n\n.pollbar4 {\n	background-color: #E41653;\n	border-bottom-color: #BE1E4A;\n	border-right-color: #BE1E4A;\n}\n\n.rtl .pollbar4 {\n	border-left-color: #BE1E4A;\n}\n\n.pollbar5 {\n	background-color: #F81157;\n	border-bottom-color: #D11A4E;\n	border-right-color: #D11A4E;\n}\n\n.rtl .pollbar5 {\n	border-left-color: #D11A4E;\n}\n\n/* Poster profile block\n----------------------------------------*/\n.postprofile {\n	color: #666666;\n	border-left-color: #FFFFFF;\n}\n\n.rtl .postprofile {\n	border-right-color: #FFFFFF;\n	border-left-color: transparent;\n}\n\n.pm .postprofile {\n	border-left-color: #DDDDDD;\n}\n\n.rtl .pm .postprofile {\n	border-right-color: #DDDDDD;\n	border-left-color: transparent;\n}\n\n.postprofile strong {\n	color: #000000;\n}\n\n.online {\n	background-image: url(\"{IMG_ICON_USER_ONLINE_SRC}\");\n}\n\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for buttons.css\n-------------------------------------------------------------- */\n\n/* Big button images */\n.reply-icon span	{ background-image: url(\"{IMG_BUTTON_TOPIC_REPLY_SRC}\"); }\n.post-icon span		{ background-image: url(\"{IMG_BUTTON_TOPIC_NEW_SRC}\"); }\n.locked-icon span	{ background-image: url(\"{IMG_BUTTON_TOPIC_LOCKED_SRC}\"); }\n.pmreply-icon span	{ background-image: url(\"{IMG_BUTTON_PM_REPLY_SRC}\") ;}\n.newpm-icon span 	{ background-image: url(\"{IMG_BUTTON_PM_NEW_SRC}\") ;}\n.forwardpm-icon span	{ background-image: url(\"{IMG_BUTTON_PM_FORWARD_SRC}\") ;}\n\na.print {\n	background-image: url(\"{T_THEME_PATH}/images/icon_print.gif\");\n}\n\na.sendemail {\n	background-image: url(\"{T_THEME_PATH}/images/icon_sendemail.gif\");\n}\n\na.fontsize {\n	background-image: url(\"{T_THEME_PATH}/images/icon_fontsize.gif\");\n}\n\n/* Icon images\n---------------------------------------- */\n.sitehome						{ background-image: url(\"{T_THEME_PATH}/images/icon_home.gif\"); }\n.icon-faq						{ background-image: url(\"{T_THEME_PATH}/images/icon_faq.gif\"); }\n.icon-members					{ background-image: url(\"{T_THEME_PATH}/images/icon_members.gif\"); }\n.icon-home						{ background-image: url(\"{T_THEME_PATH}/images/icon_home.gif\"); }\n.icon-ucp						{ background-image: url(\"{T_THEME_PATH}/images/icon_ucp.gif\"); }\n.icon-register					{ background-image: url(\"{T_THEME_PATH}/images/icon_register.gif\"); }\n.icon-logout					{ background-image: url(\"{T_THEME_PATH}/images/icon_logout.gif\"); }\n.icon-bookmark					{ background-image: url(\"{T_THEME_PATH}/images/icon_bookmark.gif\"); }\n.icon-bump						{ background-image: url(\"{T_THEME_PATH}/images/icon_bump.gif\"); }\n.icon-subscribe					{ background-image: url(\"{T_THEME_PATH}/images/icon_subscribe.gif\"); }\n.icon-unsubscribe				{ background-image: url(\"{T_THEME_PATH}/images/icon_unsubscribe.gif\"); }\n.icon-pages						{ background-image: url(\"{T_THEME_PATH}/images/icon_pages.gif\"); }\n.icon-search					{ background-image: url(\"{T_THEME_PATH}/images/icon_search.gif\"); }\n\n/* Profile & navigation icons */\n.email-icon, .email-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_EMAIL_SRC}\"); }\n.aim-icon, .aim-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_AIM_SRC}\"); }\n.yahoo-icon, .yahoo-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_YAHOO_SRC}\"); }\n.web-icon, .web-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_WWW_SRC}\"); }\n.msnm-icon, .msnm-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_MSNM_SRC}\"); }\n.icq-icon, .icq-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_ICQ_SRC}\"); }\n.jabber-icon, .jabber-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_JABBER_SRC}\"); }\n.pm-icon, .pm-icon a				{ background-image: url(\"{IMG_ICON_CONTACT_PM_SRC}\"); }\n.quote-icon, .quote-icon a		{ background-image: url(\"{IMG_ICON_POST_QUOTE_SRC}\"); }\n\n/* Moderator icons */\n.report-icon, .report-icon a		{ background-image: url(\"{IMG_ICON_POST_REPORT_SRC}\"); }\n.edit-icon, .edit-icon a			{ background-image: url(\"{IMG_ICON_POST_EDIT_SRC}\"); }\n.delete-icon, .delete-icon a		{ background-image: url(\"{IMG_ICON_POST_DELETE_SRC}\"); }\n.info-icon, .info-icon a			{ background-image: url(\"{IMG_ICON_POST_INFO_SRC}\"); }\n.warn-icon, .warn-icon a			{ background-image: url(\"{IMG_ICON_USER_WARN_SRC}\"); } /* Need updated warn icon */\n\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for cp.css\n-------------------------------------------------------------- */\n\n/* Main CP box\n----------------------------------------*/\n\n#cp-main h3, #cp-main hr, #cp-menu hr {\n	border-color: #A4B3BF;\n}\n\n#cp-main .panel li.row {\n	border-bottom-color: #B5C1CB;\n	border-top-color: #F9F9F9;\n}\n\nul.cplist {\n	border-top-color: #B5C1CB;\n}\n\n#cp-main .panel li.header dd, #cp-main .panel li.header dt {\n	color: #000000;\n}\n\n#cp-main table.table1 thead th {\n	color: #333333;\n	border-bottom-color: #333333;\n}\n\n#cp-main .pm-message {\n	border-color: #DBDEE2;\n	background-color: #FFFFFF;\n}\n\n/* CP tabbed menu\n----------------------------------------*/\n#tabs a {\n	background-image: url(\"{T_THEME_PATH}/images/bg_tabs1.gif\");\n}\n\n#tabs a span {\n	background-image: url(\"{T_THEME_PATH}/images/bg_tabs2.gif\");\n	color: #536482;\n}\n\n#tabs a:hover span {\n	color: #BC2A4D;\n}\n\n#tabs .activetab a {\n	border-bottom-color: #CADCEB;\n}\n\n#tabs .activetab a span {\n	color: #333333;\n}\n\n#tabs .activetab a:hover span {\n	color: #000000;\n}\n\n/* Mini tabbed menu used in MCP\n----------------------------------------*/\n#minitabs li {\n	background-color: #E1EBF2;\n}\n\n#minitabs li.activetab {\n	background-color: #F9F9F9;\n}\n\n#minitabs li.activetab a, #minitabs li.activetab a:hover {\n	color: #333333;\n}\n\n/* UCP navigation menu\n----------------------------------------*/\n\n/* Link styles for the sub-section links */\n#navigation a {\n	color: #333;\n	background-color: #B2C2CF;\n	background-image: url(\"{T_THEME_PATH}/images/bg_menu.gif\");\n}\n\n.rtl #navigation a {\n	background-image: url(\"{T_THEME_PATH}/images/bg_menu_rtl.gif\");\n	background-position: 0 100%;\n}\n\n#navigation a:hover {\n	background-image: none;\n	background-color: #aabac6;\n	color: #BC2A4D;\n}\n\n#navigation #active-subsection a {\n	color: #D31141;\n	background-color: #F9F9F9;\n	background-image: none;\n}\n\n#navigation #active-subsection a:hover {\n	color: #D31141;\n}\n\n/* Preferences pane layout\n----------------------------------------*/\n#cp-main h2 {\n	color: #333333;\n}\n\n#cp-main .panel {\n	background-color: #F9F9F9;\n}\n\n#cp-main .pm {\n	background-color: #FFFFFF;\n}\n\n#cp-main span.corners-top, #cp-menu span.corners-top {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left2.gif\");\n}\n\n#cp-main span.corners-top span, #cp-menu span.corners-top span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right2.gif\");\n}\n\n#cp-main span.corners-bottom, #cp-menu span.corners-bottom {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left2.gif\");\n}\n\n#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right2.gif\");\n}\n\n/* Topicreview */\n#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\");\n}\n\n#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\");\n}\n\n#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\");\n}\n\n#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\");\n}\n\n/* Friends list */\n.cp-mini {\n	background-color: #eef5f9;\n}\n\ndl.mini dt {\n	color: #425067;\n}\n\n/* PM Styles\n----------------------------------------*/\n/* PM Message history */\n.current {\n	color: #000000 !important;\n}\n\n/* PM marking colours */\n.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {\n	border-left-color: #BC2A4D;\n	border-right-color: #BC2A4D;\n}\n\n.pmlist li.pm_marked_colour, .pm_marked_colour {\n	border-color: #FF6600;\n}\n\n.pmlist li.pm_replied_colour, .pm_replied_colour {\n	border-color: #A9B8C2;\n}\n\n.pmlist li.pm_friend_colour, .pm_friend_colour {\n	border-color: #5D8FBD;\n}\n\n.pmlist li.pm_foe_colour, .pm_foe_colour {\n	border-color: #000000;\n}\n\n/* Avatar gallery */\n#gallery label {\n	background-color: #FFFFFF;\n	border-color: #CCC;\n}\n\n#gallery label:hover {\n	background-color: #EEE;\n}\n\n/*  	\n--------------------------------------------------------------\nColours and backgrounds for forms.css\n-------------------------------------------------------------- */\n\n/* General form styles\n----------------------------------------*/\nselect {\n	border-color: #666666;\n	background-color: #FAFAFA;\n	color: #000;\n}\n\nlabel {\n	color: #425067;\n}\n\noption.disabled-option {\n	color: graytext;\n}\n\n/* Definition list layout for forms\n---------------------------------------- */\ndd label {\n	color: #333;\n}\n\n/* Hover effects */\nfieldset dl:hover dt label {\n	color: #000000;\n}\n\nfieldset.fields2 dl:hover dt label {\n	color: inherit;\n}\n\n/* Quick-login on index page */\nfieldset.quick-login input.inputbox {\n	background-color: #F2F3F3;\n}\n\n/* Posting page styles\n----------------------------------------*/\n\n#message-box textarea {\n	color: #333333;\n}\n\n/* Input field styles\n---------------------------------------- */\n.inputbox {\n	background-color: #FFFFFF; \n	border-color: #B4BAC0;\n	color: #333333;\n}\n\n.inputbox:hover {\n	border-color: #11A3EA;\n}\n\n.inputbox:focus {\n	border-color: #11A3EA;\n	color: #0F4987;\n}\n\n/* Form button styles\n---------------------------------------- */\n\na.button1, input.button1, input.button3, a.button2, input.button2 {\n	color: #000;\n	background-color: #FAFAFA;\n	background-image: url(\"{T_THEME_PATH}/images/bg_button.gif\");\n}\n\na.button1, input.button1 {\n	border-color: #666666;\n}\n\ninput.button3 {\n	background-image: none;\n}\n\n/* Alternative button */\na.button2, input.button2, input.button3 {\n	border-color: #666666;\n}\n\n/* <a> button in the style of the form buttons */\na.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {\n	color: #000000;\n}\n\n/* Hover states */\na.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {\n	border-color: #BC2A4D;\n	color: #BC2A4D;\n}\n\ninput.search {\n	background-image: url(\"{T_THEME_PATH}/images/icon_textbox_search.gif\");\n}\n\ninput.disabled {\n	color: #666666;\n}\n');
/*!40000 ALTER TABLE `phpbb_styles_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics`
--

DROP TABLE IF EXISTS `phpbb_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics` (
  `topic_id` mediumint(8) unsigned NOT NULL auto_increment,
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `icon_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_attachment` tinyint(1) unsigned NOT NULL default '0',
  `topic_approved` tinyint(1) unsigned NOT NULL default '1',
  `topic_reported` tinyint(1) unsigned NOT NULL default '0',
  `topic_title` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `topic_poster` mediumint(8) unsigned NOT NULL default '0',
  `topic_time` int(11) unsigned NOT NULL default '0',
  `topic_time_limit` int(11) unsigned NOT NULL default '0',
  `topic_views` mediumint(8) unsigned NOT NULL default '0',
  `topic_replies` mediumint(8) unsigned NOT NULL default '0',
  `topic_replies_real` mediumint(8) unsigned NOT NULL default '0',
  `topic_status` tinyint(3) NOT NULL default '0',
  `topic_type` tinyint(3) NOT NULL default '0',
  `topic_first_post_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_first_poster_name` varchar(255) collate utf8_bin NOT NULL default '',
  `topic_first_poster_colour` varchar(6) collate utf8_bin NOT NULL default '',
  `topic_last_post_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_last_poster_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_last_poster_name` varchar(255) collate utf8_bin NOT NULL default '',
  `topic_last_poster_colour` varchar(6) collate utf8_bin NOT NULL default '',
  `topic_last_post_subject` varchar(255) collate utf8_bin NOT NULL default '',
  `topic_last_post_time` int(11) unsigned NOT NULL default '0',
  `topic_last_view_time` int(11) unsigned NOT NULL default '0',
  `topic_moved_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_bumped` tinyint(1) unsigned NOT NULL default '0',
  `topic_bumper` mediumint(8) unsigned NOT NULL default '0',
  `poll_title` varchar(255) collate utf8_bin NOT NULL default '',
  `poll_start` int(11) unsigned NOT NULL default '0',
  `poll_length` int(11) unsigned NOT NULL default '0',
  `poll_max_options` tinyint(4) NOT NULL default '1',
  `poll_last_vote` int(11) unsigned NOT NULL default '0',
  `poll_vote_change` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `forum_id_type` (`forum_id`,`topic_type`),
  KEY `last_post_time` (`topic_last_post_time`),
  KEY `topic_approved` (`topic_approved`),
  KEY `forum_appr_last` (`forum_id`,`topic_approved`,`topic_last_post_id`),
  KEY `fid_time_moved` (`forum_id`,`topic_last_post_time`,`topic_moved_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics`
--

LOCK TABLES `phpbb_topics` WRITE;
/*!40000 ALTER TABLE `phpbb_topics` DISABLE KEYS */;
INSERT INTO `phpbb_topics` VALUES (9,2,0,0,0,0,'medication risperdal tardive dyskinesia',77,1367840964,0,0,0,0,0,0,9,'coxchese','',9,77,'coxchese','','medication risperdal tardive dyskinesia',1367840964,1367840964,0,0,0,'',0,0,1,0,0),(10,2,0,0,0,0,'hi all man',78,1368632641,0,1,0,0,0,0,10,'Toissiotoma','',10,78,'Toissiotoma','','hi all man',1368632641,1368794025,0,0,0,'',0,0,1,0,0);
/*!40000 ALTER TABLE `phpbb_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_posted`
--

DROP TABLE IF EXISTS `phpbb_topics_posted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_posted` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_posted` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_posted`
--

LOCK TABLES `phpbb_topics_posted` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_posted` DISABLE KEYS */;
INSERT INTO `phpbb_topics_posted` VALUES (77,9,1),(78,10,1);
/*!40000 ALTER TABLE `phpbb_topics_posted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_track`
--

DROP TABLE IF EXISTS `phpbb_topics_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_track` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` mediumint(8) unsigned NOT NULL default '0',
  `mark_time` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`topic_id`),
  KEY `topic_id` (`topic_id`),
  KEY `forum_id` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_track`
--

LOCK TABLES `phpbb_topics_track` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_track` DISABLE KEYS */;
INSERT INTO `phpbb_topics_track` VALUES (78,10,2,1368632642),(77,9,2,1367840964),(58,10,2,1368632641);
/*!40000 ALTER TABLE `phpbb_topics_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_watch`
--

DROP TABLE IF EXISTS `phpbb_topics_watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_watch` (
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `notify_status` tinyint(1) unsigned NOT NULL default '0',
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`),
  KEY `notify_stat` (`notify_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_watch`
--

LOCK TABLES `phpbb_topics_watch` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_watch` DISABLE KEYS */;
INSERT INTO `phpbb_topics_watch` VALUES (10,78,0),(9,77,0);
/*!40000 ALTER TABLE `phpbb_topics_watch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_user_group`
--

DROP TABLE IF EXISTS `phpbb_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_user_group` (
  `group_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `group_leader` tinyint(1) unsigned NOT NULL default '0',
  `user_pending` tinyint(1) unsigned NOT NULL default '1',
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `group_leader` (`group_leader`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_user_group`
--

LOCK TABLES `phpbb_user_group` WRITE;
/*!40000 ALTER TABLE `phpbb_user_group` DISABLE KEYS */;
INSERT INTO `phpbb_user_group` VALUES (1,1,0,0),(2,2,0,0),(4,2,0,0),(5,2,1,0),(6,3,0,0),(6,4,0,0),(6,5,0,0),(6,6,0,0),(6,7,0,0),(6,8,0,0),(6,9,0,0),(6,10,0,0),(6,11,0,0),(6,12,0,0),(6,13,0,0),(6,14,0,0),(6,15,0,0),(6,16,0,0),(6,17,0,0),(6,18,0,0),(6,19,0,0),(6,20,0,0),(6,21,0,0),(6,22,0,0),(6,23,0,0),(6,24,0,0),(6,25,0,0),(6,26,0,0),(6,27,0,0),(6,28,0,0),(6,29,0,0),(6,30,0,0),(6,31,0,0),(6,32,0,0),(6,33,0,0),(6,34,0,0),(6,35,0,0),(6,36,0,0),(6,37,0,0),(6,38,0,0),(6,39,0,0),(6,40,0,0),(6,41,0,0),(6,42,0,0),(6,43,0,0),(6,44,0,0),(6,45,0,0),(6,46,0,0),(6,47,0,0),(6,48,0,0),(6,49,0,0),(6,50,0,0),(6,51,0,0),(6,52,0,0),(6,53,0,0),(7,80,0,0),(2,80,0,0),(7,79,0,0),(2,79,0,0),(7,78,0,0),(2,78,0,0),(7,77,0,0),(2,77,0,0),(2,58,0,0),(7,58,0,0),(7,76,0,0),(2,76,0,0),(7,75,0,0),(2,75,0,0),(7,74,0,0),(2,74,0,0),(7,73,0,0),(2,73,0,0),(7,72,0,0),(2,72,0,0),(7,71,0,0),(2,71,0,0),(4,70,0,0),(5,58,0,0),(5,68,0,0),(5,70,0,0),(2,67,0,0),(7,67,0,0),(2,68,0,0),(7,68,0,0),(7,70,0,0),(2,70,0,0),(2,81,0,0),(7,81,0,0);
/*!40000 ALTER TABLE `phpbb_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_users`
--

DROP TABLE IF EXISTS `phpbb_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_users` (
  `user_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_type` tinyint(2) NOT NULL default '0',
  `group_id` mediumint(8) unsigned NOT NULL default '3',
  `user_permissions` mediumtext collate utf8_bin NOT NULL,
  `user_perm_from` mediumint(8) unsigned NOT NULL default '0',
  `user_ip` varchar(40) collate utf8_bin NOT NULL default '',
  `user_regdate` int(11) unsigned NOT NULL default '0',
  `username` varchar(255) collate utf8_bin NOT NULL default '',
  `username_clean` varchar(255) collate utf8_bin NOT NULL default '',
  `user_password` varchar(40) collate utf8_bin NOT NULL default '',
  `user_passchg` int(11) unsigned NOT NULL default '0',
  `user_pass_convert` tinyint(1) unsigned NOT NULL default '0',
  `user_email` varchar(100) collate utf8_bin NOT NULL default '',
  `user_email_hash` bigint(20) NOT NULL default '0',
  `user_birthday` varchar(10) collate utf8_bin NOT NULL default '',
  `user_lastvisit` int(11) unsigned NOT NULL default '0',
  `user_lastmark` int(11) unsigned NOT NULL default '0',
  `user_lastpost_time` int(11) unsigned NOT NULL default '0',
  `user_lastpage` varchar(200) collate utf8_bin NOT NULL default '',
  `user_last_confirm_key` varchar(10) collate utf8_bin NOT NULL default '',
  `user_last_search` int(11) unsigned NOT NULL default '0',
  `user_warnings` tinyint(4) NOT NULL default '0',
  `user_last_warning` int(11) unsigned NOT NULL default '0',
  `user_login_attempts` tinyint(4) NOT NULL default '0',
  `user_inactive_reason` tinyint(2) NOT NULL default '0',
  `user_inactive_time` int(11) unsigned NOT NULL default '0',
  `user_posts` mediumint(8) unsigned NOT NULL default '0',
  `user_lang` varchar(30) collate utf8_bin NOT NULL default '',
  `user_timezone` decimal(5,2) NOT NULL default '0.00',
  `user_dst` tinyint(1) unsigned NOT NULL default '0',
  `user_dateformat` varchar(30) collate utf8_bin NOT NULL default 'd M Y H:i',
  `user_style` mediumint(8) unsigned NOT NULL default '0',
  `user_rank` mediumint(8) unsigned NOT NULL default '0',
  `user_colour` varchar(6) collate utf8_bin NOT NULL default '',
  `user_new_privmsg` int(4) NOT NULL default '0',
  `user_unread_privmsg` int(4) NOT NULL default '0',
  `user_last_privmsg` int(11) unsigned NOT NULL default '0',
  `user_message_rules` tinyint(1) unsigned NOT NULL default '0',
  `user_full_folder` int(11) NOT NULL default '-3',
  `user_emailtime` int(11) unsigned NOT NULL default '0',
  `user_topic_show_days` smallint(4) unsigned NOT NULL default '0',
  `user_topic_sortby_type` varchar(1) collate utf8_bin NOT NULL default 't',
  `user_topic_sortby_dir` varchar(1) collate utf8_bin NOT NULL default 'd',
  `user_post_show_days` smallint(4) unsigned NOT NULL default '0',
  `user_post_sortby_type` varchar(1) collate utf8_bin NOT NULL default 't',
  `user_post_sortby_dir` varchar(1) collate utf8_bin NOT NULL default 'a',
  `user_notify` tinyint(1) unsigned NOT NULL default '0',
  `user_notify_pm` tinyint(1) unsigned NOT NULL default '1',
  `user_notify_type` tinyint(4) NOT NULL default '0',
  `user_allow_pm` tinyint(1) unsigned NOT NULL default '1',
  `user_allow_viewonline` tinyint(1) unsigned NOT NULL default '1',
  `user_allow_viewemail` tinyint(1) unsigned NOT NULL default '1',
  `user_allow_massemail` tinyint(1) unsigned NOT NULL default '1',
  `user_options` int(11) unsigned NOT NULL default '230271',
  `user_avatar` varchar(255) collate utf8_bin NOT NULL default '',
  `user_avatar_type` tinyint(2) NOT NULL default '0',
  `user_avatar_width` smallint(4) unsigned NOT NULL default '0',
  `user_avatar_height` smallint(4) unsigned NOT NULL default '0',
  `user_sig` mediumtext collate utf8_bin NOT NULL,
  `user_sig_bbcode_uid` varchar(8) collate utf8_bin NOT NULL default '',
  `user_sig_bbcode_bitfield` varchar(255) collate utf8_bin NOT NULL default '',
  `user_from` varchar(100) collate utf8_bin NOT NULL default '',
  `user_icq` varchar(15) collate utf8_bin NOT NULL default '',
  `user_aim` varchar(255) collate utf8_bin NOT NULL default '',
  `user_yim` varchar(255) collate utf8_bin NOT NULL default '',
  `user_msnm` varchar(255) collate utf8_bin NOT NULL default '',
  `user_jabber` varchar(255) collate utf8_bin NOT NULL default '',
  `user_website` varchar(200) collate utf8_bin NOT NULL default '',
  `user_occ` text collate utf8_bin NOT NULL,
  `user_interests` text collate utf8_bin NOT NULL,
  `user_actkey` varchar(32) collate utf8_bin NOT NULL default '',
  `user_newpasswd` varchar(40) collate utf8_bin NOT NULL default '',
  `user_form_salt` varchar(32) collate utf8_bin NOT NULL default '',
  `user_new` tinyint(1) unsigned NOT NULL default '1',
  `user_reminded` tinyint(4) NOT NULL default '0',
  `user_reminded_time` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username_clean` (`username_clean`),
  KEY `user_birthday` (`user_birthday`),
  KEY `user_email_hash` (`user_email_hash`),
  KEY `user_type` (`user_type`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_users`
--

LOCK TABLES `phpbb_users` WRITE;
/*!40000 ALTER TABLE `phpbb_users` DISABLE KEYS */;
INSERT INTO `phpbb_users` VALUES (1,2,1,'00000000003khra3nk\ni1cjyo000000\ni1cjyo000000',0,'',1360014818,'Anonymous','anonymous','',0,0,'',0,'',0,0,0,'','3JBZKANB82',0,0,0,0,0,0,0,'en',0.00,0,'d M Y H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','e3273230d177dfb0',1,0,0),(2,3,5,'zik0zjzik0zjzik0xs\ni1cjyo000000\nzik0zjzhb2tc',0,'127.0.0.1',1360014818,'administrator','administrator','$H$9bLcfTdSdNyrQjqva8Bbb5hEYelee1/',0,0,'m.jankowski@miart.pl',332594801820,'',1364500855,0,0,'index.php','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,1,'AA0000',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','812fa42e21b28140',1,0,0),(3,2,6,'',0,'',1360014854,'AdsBot [Google]','adsbot [google]','',1360014854,0,'',0,'',0,1360014854,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','8ad7bdc5bf845a65',0,0,0),(4,2,6,'',0,'',1360014855,'Alexa [Bot]','alexa [bot]','',1360014855,0,'',0,'',0,1360014855,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','1bd8b28241be41cb',0,0,0),(5,2,6,'',0,'',1360014855,'Alta Vista [Bot]','alta vista [bot]','',1360014855,0,'',0,'',0,1360014855,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','27b1ae01cb387895',0,0,0),(6,2,6,'',0,'',1360014855,'Ask Jeeves [Bot]','ask jeeves [bot]','',1360014855,0,'',0,'',0,1360014855,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','e09df466441c2ae0',0,0,0),(7,2,6,'',0,'',1360014855,'Baidu [Spider]','baidu [spider]','',1360014855,0,'',0,'',0,1360014855,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','8262be397a60b710',0,0,0),(8,2,6,'\ni1cjyo000000\ni1cjr4000000',0,'',1360014856,'Bing [Bot]','bing [bot]','',1360014856,0,'',0,'',1372338979,1360014856,0,'faq.php','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','32840d5bc4b9687b',0,0,0),(9,2,6,'\ni1cjyo000000\ni1cjr4000000',0,'',1360014856,'Exabot [Bot]','exabot [bot]','',1360014856,0,'',0,'',1372444505,1360014856,0,'index.php','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','cd56b1ae8da574ed',0,0,0),(10,2,6,'',0,'',1360014856,'FAST Enterprise [Crawler]','fast enterprise [crawler]','',1360014856,0,'',0,'',0,1360014856,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','fa8dbbbf1896a5c7',0,0,0),(11,2,6,'',0,'',1360014856,'FAST WebCrawler [Crawler]','fast webcrawler [crawler]','',1360014856,0,'',0,'',0,1360014856,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','f92f0389883a1772',0,0,0),(12,2,6,'',0,'',1360014857,'Francis [Bot]','francis [bot]','',1360014857,0,'',0,'',0,1360014857,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c39174ef37c14f2e',0,0,0),(13,2,6,'',0,'',1360014857,'Gigabot [Bot]','gigabot [bot]','',1360014857,0,'',0,'',0,1360014857,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6be04af8a03b32ac',0,0,0),(14,2,6,'',0,'',1360014857,'Google Adsense [Bot]','google adsense [bot]','',1360014857,0,'',0,'',0,1360014857,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','b83591aeff406952',0,0,0),(15,2,6,'',0,'',1360014857,'Google Desktop','google desktop','',1360014857,0,'',0,'',0,1360014857,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','927c7e5bde37dfca',0,0,0),(16,2,6,'',0,'',1360014858,'Google Feedfetcher','google feedfetcher','',1360014858,0,'',0,'',0,1360014858,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','97ffb17acc890d17',0,0,0),(17,2,6,'\ni1cjyo000000\ni1cjr4000000',0,'',1360014858,'Google [Bot]','google [bot]','',1360014858,0,'',0,'',1372453510,1360014858,0,'viewforum.php?f=1','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c4ab49ddf585d3c0',0,0,0),(18,2,6,'',0,'',1360014858,'Heise IT-Markt [Crawler]','heise it-markt [crawler]','',1360014858,0,'',0,'',0,1360014858,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','76a99470f9d92c3e',0,0,0),(19,2,6,'',0,'',1360014858,'Heritrix [Crawler]','heritrix [crawler]','',1360014858,0,'',0,'',0,1360014858,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','915917c76ad81f28',0,0,0),(20,2,6,'',0,'',1360014859,'IBM Research [Bot]','ibm research [bot]','',1360014859,0,'',0,'',0,1360014859,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','062515ff13651c6f',0,0,0),(21,2,6,'',0,'',1360014859,'ICCrawler - ICjobs','iccrawler - icjobs','',1360014859,0,'',0,'',0,1360014859,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','beb0625d0a774dfb',0,0,0),(22,2,6,'',0,'',1360014859,'ichiro [Crawler]','ichiro [crawler]','',1360014859,0,'',0,'',0,1360014859,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','11ebd7da2e4e1cf0',0,0,0),(23,2,6,'',0,'',1360014859,'Majestic-12 [Bot]','majestic-12 [bot]','',1360014859,0,'',0,'',1361503539,1360014859,0,'memberlist.php?mode=viewprofile&u=2','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','509d16d97e290ed2',0,0,0),(24,2,6,'',0,'',1360014860,'Metager [Bot]','metager [bot]','',1360014860,0,'',0,'',0,1360014860,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','fd9a0e3265bd6fc3',0,0,0),(25,2,6,'',0,'',1360014860,'MSN NewsBlogs','msn newsblogs','',1360014860,0,'',0,'',0,1360014860,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','9dd67b9ff2e5b018',0,0,0),(26,2,6,'',0,'',1360014860,'MSN [Bot]','msn [bot]','',1360014860,0,'',0,'',0,1360014860,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','d028b44497de9391',0,0,0),(27,2,6,'',0,'',1360014860,'MSNbot Media','msnbot media','',1360014860,0,'',0,'',0,1360014860,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','0b12d03291781fd4',0,0,0),(28,2,6,'',0,'',1360014861,'NG-Search [Bot]','ng-search [bot]','',1360014861,0,'',0,'',0,1360014861,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','3671a54caef4a59c',0,0,0),(29,2,6,'',0,'',1360014861,'Nutch [Bot]','nutch [bot]','',1360014861,0,'',0,'',0,1360014861,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6185198d243b4d64',0,0,0),(30,2,6,'',0,'',1360014861,'Nutch/CVS [Bot]','nutch/cvs [bot]','',1360014861,0,'',0,'',0,1360014861,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','595d81fc9a07e85b',0,0,0),(31,2,6,'',0,'',1360014861,'OmniExplorer [Bot]','omniexplorer [bot]','',1360014861,0,'',0,'',0,1360014861,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','2f453dd7733abeba',0,0,0),(32,2,6,'',0,'',1360014862,'Online link [Validator]','online link [validator]','',1360014862,0,'',0,'',0,1360014862,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6ea05199ad21f014',0,0,0),(33,2,6,'',0,'',1360014862,'psbot [Picsearch]','psbot [picsearch]','',1360014862,0,'',0,'',0,1360014862,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','0054caf355d2d231',0,0,0),(34,2,6,'',0,'',1360014862,'Seekport [Bot]','seekport [bot]','',1360014862,0,'',0,'',0,1360014862,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','988f35d848f60b5f',0,0,0),(35,2,6,'',0,'',1360014862,'Sensis [Crawler]','sensis [crawler]','',1360014862,0,'',0,'',0,1360014862,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','07ae878d14f4c844',0,0,0),(36,2,6,'',0,'',1360014863,'SEO Crawler','seo crawler','',1360014863,0,'',0,'',0,1360014863,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','7df14a4e7db4e142',0,0,0),(37,2,6,'',0,'',1360014863,'Seoma [Crawler]','seoma [crawler]','',1360014863,0,'',0,'',0,1360014863,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','640c4cc48156af85',0,0,0),(38,2,6,'',0,'',1360014863,'SEOSearch [Crawler]','seosearch [crawler]','',1360014863,0,'',0,'',0,1360014863,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','a227d6eedc76d19d',0,0,0),(39,2,6,'',0,'',1360014864,'Snappy [Bot]','snappy [bot]','',1360014864,0,'',0,'',0,1360014864,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','57f3e29dd3d56543',0,0,0),(40,2,6,'',0,'',1360014864,'Steeler [Crawler]','steeler [crawler]','',1360014864,0,'',0,'',0,1360014864,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','a959a7eb6f20d167',0,0,0),(41,2,6,'',0,'',1360014864,'Synoo [Bot]','synoo [bot]','',1360014864,0,'',0,'',0,1360014864,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','3ce6702eecb6df1e',0,0,0),(42,2,6,'',0,'',1360014864,'Telekom [Bot]','telekom [bot]','',1360014864,0,'',0,'',0,1360014864,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','74c0cd601652fea2',0,0,0),(43,2,6,'',0,'',1360014864,'TurnitinBot [Bot]','turnitinbot [bot]','',1360014864,0,'',0,'',0,1360014864,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','fe51b32766b8fc7a',0,0,0),(44,2,6,'',0,'',1360014865,'Voyager [Bot]','voyager [bot]','',1360014865,0,'',0,'',0,1360014865,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','628db46537fc8886',0,0,0),(45,2,6,'',0,'',1360014865,'W3 [Sitesearch]','w3 [sitesearch]','',1360014865,0,'',0,'',0,1360014865,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','9a5ace8da72d2ef6',0,0,0),(46,2,6,'',0,'',1360014865,'W3C [Linkcheck]','w3c [linkcheck]','',1360014865,0,'',0,'',0,1360014865,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','aa7cf60f5fa24d62',0,0,0),(47,2,6,'',0,'',1360014865,'W3C [Validator]','w3c [validator]','',1360014865,0,'',0,'',0,1360014865,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c07645c7e517ddb6',0,0,0),(48,2,6,'',0,'',1360014866,'WiseNut [Bot]','wisenut [bot]','',1360014866,0,'',0,'',0,1360014866,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','17f227a946ac9e5b',0,0,0),(49,2,6,'',0,'',1360014866,'YaCy [Bot]','yacy [bot]','',1360014866,0,'',0,'',0,1360014866,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','e0a29e8260795757',0,0,0),(50,2,6,'',0,'',1360014866,'Yahoo MMCrawler [Bot]','yahoo mmcrawler [bot]','',1360014866,0,'',0,'',0,1360014866,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','291a85f0d17cc70e',0,0,0),(51,2,6,'',0,'',1360014866,'Yahoo Slurp [Bot]','yahoo slurp [bot]','',1360014866,0,'',0,'',0,1360014866,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','06b9d96df056e7ca',0,0,0),(52,2,6,'',0,'',1360014867,'Yahoo [Bot]','yahoo [bot]','',1360014867,0,'',0,'',0,1360014867,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','8d2b2b388b503169',0,0,0),(53,2,6,'',0,'',1360014867,'YahooSeeker [Bot]','yahooseeker [bot]','',1360014867,0,'',0,'',0,1360014867,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','82e900cafcd6c08b',0,0,0),(71,1,2,'',0,'178.210.216.140',1364768115,'icogia','icogia','$H$9dB98jPQVwWRvpQsB/pVaaW4CNAClp/',1364768115,0,'abobbyittay@cmail.com',231466663821,'',0,1364768115,0,'','',0,0,0,0,1,1364768115,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','7AS6JWP','','639d3bb1caecea51',1,0,0),(58,3,5,'005m9rzik0zjw6ovy8\ni1cjyo000000\nziimf3zhb2tc',0,'153.19.102.13',1362046256,'lulkiewicz','lulkiewicz','$H$9YQzQu3wnq3Ipdr95mw3pdJIRj5qVn/',1362046256,0,'alulkiewicz@gumed.edu.pl',347871083324,'',1368794055,1362046256,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,1,'AA0000',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','223b4eb62d90198b',1,0,0),(72,1,2,'',0,'178.210.216.140',1365166883,'IcexCr','icexcr','$H$9FBEKeou2htSHuCn/BQIRHev4a1oZJ/',1365166883,0,'annatyndexta@ymail.net',36189749822,'',0,1365166883,0,'','',0,0,0,0,1,1365166883,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','1HAVVAM6','','1a788b1ba83abe66',1,0,0),(73,0,2,'00000000006xrqeiww\ni1cjyo000000\nqlaq52000000',0,'46.246.38.200',1365507521,'Unakheiccah','unakheiccah','$H$9fIdKrJl2kkaqSwthQyuqe0lfcL6M/1',1365507521,0,'profiledoxr@hotmail.com',264489315623,' 2- 1-1984',1365508536,1365507521,0,'posting.php?mode=post&f=2','',0,0,0,0,0,0,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','Tanzania','345436622','','','','','','Pharmaceutical, biotech','Religion, spiritual','','','821581c686fa044c',1,0,0),(74,1,2,'',0,'92.113.53.102',1365641689,'MockBl','mockbl','$H$9ErPycQwrNXDpWlqds4polh4zpZNlt/',1365641689,0,'velesyjuiory@ymail.net',196248611722,'',0,1365641689,0,'','',0,0,0,0,1,1365641689,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','3M92Q6','','e2bdca181634cd75',1,0,0),(75,1,2,'',0,'95.134.77.164',1366244816,'Illere','illere','$H$9E4MV55BWRggWl69V9wMaHC6HcVQWZ/',1366244816,0,'pitttts@ymail.com',1606563117,'',0,1366244816,0,'','',0,0,0,0,1,1366244816,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','10FSOQ49','','cdabbd63a5126cb8',1,0,0),(76,1,2,'',0,'178.210.216.140',1366761991,'lawkni','lawkni','$H$9vHGq26RafTn4QbFoqmA0nqRczSBSu.',1366761991,0,'aalexeygooglty@e-mail.com',153314998825,'',0,1366761991,0,'','',0,0,0,0,1,1366761991,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','10CVQ0SQ','','9f6c1581a9fe59ed',1,0,0),(77,0,2,'00000000006xrqeiww\ni1cjyo000000\nqlaq52000000',0,'178.32.217.217',1367517981,'coxchese','coxchese','$H$98UH4mKiiLcbU69XEDxsF5MUzG3SNN1',1367517981,0,'coxchesestdut@yandex.ru',315151227523,'',1367840966,1367517981,1367840964,'memberlist.php?mode=viewprofile&u=77','',0,0,0,0,0,0,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','df1d0339c2e7ee04',1,0,0),(78,0,2,'00000000006xrqeiww\ni1cjyo000000\nqlaq52000000',0,'46.118.125.111',1368408552,'Toissiotoma','toissiotoma','$H$9EBcNigrjR8OzyP4u69bfkEIY0.foY0',1368408552,0,'wroxerserge@hotmail.com',258936044923,'23- 8-1988',1368408901,1368408552,1368632641,'ucp.php?i=profile&mode=profile_info','',0,0,0,0,0,0,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','Puerto Rico','334453887','','','','','','Estate','Watching sports','','','09dee732ca1af7e0',1,0,0),(79,1,2,'',0,'178.210.216.140',1370122378,'Scavyl','scavyl','$H$9X8xdyu/niMwkRYod13rtuGy2p3Ia0.',1370122378,0,'dizycttsa@cmail.com',105240731819,'',0,1370122378,0,'','',0,0,0,0,1,1370122378,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','I48DT4UK7P','','d2ff28bf0fc9d471',1,0,0),(80,1,2,'',0,'178.210.216.140',1370527699,'fanget','fanget','$H$9RBOr7rRjTeG/PmfrWkvIEM6lHpYWR.',1370527699,0,'bgegorasfrances@gotmail.com',159896974427,'',0,1370527699,0,'','',0,0,0,0,1,1370527699,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','2NGCOOPL','','f6919684461705aa',1,0,0),(81,0,2,'00000000006xrqeiww\ni1cjyo000000\nqlaq52000000',0,'217.132.30.60',1370849160,'AaAntiqueimigue','aaantiqueimigue','$H$96Lh7zZJGJYiMfObq8FL0IUNYvR8SW1',1370849160,0,'zbuditcyalove@yahoo.co.uk',206049419525,'14-10-1977',0,1370849160,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','Philippines','262421341','','','','','http://www.movealex.ru','Retail','Bowling','','','2384131acf587c4c',1,0,0),(70,3,5,'zik0zjzik0zjw6ovy8\ni1cjyo000000\nziimf3zhb2tc',0,'89.67.201.68',1364499732,'test_user','test_user','$H$9ljIrXcK8EkmpysSHa1pm2Zn5sxehB.',1364499732,0,'biuro@miart.pl',203793828914,'',1364501060,1364499732,0,'adm/index.php?i=users&mode=overview&u=70','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,1,'AA0000',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','1L7RLL9TK','','2dcd16b5dd2bc49f',1,0,0),(67,0,2,'',0,'94.42.177.36',1363868294,'dzielny','dzielny','$H$9zjkielbv.CYfj2CMz7amwacj3Lqry/',1363868294,0,'slgar@esculap.pl',347222190416,'',1363868344,1363868294,0,'memberlist.php','',0,0,0,0,0,0,0,'pl',1.00,0,'|j M Y|, \\o H:i',1,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','eb69ce2a04f4245b',1,0,0),(68,3,5,'',0,'176.111.231.170',1364421222,'Jacek Nasiłowski','jacek nasil̷owski','$H$9bPOBh6ShDnYW7zhZl/FftEeip9esl0',1364421222,0,'jnasilowski@wum.edu.pl',364050961622,'',1364472607,1364421222,0,'','',0,0,0,0,0,0,0,'pl',0.00,0,'|j M Y|, \\o H:i',1,1,'AA0000',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','d426ecd7334f5032',1,0,0);
/*!40000 ALTER TABLE `phpbb_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_warnings`
--

DROP TABLE IF EXISTS `phpbb_warnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_warnings` (
  `warning_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `post_id` mediumint(8) unsigned NOT NULL default '0',
  `log_id` mediumint(8) unsigned NOT NULL default '0',
  `warning_time` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`warning_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_warnings`
--

LOCK TABLES `phpbb_warnings` WRITE;
/*!40000 ALTER TABLE `phpbb_warnings` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_warnings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_words`
--

DROP TABLE IF EXISTS `phpbb_words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_words` (
  `word_id` mediumint(8) unsigned NOT NULL auto_increment,
  `word` varchar(255) collate utf8_bin NOT NULL default '',
  `replacement` varchar(255) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`word_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_words`
--

LOCK TABLES `phpbb_words` WRITE;
/*!40000 ALTER TABLE `phpbb_words` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_words` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_zebra`
--

DROP TABLE IF EXISTS `phpbb_zebra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_zebra` (
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `zebra_id` mediumint(8) unsigned NOT NULL default '0',
  `friend` tinyint(1) unsigned NOT NULL default '0',
  `foe` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`zebra_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_zebra`
--

LOCK TABLES `phpbb_zebra` WRITE;
/*!40000 ALTER TABLE `phpbb_zebra` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_zebra` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-29 11:33:36
