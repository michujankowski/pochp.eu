-- MySQL dump 10.14  Distrib 10.0.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: pochp.eu    Database: pochp_db
-- ------------------------------------------------------
-- Server version	5.0.67-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `exp_accessories`
--

DROP TABLE IF EXISTS `exp_accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL auto_increment,
  `class` varchar(75) NOT NULL default '',
  `member_groups` varchar(50) NOT NULL default 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY  (`accessory_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_accessories`
--

LOCK TABLES `exp_accessories` WRITE;
/*!40000 ALTER TABLE `exp_accessories` DISABLE KEYS */;
INSERT INTO `exp_accessories` VALUES (1,'Expressionengine_info_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.0'),(2,'Nsm_tiny_mce_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.0.0');
/*!40000 ALTER TABLE `exp_accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_actions`
--

DROP TABLE IF EXISTS `exp_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL auto_increment,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY  (`action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_actions`
--

LOCK TABLES `exp_actions` WRITE;
/*!40000 ALTER TABLE `exp_actions` DISABLE KEYS */;
INSERT INTO `exp_actions` VALUES (1,'Comment','insert_new_comment'),(2,'Comment_mcp','delete_comment_notification'),(3,'Comment','comment_subscribe'),(4,'Comment','edit_comment'),(5,'Email','send_email'),(6,'Safecracker','submit_entry'),(7,'Safecracker','combo_loader'),(8,'Search','do_search'),(9,'Channel','insert_new_entry'),(10,'Channel','filemanager_endpoint'),(11,'Channel','smiley_pop'),(12,'Member','registration_form'),(13,'Member','register_member'),(14,'Member','activate_member'),(15,'Member','member_login'),(16,'Member','member_logout'),(17,'Member','retrieve_password'),(18,'Member','reset_password'),(19,'Member','send_member_email'),(20,'Member','update_un_pw'),(21,'Member','member_search'),(22,'Member','member_delete'),(23,'Ptpz','index');
/*!40000 ALTER TABLE `exp_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_captcha`
--

DROP TABLE IF EXISTS `exp_captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL auto_increment,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY  (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_captcha`
--

LOCK TABLES `exp_captcha` WRITE;
/*!40000 ALTER TABLE `exp_captcha` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_categories`
--

DROP TABLE IF EXISTS `exp_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) default NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_categories`
--

LOCK TABLES `exp_categories` WRITE;
/*!40000 ALTER TABLE `exp_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_field_data`
--

DROP TABLE IF EXISTS `exp_category_field_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_field_data`
--

LOCK TABLES `exp_category_field_data` WRITE;
/*!40000 ALTER TABLE `exp_category_field_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_category_field_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_fields`
--

DROP TABLE IF EXISTS `exp_category_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL default '',
  `field_label` varchar(50) NOT NULL default '',
  `field_type` varchar(12) NOT NULL default 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL default '128',
  `field_ta_rows` tinyint(2) NOT NULL default '8',
  `field_default_fmt` varchar(40) NOT NULL default 'none',
  `field_show_fmt` char(1) NOT NULL default 'y',
  `field_text_direction` char(3) NOT NULL default 'ltr',
  `field_required` char(1) NOT NULL default 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_fields`
--

LOCK TABLES `exp_category_fields` WRITE;
/*!40000 ALTER TABLE `exp_category_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_category_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_groups`
--

DROP TABLE IF EXISTS `exp_category_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL default 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL default '0',
  `field_html_formatting` char(4) NOT NULL default 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_groups`
--

LOCK TABLES `exp_category_groups` WRITE;
/*!40000 ALTER TABLE `exp_category_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_category_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_posts`
--

DROP TABLE IF EXISTS `exp_category_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_posts`
--

LOCK TABLES `exp_category_posts` WRITE;
/*!40000 ALTER TABLE `exp_category_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_category_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_data`
--

DROP TABLE IF EXISTS `exp_channel_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` int(11) default '0',
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_data`
--

LOCK TABLES `exp_channel_data` WRITE;
/*!40000 ALTER TABLE `exp_channel_data` DISABLE KEYS */;
INSERT INTO `exp_channel_data` VALUES (1,1,1,'<div style=\"float: right; padding: 1.5em 0 10px 20px;\"><img alt=\"Anna Janowicz\" height=\"200\" src=\"/images/uploads/prof_gorecka_copy.jpeg\" style=\"display: inline;\" width=\"152\" />\n<p style=\"text-align: right; clear: none; margin-bottom: 0;\">Prof. Dorota G&oacute;recka</p>\n</div>\n<h2 style=\"clear: none;\">Profesor Doroty G&oacute;reckiej</h2>\n<p style=\"clear: none;\">We wrześniu 2011, podczas dorocznego zjazdu, Europejskie Towarzystwo Oddechowe (ERS) przedstawiło dokument zatytułowany European Respiratory roadmap prezentujący stanowisko towarzystwa i kierunki proponowanych działań w zapobieganiu, leczeniu, organizacji ochrony zdrowia i nakład&oacute;w na naukę w zakresie chor&oacute;b układu oddechowego. Jest to drugi, po White Book z 2003 roku, dokument przedstawiający sytuację zdrowotną w krajach UE i wskazujący jak istotną rolę odgrywają choroby płuc. Choroby płuc należą do najczęstszych chor&oacute;b na świecie. Około&nbsp; 500 milion&oacute;w ludzi choruje na astmę i przewlekłą obturacyjną chorobę płuc, a około 9,27 milion&oacute;w na gruźlicę.</p>\n<p style=\"clear: none;\">Wg Światowej Organizacji Zdrowia z powodu przewlekłych chor&oacute;b układu oddechowego umiera rocznie około <br />4 milion&oacute;w os&oacute;b, a w najbliższym dziesięcioleciu liczba ta wzrośnie o około 30%. W czasie ostatnich 30 lat obserwuje się niepokojącą tendencję wzrostu śmiertelności z powodu POChP, - najczęstszej choroby płuc prowadzącej do zgonu - kt&oacute;ra w tym czasie podwoiła się, podczas gdy śmiertelność z powodu chor&oacute;b układu krążenia, udaru, a nawet z&nbsp;powodu chor&oacute;b nowotworowych systematycznie zmniejsza się.</p>\n<p>W Polsce około 20% chorych na POChP (400 tysięcy) ma ciężką lub bardzo ciężką postać choroby. Takie zaawansowanie wymaga objęcia pacjent&oacute;w kompleksową opieką medyczną, niezbędną dla uzyskania poprawy objaw&oacute;w i jakości życia, zmniejszenia liczby hospitalizacji, a&nbsp;w&nbsp;ostatnim okresie życia ulżenia cierpieniom fizycznym, psychicznym i og&oacute;lnej niesprawności poprzez opiekę hospicyjną.</p>\n<p style=\"clear: none;\">Wydaje się, że poprawę sytuacji chorych na zaawansowaną POChP można uzyskać wprowadzając <strong>Model zintegrowanej opieki</strong>. Zapewnia on odpowiednią wielodyscyplinarną opiekę medyczną i psychologiczną oraz tak potrzebne wielu chorym wsparcie w radzeniu sobie z chorobą. Model ten sprawdził się w pilotażowych badaniach w innych krajach.<br /><br /></p>\n<hr style=\"height: 1px; background: #ddd;\" />\n<p><span style=\"text-decoration: underline;\">Aktywne linki prowadzące do artykuł&oacute;w:</span></p>\n<ul>\n<li><a href=\"http://www.pneumonologia.viamedica.pl/\"><em>Zintegrowana opieka medyczna u chorych na zaawansowaną przewlekłą obturacyjną chorobę płuc</em>; Ewa Jassem, Dorota G&oacute;recka, Piotr Krakowiak, Jerzy Kozielski, J. Marek Słomiński, Małgorzata Krajnik, Andrzej M. Fal; Pneumonologia i Alergoloia Polska;<strong> tom 78 nr 2, 2012</strong></a></li>\n</ul>\n<p style=\"padding-left: 30px;\"><span style=\"font-size: xx-small;\">Po kliknięciu na link, przechodzimy na stronę gł&oacute;wną, następnie należy wybrać odpowiedni tom z archiwum</span></p>\n<ul>\n<li><a href=\"http://www.pamw.pl/sites/default/files/PAMW_10-2010_22-Kozielski_0.pdf\"><em>Integrated care for patients with advanced chronic obstructive pulmonary disease: a new approach to organization.</em>; Ewa Jassem, Jerzy Kozielski, Dorota Gorecka, Piotr Krakowiak, Małgorzata Krajnik, Jan M. Słomiński; Wydawnictwo: Polskie archiwum medycyny wewnętrznej</a></li>\n</ul>','xhtml','','xhtml','','none','','none','','none',0,'none','','none','','none','','xhtml','','xhtml','','none'),(2,1,3,'',NULL,'',NULL,'',NULL,'','none','1','none',0,'none','','none','','none','','xhtml','','xhtml','','none'),(3,1,2,'',NULL,'',NULL,'',NULL,'1','none','','none',0,'none','','none','','none','','xhtml','','xhtml','','none'),(4,1,1,'<p><strong><span style=\"font-size: small;\">Dzisiejszy stan wiedzy o możliwosciach leczenia POChP świadczy, że jedynym postępowaniem medycznym hamującym postęp choroby jest wczesne rozpoznanie i skuteczne działania antytytoniowe.</span></strong></p>\n<p>&nbsp;<img alt=\"Picture1\" height=\"306\" src=\"/images/uploads/mapka_03.png\" style=\"display: block; margin-left: auto; margin-right: auto; box-shadow: none !important;\" title=\"Picture1\" width=\"400\" /></p>','xhtml','','xhtml','','none','',NULL,'',NULL,5,'none','','none','','none','','xhtml','','xhtml','','none'),(5,1,4,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'1','none','<a href=\"/strony/pochp\">POChP</a>','none','','xhtml','','xhtml','','none'),(8,1,1,'<p><span style=\"font-size: small; font-family: arial,helvetica,sans-serif; color: #333333;\">Rejestr POChP jest elementem Programu Pomorski Model Zintegrowanej Opieki dla Chorych na Zaawansowaną postać POChP. W 2012 roku prowadzi się badania pilotażowe na Pomorzu obejmują cztery Ośrodki w Gdańsk, Chojnicach, Słupsku i Starogardzie Gdańskim. Dostęp do Elektronicznej Karty Pacjenta mają w tym okresie wyłącznie chorzy zakwalifikowani do udziału w Programie w wymienionych Ośrodkach. Mamy nadzieję, że jak najszybciej uda nam się objać Programem więcej chorych.</span></p>','xhtml','','xhtml',' ','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(7,1,1,'<p>Rejestr POChP jest elementem Programu Pomorski Model Zintegrowanej Opieki dla Chorych na Zaawansowaną postać POChP.</p>\n<p>W 2012 roku prowadzi się badanie pilotażowe na Pomorzu obejmujące cztery Ośrodki w Gdańsku, Chojnicach, Słupsku i Starogardzie Gdańskim.</p>\n<p>Stałe uprawnienia do korzystania rejestru w tym czasie mają lekarze z wymienionych ośrodk&oacute;w. Jeśli Państwo chcieliby wprowadzić chorych do Rejestru uprzejmie prosimy o zgłoszenie się do administratora: pochp.rejestr@gumed.edu.pl</p>','xhtml','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(9,1,1,'<p><strong><span style=\"font-size: small;\">Formularz kontaktowy: np: <a href=\"mailto:pomoc@pochp.pl\">pomoc@pochp.pl</a></span></strong></p>','xhtml','a','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(10,1,1,'<h4>Dzisiejszy stan wiedzy o możliwosciach leczenia POChP świadczy, że jedynym postępowaniem medycznym hamującym postęp choroby jest wczesne rozpoznanie i skuteczne działania antytytoniowe.</h4>\n<p style=\"text-align: center;\"><img alt=\"Picture1\" height=\"492\" src=\"/images/uploads/Picture1.png\" title=\"Picture1\" width=\"520\" /></p>','none','','xhtml','','none','',NULL,'',NULL,5,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(11,1,1,'<p>Spirometria - rodzaj badania medycznego, podczas kt&oacute;rego mierzy się objętości i pojemności płuc oraz przepływy powietrza znajdującego się w płucach i oskrzelach w r&oacute;żnych fazach cyklu oddechowego. Spirometria ma na celu określenie rezerw wentylacyjnych układu oddechowego. Badanie wykonuje się przy pomocy urządzenia zwanego spirometrem. Spirometria jest niezbędna do rozpoznania i kontroli efekt&oacute;w leczenia częstych chor&oacute;b układu oddechowego:astmy i POChP.</p>\n<p>Opis badania - Przed przystąpieniem do badania spirometrycznego, pacjent wykonuje kilka głębokich wdech&oacute;w. Ostatni głęboki wdech kończy się przyłożeniem ust do ustnika, połączonego specjalną rurką z aparatem spirometrycznym. Badanie polega na jak najszybszym wdmuchiwaniu do aparatu całego zapasu powietrza zawartego w płucach. Kolejne ruchy oddechowe są wykonywane zgodnie z zaleceniami osoby wykonującej badanie.</p>','none','','xhtml','','none','',NULL,'',NULL,23,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(12,1,1,'<p>...</p>','none','','xhtml','','none','',NULL,'',NULL,24,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(13,1,1,'<p>Przewlekła obturacyjna choroba płuc (POChP) (COPD <em>Chronic Obstructive Pulmonary Disease Morbus obturativus pulmonum chronicum</em>) &ndash; zesp&oacute;ł chorobowy charakteryzujący się postępującym i niecałkowicie odwracalnym ograniczeniem przepływu powietrza przez drogi oddechowe. Ograniczenie to wynika z choroby małych dr&oacute;g oddechowych i zniszczenia miąższu płucnego (rozedmy) o r&oacute;żnym nasileniu i jest najczęściej związane z nieprawidłową odpowiedzią zapalną ze strony układu oddechowego na szkodliwe pyły i&nbsp;substancje, z jakimi chory ma do czynienia w ciągu swojego życia. Najczęstszą przyczyną POChP jest narażenie na dym tytoniowy, ale inne czynniki, takie jak substancje drażniące z powietrza oraz stany wrodzone, na przykład niedob&oacute;r alfa1-antytrypsyny, r&oacute;wnież mogą doprowadzić do rozwinięcia się choroby. W Stanach Zjednoczonych POChP jest czwartą pod względem częstości przyczyną zgon&oacute;w. Objawy są niespecyficzne, dominuje duszność. Rozpoznanie stawia się na podstawie badania spirometrycznego. Leczenie ma charakter objawowy. POChP jest chorobą nieuleczalną, a wszelkie działania lekarskie mają na&nbsp;celu spowolnienie procesu chorobowego i poprawę komfortu życia pacjenta.</p>\n<h4>POChP to choroba przewlekła</h4>\n<ul>\n<li>zwęża oskrzela</li>\n<li>niszczy płuca</li>\n<li>uszkadza inne narządy np. powoduje osteoporozę</li>\n</ul>\n<h4>Często chorzy opr&oacute;cz POChP mają inne przewlekłe choroby</h4>\n<ul>\n<li>choroba wieńcowa i nadciśnienie</li>\n<li>cukrzyca</li>\n<li>niedokrwistość</li>\n<li>rak płuca</li>\n<li>zatorowość</li>\n<li>depresja</li>\n</ul>\n<h4>Częste objawy zaawansowanej POChP</h4>\n<p><strong>Stanisława lat 70</strong> - <span>&bdquo;</span><em>...od kiedy choruję na POChP mam stale zadyszkę...\"</em></p>\n<p><strong>Władysław lat 68</strong> - <span>&bdquo;</span><em>...od wielu lat rano kaszlę i wypluwam duże ilości plwociny...\"</em></p>\n<p><strong>Dorota lat 68</strong> - <span>&bdquo;</span><em>...nie am siły wyjść po zakupy...\"</em></p>\n<p><strong>Jan lat 80</strong> - <span>&bdquo;</span><em>...w ostatnim roku dwa razy byłem w szpitalu z powodu zaostrzenia POChP...\"</em></p>','none','','xhtml','','none','',NULL,'',NULL,5,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(14,1,1,'<h4>Palenie tytoniu</h4>\n<p>Do najlepiej udokumentowanych czynnik&oacute;w ryzyka wystąpienia przewlekłej obturacyjnej choroby płuc należy palenie tytoniu (papierosy, fajki, cygara). Ryzyko jest zależne od dawki, im większe narażenie, tym większa częstość występowania POChP. Palenie fajek czy cygar powoduje nieco mniejszą chorobowość i umieralność niż u palaczy papieros&oacute;w, ale i tak istotnie wyższą niż u populacji niepalącej. Z uwagi na to, że nie u&nbsp;wszystkich palaczy tytoniu rozwija się klinicznie jawna POChP, postuluje się wsp&oacute;łistniejący udział czynnik&oacute;w genetycznych, kt&oacute;re mogą modyfikować odpowiedź organizmu na szkodliwe substancje. Zwiększone ryzyko wystąpienia POChP może r&oacute;wnież warunkować bierne palenie tytoniu.</p>\n<h4>Zanieczyszczenie powietrza i narażenie zawodowe</h4>\n<p>Wśr&oacute;d innych wziewnych czynnik&oacute;w ryzyka wymienia się r&oacute;wnież narażenie zawodowe na pyły przemysłowe i substancje chemiczne. Szacuje się, że ww. czynniki odpowiadają za 10-20% przypadk&oacute;w rozwoju POChP. R&oacute;wnież spalanie w warunkach domowych biomasy&nbsp; (zwłaszcza w źle wentylowanych pomieszczeniach) może być czynnikiem ryzyka, zwłaszcza u kobiet w krajach rozwijających się. Nie ma badań na temat wpływu zanieczyszczenia środowiska na rozw&oacute;j POChP, niemniej wiadomo, że skażenie powietrza atmosferycznego wiąże się z istotnym pogorszeniem czynności układu oddechowego.</p>\n<h4>POChP jako choroba autoimmunologiczna</h4>\n<p>Pojawiają się doniesienia m&oacute;wiące o autoimmunologicznym składniku, kt&oacute;ry odgrywa rolę w POChP. Potwierdzeniem teorii jest obecność autoreaktywnych limfocyt&oacute;w T i autoprzeciwciał. Dodatkowo u wielu os&oacute;b, kt&oacute;re rzuciły palenie, nadal zachodzą przewlekłe procesy zapalne powodujące nieadekwatną do wieku, postępującą utratę czynności płuc.</p>\n<h4><strong>Dlaczego zachorowałem na POChP</strong></h4>\n<ul>\n<li><em><span>&bdquo;</span>papierosy zacząłem palić mając 20 lat.\"</em></li>\n<li><em><span>&bdquo;</span>w dzieciństwie często chorowałam na oskrzela i dwa razy miałam zapalenie płuc.\"</em></li>\n<li><em><span>&bdquo;</span>pracowałem w dużym zapyleniu i nie używałem maski ochronnej.\"</em></li>\n</ul>\n<h4>10 najczęstszych przyczyn POChP</h4>\n<ol>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>palenie papieros&oacute;w</li>\n<li>inne przyczyny</li>\n</ol>','none','','xhtml','','none','',NULL,'',NULL,5,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(15,1,1,'<p>POChP jest chorobą nieuleczalną i postępującą w czasie, a wszelkie działania lekarskie mają na celu spowolnienie dalszego postępu choroby i poprawę komfortu życia pacjenta. Terapia powinna zostać dostosowana do potrzeb indywidualnego pacjenta i zależy od ciężkości stanu, a także od stopnia dostępności leczenia, wsp&oacute;łpracy z pacjentem, uwarunkowań kulturowych i lokalnych.</p>\n<p>Istotną kwestią w leczeniu POChP jest właściwa edukacja pacjenta i jego rodziny. Wiedza o chorobie pomoże pacjentowi radzić sobie z POChP w codziennym życiu, a także poprawi og&oacute;lny stan zdrowia, mimo że obiektywnie nie wpłynie na czynność płuc. Największe znaczenie ma ukierunkowanie pacjenta na działania, kt&oacute;re mają na celu usunięcie z jego otoczenia czynnik&oacute;w wpływających negatywnie na postęp choroby, a&nbsp;zwłaszcza zmobilizowanie pacjenta do zaprzestania palenia papieros&oacute;w.</p>\n<h4>Jak zapobiegać POChP?</h4>\n<ul>\n<li>Unikać czynnik&oacute;w ryzyka -&gt; sprawdź w zakładce <span style=\"text-decoration: underline;\"><span style=\"color: #ff0000;\"><a href=\"/strony/czynniki_ryzyka_pochp%20\"><span style=\"color: #ff0000;\">\"Czynniki ryzyka POChP\"</span></a></span></span></li>\n<li>Prowadzić zdrowy styl życia</li>\n<li>Wykonywać badanie spirometryczne</li>\n</ul>\n<h4>Kto może bezpłatnie wykonać badanie spirometryczne w ramach refundacji z NFZ?</h4>\n<p>W Polsce prowadzi się program <strong>Profilaktyka chor&oacute;b odtytoniowych (w tym POChP),</strong> finansowany przez NFZ. Jest on przeznaczony dla kobiet i mężczyzn w wieku powyżej 18 lat, palących papierosy, a w szczeg&oacute;lności do os&oacute;b w wieku 40-65 lat, kt&oacute;re nie miały wykonanych badań spirometrycznych w czasie ostatnich 3 lat oraz u kt&oacute;rych nie rozpoznano wcześniej POChP, rozedmy lub przewlekłego zapalenia oskrzeli.</p>\n<p>Z programu mogą korzystać <strong>wszyscy ubezpieczeni</strong>, kt&oacute;rzy spełniają wymienione powyżej warunki. Nie potrzeba skierowania od lekarza rodzinnego, warto jednak sprawdzić w odpowiednim wojew&oacute;dzkim oddziale NFZ, kt&oacute;re plac&oacute;wki ochrony zdrowia realizują ten program.</p>','none','','xhtml','','none','',NULL,'',NULL,5,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(16,1,1,'<p>Złotym standardem w rozpoznaniu i monitorowaniu choroby jest spirometria. Na podstawie obrazu klinicznego można z dużą pewnością podejrzewać rozpoznanie, niemniej ostatecznym dowodem potwierdzającym jest wynik badania spirometrycznego. Spirometrię należy wykonać u każdego pacjenta z podejrzeniem POChP. Stwierdzenie upośledzenia natężonej pojemności żywności (FVC) poniżej 80% wartości należnej (dostosowanej do płci, wieku, wzrostu i rasy) oraz zmniejszenie wskażnika Tiffeneau poniżej 70%, potwierdzają rozpoznanie. Na podstawie wartości spirometrycznych dokonuje się szacunkowego podziału choroby na stadia (patrz sekcja przebieg naturalny).</p>\n<p>W przeglądzie systematycznym wykazano, że żaden pojedynczy objaw przedmiotowy lub podmiotowy, nie może jednoznacznie wskazać na rozpoznanie POChP. W jednym z badań wykazano, że jednoczesne stwierdzenie w wywiadzie: 30 paczkolat, osłabienia odgłos&oacute;w oddechowych i szczytowy przepływ wynoszący &lt;350 l/min, dają łącznie 98% czułość prawidłowego zdiagnozowania POChP.</p>\n<h4>POChP rozpoznaje lekarz</h4>\n<ul>\n<li>Jeśli paliłeś albo palisz papierosy...</li>\n<li>Jeśli pracowałeś wśr&oacute;d os&oacute;b palących w tym samym pomieszczeniu...</li>\n<li>Jaśli masz przewlekle kaszel...</li>\n<li>Jeśli masz zadyszkę...</li>\n</ul>\n<h2 style=\"text-align: center;\"><span style=\"font-size: medium;\"><strong><span style=\"color: #ff0000;\">...ZWR&Oacute;Ć SIĘ DO LEKARZA!!!</span></strong></span></h2>','none','','xhtml','','none','',NULL,'',NULL,23,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(17,1,1,'<p>Spirometria - rodzaj badania medycznego, podczas kt&oacute;rego mierzy się objętości i pojemności płuc oraz przepływy powietrza znajdującego się w płucach i oskrzelach w r&oacute;żnych fazach cyklu oddechowego. Spirometria ma na celu określenie rezerw wentylacyjnych układu oddechowego. Badanie wykonuje się przy pomocy urządzenia zwanego spirometrem. Spirometria jest niezbędna do rozpoznania i kontroli efekt&oacute;w leczenia częstych chor&oacute;b układu oddechowego:astmy i POChP.</p>\n<p>Opis badania - Przed przystąpieniem do badania spirometrycznego, pacjent wykonuje kilka głębokich wdech&oacute;w. Ostatni głęboki wdech kończy się przyłożeniem ust do ustnika, połączonego specjalną rurką z&nbsp;aparatem spirometrycznym. Badanie polega na jak najszybszym wdmuchiwaniu do aparatu całego zapasu powietrza zawartego w płucach. Kolejne ruchy oddechowe są wykonywane zgodnie z zaleceniami osoby wykonującej badanie.</p>\n<p>W Polsce prowadzi się program <strong>Profilaktyka chor&oacute;b odtytoniowych (w tym POChP),</strong> finansowany przez NFZ. Jest on przeznaczony dla kobiet i mężczyzn w wieku powyżej 18 lat, palących papierosy, a&nbsp;w&nbsp;szczeg&oacute;lności do os&oacute;b w wieku 40-65 lat, kt&oacute;re nie miały wykonanych badań spirometrycznych w czasie ostatnich 3 lat oraz u kt&oacute;rych nie rozpoznano wcześniej POChP, rozedmy lub przewlekłego zapalenia oskrzeli.</p>\n<p>Z programu mogą korzystać wszyscy ubezpieczeni, kt&oacute;rzy spełniają wymienione powyżej warunki. Nie potrzeba skierowania od lekarza rodzinnego, warto jednak sprawdzić w odpowiednim wojew&oacute;dzkim oddziale NFZ, kt&oacute;re plac&oacute;wki ochrony zdrowia realizują ten program.</p>','none','','xhtml','','none','',NULL,'',NULL,23,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(18,1,1,'<p>Jeśli zaobserwujesz u siebie pogorszenie objaw&oacute;w, nie zwlekaj - skontaktuj się z lekarzem prowadzącym.</p>','none','','xhtml','','none','',NULL,'',NULL,23,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(19,1,1,'<p>Palący żyją średnio o ok. 14 lat kr&oacute;cej niż niepalący. Badania medyczne dowodzą, że palenie jest jedną z wiodących przyczyn takich chor&oacute;b jak: przewlekła obturacyjna choroba płuc (POChP), rak płuc, choroba niedokrwienna serca i wiele innych. Palenie podczas ciąży może r&oacute;wnież doprowadzić do komplikacji podczas porodu, a także spowodować gorszy rozw&oacute;j psychofizyczny dziecka.</p>','none','','xhtml','','none','',NULL,'',NULL,24,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(20,1,1,'<p>Z jakiegoś powodu zastanawiasz się czy nie rzucić palenia. Zastan&oacute;w się skąd w og&oacute;le wziął się ten pomysł? Co Ci przeszkadza w paleniu? Myślisz, ze coś m&oacute;głbyś zyskać rzucając ten nał&oacute;g. Być może jesteś przerażony samą myślą o rzucaniu, a może już kiedyś doświadczyłeś, że niepalenie jest możliwe.&nbsp; Czy zastanawiałeś się już, jaki wpływ na Tw&oacute;j organizm ma palenie &ndash; co się dzieje gdy palisz? Przeczytaj informację o&nbsp;szkodliwości. Palisz bo ... czy wiesz co zyskujesz, a co tracisz? Rozważ za i przeciw paleniu i papierosem &ndash; pomoże Ci w&nbsp;tym celu przygotowana tabelka. Może często przychodzi Ci do głowy myśl: &bdquo;Chcę rzucić palenie, ale&hellip;&rdquo;, &bdquo;Czy uda mi się rzucić?&rdquo;. Być może masz r&oacute;żne obawy na ten temat &ndash; warto abyś się nim bliżej przyjrzał i&nbsp;bardziej świadomie podjął decyzję o rzucaniu palenia wejdź na Twoje objawy. Czy wiesz już, dlaczego chciałbyś nie palić? Spr&oacute;buj zastanowić się, z jakich powod&oacute;w właśnie dla Ciebie byłoby korzystne rzucenie palenia.</p>\n<p>Wybierz te, kt&oacute;re dotyczą Ciebie:</p>\n<ul>\n<li>obawiam się o swoje zdrowie</li>\n<li>czuję, że palenie mi szkodzi</li>\n<li>jestem chory</li>\n<li>ktoś mnie namawia</li>\n<li>dla rodziny, bliskich</li>\n<li>jestem zmęczony nałogiem</li>\n<li>męczy mnie ciągłe palenie</li>\n<li>nie chcę zatruwać innych, obawiam się o zdrowie bliskich</li>\n<li>nie chcę tracić swoich pieniędzy</li>\n<li>mam swoje osobiste powody</li>\n<li>nie mogę palić w miejscu pracy</li>\n<li>spodziewam się dziecka</li>\n<li>nie chcę śmierdzieć dymem tytoniowym,</li>\n<li>mam jeszcze inny pow&oacute;d..</li>\n</ul>\n<p>Czy wybrałeś sw&oacute;j pow&oacute;d? Najlepiej byłoby gdybyś chciał rzucić dla siebie, choć czasem może wystarczyć impuls np. ktoś ze znajomych właśnie rzucił, a może rozchorowałeś się i po prostu nie masz siły palić dalej. Pamiętaj, że każdy pow&oacute;d jest dobry, aby rzucić palenie! Twoja motywacja jest tu bardzo ważna. Gdy masz już pomysł, dlaczego nie chcesz palić, zastan&oacute;w się nad tym: &bdquo;co wiesz o swoim paleniu?&rdquo; oraz \"gdzie i&nbsp;w&nbsp;jakich sytuacjach sięgasz po papierosa?\"</p>','none','','xhtml','','none','',NULL,'',NULL,24,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(21,1,1,'<h4>Metody Farmakologiczne:</h4>\n<p class=\"p1\">Aby jeszcze skuteczniej pom&oacute;c rozstać się z tytoniowym nałogiem, na polskim rynku są dostępne specjalistyczne leki. Ważne jest aby nie stosować ich w spos&oacute;b instrumentalny, czy magiczny tzn. wezmę lek i zapomnę o paleniu. Nie istnieje lek, kt&oacute;ry działa w taki spos&oacute;b. Lek ma pom&oacute;c w rzuceniu palenia poprzez <a href=\"http://m.in/\"><span class=\"s1\">m.in</span></a>. łagodzenie objaw&oacute;w głodu nikotynowego. Wśr&oacute;d lek&oacute;w wspomagających rzucanie palenia znajdują się <a href=\"http://m.in/\"><span class=\"s1\">m.in</span></a>.: kuracje z&nbsp;nikotyną tzw. Nikotynowa Terapia Zastępcza oraz inne specjalistyczne leki nie zawierające nikotyny.</p>\n<p class=\"p1\">W jaki spos&oacute;b wybrać lek odpowiedni dla siebie? Ważnych jest kilka czynnik&oacute;w: Po pierwsze: poziom uzależnienia &ndash; sprawdź czy potrzebne jest Ci wsparcie lekami - zajrzyj do mechanizm&oacute;w uzależnienia, zr&oacute;b sobie test uzależnienia, przeprowadź wywiad dotyczący palenia papieros&oacute;w oraz sprawdź swoją motywację do rzucenia palenia. Po drugie, aby wykluczyć przeciwwskazania dotyczące stosowania przez Ciebie wybranego leku ważna jest konsultacja z lekarzem pierwszego kontaktu, lub wizyta w specjalistycznej Poradni Pomocy Palącym. Po wstępne informacje możesz też zadzwonić do Telefonicznej Poradni Pomocy Palącym - 801 108 108.</p>\n<p></p>\n<p class=\"p1\">W wyjściu z nałogu mogą pom&oacute;c lekarze rodzinni oraz specjaliści przyjmujący w Poradni Pomocy Palącym. Zapraszamy do skorzystania z bezpłatnej pomocy, w ramach ubezpieczenia w NFZ.<br />Rejestracja do Uniwersyteckiej Poradni Pomocy Palącym, mieszczącej się w Gdańsku, przy ul. Dębinki 7c, pod nr tel. (58) 349 26 05, od pn do pt w godz. 8.00-15.00<br />Więcej informacji na: <a href=\"http://www.uck.gda.pl/content/view/1432/604/\"><span class=\"s2\">http://www.uck.gda.pl/content/view/1432/604/</span></a></p>\n<p class=\"p1\"><span class=\"s2\"><img src=\"/images/map.jpg\" style=\"display: block; margin-left: auto; margin-right: auto;\" width=\"500\" /><br /></span></p>','none','','xhtml','','none','',NULL,'',NULL,24,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(22,1,1,'<p>Jeśli jesteś osobą palącą, kt&oacute;ra nie myśli o rzucaniu palenia, lub Ty sam m&oacute;wisz o sobie: &bdquo;PALĘ, BO LUBIĘ&rdquo;, to&nbsp;właśnie znalazłeś się we właściwym miejscu. Nie chcemy namawiać Cię do rzucenie palenia, a jedynie do&nbsp;zebrania garści informacji, kt&oacute;re dotyczą Ciebie. Zapewne masz własne przekonania na temat papieros&oacute;w, może jednak warto abyś poznał fakty i mity&nbsp;dotyczące palenia. Może zweryfikujesz niekt&oacute;re swoje sądy, a&nbsp;inne znajdą potwierdzenie? Lubisz decydować o swoim losie? W świadomy spos&oacute;b starasz się podejmować decyzje dotyczące całego Twojego życia i życia Twoich bliskich? To samo powinno dotyczyć palenia. Sprawdź, na co decydujesz się sięgając po każdego następnego papierosa &ndash; zajrzyj do info o&nbsp;szkodliwości. Proponujemy Ci też sprawdzenie, co inni palacze sądzą o swoim nałogu, jakie za i przeciw paleniu i papierosom&nbsp; przedstawiają. Zastanawiałeś się nad tym, czy jesteś uzależniony i jaki charakter ma Twoje palenie? Poczytaj o mechanizmach uzaleznienia i sprawdź, czy palisz nałogowo &ndash; zr&oacute;b test uzaleznienia. Jeśli nie myślisz o rzucaniu palenia &ndash; może warto, abyś zminimalizował szkody. Jeśli choć przez chwilę pomyślałeś, czy nie byłoby warto rzucić palenia przejdź na inne nasze podstrony &ndash; zastan&oacute;w się&hellip; a&nbsp;może rzucić?</p>','none','','xhtml','','none','',NULL,'',NULL,24,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(26,1,1,'<p>W 2012 roku planuje się przeprowadzenie na Pomorzu (projekt &bdquo;POMORZE&rdquo;) pilotowego badania, analizującego czy zastosowanie modelu pozwala istotnie poprawić przebieg POChP i zmniejszyć koszty leczenia.</p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3><img height=\"63\" src=\"/images/uploads/zdrowie_dla_pomorzan.jpg\" style=\"box-shadow: none; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 20px; float: right;\" width=\"60\" />Autorzy projektu &bdquo;POMORZE&rdquo; , kt&oacute;ry realizowany jest w ramach Programu Zdrowie dla Pomorzan:</h3>\n<table border=\"0\" style=\"width: 100%; text-align: center;\">\n<tbody>\n<tr>\n<td style=\"color: #898888; border-width: 0px;\" valign=\"middle\">\n<p><a href=\"http://www.alergologia.gumed.edu.pl/\" target=\"_blank\"><img height=\"120\" src=\"/images/uploads/GUM2.jpg\" style=\"box-shadow: none; display: block; margin-left: auto; margin-right: auto;\" width=\"102\" /></a></p>\n<p style=\"text-align: center;\"><span>Klinika Alergologii Gdańskiego Uniwersytetu Medycznego</span></p>\n</td>\n<td style=\"color: #898888; border-width: 0px;\" valign=\"middle\"><a href=\"http://www.ptpz.pl/\" target=\"_blank\"><img height=\"60\" src=\"/images/uploads/PTPZ.jpg\" style=\"box-shadow: none; display: inline;\" width=\"321\" /></a></td>\n</tr>\n<tr>\n<td style=\"color: #898888; border-width: 0px;\" valign=\"middle\"></td>\n<td style=\"color: #898888; border-width: 0px;\" valign=\"middle\"></td>\n</tr>\n<tr>\n<td colspan=\"2\" style=\"color: #898888; border-width: 0px;\" valign=\"middle\"><a href=\"http://www.lubiepomagac.pl/\" target=\"_blank\"><img height=\"50\" src=\"/images/uploads/LP.jpg\" style=\"box-shadow: none; display: inline; margin-top: 20px;\" width=\"267\" /></a></td>\n</tr>\n<tr>\n<td colspan=\"2\" style=\"color: #898888; border-width: 0px;\" valign=\"middle\"></td>\n</tr>\n</tbody>\n</table>\n<h3>Cele projektu</h3>\n<h5 style=\"text-align: left; color: #3b5990;\">Cel pierwszorzędowy</h5>\n<ul style=\"padding-left: 20px;\">\n<li>zmniejszyć liczbę zaostrzeń wymagających leczenia szpitalnego u chorych na zaawansowaną POChP</li>\n</ul>\n<h5 style=\"text-align: left; color: #3b5990;\">Cele następne</h5>\n<ul style=\"padding-left: 20px;\">\n<li>Poprawa jakości życia chorych</li>\n<li>Zmniejszenie koszt&oacute;w leczenia</li>\n<li>Utworzenie rejestru POChP</li>\n<li>Włączenie wyspecjalizowanych koordynujących pielęgniarek do zespołu terapeutycznego</li>\n<li>Włączenie opieki społecznej i/lub wolontariatu do procesu leczniczego</li>\n</ul>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Etapy Projektu</h3>\n<h5 style=\"text-align: left; color: #3b5990;\">Etap I</h5>\n<div style=\"float: right; width: 200px; text-align: right; display: inline-block;\"><img height=\"100\" src=\"/images/uploads/Picture5.png\" style=\"display: inline;\" width=\"99\" />\n<p style=\"text-align: right; font-size: 10px;\">mgr Lidia Werachowska<br />koordynatorka projektu &bdquo;Chojnice\"</p>\n</div>\n<p style=\"clear: none;\"><strong>Określenie możliwości prowadzenia projektu zintegrowanej opieki w POChP</strong> ze szczeg&oacute;lnym uwzględnieniem akceptacji chorego dla wizyt wspierających w domu. Wstępne badanie przeprowadzone w Chojnicach <strong>(projekt &bdquo;CHOJNICE&rdquo; &ndash; ukończony)</strong> wykazało pełną akceptację chorych i ich rodzin.</p>\n<h5 style=\"text-align: left; color: #3b5990; clear: none;\">Etap II</h5>\n<p style=\"clear: none;\">Pomorski model zintegrowanej opieki nad chorymi na zaawansowaną POChP. Badanie obserwacyjne<br /><br /><br /></p>\n<hr style=\"height: 1px; background: #ddd; clear: both; margin-bottom: 40px;\" />\n<h3>Kwalifikacja chorych do projektu</h3>\n<p>Do zintegrowanej opieki kwalifikuje się chorych z rozpoznaną zaawansowaną POChP (zgodnie z kryteriami GOLD i PTChP), kt&oacute;rzy wyrazili pisemną zgodę na udział w badaniu i w roku poprzedzającym udział w badaniu mieli co najmniej 3 zaostrzenia, w tym co najmniej jedno wymagające hospitalizacji.</p>\n<p>Miejscem rekrutacji chorych będą oddziały szpitalne ośrodk&oacute;w realizujących program.<br /><br /><br /></p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Ośrodki realizujące projekt</h3>\n<p style=\"text-align: center;\"><img height=\"430\" src=\"/images/uploads/mapka_placowki.png\" style=\"display: inline;\" width=\"660\" /></p>\n<p style=\"text-align: center;\"></p>\n<h5 style=\"text-align: left; color: #3b5990;\">Metody realizacji projektu</h5>\n<ul style=\"padding-left: 20px;\">\n<li>&nbsp;leczenie zgodne z obowiązującymi międzynarodowymi i polskimi standardami</li>\n<li>&nbsp;nadz&oacute;r leczenia przez koordynującą pielęgniarkę, za pomocą telefonicznych wizyt - jeden raz w miesiącu</li>\n<li>&nbsp;wsparcie chorych w warunkach domowych &ndash; domowe wizyty pomocnik&oacute;w socjalnych raz na dwa tygodnie przez 2 godziny</li>\n</ul>\n<p style=\"text-align: left;\"><strong>Projekt zakłada zmniejszenie liczby zaostrzeń wymagających leczenia szpitalnego o 20% w odniesieniu do liczby zaostrzeń w roku poprzedzającym badanie.</strong></p>\n<h5 style=\"text-align: left; color: #3b5990;\">Przewidywane efekty</h5>\n<ul style=\"padding-left: 20px;\">\n<li><strong>&nbsp;</strong> zmniejszenie liczby wskaźnika rocznych zaostrzeń</li>\n<li>&nbsp; poprawa jakości życia i radzenia sobie z chorobą</li>\n<li>&nbsp; zmniejszenie koszt&oacute;w leczenia</li>\n</ul>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(23,1,4,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'1','none','<a href=\"/strony/spirometria\">Spirometria</a>','none','','xhtml','','xhtml','','none'),(24,1,4,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'1','none','<a href=\"/strony/palenie_papierosow\">Palenie papierosów</a>','none','','xhtml','','xhtml','','none'),(25,1,1,'<p class=\"p1\">System <strong>zintegrowanej opieki dla chorych na zaawansowaną POChP </strong>wprowadzony został, aby poprawić<strong> </strong>sytuację tej grupy chorych.</p>\n<p class=\"p1\">Zintegrowana opieka obejmuje wizyty lekarskie co trzy miesiące i dodatkowo &ndash; wizyty nieplanowane w razie objaw&oacute;w zaostrzenia. Co&nbsp;miesiąc pielęgniarka koordynująca kontaktuje się z chorym w celu sprawdzenia czy realizuje lekarskie zalecenia i, czy nie wystąpiły zaostrzenia POChP.&nbsp; Co dwa tygodnie pomocnik medyczny lub socjalny odwiedza chorego w domu w celu sprawdzenia poprawności stosowania inhalator&oacute;w, przeprowadzenia edukacyjnych rozm&oacute;w dotyczących POChP, zaostrzeń i sposobu radzenia sobie w takiej sytuacji oraz prostych ćwiczeń rehabilitacyjnych. Po roku stosowania zintegrowanej opieki przeprowadza się analizę skuteczności modelu.</p>\n<p class=\"p1\">Oczekuje się, że zintegrowana opieka dla chorych na zaawansowaną POChP pomaga poprawić wyniki leczenia w tej grupie, poprawia los chorych, kt&oacute;rzy najgorzej radzą sobie z chorobą oraz pozwala na znaczące oszczędności.</p>\n<p class=\"p2\">Wstępne badanie przeprowadzone w Chojnicach w wojew&oacute;dztwie pomorskim, wykazało pełną akceptację chorych i ich rodzin dla zintegrowanej opieki, zwłaszcza dla nadzoru pielęgniarki oraz wsparcia pomocnika medycznego i/lub socjalnego w domu chorego.</p>\n<p></p>\n<p><br /><br /></p>\n<hr style=\"height: 1px; background: #ddd;\" />\n<p></p>\n<h3>Schemat zintegrowanej opieki w przypadku stabilnej zaawansowanej POChP</h3>\n<p><img src=\"/images/uploads/schemat_a1.jpg\" style=\"display: block; margin-left: auto; margin-right: auto;\" /></p>\n<p style=\"text-align: center;\">A. Chory po zaostrzeniu szpitalnym jest skierowany do ośrodka biorącego udział w badaniu.</p>\n<p></p>\n<h5 style=\"text-align: left; color: #3b5990;\">Wizyty lekarskie</h5>\n<ul style=\"padding-left: 20px;\">\n<li><strong>Pierwsza wizyta</strong> &ndash; pneumonolog &ndash; zalecenia, plan na wypadek zaostrzenia, edukacja</li>\n<li><strong>Druga wizyta</strong> &ndash; lekarz POZ (po 3 miesiącach) &ndash; wznowienie lek&oacute;w, kontrola stabilizacji</li>\n<li><strong>Trzecia wizyta</strong> &ndash; pneumonolog (po kolejnych 3 miesiącach) &ndash; ocena przebiegu, zmiana zaleceń, jeśli wskazana, edukacja</li>\n<li><strong>Czwarta wizyta</strong> &ndash; lekarz POZ (po kolejnych 3 miesiącach) - wznowienie lek&oacute;w, kontrola stabilizacji</li>\n<li><strong>Piąta wizyta</strong> &ndash; pneumonolog (po kolejnych 3 miesiącach) &ndash; wizyta podsumowująca<span style=\"text-decoration: underline;\"><br /></span></li>\n</ul>\n<p></p>\n<h5 style=\"text-align: left; color: #3b5990;\">Wizyty telefoniczne (<strong>T) </strong>pielęgniarki-koordynatorki</h5>\n<ul style=\"padding-left: 20px;\">\n<li>Co miesiąc pomiędzy wizytami lekarskimi &ndash; informacja czy chory ma leki, czy je właściwe przyjmuje, czy jest potrzebna/realizowana pomoc w&nbsp;domu, czy chory nie pali tytoniu<br /><br /><br /></li>\n</ul>\n<hr style=\"height: 1px; background: #ddd;\" />\n<p></p>\n<h3>Schemat przebiegu wizyty pomocnika społecznego/opiekuna medycznego/wolontariusza, co 2 tygodnie</h3>\n<p><img height=\"146\" src=\"/images/uploads/Schemat_B.jpg\" style=\"display: block; margin-left: auto; margin-right: auto;\" width=\"872\" /></p>\n<p></p>\n<ul style=\"padding-left: 20px;\">\n<li><strong>Rozmowa:</strong> 15-30 minut</li>\n<li><strong>Stosowanie inhalatora/pomiary czynności życiowych: </strong>15-30 minut</li>\n<li><strong>Ćwiczenia og&oacute;lnie usprawniające i oddechowe:</strong> 15-30 minut</li>\n<li><strong>Wsparcie w bieżących problemach &ndash; realizacja recept itp.:</strong> 15-30 minut<strong><br /></strong></li>\n</ul>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(28,1,1,'<p><span style=\"text-decoration: underline;\">Artykuły do pobrania</span></p>\n<ul>\n<li><a href=\"/images/uploads/domowe_leczenie_tlenem.pdf\"><em>Domowe leczenie tlenem (DLT) w opiece paliatywnej</em>; Profesor dr hab. med. Paweł Śliwiński</a></li>\n</ul>\n<ul>\n<li><a href=\"/images/uploads/rehabilitacja_w_opiece_paliatywnej.pdf\"><em>Rehabilitacja w opiece paliatywnej</em>; Profesor dr hab. med. Paweł Śliwiński</a></li>\n</ul>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(29,1,1,'<p>Żyjemy w czasach bardzo rozpędzonych. W ciągłej gonitwie, z listą spraw do załatwienia i z zegarkiem w ręku, kt&oacute;ry zdaje się wciąż przyspieszać. Coraz częściej nawet osoby na emeryturze m&oacute;wią, że nie mają czasu&hellip; Dlatego tak bardzo podziwiam tych, kt&oacute;rzy potrafią wygospodarować kilka chwil, aby spędzić je z chorymi. To wolontariusze, kt&oacute;rzy pomagają w bardzo r&oacute;żny spos&oacute;b. Niekt&oacute;rzy po prostu są, towarzyszą chorym, czasem zrobią drobne zakupy, posprzątają, poczytają gazetę. Niekt&oacute;rzy, po odpowiednim przeszkoleniu, pomagają w&nbsp;codziennej toalecie czy pielęgnacji. Mają czas na rozmowę i wysłuchanie podopiecznego.</p>\n<div style=\"float: right; padding: 0px 0 20px 20px;\"><img alt=\"Anna Janowicz\" height=\"200\" src=\"/images/uploads/Anna_Janowicz.jpg\" style=\"display: inline;\" width=\"152\" />\n<p style=\"text-align: right;\">Anna Janowicz<br /> Fundacja Lubię Pomagać</p>\n</div>\n<p style=\"clear: none;\">Fundacja Lubię Pomagać powstała po to, by taki wolontariat promować i organizować wszędzie tam, gdzie go brakuje. Opierając się na dobrych praktykach wolontariatu medycznego na rzecz chorych terminalnie, szkoli koordynator&oacute;w i wolontariuszy, kt&oacute;rzy zajmują się osobami przewlekle chorymi przebywającymi w domach<br /> i w instytucjach opiekuńczych. W ten spos&oacute;b uzupełniamy opiekę zespoł&oacute;w, w kt&oacute;rych każdy ma swoje konkretne zadanie do spełnienia. Lekarze, pielęgniarki, rehabilitanci, psycholodzy czy pracownicy socjalni dbają o to, by ich podopieczni mieli zapewnioną opiekę medyczną i warunki socjalne. Nie zawsze jednak wystarcza im czasu, by pobyć <br />z chorym trochę dłużej i porozmawiać.</p>\n<p style=\"clear: none;\">Nie zawsze też pomoc taka dociera do wszystkich, szczeg&oacute;lnie chorych w domach. Stąd tak ważne jest uzupełnienie tej opieki o wolontariuszy, kt&oacute;rzy po odpowiednim przygotowaniu, wspierają działania opiekun&oacute;w formalnych. Pomagają też rodzinom, dla kt&oacute;rych opieka nad przewlekle chorym w domu jest wyzwaniem i wysiłkiem na lata.<br /><br />Chorzy potrzebują wolontariuszy. Potrzebują ich czasu i obecności: <em>&bdquo;Ciekawość i miłość to życie. Śmierć to nuda i brak uczuć. Nuda i brak uczuć najbardziej zagrażają dzieciom, osobom starym i ludziom przewlekle chorym. Zdrowi dorośli! Nie lekceważcie tej wiadomości. Tamci naprawdę po trochu się od tego zapadają. Mogą po cichu umrzeć na waszych oczach, jeśli tutaj na ziemi ich nie przytrzymacie.&rdquo;</em> (M. Baranowska, To jest wasze życie. Być sobą w chorobie przewlekłej, Wołowiec 2011)</p>\n<p><strong>Wolontariat na rzecz chorych na POChP</strong></p>\n<p>Według&nbsp; &bdquo;Ustawy o działalności pożytku publicznego i o wolontariacie&rdquo; wolontariusz to osoba, kt&oacute;ra ochotniczo i bez gratyfikacji finansowej wykonuje świadczenia&rdquo;. Do grona podmiot&oacute;w uprawnionych do korzystania z pomocy wolontariuszy są instytucje charytatywne, fundacje, stowarzyszenia oraz ośrodki opieki. Udział wolontariuszy wpisuje się doskonale w koncepcję pracy zespołowej. Aby wolontariat był skuteczny, powinien mieć swojego koordynatora, czyli osobę odpowiedzialną za rekrutowanie, szkolenie i organizowanie pracy wolontariuszy. Ośrodki zainteresowane tą formą opieki powinny więc zacząć od wyznaczenia czy przyjęcia takiej osoby. Może być to psycholog, pracownik socjalny, pracownik administracyjny lub doświadczony wolontariusz czy właściwie przygotowany opiekun medyczny. Podjęcie stałej wsp&oacute;łpracy <br />z wolontariuszem wymaga określenia zakresu jego obowiązk&oacute;w, miejsca w zespole oraz ustalenia ram czasowych. <br />Zar&oacute;wno w domu, jak i w ośrodku należy wsp&oacute;lnie uzgodnić, w jakich godzinach pomoc będzie potrzebna i ilu wolontariuszy może ją świadczyć <br />w tym samym czasie. Dzięki koordynatorowi, właściwemu przygotowaniu (przeszkoleniu) wolontariuszy i wprowadzeniu ich do zespołu, wolontaryjna pomoc będzie wsparciem, nie przeszkodą.</p>\n<p><strong>Jak wolontariusze mogą pom&oacute;c chorym na POChP? </strong></p>\n<p>Pomoc wolontariuszy potrzebna jest przede wszystkim chorym na zaawansowane postaci POChP. Są oni w dużym stopniu niesamodzielni, mają ograniczone możliwości wykonywania codziennych czynności, często też obniżoną sprawność intelektualną. Oznacza to, że nie zawsze rozumieją lekarskie zalecenia lub o nich zapominają. Nie pamiętają o zażywaniu lek&oacute;w lub robią to nieprawidłowo, co znacznie utrudnia proces leczenia<br /> i prowadzi do zaostrzeń. Dlatego&nbsp; jednym z zadań wolontariuszy może być edukacja chorych oraz ich bliskich dotycząca technik inhalacji oraz obsługi nebulizator&oacute;w. Ważną rolą wolontariuszy jest monitorowanie zużycia lek&oacute;w i powiadamianie o tym pielęgniarki lub lekarza, zapobieganie przerwom w leczeniu. Odpowiednio poinstruowani mogą pomagać w ruchowej rehabilitacji chorych. Obecność wolontariusza podczas ćwiczeń daje chorym poczucie bezpieczeństwa i jest jednym z czynnik&oacute;w motywujących do uzyskiwania jak najlepszych wynik&oacute;w. Dla chorych ważna jest także sama obecność wolontariuszy i pomoc w codziennych czynnościach, z kt&oacute;rymi sami już sobie nie radzą. Wolontariusze, przebywając <br />z chorymi, obserwując ich zachowania, mogą też wychwycić zmiany i rodzące się wraz z nimi obawy. Duszność i osłabienie doprowadzają do tego, że chory nie jest w stanie wykonywać podstawowych czynności życiowych. Uciążliwe staje się nawet spożywanie posiłk&oacute;w. Chory szybko się męczy, czasem rezygnuje z jedzenia. Rolą wolontariusza jest nakłanianie do przyjęcia pokarm&oacute;w, podanie posiłku ze spokojem i cierpliwością.</p>\n<p>Możliwości pomagania chorym jest wiele. Wystarczy odrobina chęci i czasu. Szkolenie i wsparcie koordynatora oraz innych członk&oacute;w zespołu opiekuńczego przygotowuje do opieki nad chorymi. Zapraszamy do wsp&oacute;łpracy!<br /><br /></p>\n<hr style=\"height: 1px; background: #ddd;\" />\n<p><a href=\"/images/uploads/Ksiazki_E_J_.pdf\"><span style=\"text-decoration: underline;\"><strong>Ciekawa literatura na temat wolontariatu</strong></span></a></p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(30,1,1,'<link href=\"http://vjs.zencdn.net/c/video-js.css\" rel=\"stylesheet\">\n<script src=\"http://vjs.zencdn.net/c/video.js\"></script>\n<style>\n#my_video_1 div { overflow:visible !important;}\n#my_video_1 { margin-top:20px;}\n</style>\n<video id=\"my_video_1\" class=\"video-js vjs-default-skin\" controls preload=\"auto\" width=\"922\" height=\"692\" data-setup=\"{}\" src=\"/images/uploads/Rehabilitacja_POChP_1.mp4\">\n<source src=\"/images/uploads/Rehabilitacja_POChP_1.mp4\" type=\'video/mp4\'>\n</video>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(31,1,1,'<p>&nbsp;</p>\n<h3><a href=\"/strony/adresy-placowek-realizujcych-projekt\" style=\"font-family: Museo700;\">Adresy plac&oacute;wek realizujących Projekt</a></h3>\n<h3><a href=\"#gabinety\" style=\"font-family: Museo700;\">Adresy gabinet&oacute;w pneumonologicznych&nbsp;&nbsp;&nbsp;</a>&nbsp;</h3>\n<h3><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\" style=\"font-family: Museo700;\">Adresy plac&oacute;wek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a></h3>\n<h3><a href=\"http://www.catestonline.org/english/index_Polish.htm\" style=\"font-family: Museo700;\" target=\"_blank\">Test oceniający POChP</a></h3>\n<p>&nbsp;</p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(38,1,1,'<p class=\"p1\"><strong>Podstawowe cele przyjętej przez Polskie Towarzystwo Program&oacute;w Zdrowotnych polityki prywatności i wykorzystywania plik&oacute;w cookies w serwisach internetowych</strong></p>\n<p class=\"p2\">1. Niniejszy dokument zawierający Politykę prywatności i wykorzystywania plik&oacute;w cookies we wszystkich naszych serwisach internetowych w domenach:</p>\n<ul class=\"ul1\">\n<li class=\"li3\"><strong></strong><a href=\"http://ptpz.pl/\"><span class=\"s1\"><strong>ptpz.pl</strong></span></a><span class=\"s2\">,</span></li>\n<li class=\"li3\"><strong></strong><a href=\"http://dawca.pl/\"><span class=\"s1\"><strong>dawca.pl</strong></span></a><span class=\"s2\">,</span></li>\n</ul>\n<p class=\"p4\">(zwanych dalej także łącznie &bdquo;<strong>Serwisami</strong>&rdquo;) został stworzony i przyjęty przez <strong>Polskie Towarzystwo Program&oacute;w Zdrowotnych</strong> z siedzibą w Gdańsku przy ul. Al.Zwycięstwa 42a, 80-210 Gdańsk, wpisane do Krajowego Rejestru Sądowego pod nr KRS 0000241772, NIP 583-29-26-176, REGON 220116117 (zwane dalej: &bdquo;PTPZ\" lub zamiennie &bdquo;Polskie Towarzystwo Program&oacute;w Zdrowotnych&rdquo;) w celu:</p>\n<ul class=\"ul1\">\n<li class=\"li4\">podkreślenia szczeg&oacute;lnego znaczenia, jakie przypisujemy problematyce ochrony prywatności użytkownik&oacute;w odwiedzających Serwisy.</li>\n<li class=\"li4\">udzielenia Użytkownikom informacji dot. wykorzystywania plik&oacute;w cookies w tych Serwisach, wymaganej przez przepisy ustawy z dnia 16 lipca 200 4. Prawo telekomunikacyjne (Dz. U. Nr 171, poz. 1800 ze zm.), zwanej dalej &bdquo;Prawem telekomunikacyjnym&rdquo;, w brzmieniu obowiązującym od 22 marca 2013 r.</li>\n</ul>\n<p class=\"p2\">2. Poniżej zostały zawarte zasady zbierania i wykorzystywania informacji o użytkownikach obowiązujące w Serwisach.</p>\n<p class=\"p2\">3. Zapewniamy, że naszym nadrzędnym celem jest zapewnienie Użytkownikom Serwis&oacute;w ochrony prywatności na poziomie co najmniej odpowiadającym standardom określonym w obowiązujących przepisach prawnych.</p>\n<p class=\"p2\">4. Zapewniamy także, że zależy nam na przestrzeganiu obowiązujących przepis&oacute;w prawnych, zwłaszcza tych, kt&oacute;re chronią prywatność Użytkownik&oacute;w serwis&oacute;w internetowych.</p>\n<p class=\"p2\">5. Zastrzegamy jednocześnie, że w ramach Serwis&oacute;w mogą zostać zamieszczone linki umożliwiające Użytkownikom bezpośrednie dotarcie do innych stron internetowych. PTPZ nie ma wpływu na prowadzoną przez administrator&oacute;w tych stron politykę prywatności, ani politykę wykorzystywania plik&oacute;w cookies i nie ponosi za nie odpowiedzialności. Jedyne co możemy zrobić, to doradzić Państwu, by przed skorzystaniem z zasob&oacute;w przez nie oferowanych, zapoznali się Państwo z dokumentem dot. polityki prywatności, jak i dokumentem stosowania plik&oacute;w cookies, jeżeli takowe zostały stworzone, a w ich braku skontaktowali się z administratorem danej strony celem uzyskania informacji na ten temat.</p>\n<p class=\"p2\">6. Zastrzegamy także, że na podstawie zawartych przez nas um&oacute;w w ramach serwis&oacute;w <a href=\"http://ptpz.pl/\"><span class=\"s3\">ptpz.pl</span></a>, <a href=\"http://dawca.pl/\"><span class=\"s3\">dawca.pl</span></a>, udostępniamy miejsca na zamieszczanie informacji o naszych partnerach. Partnerzy ci winny przestrzegać obowiązujących przepis&oacute;w prawnych, w tym tych, kt&oacute;re odnoszą się do ochrony prywatności użytkownik&oacute;w serwis&oacute;w internetowych. Jednakże namawiamy użytkownik&oacute;w, by zapoznawali się z dokumentami dot. polityki ochrony prywatności i zasad wykorzystywania cookies przez te podmioty.&nbsp;</p>\n<p class=\"p1\"><strong>Informacje o użytkownikach gromadzone w Serwisach i spos&oacute;b ich wykorzystania</strong></p>\n<p class=\"p2\">Serwisy ograniczają zbieranie i wykorzystywanie informacji o ich użytkownikach do niezbędnego minimum, wymaganego do świadczenia na ich rzecz usług na pożądanym poziomie.</p>\n<p class=\"p2\">Podkreślamy, że tylko w niekt&oacute;rych przypadkach , np. przy formularzach zam&oacute;wień zbieramy bardziej szczeg&oacute;łowe informacje o użytkownikach, takie jak adresy poczty elektronicznej lub inne dane osobowe.</p>\n<p class=\"p2\">Zapewniamy, że wszelkie dane osobowe, jeżeli zostały zebrane w ramach Serwis&oacute;w, służą gł&oacute;wnie do realizacji zobowiązań PTPZ wobec członk&oacute;w wspierających. Informacje te nie są udostępniane osobom trzecim, chyba że:</p>\n<ul class=\"ul1\">\n<li class=\"li4\">po uzyskaniu uprzedniej wyraźnej zgody os&oacute;b, kt&oacute;rych one dotyczą, na takie działanie</li>\n<li class=\"li4\">lub</li>\n<li class=\"li4\">jeżeli obowiązek przekazania tych danych wynika (będzie wynikać) z przepis&oacute;w prawnych.</li>\n</ul>\n<p class=\"p1\"><strong>Polityka wobec plik&oacute;w cookie</strong></p>\n<p class=\"p2\">Informujemy, że termin &bdquo;plik cookie&rdquo; używany w niniejszym dokumencie odnosi się do plik&oacute;w cookies i innych podobnych narzędzi opisanych w Dyrektywie 2009/136/WE Parlamentu Europejskiego dotyczącej przetwarzania danych osobowych i ochrony prywatności w sektorze łączności elektronicznej (Dyrektywa o prywatności i łączności elektronicznej) oraz art. 173 Prawa telekomunikacyjnego.</p>\n<p class=\"p2\">Cookies to małe pliki tekstowe zapisywane na komputerze użytkownika lub innym urządzeniu mobilnym, w czasie korzystania przez niego z serwis&oacute;w internetowych. Pliki te służą <a href=\"http://m.in/\"><span class=\"s3\">m.in</span></a>. korzystaniu z r&oacute;żnych funkcji przewidzianych na danej stronie internetowej lub potwierdzeniu, że dany użytkownik widział określone treści z danej witryny internetowej.</p>\n<p class=\"p5\"><strong></strong></p>\n<p class=\"p2\">Wśr&oacute;d plik&oacute;w cookies wyr&oacute;żnić możne te, kt&oacute;re są niezbędne do działania serwis&oacute;w PTPZ. Należące do tej kategorii pliki cookies wykorzystywane są, aby zapewnić:</p>\n<ul class=\"ul1\">\n<li class=\"li4\">utrzymanie sesji użytkownika;</li>\n<li class=\"li4\">zapisanie stanu sesji użytkownika</li>\n<li class=\"li4\">monitoring dostępności usług.</li>\n</ul>\n<p class=\"p4\">Jednocześnie informujemy r&oacute;wnież, iż niekt&oacute;re elementy serwis&oacute;w PTPZ obsługują Partnerzy umieszczający banery na stronach WWW Polskiego Towarzystwa Program&oacute;w Zdrowotnych. Zawartości pochodzące od Partner&oacute;w, mogą zawierać pliki cookies innych podmiot&oacute;w, w związku z czym zaleca się aby Użytkownik zapoznał się z zasadami wykorzystywania tych plik&oacute;w cookies.</p>\n<p class=\"p6\"><strong>Zarządzanie plikami cookies</strong></p>\n<p class=\"p2\">Użytkownik może zarządzać plikami cookies wykorzystywanymi przez PTPZ lub przez jakichkolwiek innych zewnętrznych dostawc&oacute;w, zmieniając ustawienia swojej przeglądarki internetowej.</p>\n<p class=\"p2\">PTPZ zastrzega jednocześnie, że po odrzuceniu plik&oacute;w cookies niekt&oacute;re funkcje oferowane przez nasze serwisy mogą nie działać prawidłowo, a nawet w niekt&oacute;rych przypadkach wiąże się to z całkowitym uniemożliwieniem korzystania z wybranego produktu.</p>\n<p class=\"p1\"><strong>Ochrona prywatność os&oacute;b, kt&oacute;re nie mają pełnej zdolności do czynności prawnych</strong></p>\n<p class=\"p2\">Zastrzegamy, że Serwisy nie zbierają, nie monitorują ani nie weryfikują informacji na temat wieku os&oacute;b odwiedzających go ani innych, kt&oacute;rych zebranie pozwoliłoby na ustalenie, czy Użytkownik ma zdolność do czynności prawnych.</p>\n<p class=\"p2\">Osoby nie mające pełnej zdolności do czynności prawnych nie powinny dokonywać zam&oacute;wień usług świadczonych przez Serwisy, chyba że ich przedstawiciele ustawowi wyrażą na to zgodę, jeżeli taka zgoda będzie wystarczająca w świetle obowiązującego prawa.</p>\n<p class=\"p1\"><strong>Zabezpieczenia stosowane przez PTPZ</strong></p>\n<p class=\"p2\">Serwisy są zaopatrzone w środki bezpieczeństwa, mające na celu ochronę danych pozostających pod naszą kontrolą przed ich utratą, niewłaściwym wykorzystaniem czy modyfikacją. Zobowiązujemy się chronić wszelkie informacje ujawnione nam przez Odwiedzających zgodnie z normami ochrony bezpieczeństwa i zachowania poufności.</p>\n<p class=\"p2\">Prawa dostępu do danych osobowych użytkownik&oacute;w serwisu zostały w restrykcyjny spos&oacute;b ograniczone, tak aby informacje te nie znalazły się w rękach os&oacute;b niepowołanych. Dostęp do danych osobowych ma tylko ograniczona liczba uprawnionych wsp&oacute;łpracownik&oacute;w PTPZ zgodnie z ustawą o ochronie danych osobowych oraz aktem wykonawczym wydanym na jej podstawie.</p>\n<p class=\"p1\"><strong>Ewentualne zmiany w polityce ochrony prywatności PTPZ</strong></p>\n<p class=\"p2\">Rozw&oacute;j naszych Serwis&oacute;w, postęp technologiczny, jak i zmiany przepis&oacute;w prawnych mogą spowodować konieczność, a nawet obowiązek prawny, wprowadzenia zmian w dotychczas prowadzonej przez Wydawnictwo polityce w zakresie ochrony prywatności. Jeżeli zmiany takie będą miały miejsce, poinformujemy o nich Użytkownik&oacute;w Serwisu natychmiast, w ramach zakładki &bdquo;Polityka prywatności.</p>\n<p class=\"p2\">Zachęcamy zatem wszystkich Użytkownik&oacute;w do zaglądania do tego miejsca w każdym przypadku połączenia się z Serwisem.</p>\n<p class=\"p4\">&nbsp;</p>\n<p class=\"p6\"><strong>Kontakt z PTPZ</strong></p>\n<p class=\"p2\">Wszystkich Użytkownik&oacute;w, kt&oacute;rzy mają jakiekolwiek pytania, uwagi lub zastrzeżenia dotyczące stosowanej przez PTPZ polityki w zakresie ochrony prywatności w ramach Serwisu, w tym dotyczące wyjaśnienia ewentualnych wątpliwości związanych z treścią niniejszej informacji, prosimy o wysłanie ich pod adres: <a href=\"mailto:kontakt@ptpz.pl\"><span class=\"s3\">kontakt@ptpz.pl</span></a>&nbsp;</p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(27,1,1,'<p>W zaawansowanej postaci POChP chorzy z reguły cierpią z powodu duszności, kaszlu z wykrztuszaniem plwociny i złej tolerancji wysiłku. Chorzy ci&nbsp;mają częste zaostrzenia, kt&oacute;re niejednokrotnie wymagają leczenia szpitalnego. Dodatkowo, dotyczy to zwłaszcza chorych po 65 rż, stwierdza się u&nbsp;nich jedną lub więcej chor&oacute;b wsp&oacute;łwystępujących (w tej grupie dużym problemem jest nierozpoznanie depresji) i/lub objawy niewydolności oddychania. Wszystkie te czynniki znacząco pogarszają jakość życia i sprawiają, że chorzy gorzej radzą sobie z chorobą i wypełnianiem lekarskich zaleceń. To z&nbsp;kolei stanowi istotne ryzyko pogorszenia przebiegu choroby i zaostrzeń.</p>\n<div style=\"float: right;\"><img height=\"268\" src=\"/images/uploads/mapka_03.png\" style=\"margin-left: 20px;\" width=\"350\" /></div>\n<p style=\"clear: none;\">Celem zintegrowanej opieki jest poprawa jakości życia poprzez otoczenie chorego i&nbsp;jego rodziny holistyczną opieką odpowiadającą na ich potrzeby medyczne, psychiczne i&nbsp;socjalne. Wymiernym celem tego programu jest zmniejszenie rocznej liczby zaostrzeń, zwłaszcza tych, kt&oacute;re wymagają leczenia szpitalnego. Oczekuje się, że wpłynie to na znaczące zmniejszenie wydatk&oacute;w przeznaczanych na leczenie zaawansowanych postaci POChP.</p>\n<p style=\"clear: none;\"><strong>Program </strong>zakłada stałą opiekę lekarską zgodną z krajowymi i międzynarodowymi zaleceniami, nadz&oacute;r pielęgniarki nad wypełnianiem lekarskich zaleceń (tzw. wizyty telefoniczne) oraz wsparcie w domu aktywizujące chorego fizycznie i intelektualnie. Ważnym elementem modelu jest ścisła wsp&oacute;łpraca pomiędzy opiekunami medycznymi i&nbsp;niemedycznymi.</p>\n<h4>Cele programu</h4>\n<ul style=\"padding-left: 20px;\">\n<li>zmniejszenie liczby zaostrzeń</li>\n<li>poprawa przebiegu POChP</li>\n<li>zmniejszenie wydatk&oacute;w na leczenie POChP</li>\n</ul>\n<p>oraz</p>\n<ul style=\"padding-left: 20px;\">\n<li>poznanie epidemiologii POChP</li>\n<li>edukacja i społeczna świadomość dotycząca POChP</li>\n<li>rozw&oacute;j wsp&oacute;łpracy pomiędzy pracownikami medycznymi i socjalnymi</li>\n<li>rozw&oacute;j wolontariatu</li>\n<li>miejsca&nbsp;pracy dla pracownik&oacute;w medycznych</li>\n</ul>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(32,1,2,'',NULL,'',NULL,'',NULL,'1','none','',NULL,0,NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','none'),(33,1,1,'<h3>Co to jest POChP?</h3>\n<p><span style=\"text-decoration: underline;\">Definicja POChP wg zaleceń Polskiego Towarzystwa Chor&oacute;b Płuc</span></p>\n<p>Przewlekła obturacyjna choroba płuc (POChP) jest powszechną chorobą, kt&oacute;rej można zapobiegać i można leczyć. Cechuje się&nbsp; utrwalonym ograniczeniem przepływu powietrza przez drogi oddechowe, kt&oacute;re zazwyczaj postępuje i jest związane z nadmierną reakcją zapalną w&nbsp;oskrzelach i płucach na szkodliwe działanie&nbsp; gaz&oacute;w i pył&oacute;w, w Polsce najczęściej dymu tytoniowego. Zaostrzenia i choroby wsp&oacute;łistniejące niekorzystnie wpływają na ciężkość przebiegu&nbsp; POChP u poszczeg&oacute;lnych chorych.</p>\n<p style=\"float: right; padding: 0; margin-top: -10px;\"><img height=\"93\" src=\"/images/uploads/zakaz_palenia.jpg\" width=\"250\" /></p>\n<p style=\"clear: none;\"><strong>Najlepszą metodą zapobiegania POChP jest niepalenie papieros&oacute;w.</strong></p>\n<p style=\"clear: none;\"><strong>Rzucenie nałogu jest jednym z najważniejszych element&oacute;w leczenia POChP.</strong></p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Jak rozwija się POChP?</h3>\n<p>POChP rozwija się podstępnie, latami. Najczęściej jest następstwem palenia papieros&oacute;w, chociaż inne przyczyny mogą także prowadzić do POChP&nbsp; &ndash; na przykład zanieczyszczenia powietrza lub częste zakażenia płuc w dzieciństwie.</p>\n<p>Osoby powyżej 40. rż, kt&oacute;re palą lub paliły nałogowo papierosy powinny skorzystać z możliwości wykonania bezpłatnego badania spirometrycznego w ramach programu profilaktycznego finansowanego przez NFZ.</p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Choroby wsp&oacute;łistniejące</h3>\n<p>Chorzy na POChP często mają także inne przewlekłe choroby</p>\n<p><img src=\"/images/uploads/choroby_wspoistniejace.png\" style=\"display: block; margin-left: auto; margin-right: auto; box-shadow: none !important;\" width=\"600\" />&nbsp;</p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Objawy</h3>\n<h4 style=\"text-align: left; color: #3b5990;\">Duszność i zła tolerancja wysiłku</h4>\n<p>Duszność jest częstym objawem POChP.<br />Duszność w POChP można ocenić za pomocą prostej skali:</p>\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\n<tbody>\n<tr>\n<td width=\"200\">\n<p align=\"center\" style=\"text-align: center;\"><strong>Nasilenie duszności</strong></p>\n</td>\n<td>\n<p align=\"center\" style=\"text-align: center;\"><strong>Okoliczności występowania duszności</strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"background-color: #f0faff; text-align: center !important;\" valign=\"middle\">\n<p style=\"text-align: center !important;\">stopień 0</p>\n</td>\n<td valign=\"top\">\n<p>nie odczuwa duszności</p>\n</td>\n</tr>\n<tr>\n<td style=\"background-color: #eaf6ff; background-position: initial initial; background-repeat: initial initial;\" valign=\"middle\">\n<p style=\"text-align: center !important;\">stopień 1</p>\n</td>\n<td valign=\"top\">\n<p style=\"text-align: left;\">duszność występująca podczas szybkiego marszu po terenie płaskim lub podczas marszu pod g&oacute;rę albo&nbsp;wchodzeniu na pierwsze piętro normalnym krokiem</p>\n</td>\n</tr>\n<tr>\n<td style=\"background-color: #dff0ff; text-align: center !important; background-position: initial initial; background-repeat: initial initial;\" valign=\"middle\">\n<p style=\"text-align: center !important;\">stopień 2</p>\n</td>\n<td style=\"background-position: initial initial; background-repeat: initial initial;\" valign=\"top\">\n<p>duszność przy dotrzymywaniu kroku w marszu po terenie płaskim osobie zdrowej w tym samym wieku</p>\n</td>\n</tr>\n<tr>\n<td style=\"background-color: #d5eaff; text-align: center !important; background-position: initial initial; background-repeat: initial initial;\" valign=\"middle\">\n<p style=\"text-align: center !important;\">stopień 3</p>\n</td>\n<td valign=\"top\">\n<p>duszność podczas marszu po terenie płaskim we własnym tempie lub konieczność zatrzymania się po kilku minutach lub przejściu mniej niż 100 m</p>\n</td>\n</tr>\n<tr>\n<td style=\"background-color: #cae4ff; text-align: center !important; background-position: initial initial; background-repeat: initial initial;\" valign=\"middle\">\n<p style=\"text-align: center !important;\">stopień 4</p>\n</td>\n<td valign=\"top\">\n<p>duszność podczas niewielkich wysiłk&oacute;w, jak jedzenie, mycie i ubieranie się, niemożność wyjścia z domu z&nbsp;powodu duszności</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>Jeśli ocenia Pan/Pani swoją wydolność na 2 lub więcej punkt&oacute;w i dotychczas nie rozpoznano POChP &ndash; należy zgłosić się do lekarza.</p>\n<p>Warto znać i pamiętać wyniki badania spirometrycznego.</p>\n<p>Ważny jest wskaźnik FVC i FEV1. Kolejne wyniki warto zapisywać w tabelce.</p>\n<p>(wz&oacute;r tabelki)</p>\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\n<tbody>\n<tr>\n<td colspan=\"5\" valign=\"top\">\n<p><strong>Imię i nazwisko:</strong></p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\" width=\"20%\">\n<p><strong>Data</strong></p>\n</td>\n<td valign=\"top\" width=\"20%\">\n<p><strong>FVC (mililitry)</strong></p>\n</td>\n<td valign=\"top\" width=\"20%\">\n<p><strong>FVC (%)</strong></p>\n</td>\n<td valign=\"top\" width=\"20%\">\n<p><strong>FEV1 (mililitry)</strong></p>\n</td>\n<td valign=\"top\" width=\"20%\">\n<p><strong>FEV1 (%)</strong></p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n<td valign=\"top\">\n<p>&nbsp;</p>\n</td>\n</tr>\n</tbody>\n</table>\n<h4 style=\"text-align: left; color: #3b5990;\">Kaszel i wykrztuszanie</h4>\n<p>Kaszel, suchy lub z wykrztuszaniem plwociny jest częstym objawem w POChP. Nasilenie kaszlu i zwiększenie wydzielania, a także pojawienie się ropnej plwociny jest wskazaniem do skontaktowania się z lekarzem.</p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Leczenie</h3>\n<p>PoChP jest przewlekłą i postępującą chorobą. Tak jak nadciśnienie lub cukrzyca wymaga regularnego i stałego leczenia. Leczenie zawsze ustala lekarz &ndash; adresy lekarzy chor&oacute;b płuc znajdują się w zakładce &bdquo;dla pacjent&oacute;w i opiekun&oacute;w&rdquo;.</p>\n<p>W leczeniu POChP najczęściej stosuje się leki w inhalatorkach</p>\n<p style=\"float: right; padding: 0; margin-top: -10px;\"><img height=\"93\" src=\"/images/uploads/zakaz_palenia.jpg\" width=\"250\" /></p>\n<h3 style=\"clear: none;\">Bardzo ważne jest zaprzestanie palenia papieros&oacute;w</h3>\n<p style=\"clear: none;\">Bardzo ważna jest rehabilitacja og&oacute;lnie usprawniająca i oddechowa &ndash; więcej w zakładce <span style=\"text-decoration: underline;\">&bdquo;rehabilitacja&rdquo;&nbsp;</span></p>\n<p style=\"clear: none;\">Ważne są coroczne szczepienia przeciw grypie i przeciw pneumonokokom (raz na pięć lat)</p>\n<hr style=\"height: 1px; background: #ddd; margin-bottom: 40px;\" />\n<h3>Zaostrzenie</h3>\n<p>Zaostrzenie POChP &ndash; to nagłe pogorszenie duszności, kaszlu i większego wykrztuszania plwociny (zwłaszcza ropnej) utrzymujące się dłużej niż jeden dzień.</p>\n<p>Chorzy, kt&oacute;rzy otrzymali od swoich lekarzy pisemne zalecenie, co robić na wypadek zaostrzenia, powinni je dokładnie wypełnić.</p>\n<p>Zaostrzenie jest wskazaniem do wizyty u lekarza, zwłaszcza, jeśli chory nie otrzymał wcześniej zalecenia jak postępować w takim przypadku.</p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(34,1,1,'<p>Rejester lekarzy - dowiedz się więcej</p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(36,1,1,'<p></p>\n<p></p>\n<div style=\"overflow: hidden;\">\n<div style=\"width: 30%; float: left; clear: none;\">\n<p><strong>Gdański Uniwersytet Medyczny</strong></p>\n<p><span style=\"text-decoration: underline;\">Klinika Alergologii</span></p>\n<p>ul. Dębinki 7</p>\n<p>80-211 Gdańsk</p>\n<p>tel. (58) 349 16 25</p>\n<p></p>\n<p><span style=\"text-decoration: underline;\">Klinika Pneumonologii</span></p>\n<p>ul. Dębinki 7</p>\n<p>80-211 Gdańsk</p>\n<p>tel. (58) 349 25 06</p>\n<p></p>\n</div>\n<div style=\"width: 50%; float: left; clear: none;\">\n<p style=\"text-align: center; float: left; width: 45%;\"><img alt=\"dr Iwona Damps -Konstańska\" height=\"132\" src=\"/images/uploads/dr_Iwona_Damps-Konstanska.jpg\" style=\"display: inline;\" title=\"dr Iwona Damps -Konstańska\" width=\"100\" /><br />dr Iwona Damps -Konstańska</p>\n<p style=\"text-align: center; float: left; width: 45%;\"><img alt=\"dr Bogumiła Cynowska\" height=\"132\" src=\"/images/uploads/dr_Bogumila_Cynowska.jpg\" style=\"display: inline;\" title=\"dr Bogumiła Cynowska\" width=\"100\" /><br />dr Bogumiła Cynowska</p>\n</div>\n</div>\n<div style=\"overflow: hidden;\">\n<div style=\"width: 30%; float: left; clear: none;\">\n<p><strong>Szpital Rejonowy SPZOZ</strong></p>\n<p>Poradnia Chor&oacute;b Płuc</p>\n<p>ul. Leśna 8</p>\n<p>89-600 Chojnice</p>\n<p>tel. (52) 395 69 65</p>\n<p></p>\n</div>\n<div style=\"width: 50%; float: left; clear: none;\">\n<p style=\"text-align: center; float: left; width: 45%;\"><img alt=\"lek. med. Małgorzata Kaczmarek\" height=\"132\" src=\"/images/uploads/lek._med._Malgorzata_Kaczmarek.jpg\" style=\"display: inline;\" title=\"lek. med. Małgorzata Kaczmarek\" width=\"100\" /><br />lek. med. Małgorzata Kaczmarek</p>\n</div>\n</div>\n<div style=\"overflow: hidden;\">\n<div style=\"width: 30%; float: left; clear: none;\">\n<p><strong>Niepubliczny Zakład Opieki Zdrowotnej</strong></p>\n<p>Proadnia Chor&oacute;b Płuc w Słupsku</p>\n<p>ul. Hubalczyk&oacute;w 5/11</p>\n<p>76-200 Słupsk</p>\n<p>tel. (59) 842 01 84</p>\n<p></p>\n</div>\n<div style=\"width: 50%; float: left; clear: none;\">\n<p style=\"text-align: center; float: left; width: 45%;\"><img alt=\"lek. med. Sławomir Grabicz\" height=\"132\" src=\"/images/uploads/lek._med._Slawomir_Garbicz.jpg\" style=\"display: inline;\" title=\"lek. med. Sławomir Grabicz\" width=\"100\" /><br />lek. med. Sławomir Grabicz</p>\n</div>\n</div>\n&nbsp;\n<div style=\"overflow: hidden;\">\n<div style=\"width: 30%; float: left; clear: none;\">\n<p style=\"text-align: left;\"><strong>Poradnia Gruźlicy i Chor&oacute;b Płuc</strong></p>\n<p style=\"text-align: left;\">ul. Hallera 21</p>\n<p style=\"text-align: left;\">Starogard Gdański</p>\n<p style=\"text-align: left;\">tel. (58) 775 40 40</p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left;\"></p>\n<p style=\"text-align: left; left: 20px; top: 20px; width: 100px; height: 100px; position: absolute; z-index: 1;\">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n</div>\n<div style=\"width: 50%; float: left; clear: none;\">\n<p style=\"text-align: center; float: left; width: 45%;\"><img alt=\"lek.med. Katarzyna Krejci-Badowicz\" height=\"132\" src=\"/images/uploads/lek.med._Katarzyna_Krejci-Badowicz.jpeg\" style=\"display: inline;\" title=\"lek.med. Katarzyna Krejci-Badowicz\" width=\"100\" /><br />lek.med. Katarzyna Krejci-Badowicz</p>\n<p style=\"text-align: center; float: left; width: 45%;\"></p>\n<p style=\"text-align: center; float: left; width: 45%;\"></p>\n</div>\n</div>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(37,1,1,'<p></p>\n<p></p>\n<p><strong>SP CSK</strong></p>\n<p><strong>Klinika Chor&oacute;b Wewnętrznych, Pneumonologii i Alergologii</strong></p>\n<p><span>ul. Banacha 1a</span></p>\n<p><span>02-097 <strong>WARSZAWA</strong></span></p>\n<p><span>tel. 22/599-28-51 (lekarz); tel. 22/559-15-68 (rejestracja) </span></p>\n<hr />\n<p></p>\n<p><strong>SP ZOZ</strong></p>\n<p><strong>Poradnia Domowego Leczenia Tlenem</strong></p>\n<p><span>ul. Aleje Grunwaldzkie 1</span></p>\n<p><span>22-600 <strong>TOMASZ&Oacute;W LUB.</strong><br /></span></p>\n<p><span>tel. 84/664-44-11 wew. 345</span></p>\n<p><span>lek. med. Alina Brzozowska-Gałan</span></p>\n<hr />\n<p><strong>Uniwersyteckie Centrum Kliniczne w Gdańsku</strong></p>\n<p><strong>Klinika Alergologii i Pneumonologii</strong></p>\n<p>ul. Dębinki 7</p>\n<p>80-952 <strong>GDAŃSK</strong></p>\n<p>bud.4 I piętro</p>\n<p>tel. 58/349-16-25 (sekretariat)</p>\n<p>dr Iwona Damps-Konstańska (damik@gumed.edu.pl)</p>\n<p>dr Beata Wajda (bwajda@gumed.edu.pl)</p>\n<hr />\n<p><strong>Poradnia Domowego Leczenia Tlenem</strong></p>\n<p>Beata Asankowicz-Bargiel i PARTNERZY</p>\n<p>Lekarze Sp&oacute;łka Partnerska</p>\n<p>ul. Limanowskiego 20/22</p>\n<p>63-400<strong> OSTR&Oacute;W WIELKOPOLSKI</strong></p>\n<p>tel. 62/735-44-81</p>\n<hr />\n<p><strong>Zesp&oacute;ł Przychodni Specjalistycznych</strong></p>\n<p><strong>Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</strong></p>\n<p><strong>Ośrodek Domowego Leczenia TLENEM</strong></p>\n<p>ul. Mostowa 6</p>\n<p>33-100 <strong>TARN&Oacute;W</strong></p>\n<p>tel. 77/849-29-47</p>\n<hr />\n<p><strong>Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</strong></p>\n<p><strong>Ośrodek Tlenoterapii Domowej</strong></p>\n<p>ul. Generała Orlicz-Dreszera 10</p>\n<p>22-100<strong> CHEŁM</strong></p>\n<hr />\n<p><strong>Wojew&oacute;dzki Zesp&oacute;ł Zakład&oacute;w Opieki Zdrowotnej</strong></p>\n<p><strong>Centrum Leczenia Chor&oacute;b Płuc i Rehabilitacji</strong></p>\n<p><strong>Centrum Tlenoterapii</strong></p>\n<p>Zesp&oacute;ł Domowego Leczenia Tlenem</p>\n<p>ul. Ok&oacute;lna 181</p>\n<p>91-520 <strong>Ł&Oacute;DŹ</strong></p>\n<p>tel. 42/61-77-369; 42/61-77-423</p>\n<p>dr n.med. Katarzyna Rzeszutek</p>\n<hr />\n<p><strong>Samodzielny Publiczny Zesp&oacute;ł Grużlicy i Chor&oacute;b Płuc w Olsztynie</strong></p>\n<p><strong>Ośrodek Domowego Leczenia Tlenem</strong></p>\n<p>ul. Jagiellońska 78</p>\n<p>10-357<strong> OLSZTYN</strong></p>\n<p>tel. 89/532-29-01</p>\n<hr />\n<p><strong>ZESP&Oacute;Ł DOMOWEGO LECZENIA TLENEM</strong></p>\n<p>Samodzielny Specjalistyczny Zesp&oacute;ł Zakład&oacute;w Opieki Zdrowotnej im. dr. Teodora Dunina</p>\n<p>Alejsa Teodora Dunina 1</p>\n<p>05-320<strong> MROZY</strong></p>\n<p>TEL. 25/757-43-43; 757-49-51</p>\n<p>lek. Beata Pielasa, lek. Mirosław Trzciński</p>\n<hr />\n<p><strong>OŚRODEK DOMOWEGO LECZENIA TLENEM </strong></p>\n<p><strong>przy Oddziale Chor&oacute;b Płuc</strong></p>\n<p>ul. Wyszyńskiego 11</p>\n<p>11-200 <strong>BARTOSZYCE</strong></p>\n<p>tel. 89/764-94-33</p>\n<p>oddzialplucny@wp.pl</p>\n<p>dr Hanna Wołodko</p>\n<hr />\n<p><strong>OŚRODEK DOMOWEGO LECZENIA TLENEM</strong></p>\n<p>Szpital Specjalistyczny w Prabutach Sp. z o.o.</p>\n<p>ul. Kuracyjana 30</p>\n<p>82-550 <strong>PRABUTY </strong></p>\n<p>tel. 55/262-43-28; 55/262-43-40</p>\n<p>dr Wanda Setto</p>','none','','xhtml','','none','',NULL,'',NULL,0,'none','',NULL,'',NULL,'','xhtml','','xhtml','','none'),(39,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Klinika Chorób Wewnętrznych, \nPneumonologii i Alergologii\n\nul. Banacha 1a\n\n02-097 WARSZAWA\n\ntel. 22/599-28-51 (lekarz)\ntel. 22/559-15-68 (rejestracja)','xhtml','SP CSK','xhtml','<iframe width=\"610\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Stefana+Banacha+1A,+Warszawa&amp;aq=2&amp;oq=ul.+Banacha+1a&amp;sll=54.361087,18.690027&amp;sspn=0.327675,0.76767&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Stefana+Banacha+1A,+Warszawa,+mazowieckie&amp;ll=52.214391,20.984831&amp;spn=0.021036,0.052271&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Stefana+Banacha+1A,+Warszawa&amp;aq=2&amp;oq=ul.+Banacha+1a&amp;sll=54.361087,18.690027&amp;sspn=0.327675,0.76767&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Stefana+Banacha+1A,+Warszawa,+mazowieckie&amp;ll=52.214391,20.984831&amp;spn=0.021036,0.052271&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(42,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Poradnia Domowego Leczenia Tlenem\n\nul. Aleje Grunwaldzkie 1\n\n22-600 TOMASZÓW LUB.\n\ntel. 84/664-44-11 wew. 345\n\nlek. med. Alina Brzozowska-Gałan','xhtml','SP ZOZ','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Aleje+Grunwaldzkie+1,+Tomasz%C3%B3w+Lubelski,+lubelskie+&amp;aq=&amp;sll=50.443069,23.416043&amp;sspn=0.011192,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Aleje+Grunwaldzkie+1,+Tomasz%C3%B3w+Lubelski,+tomaszowski,+lubelskie&amp;ll=50.449689,23.414869&amp;spn=0.021861,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Aleje+Grunwaldzkie+1,+Tomasz%C3%B3w+Lubelski,+lubelskie+&amp;aq=&amp;sll=50.443069,23.416043&amp;sspn=0.011192,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Aleje+Grunwaldzkie+1,+Tomasz%C3%B3w+Lubelski,+tomaszowski,+lubelskie&amp;ll=50.449689,23.414869&amp;spn=0.021861,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(43,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Klinika Alergologii i Pneumonologii\n\nul. Dębinki 7\n\n80-952 GDAŃSK\n\nbud.4 I piętro\n\ntel. 58/349-16-25 (sekretariat)\n\ndr Iwona Damps-Konstańska \n(damik@gumed.edu.pl)\n\ndr Beata Wajda (bwajda@gumed.edu.pl)\n','xhtml','Uniwersyteckie Centrum Kliniczne w Gdańsku','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=gda%C5%84sk+ul.+D%C4%99binki+7&amp;aq=&amp;sll=54.529995,18.49912&amp;sspn=0.652651,1.535339&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=D%C4%99binki+7,+Gda%C5%84sk,+pomorskie&amp;ll=54.370659,18.618307&amp;spn=0.02,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=gda%C5%84sk+ul.+D%C4%99binki+7&amp;aq=&amp;sll=54.529995,18.49912&amp;sspn=0.652651,1.535339&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=D%C4%99binki+7,+Gda%C5%84sk,+pomorskie&amp;ll=54.370659,18.618307&amp;spn=0.02,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(44,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Beata Asankowicz-Bargiel i PARTNERZY\n\nLekarze Spółka Partnerska\n\nul. Limanowskiego 20/22\n\n63-400 OSTRÓW WIELKOPOLSKI\n\ntel. 62/735-44-81','xhtml','Poradnia Domowego Leczenia Tlenem','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Limanowskiego+20%2F22++OSTR%C3%93W+WIELKOPOLSKI&amp;aq=&amp;sll=54.365883,18.621526&amp;sspn=0.020477,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Boles%C5%82awa+Limanowskiego+20%2F22,+Ostr%C3%B3w+Wielkopolski,+ostrowski,+wielkopolskie&amp;ll=51.659885,17.822142&amp;spn=0.021297,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Limanowskiego+20%2F22++OSTR%C3%93W+WIELKOPOLSKI&amp;aq=&amp;sll=54.365883,18.621526&amp;sspn=0.020477,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Boles%C5%82awa+Limanowskiego+20%2F22,+Ostr%C3%B3w+Wielkopolski,+ostrowski,+wielkopolskie&amp;ll=51.659885,17.822142&amp;spn=0.021297,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(45,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Ośrodek Domowego Leczenia TLENEM\n\nul. Mostowa 6\n\n33-100 TARNÓW\n\ntel. 77/849-29-47','xhtml','Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Mostowa+6,+Tarn%C3%B3w&amp;aq=0&amp;oq=Mostowa+6+tar&amp;sll=51.654188,17.824373&amp;sspn=0.021806,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Mostowa+6,+Tarn%C3%B3w,+ma%C5%82opolskie&amp;ll=50.016564,20.998478&amp;spn=0.022061,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Mostowa+6,+Tarn%C3%B3w&amp;aq=0&amp;oq=Mostowa+6+tar&amp;sll=51.654188,17.824373&amp;sspn=0.021806,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Mostowa+6,+Tarn%C3%B3w,+ma%C5%82opolskie&amp;ll=50.016564,20.998478&amp;spn=0.022061,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(46,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Ośrodek Tlenoterapii Domowej\n\nul. Gustawa Orlicz-Dreszera 10\n\n22-100 CHEŁM','xhtml','Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Che%C5%82m+Orlicz-Dreszera+10&amp;aq=&amp;sll=50.928895,23.366915&amp;sspn=0.708891,1.535339&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Doktora+Gustawa+Orlicz-Dreszera+10,+Che%C5%82m,+lubelskie&amp;ll=51.139725,23.466368&amp;spn=0.021541,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Che%C5%82m+Orlicz-Dreszera+10&amp;aq=&amp;sll=50.928895,23.366915&amp;sspn=0.708891,1.535339&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Doktora+Gustawa+Orlicz-Dreszera+10,+Che%C5%82m,+lubelskie&amp;ll=51.139725,23.466368&amp;spn=0.021541,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(47,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Centrum Leczenia Chorób Płuc i Rehabilitacji\n\nCentrum Tlenoterapii\n\nZespół Domowego Leczenia Tlenem\n\nul. Okólna 181\n\n91-520 ŁÓDŹ\n\ntel. 42/61-77-369; 42/61-77-423\n\ndr n.med. Katarzyna Rzeszutek','xhtml','Wojewódzki Zespół Zakładów Opieki Zdrowotnej','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Ok%C3%B3lna+181,+%C5%81%C3%B3d%C5%BA&amp;aq=0&amp;oq=Ok%C3%B3lna+181&amp;sll=51.13459,23.46696&amp;sspn=0.011027,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Ok%C3%B3lna+181,+%C5%81%C3%B3d%C5%BA,+%C5%82%C3%B3dzkie&amp;ll=51.855609,19.477472&amp;spn=0.021205,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Ok%C3%B3lna+181,+%C5%81%C3%B3d%C5%BA&amp;aq=0&amp;oq=Ok%C3%B3lna+181&amp;sll=51.13459,23.46696&amp;sspn=0.011027,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Ok%C3%B3lna+181,+%C5%81%C3%B3d%C5%BA,+%C5%82%C3%B3dzkie&amp;ll=51.855609,19.477472&amp;spn=0.021205,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(48,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Ośrodek Domowego Leczenia Tlenem\n\nul. Jagiellońska 78\n\n10-357 OLSZTYN\n\ntel. 89/532-29-01','xhtml','Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Jagiello%C5%84ska+78,+Olsztyn&amp;aq=0&amp;oq=Jagiello%C5%84ska+78,+ol&amp;sll=51.847678,19.479419&amp;sspn=0.010856,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Jagiello%C5%84ska+78,+11-041+Olsztyn,+warmi%C5%84sko-mazurskie&amp;ll=53.809825,20.505638&amp;spn=0.020272,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Jagiello%C5%84ska+78,+Olsztyn&amp;aq=0&amp;oq=Jagiello%C5%84ska+78,+ol&amp;sll=51.847678,19.479419&amp;sspn=0.010856,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Jagiello%C5%84ska+78,+11-041+Olsztyn,+warmi%C5%84sko-mazurskie&amp;ll=53.809825,20.505638&amp;spn=0.020272,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(49,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Samodzielny Specjalistyczny Zespół Zakładów Opieki Zdrowotnej im. dr. Teodora Dunina\n\nAlejsa Teodora Dunina 1\n\n05-320 MROZY\n\nTEL. 25/757-43-43; 757-49-51\n\nlek. Beata Pielasa, lek. Mirosław Trzciński','xhtml','ZESPÓŁ DOMOWEGO LECZENIA TLENEM','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Dunina+1,+Mrozy&amp;aq=&amp;sll=52.163614,21.810479&amp;sspn=0.02156,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=Dunina+1,&amp;hnear=Mrozy&amp;cid=18415175393835458500&amp;ll=52.163614,21.842794&amp;spn=0.02106,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Dunina+1,+Mrozy&amp;aq=&amp;sll=52.163614,21.810479&amp;sspn=0.02156,0.047979&amp;t=m&amp;ie=UTF8&amp;hq=Dunina+1,&amp;hnear=Mrozy&amp;cid=18415175393835458500&amp;ll=52.163614,21.842794&amp;spn=0.02106,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(50,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'przy Oddziale Chorób Płuc\n\nul. Wyszyńskiego 11\n\n11-200 BARTOSZYCE\n\ntel. 89/764-94-33\n\noddzialplucny@wp.pl\n\ndr Hanna Wołodko','xhtml','OŚRODEK DOMOWEGO LECZENIA TLENEM','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Wyszy%C5%84skiego+11+bartoszyce&amp;aq=&amp;sll=52.161139,21.831551&amp;sspn=0.043122,0.095959&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Kardyna%C5%82a+Stefana+Wyszy%C5%84skiego+11,+Bartoszyce,+bartoszycki,+warmi%C5%84sko-mazurskie&amp;ll=54.264873,20.789652&amp;spn=0.020051,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Wyszy%C5%84skiego+11+bartoszyce&amp;aq=&amp;sll=52.161139,21.831551&amp;sspn=0.043122,0.095959&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Kardyna%C5%82a+Stefana+Wyszy%C5%84skiego+11,+Bartoszyce,+bartoszycki,+warmi%C5%84sko-mazurskie&amp;ll=54.264873,20.789652&amp;spn=0.020051,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none'),(51,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,0,NULL,'',NULL,'',NULL,'Szpital Specjalistyczny w Prabutach Sp. z o.o.\n\nul. Kuracyjana 30\n\n82-550 PRABUTY\n\ntel. 55/262-43-28; 55/262-43-40\n\ndr Wanda Setto','xhtml','OŚRODEK DOMOWEGO LECZENIA TLENEM','xhtml','<iframe width=\"600\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Kuracyjna+30,+Prabuty&amp;aq=0&amp;oq=Kuracyjana+30+&amp;sll=54.258541,20.792365&amp;sspn=0.010265,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Kuracyjna+30,+82-550+Prabuty,+kwidzy%C5%84ski,+pomorskie&amp;ll=53.742671,19.209509&amp;spn=0.020305,0.051413&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Kuracyjna+30,+Prabuty&amp;aq=0&amp;oq=Kuracyjana+30+&amp;sll=54.258541,20.792365&amp;sspn=0.010265,0.02399&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Kuracyjna+30,+82-550+Prabuty,+kwidzy%C5%84ski,+pomorskie&amp;ll=53.742671,19.209509&amp;spn=0.020305,0.051413&amp;z=14&amp;iwloc=A\" style=\"color:#0000FF;text-align:left\">Wyświetl większą mapę</a></small>','none');
/*!40000 ALTER TABLE `exp_channel_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_entries_autosave`
--

DROP TABLE IF EXISTS `exp_channel_entries_autosave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL auto_increment,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL default '0',
  `pentry_id` int(10) NOT NULL default '0',
  `forum_topic_id` int(10) unsigned default NULL,
  `ip_address` varchar(16) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL default 'n',
  `view_count_one` int(10) unsigned NOT NULL default '0',
  `view_count_two` int(10) unsigned NOT NULL default '0',
  `view_count_three` int(10) unsigned NOT NULL default '0',
  `view_count_four` int(10) unsigned NOT NULL default '0',
  `allow_comments` varchar(1) NOT NULL default 'y',
  `sticky` varchar(1) NOT NULL default 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL default 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL default '0',
  `comment_expiration_date` int(10) NOT NULL default '0',
  `edit_date` bigint(14) default NULL,
  `recent_comment_date` int(10) default NULL,
  `comment_total` int(4) unsigned NOT NULL default '0',
  `entry_data` text,
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_entries_autosave`
--

LOCK TABLES `exp_channel_entries_autosave` WRITE;
/*!40000 ALTER TABLE `exp_channel_entries_autosave` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_channel_entries_autosave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_fields`
--

DROP TABLE IF EXISTS `exp_channel_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL default 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL default 'n',
  `field_pre_channel_id` int(6) unsigned default NULL,
  `field_pre_field_id` int(6) unsigned default NULL,
  `field_related_to` varchar(12) NOT NULL default 'channel',
  `field_related_id` int(6) unsigned NOT NULL default '0',
  `field_related_orderby` varchar(12) NOT NULL default 'date',
  `field_related_sort` varchar(4) NOT NULL default 'desc',
  `field_related_max` smallint(4) NOT NULL default '0',
  `field_ta_rows` tinyint(2) default '8',
  `field_maxl` smallint(3) default NULL,
  `field_required` char(1) NOT NULL default 'n',
  `field_text_direction` char(3) NOT NULL default 'ltr',
  `field_search` char(1) NOT NULL default 'n',
  `field_is_hidden` char(1) NOT NULL default 'n',
  `field_fmt` varchar(40) NOT NULL default 'xhtml',
  `field_show_fmt` char(1) NOT NULL default 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL default 'any',
  `field_settings` text,
  PRIMARY KEY  (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_fields`
--

LOCK TABLES `exp_channel_fields` WRITE;
/*!40000 ALTER TABLE `exp_channel_fields` DISABLE KEYS */;
INSERT INTO `exp_channel_fields` VALUES (1,1,1,'site_content','Treść strony','Prosimy wpisać treść strony','nsm_tiny_mce','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','y','n','none','n',3,'any','YTo4OntzOjQ6ImNvbmYiO3M6MTE6ImFkdmFuY2VkLmpzIjtzOjY6ImhlaWdodCI7czozOiIzMDAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(2,1,1,'site_abstract','Abstrakt','Prosimy wpisać abstrakt strony','textarea','','0',0,0,'channel',1,'title','desc',0,6,128,'n','ltr','y','y','xhtml','n',2,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6InkiO30='),(3,1,1,'site_subtitle','Krótka treść pod tytułem','Prosimy wpisać krótką treść pod tytułem','text','','0',0,0,'channel',1,'title','desc',0,6,128,'n','ltr','y','n','none','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(4,1,2,'banner','Banery','','matrix','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',1,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMyI7czo3OiJjb2xfaWRzIjthOjU6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjciO2k6MjtzOjE6IjIiO2k6MztzOjE6IjMiO2k6NDtzOjE6IjQiO319'),(5,1,3,'logo','Loga','','matrix','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',1,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MToiNSI7aToxO3M6MjoiMTAiO2k6MjtzOjE6IjYiO319'),(6,1,1,'site_menu','Menu','Id menu','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',4,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NzoiaW50ZWdlciI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),(7,1,4,'menu_links','Linki','','matrix','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',1,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiOCI7aToxO3M6MToiOSI7fX0='),(8,1,4,'menu_title','Nagłówek','','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(13,1,5,'opis','opis','opis','textarea','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','xhtml','n',2,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(14,1,5,'tytul','tytul','','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(15,1,5,'iframe_map','iframe_map','iframe_map','text','','0',0,0,'channel',2,'title','desc',0,6,5000,'y','ltr','n','n','none','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
/*!40000 ALTER TABLE `exp_channel_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_member_groups`
--

DROP TABLE IF EXISTS `exp_channel_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_member_groups`
--

LOCK TABLES `exp_channel_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_channel_member_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_channel_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_titles`
--

DROP TABLE IF EXISTS `exp_channel_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL default '0',
  `pentry_id` int(10) NOT NULL default '0',
  `forum_topic_id` int(10) unsigned default NULL,
  `ip_address` varchar(16) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL default 'n',
  `view_count_one` int(10) unsigned NOT NULL default '0',
  `view_count_two` int(10) unsigned NOT NULL default '0',
  `view_count_three` int(10) unsigned NOT NULL default '0',
  `view_count_four` int(10) unsigned NOT NULL default '0',
  `allow_comments` varchar(1) NOT NULL default 'y',
  `sticky` varchar(1) NOT NULL default 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL default 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL default '0',
  `comment_expiration_date` int(10) NOT NULL default '0',
  `edit_date` bigint(14) default NULL,
  `recent_comment_date` int(10) default NULL,
  `comment_total` int(4) unsigned NOT NULL default '0',
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_titles`
--

LOCK TABLES `exp_channel_titles` WRITE;
/*!40000 ALTER TABLE `exp_channel_titles` DISABLE KEYS */;
INSERT INTO `exp_channel_titles` VALUES (1,1,1,1,0,NULL,'89.67.201.68','Wypowiedź Prezesa Polskiego Towarzystwa Chorób Płuc','dorota-gorecka','open','y',0,0,0,0,'y','n',1323172770,'n','2011','12','06',0,0,20130401191131,0,0),(2,1,3,2,0,NULL,'89.69.164.162','Loga','loga','open','y',0,0,0,0,'y','n',1328781229,'n','2012','02','09',0,0,20130106122551,0,0),(3,1,2,2,0,NULL,'89.67.201.68','Banery','banery','open','y',0,0,0,0,'y','n',1328781192,'n','2012','02','09',0,0,20130328211114,0,0),(4,1,1,2,0,NULL,'87.204.0.67','POChP','pochp','open','y',0,0,0,0,'y','n',1328798877,'n','2012','02','09',0,0,20121105153959,0,0),(5,1,4,2,0,NULL,'87.204.0.67','Menu POChP','menu-popchp','open','y',0,0,0,0,'y','n',1328801008,'n','2012','02','09',0,0,20121029154829,0,0),(8,1,1,1,0,NULL,'87.204.0.67','Rejestracja dla Chorych Elektroniczna Karta Pacjenta','elektroniczna_karta_pacjenta','open','y',0,0,0,0,'y','n',1328873083,'n','2012','02','10',0,0,20120224123445,0,0),(7,1,1,1,0,NULL,'87.204.0.67','Rejestr lekarzy','rejestr_lekarzy','open','y',0,0,0,0,'y','n',1328871205,'n','2012','02','10',0,0,20121106110526,0,0),(9,1,1,1,0,NULL,'87.204.0.67','Pomoc','pomoc','open','y',0,0,0,0,'y','n',1328874049,'n','2012','02','10',0,0,20121029163850,0,0),(10,1,1,1,0,NULL,'87.204.0.67','POChP','strony-pochp','open','y',0,0,0,0,'y','n',1329126730,'n','2012','02','13',0,0,20121105153911,0,0),(11,1,1,1,0,NULL,'87.204.0.67','Spirometria','spirometria','open','y',0,0,0,0,'y','n',1329127266,'n','2012','02','13',0,0,20120306121208,0,0),(12,1,1,1,0,NULL,'87.204.0.67','Palenie papierosów','palenie_papierosow','open','y',0,0,0,0,'y','n',1329127606,'n','2012','02','13',0,0,20121003153947,0,0),(13,1,1,2,0,NULL,'87.204.0.67','Co to jest POChP?','co_to_jest_pochp','open','y',0,0,0,0,'y','n',1329818757,'n','2012','02','21',0,0,20121030163859,0,0),(14,1,1,1,0,NULL,'87.204.0.67','Czynniki ryzyka POChP','czynniki_ryzyka_pochp','open','y',0,0,0,0,'y','n',1329819046,'n','2012','02','21',0,0,20121030163947,0,0),(15,1,1,1,0,NULL,'87.204.0.67','Jak zapobiegać POChP','jak_zapobiegac','open','y',0,0,0,0,'y','n',1329819985,'n','2012','02','21',0,0,20121107095826,0,0),(16,1,1,1,0,NULL,'87.204.0.67','Jak rozpoznać POChP','jak_rozpoznac_pochp','open','y',0,0,0,0,'y','n',1329820194,'n','2012','02','21',0,0,20121029152255,0,0),(17,1,1,1,0,NULL,'87.204.0.67','Co to jest spirometria?','co_to_jest_spirometria','open','y',0,0,0,0,'y','n',1329820452,'n','2012','02','21',0,0,20121030164113,0,0),(18,1,1,1,0,NULL,'87.204.0.67','Do kogo się zwrócić?','do_kogo_sie_zwrocic','open','y',0,0,0,0,'y','n',1329820728,'n','2012','02','21',0,0,20121029152449,0,0),(19,1,1,1,0,NULL,'87.204.0.67','Dlaczego palenie jest złe?','dlaczego_palenie_jest_zle','open','y',0,0,0,0,'y','n',1329820815,'n','2012','02','21',0,0,20121107095616,0,0),(20,1,1,1,0,NULL,'89.67.201.68','Jak rzucić ?','jak_rzucic','open','y',0,0,0,0,'y','n',1329821050,'n','2012','02','21',0,0,20130401193211,0,0),(21,1,1,1,0,NULL,'89.67.201.68','Leczenie uzależnienia','leczenie_uzaleznienia','open','y',0,0,0,0,'y','n',1329821162,'n','2012','02','21',0,0,20130401194303,0,0),(22,1,1,1,0,NULL,'87.204.0.67','Palenie choroba zakaźna','palenie_choroba_zakazna','open','y',0,0,0,0,'y','n',1329821324,'n','2012','02','21',0,0,20121107095545,0,0),(23,1,4,3,0,NULL,'87.204.0.67','Menu Spirometria','menu-spirometria','open','y',0,0,0,0,'y','n',1330010924,'n','2012','02','23',0,0,20121029154945,0,0),(25,1,1,1,0,NULL,'89.67.201.68','Program zintegrowanej opieki dla chorych na zaawansowaną POChP','zycie-z-pochp','open','y',0,0,0,0,'y','n',1332495276,'n','2012','03','23',0,0,20130401191337,0,0),(24,1,4,3,0,NULL,'87.204.0.67','Menu Palenie papierosow','menu-palenie-papierosow','open','y',0,0,0,0,'y','n',1330011124,'n','2012','02','23',0,0,20121029154906,0,0),(26,1,1,1,0,NULL,'89.67.201.68','Projekt ,,Pomorze\"','system-opieki','open','y',0,0,0,0,'y','n',1332498589,'n','2012','03','23',0,0,20130401185950,0,0),(27,1,1,1,0,NULL,'87.204.0.67','O programie','o-programie','open','y',0,0,0,0,'y','n',1336730715,'n','2012','05','11',0,0,20121030163216,0,0),(28,1,1,1,0,NULL,'87.204.0.67','Bibioteka','bibioteka','open','y',0,0,0,0,'y','n',1341827444,'n','2012','07','09',0,0,20120710152346,0,0),(29,1,1,1,0,NULL,'89.67.201.68','Dla wolontariuszy','dla-wolontariuszy','open','y',0,0,0,0,'y','n',1341827600,'n','2012','07','09',0,0,20130401191921,0,0),(30,1,1,1,0,NULL,'87.204.0.67','Rehabilitacja','rehabilitacja','open','y',0,0,0,0,'y','n',1341827597,'n','2012','07','09',0,0,20120912134019,0,0),(31,1,1,1,0,NULL,'89.67.201.68','Dla pacjentów i opiekunów','dla-pacjentow-i-opiekunow','open','y',0,0,0,0,'y','n',1341827672,'n','2012','07','09',0,0,20130414111633,0,0),(32,1,2,1,0,NULL,'87.204.0.67','tyuuuuuuuuuuuuui','tyuuuuuuuuuuuuui','closed','y',0,0,0,0,'y','n',1343988153,'n','2012','08','03',1344427353,1344427353,20120803120735,0,0),(33,1,1,1,0,NULL,'89.67.201.68','Edukacja','edukacja','open','y',0,0,0,0,'y','n',1345113800,'n','2012','08','16',0,0,20130401192121,0,0),(34,1,1,1,0,NULL,'87.204.0.67','Rejester lekarzy - dowiedz się więcej','rejester-lekarzy-dowiedz-si-wicej','open','y',0,0,0,0,'y','n',1345114125,'n','2012','08','16',0,0,20120816124846,0,0),(36,1,1,1,0,NULL,'89.69.184.11','Adresy placówek realizujących Projekt','adresy-placowek-realizujcych-projekt','open','y',0,0,0,0,'y','n',1361902144,'n','2013','02','26',0,0,20130226191505,0,0),(37,1,1,1,0,NULL,'89.69.184.11','Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)','adresy-placowek-prowadzcych-domowe-leczenie-tlenem-dlt','open','y',0,0,0,0,'y','n',1361903612,'n','2013','02','26',0,0,20130226193533,0,0),(38,1,1,1,0,NULL,'89.67.201.68','Polityka prywatności i wykorzystywania plików cookies w serwisach internetowych PTPZ','polityka-prywatnoci-i-wykorzystywania-plikow-cookies-w-serwisach-internetow','open','y',0,0,0,0,'y','n',1364834005,'n','2013','04','01',0,0,20130401183427,0,0),(39,1,5,1,0,NULL,'89.67.201.68','SP CSK','sp-csk','open','y',0,0,0,0,'n','n',1365707601,'n','2013','04','11',0,0,20130414104122,0,0),(42,1,5,1,0,NULL,'89.67.201.68','SP ZOZ','sp-zoz','open','y',0,0,0,0,'n','n',1365936082,'n','2013','04','14',0,0,20130414104423,0,0),(43,1,5,1,0,NULL,'89.67.201.68','Uniwersyteckie Centrum Kliniczne w Gdańsku','uniwersyteckie-centrum-kliniczne-w-gdasku','open','y',0,0,0,0,'n','n',1365936385,'n','2013','04','14',0,0,20130414105126,0,0),(44,1,5,1,0,NULL,'89.67.201.68','Poradnia Domowego Leczenia Tlenem','poradnia-domowego-leczenia-tlenem','open','y',0,0,0,0,'n','n',1365936756,'n','2013','04','14',0,0,20130414105737,0,0),(45,1,5,1,0,NULL,'89.67.201.68','Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie','samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie','open','y',0,0,0,0,'n','n',1365937127,'n','2013','04','14',0,0,20130414105948,0,0),(46,1,5,1,0,NULL,'89.67.201.68','Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.','niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o','open','y',0,0,0,0,'n','n',1365937269,'n','2013','04','14',0,0,20130414110310,0,0),(47,1,5,1,0,NULL,'89.67.201.68','Wojewódzki Zespół Zakładów Opieki Zdrowotnej','wojewodzki-zespo-zakadow-opieki-zdrowotnej','open','y',0,0,0,0,'n','n',1365937474,'n','2013','04','14',0,0,20130414110535,0,0),(48,1,5,1,0,NULL,'89.67.201.68','Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie','samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie','open','y',0,0,0,0,'n','n',1365937584,'n','2013','04','14',0,0,20130414110726,0,0),(49,1,5,1,0,NULL,'89.67.201.68','ZESPÓŁ DOMOWEGO LECZENIA TLENEM','zespo-domowego-leczenia-tlenem','open','y',0,0,0,0,'n','n',1365937685,'n','2013','04','14',0,0,20130414111006,0,0),(50,1,5,1,0,NULL,'89.67.201.68','OŚRODEK DOMOWEGO LECZENIA TLENEM','orodek-domowego-leczenia-tlenem','open','y',0,0,0,0,'n','n',1365937874,'n','2013','04','14',0,0,20130414111215,0,0),(51,1,5,1,0,NULL,'89.67.201.68','OŚRODEK DOMOWEGO LECZENIA TLENEM','orodek-domowego-leczenia-tlenem1','open','y',0,0,0,0,'n','n',1365937959,'n','2013','04','14',0,0,20130414111340,0,0);
/*!40000 ALTER TABLE `exp_channel_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channels`
--

DROP TABLE IF EXISTS `exp_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) default NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL default '0',
  `total_comments` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `cat_group` varchar(255) default NULL,
  `status_group` int(4) unsigned default NULL,
  `deft_status` varchar(50) NOT NULL default 'open',
  `field_group` int(4) unsigned default NULL,
  `search_excerpt` int(4) unsigned default NULL,
  `deft_category` varchar(60) default NULL,
  `deft_comments` char(1) NOT NULL default 'y',
  `channel_require_membership` char(1) NOT NULL default 'y',
  `channel_max_chars` int(5) unsigned default NULL,
  `channel_html_formatting` char(4) NOT NULL default 'all',
  `channel_allow_img_urls` char(1) NOT NULL default 'y',
  `channel_auto_link_urls` char(1) NOT NULL default 'n',
  `channel_notify` char(1) NOT NULL default 'n',
  `channel_notify_emails` varchar(255) default NULL,
  `comment_url` varchar(80) default NULL,
  `comment_system_enabled` char(1) NOT NULL default 'y',
  `comment_require_membership` char(1) NOT NULL default 'n',
  `comment_use_captcha` char(1) NOT NULL default 'n',
  `comment_moderate` char(1) NOT NULL default 'n',
  `comment_max_chars` int(5) unsigned default '5000',
  `comment_timelock` int(5) unsigned NOT NULL default '0',
  `comment_require_email` char(1) NOT NULL default 'y',
  `comment_text_formatting` char(5) NOT NULL default 'xhtml',
  `comment_html_formatting` char(4) NOT NULL default 'safe',
  `comment_allow_img_urls` char(1) NOT NULL default 'n',
  `comment_auto_link_urls` char(1) NOT NULL default 'y',
  `comment_notify` char(1) NOT NULL default 'n',
  `comment_notify_authors` char(1) NOT NULL default 'n',
  `comment_notify_emails` varchar(255) default NULL,
  `comment_expiration` int(4) unsigned NOT NULL default '0',
  `search_results_url` varchar(80) default NULL,
  `ping_return_url` varchar(80) default NULL,
  `show_button_cluster` char(1) NOT NULL default 'y',
  `rss_url` varchar(80) default NULL,
  `enable_versioning` char(1) NOT NULL default 'n',
  `max_revisions` smallint(4) unsigned NOT NULL default '10',
  `default_entry_title` varchar(100) default NULL,
  `url_title_prefix` varchar(80) default NULL,
  `live_look_template` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channels`
--

LOCK TABLES `exp_channels` WRITE;
/*!40000 ALTER TABLE `exp_channels` DISABLE KEYS */;
INSERT INTO `exp_channels` VALUES (1,1,'pages','Strony','http://ptpz-pochp/index.php',NULL,'en',30,0,1364834005,0,NULL,1,'open',1,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(2,1,'banners','Banery','http://ptpz-pochp.local/','','en',1,0,1328781192,0,NULL,NULL,'open',2,NULL,'','y','y',NULL,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),(3,1,'partners','Loga','http://ptpz-pochp.local/',NULL,'en',1,0,1328781229,0,NULL,NULL,'open',3,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(4,1,'menu','Menu','http://ptpz-pochp.local/',NULL,'en',3,0,1330011124,0,NULL,NULL,'open',4,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(5,1,'mapa','Mapa','http://pochp.eu/','','en',11,0,1365937959,0,NULL,1,'open',5,NULL,'','y','y',NULL,'all','y','n','n','','','n','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',18);
/*!40000 ALTER TABLE `exp_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_comment_subscriptions`
--

DROP TABLE IF EXISTS `exp_comment_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL auto_increment,
  `entry_id` int(10) unsigned default NULL,
  `member_id` int(10) default '0',
  `email` varchar(50) default NULL,
  `subscription_date` varchar(10) default NULL,
  `notification_sent` char(1) default 'n',
  `hash` varchar(15) default NULL,
  PRIMARY KEY  (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_comment_subscriptions`
--

LOCK TABLES `exp_comment_subscriptions` WRITE;
/*!40000 ALTER TABLE `exp_comment_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_comment_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_comments`
--

DROP TABLE IF EXISTS `exp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) default '1',
  `entry_id` int(10) unsigned default '0',
  `channel_id` int(4) unsigned default '1',
  `author_id` int(10) unsigned default '0',
  `status` char(1) default '0',
  `name` varchar(50) default NULL,
  `email` varchar(50) default NULL,
  `url` varchar(75) default NULL,
  `location` varchar(50) default NULL,
  `ip_address` varchar(16) default NULL,
  `comment_date` int(10) default NULL,
  `edit_date` int(10) default NULL,
  `comment` text,
  PRIMARY KEY  (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_comments`
--

LOCK TABLES `exp_comments` WRITE;
/*!40000 ALTER TABLE `exp_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_cp_log`
--

DROP TABLE IF EXISTS `exp_cp_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=214 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_cp_log`
--

LOCK TABLES `exp_cp_log` WRITE;
/*!40000 ALTER TABLE `exp_cp_log` DISABLE KEYS */;
INSERT INTO `exp_cp_log` VALUES (1,1,1,'admin','127.0.0.1',1323087478,'Logged in'),(2,1,1,'admin','127.0.0.1',1323087710,'Logged out'),(3,1,1,'admin','127.0.0.1',1323087712,'Logged in'),(4,1,1,'admin','127.0.0.1',1323092375,'Channel Created:&nbsp;&nbsp;Strony'),(5,1,1,'admin','127.0.0.1',1323092391,'Field Group Created:&nbsp;Strony'),(6,1,1,'admin','127.0.0.1',1323171382,'Logged in'),(7,1,1,'admin','127.0.0.1',1323179702,'Logged out'),(8,1,1,'admin','87.204.0.67',1323442317,'Logged in'),(9,1,1,'admin','87.204.0.67',1324031229,'Logged in'),(10,1,1,'admin','87.204.0.67',1324551877,'Logged in'),(11,1,1,'admin','87.204.0.67',1325870037,'Logged in'),(12,1,1,'admin','87.204.0.67',1326294048,'Logged in'),(13,1,1,'admin','87.204.0.67',1326294114,'Logged in'),(14,1,1,'admin','87.204.0.67',1326294736,'Logged in'),(15,1,1,'admin','87.204.0.67',1328715279,'Logged in'),(16,1,1,'admin','87.204.0.67',1328715414,'Member profile created:&nbsp;&nbsp;jflak'),(17,1,1,'admin','87.204.0.67',1328715424,'Logged out'),(18,1,2,'jflak','87.204.0.67',1328715434,'Logged in'),(19,1,2,'jflak','87.204.0.67',1328715477,'Logged out'),(20,1,2,'jflak','87.204.0.67',1328715519,'Logged in'),(21,1,2,'jflak','127.0.0.1',1328780967,'Logged in'),(22,1,2,'jflak','127.0.0.1',1328781133,'Channel Created:&nbsp;&nbsp;Banery'),(23,1,2,'jflak','127.0.0.1',1328781263,'Channel Created:&nbsp;&nbsp;Loga'),(24,1,2,'jflak','127.0.0.1',1328781551,'Field Group Created:&nbsp;Banery'),(25,1,2,'jflak','127.0.0.1',1328783014,'Field Group Created:&nbsp;Loga'),(26,1,2,'jflak','127.0.0.1',1328799607,'Field Group Created:&nbsp;Menu'),(27,1,2,'jflak','127.0.0.1',1328800984,'Channel Created:&nbsp;&nbsp;Menu'),(28,1,2,'jflak','127.0.0.1',1328861389,'Logged in'),(29,1,2,'jflak','87.204.0.67',1328868811,'Logged in'),(30,1,1,'admin','87.204.0.67',1328871040,'Logged in'),(31,1,2,'jflak','87.204.0.67',1328877350,'Logged in'),(32,1,1,'admin','87.204.0.67',1328881222,'Logged out'),(33,1,1,'admin','87.204.0.67',1328887654,'Logged in'),(34,1,1,'admin','87.204.0.67',1328887700,'Logged out'),(35,1,1,'admin','87.204.0.67',1328887700,'Logged out'),(36,1,1,'admin','87.204.0.67',1329125378,'Logged in'),(37,1,1,'admin','87.204.0.67',1329135531,'Logged out'),(38,1,2,'jflak','87.204.0.67',1329222447,'Logged in'),(39,1,1,'admin','87.204.0.67',1329226068,'Logged in'),(40,1,2,'jflak','87.204.0.67',1329229722,'Logged out'),(41,1,2,'jflak','87.204.0.67',1329295900,'Logged in'),(42,1,2,'jflak','87.204.0.67',1329396139,'Logged in'),(43,1,2,'jflak','87.204.0.67',1329396195,'Member profile created:&nbsp;&nbsp;alasocka'),(44,1,2,'jflak','87.204.0.67',1329397177,'Logged out'),(45,1,1,'admin','87.204.0.67',1329818514,'Logged in'),(46,1,2,'jflak','87.204.0.67',1329818734,'Logged in'),(47,1,1,'admin','87.204.0.67',1329837785,'Logged in'),(48,1,1,'admin','87.204.0.67',1329900139,'Logged in'),(49,1,1,'admin','87.204.0.67',1329986320,'Logged in'),(50,1,3,'alasocka','87.204.0.67',1329990290,'Logged in'),(51,1,2,'jflak','87.204.0.67',1329996443,'Logged in'),(52,1,3,'alasocka','87.204.0.67',1329998090,'Logged out'),(53,1,1,'admin','87.204.0.67',1329999214,'Logged in'),(54,1,2,'jflak','87.204.0.67',1330008175,'Logged in'),(55,1,3,'alasocka','87.204.0.67',1330010904,'Logged in'),(56,1,1,'admin','87.204.0.67',1330071174,'Logged in'),(57,1,3,'alasocka','87.204.0.67',1330074817,'Logged in'),(58,1,2,'jflak','87.204.0.67',1330076103,'Logged in'),(59,1,1,'admin','87.204.0.67',1330080628,'Logged out'),(60,1,1,'admin','87.204.0.67',1330081121,'Logged in'),(61,1,2,'jflak','87.204.0.67',1330439038,'Logged in'),(62,1,2,'jflak','87.204.0.67',1330508227,'Logged in'),(63,1,2,'jflak','87.204.0.67',1330529425,'Logged in'),(64,1,2,'jflak','87.204.0.67',1330591133,'Logged in'),(65,1,2,'jflak','87.204.0.67',1330698773,'Logged in'),(66,1,2,'jflak','87.204.0.67',1330934301,'Logged in'),(67,1,3,'alasocka','87.204.0.67',1330944579,'Logged in'),(68,1,3,'alasocka','87.204.0.67',1330960140,'Logged in'),(69,1,3,'alasocka','87.204.0.67',1331030915,'Logged in'),(70,1,1,'admin','87.204.0.67',1332341791,'Logged in'),(71,1,1,'admin','87.204.0.67',1332341876,'Logged in'),(72,1,1,'admin','87.204.0.67',1332495264,'Logged in'),(73,1,1,'admin','87.204.0.67',1332496496,'Logged in'),(74,1,1,'admin','87.204.0.67',1332502998,'Logged out'),(75,1,1,'admin','87.204.0.67',1332504093,'Logged in'),(76,1,2,'jflak','87.204.0.67',1333452204,'Logged in'),(77,1,2,'jflak','87.204.0.67',1334140397,'Logged in'),(78,1,1,'admin','87.204.0.67',1336572937,'Logged in'),(79,1,1,'admin','87.204.0.67',1336573532,'Logged in'),(80,1,1,'admin','87.204.0.67',1336654375,'Logged in'),(81,1,1,'admin','87.204.0.67',1336654676,'Logged in'),(82,1,1,'admin','87.204.0.67',1336726760,'Logged in'),(83,1,1,'admin','87.204.0.67',1336730324,'Logged in'),(84,1,1,'admin','87.204.0.67',1336730756,'Logged in'),(85,1,1,'admin','87.204.0.67',1336995704,'Logged in'),(86,1,1,'admin','87.204.0.67',1341559174,'Logged in'),(87,1,1,'admin','87.204.0.67',1341566577,'Logged in'),(88,1,1,'admin','87.204.0.67',1341574797,'Logged out'),(89,1,1,'admin','87.204.0.67',1341576915,'Logged in'),(90,1,1,'admin','87.204.0.67',1341577994,'Logged in'),(91,1,1,'admin','87.204.0.67',1341827299,'Logged in'),(92,1,1,'admin','87.204.0.67',1341832396,'Logged in'),(93,1,1,'admin','87.204.0.67',1341834840,'Logged in'),(94,1,1,'admin','87.204.0.67',1341926296,'Logged in'),(95,1,1,'admin','87.204.0.67',1341926418,'Logged in'),(96,1,1,'admin','87.204.0.67',1342003817,'Logged in'),(97,1,1,'admin','87.204.0.67',1342004277,'Logged in'),(98,1,1,'admin','87.204.0.67',1342015256,'Logged in'),(99,1,1,'admin','87.204.0.67',1342017275,'Logged out'),(100,1,1,'admin','87.204.0.67',1342098130,'Logged in'),(101,1,1,'admin','87.204.0.67',1342173958,'Logged in'),(102,1,1,'admin','87.204.0.67',1342179804,'Logged in'),(103,1,1,'admin','87.204.0.67',1342183954,'Logged out'),(104,1,1,'admin','87.204.0.67',1342185259,'Logged in'),(105,1,1,'admin','87.204.0.67',1342425717,'Logged in'),(106,1,1,'admin','87.204.0.67',1343383795,'Logged in'),(107,1,1,'admin','87.204.0.67',1343384977,'Logged in'),(108,1,1,'admin','87.204.0.67',1343984192,'Logged in'),(109,1,1,'admin','87.204.0.67',1343988094,'Logged in'),(110,1,1,'admin','87.204.0.67',1343988869,'Member profile created:&nbsp;&nbsp;test'),(111,1,1,'admin','87.204.0.67',1344347252,'Logged in'),(112,1,1,'admin','87.204.0.67',1345113894,'Logged in'),(113,1,1,'admin','87.204.0.67',1346053177,'Logged in'),(114,1,1,'admin','87.204.0.67',1346053308,'Logged in'),(115,1,1,'admin','87.204.0.67',1346141957,'Logged in'),(116,1,1,'admin','87.204.0.67',1347010028,'Logged in'),(117,1,1,'admin','87.204.0.67',1347449954,'Logged in'),(118,1,1,'admin','87.204.0.67',1349270430,'Logged in'),(119,1,1,'admin','87.204.0.67',1349680864,'Logged in'),(120,1,1,'admin','87.204.0.67',1349684760,'Logged in'),(121,1,1,'admin','87.204.0.67',1349769674,'Logged in'),(122,1,1,'admin','10.10.0.1',1349776664,'Logged in'),(123,1,1,'admin','87.204.0.67',1349856717,'Logged in'),(124,1,1,'admin','87.204.0.67',1349857860,'Logged out'),(125,1,1,'admin','87.204.0.67',1349857860,'Logged out'),(126,1,1,'admin','87.204.0.67',1349858014,'Logged in'),(127,1,1,'admin','87.204.0.67',1350290599,'Logged in'),(128,1,1,'admin','87.204.0.67',1350377769,'Logged in'),(129,1,1,'admin','87.204.0.67',1350547482,'Logged in'),(130,1,1,'admin','87.204.0.67',1350552269,'Logged in'),(131,1,1,'admin','87.204.0.67',1350893770,'Logged in'),(132,1,1,'admin','87.204.0.67',1350900987,'Logged out'),(133,1,1,'admin','87.204.0.67',1351511676,'Logged in'),(134,1,1,'admin','87.204.0.67',1351516054,'Logged in'),(135,1,1,'admin','87.204.0.67',1351516753,'Logged in'),(136,1,1,'admin','87.204.0.67',1351521632,'Logged in'),(137,1,1,'admin','87.204.0.67',1351522561,'Logged in'),(138,1,1,'admin','87.204.0.67',1351522713,'Logged in'),(139,1,1,'admin','87.204.0.67',1351586051,'Logged in'),(140,1,1,'admin','87.204.0.67',1351607925,'Logged in'),(141,1,1,'admin','87.204.0.67',1351609647,'Logged in'),(142,1,1,'admin','87.204.0.67',1352126199,'Logged in'),(143,1,1,'admin','87.204.0.67',1352193657,'Logged in'),(144,1,1,'admin','87.204.0.67',1352278393,'Logged in'),(145,1,1,'admin','87.204.0.67',1352370615,'Logged in'),(146,1,1,'admin','89.77.222.140',1352822053,'Logged in'),(147,1,1,'admin','127.0.0.1',1352844684,'Logged in'),(148,1,1,'admin','89.77.222.140',1352845372,'Logged in'),(149,1,1,'admin','89.77.222.140',1352845500,'Logged out'),(150,1,1,'admin','87.204.0.67',1353339868,'Logged in'),(151,1,1,'admin','89.77.222.140',1353521611,'Logged in'),(152,1,1,'admin','89.77.222.140',1354048515,'Logged in'),(153,1,1,'admin','127.0.0.1',1354049643,'Logged in'),(154,1,1,'admin','153.19.67.43',1354112847,'Logged in'),(155,1,1,'admin','153.19.67.43',1354113789,'Member profile created:&nbsp;&nbsp;michal'),(156,1,1,'admin','153.19.67.43',1354113799,'Logged out'),(157,1,1,'admin','153.19.67.43',1354113822,'Logged in'),(158,1,1,'admin','153.19.67.43',1354113857,'Logged out'),(159,1,5,'michal','153.19.67.43',1354113864,'Logged in'),(160,1,5,'michal','153.19.67.43',1354113929,'Member profile created:&nbsp;&nbsp;alukiewicz'),(161,1,5,'michal','153.19.67.43',1354113940,'Logged out'),(162,1,5,'michal','153.19.67.43',1354113976,'Logged in'),(163,1,5,'michal','153.19.67.43',1354114020,'Screen name was changed to:&nbsp;&nbsp;alulkiewicz\nUsername was changed to:&nbsp;&nbsp;alulkiewicz'),(164,1,5,'michal','153.19.67.43',1354114025,'Logged out'),(165,1,6,'alulkiewicz','153.19.67.43',1354114032,'Logged in'),(166,1,6,'alulkiewicz','153.19.67.43',1354114221,'Logged out'),(167,1,1,'admin','195.85.230.1',1354115532,'Logged in'),(168,1,1,'admin','195.85.230.1',1354530141,'Logged in'),(169,1,6,'alulkiewicz','153.19.102.13',1354800884,'Logged in'),(170,1,6,'alulkiewicz','153.19.102.13',1354801510,'Logged out'),(171,1,1,'admin','153.19.102.13',1355135803,'Logged in'),(172,1,1,'admin','153.19.102.13',1355136480,'Logged out'),(173,1,1,'admin','153.19.102.13',1355136529,'Logged in'),(174,1,1,'admin','153.19.102.13',1355137351,'Logged out'),(175,1,6,'alulkiewicz','153.19.102.13',1355137421,'Logged in'),(176,1,6,'alulkiewicz','153.19.102.13',1355137804,'Logged out'),(177,1,6,'alulkiewicz','153.19.102.13',1355137844,'Logged in'),(178,1,1,'admin','195.85.230.1',1355138566,'Logged in'),(179,1,6,'alulkiewicz','153.19.102.13',1355138737,'Logged out'),(180,1,1,'admin','195.85.230.1',1355207910,'Logged in'),(181,1,6,'alulkiewicz','153.19.102.13',1355482020,'Logged in'),(182,1,6,'alulkiewicz','153.19.102.13',1355482816,'Logged out'),(183,1,6,'alulkiewicz','153.19.102.13',1355490664,'Logged in'),(184,1,6,'alulkiewicz','153.19.102.13',1355491986,'Logged out'),(185,1,1,'admin','89.69.164.162',1355850379,'Logged in'),(186,1,1,'admin','153.19.67.43',1356013330,'Logged in'),(187,1,1,'admin','89.69.164.162',1357471162,'Logged in'),(188,1,1,'admin','89.69.164.162',1357471304,'Logged in'),(189,1,6,'alulkiewicz','153.19.102.13',1357551077,'Logged in'),(190,1,6,'alulkiewicz','153.19.102.13',1358330713,'Logged in'),(191,1,6,'alulkiewicz','153.19.102.13',1358331923,'Logged out'),(192,1,6,'alulkiewicz','153.19.102.13',1358339494,'Logged in'),(193,1,6,'alulkiewicz','153.19.102.13',1358339931,'Logged out'),(194,1,1,'admin','89.78.190.120',1359656040,'Logged in'),(195,1,1,'admin','127.0.0.1',1360010812,'Logged in'),(196,1,1,'admin','89.69.184.11',1360012275,'Logged in'),(197,1,1,'admin','89.69.184.11',1360271686,'Logged in'),(198,1,1,'admin','127.0.0.1',1360274151,'Logged in'),(199,1,1,'admin','127.0.0.1',1360412442,'Logged in'),(200,1,1,'admin','89.69.184.11',1361900758,'Logged in'),(201,1,1,'admin','89.67.201.68',1364416914,'Logged in'),(202,1,1,'admin','89.67.201.68',1364501404,'Logged in'),(203,1,1,'admin','89.67.201.68',1364834005,'Logged in'),(204,1,1,'admin','89.67.201.68',1365354689,'Logged in'),(205,1,1,'admin','89.67.201.68',1365706958,'Logged in'),(206,1,1,'admin','89.67.201.68',1365706997,'Channel Created:&nbsp;&nbsp;Mapa'),(207,1,1,'admin','89.67.201.68',1365707291,'Field Group Created:&nbsp;Mapa'),(208,1,1,'admin','89.67.201.68',1365709901,'Custom Field Deleted:&nbsp;Content'),(209,1,1,'admin','89.67.201.68',1365709905,'Custom Field Deleted:&nbsp;Nazwa'),(210,1,1,'admin','89.67.201.68',1365709958,'Custom Field Deleted:&nbsp;testuje'),(211,1,1,'admin','89.67.201.68',1365710042,'Custom Field Deleted:&nbsp;testuje'),(212,1,1,'admin','89.67.201.68',1365933613,'Logged in'),(213,1,1,'admin','89.206.36.193',1370852485,'Logged in');
/*!40000 ALTER TABLE `exp_cp_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_cp_search_index`
--

DROP TABLE IF EXISTS `exp_cp_search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL auto_increment,
  `controller` varchar(20) default NULL,
  `method` varchar(50) default NULL,
  `language` varchar(20) default NULL,
  `access` varchar(50) default NULL,
  `keywords` text,
  PRIMARY KEY  (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_cp_search_index`
--

LOCK TABLES `exp_cp_search_index` WRITE;
/*!40000 ALTER TABLE `exp_cp_search_index` DISABLE KEYS */;
INSERT INTO `exp_cp_search_index` VALUES (1,'admin_system','general_configuration','english','can_access_sys_prefs','Is system on? License Number Name of your site Name of your site\'s index page URL to the root directory of your site URL to your Control Panel index page URL to your \"themes\" folder Theme Folder Path Default Control Panel Theme Default Language Default XML Language Maximum Number of Cachable URIs New Version Auto Check URL to Documentation Directory'),(2,'admin_system','output_debugging_preferences','english','can_access_sys_prefs','Generate HTTP Page Headers? Enable GZIP Output? Force URL query strings Redirection Method Debug Preference Display Output Profiler? Display Template Debugging?'),(3,'admin_system','database_settings','english','can_access_sys_prefs','Enable Database Debugging Persistent Database Connection Enable SQL Query Caching'),(4,'admin_system','security_session_preferences','english','can_access_sys_prefs','cookie cookies Control Panel Session Type User Session Type Process form data in Secure Mode? Deny Duplicate Data? Apply Rank Denial to User-submitted Links? Allow members to change their username? Allow multiple log-ins from a single account? Require IP Address and User Agent for Login? Require IP Address and User Agent for posting? Apply XSS Filtering to uploaded files? Enable Password Lockout? Time Interval for Lockout Require Secure Passwords? Allow Dictionary Words as Passwords? Name of Dictionary File Minimum Username Length Minimum Password Length'),(5,'admin_system','throttling_configuration','english','can_access_sys_prefs','Enable Throttling Deny Access if No IP Address is Present Maximum Number of Page Loads Time Interval (in seconds) Lockout Time (in seconds) Action to Take URL for Redirect Custom Message'),(6,'admin_system','localization_settings','english','can_access_sys_prefs','Server Time Zone Server Offset (in minutes) Default Time Formatting Daylight Saving Time'),(7,'admin_system','email_configuration','english','can_access_sys_prefs','Return email address for auto-generated emails Webmaster or site name for auto-generated emails Email Character Encoding Enable Email Debugging? Email Protocol SMTP Server Address SMTP Username SMTP Password Use Batch Mode? Number of Emails Per Batch Default Mail Format Enable Word-wrapping by Default? Email Console Timelock Log Email Console Messages Enable CAPTCHAs for Tell-a-Friend and Contact emails'),(8,'admin_system','cookie_settings','english','can_access_sys_prefs','cookies Cookie Domain Cookie Path Cookie Prefix'),(9,'admin_system','image_resizing_preferences','english','can_access_sys_prefs','Image Resizing Protocol Image Converter Path Image Thumbnail Suffix'),(10,'admin_system','captcha_preferences','english','can_access_sys_prefs','Server Path to CAPTCHA Folder Full URL to CAPTCHA Folder Use TrueType Font for CAPTCHA? Add Random Number to CAPTCHA Word Require CAPTCHA with logged-in members?'),(11,'admin_system','word_censoring','english','can_access_sys_prefs','Enable Word Censoring? Censoring Replacement Word Censored Words'),(12,'admin_system','mailing_list_preferences','english','can_access_sys_prefs','Mailing List is Enabled Enable recipient list for notification of new mailing list sign-ups Email Address of Notification Recipient(s) '),(13,'admin_system','emoticon_preferences','english','can_access_sys_prefs','Display Smileys? URL to the directory containing your smileys '),(14,'admin_system','tracking_preferences','english','can_access_sys_prefs','Enable Online User Tracking? Enable Template Hit Tracking? Enable Channel Entry View Tracking? Enable Referrer Tracking? Maximum number of recent referrers to save Suspend ALL tracking when number of online visitors exceeds:'),(15,'admin_system','search_log_configuration','english','can_access_sys_prefs','Enable Search Term Logging Maximum number of recent search terms to save'),(16,'admin_content','global_channel_preferences','english','can_admin_channels','Use Category URL Titles In Links? Category URL Indicator Auto-Assign Category Parents Clear all caches when new entries are posted? Cache Dynamic Channel Queries? Word Separator for URL Titles'),(17,'admin_content','field_group_management','english','can_admin_channels','Field Group Management'),(18,'admin_content','category_management','english','can_admin_categories','Category Management'),(19,'addons_accessories','index','english','can_access_accessories','Accessories'),(20,'addons_extensions','index','english','can_access_extensions','Extensions'),(21,'addons_modules','index','english','can_access_modules','Modules'),(22,'addons_plugins','index','english','can_access_plugins','Plugins'),(23,'content_publish','index','english','all','publish new entry publ_index'),(24,'content_files','index','english','can_access_files','File Manager'),(25,'design','user_message','english','can_admin_design','User Message Template'),(26,'design','global_template_preferences','english','can_admin_design','strict_urls 404 Page save_tmpl_revisions max_tmpl_revisions save_tmpl_files tmpl_file_basepath'),(27,'design','system_offline','english','can_admin_design','System Offline Template'),(28,'design','email_notification','english','can_admin_templates','Email Notification Template'),(29,'design','member_profile_templates','english','can_admin_mbr_templates','Member Profile Template'),(30,'members','register_member','english','can_admin_members','members_register_member'),(31,'members','member_validation','english','can_admin_members','members_member_validation'),(32,'members','view_members','english','can_access_members','members_view_members'),(33,'members','ip_search','english','can_admin_members','ip IP members_ip_search'),(34,'members','custom_profile_fields','english','can_admin_members','Custom Member Profile Fields'),(35,'members','member_group_manager','english','can_admin_mbr_groups','Member Group Management'),(36,'members','member_config','english','can_admin_members','members_member_config'),(37,'members','member_banning','english','can_ban_users','members_member_banning'),(38,'members','member_search','english','all','members_member_search'),(39,'tools_data','sql_manager','english','can_access_data','Sql Manager'),(40,'tools_data','search_and_replace','english','can_access_data','Search and Replace'),(41,'tools_data','recount_stats','english','can_access_data','Recount Stats'),(42,'tools_data','php_info','english','can_access_data','PHP Info'),(43,'tools_data','clear_caching','english','can_access_data','Clear Caching'),(44,'tools_logs','view_cp_log','english','can_access_logs','View Control Panel Log'),(45,'tools_logs','view_throttle_log','english','can_access_logs','View Throttle Log'),(46,'tools_logs','view_search_log','english','can_access_logs','View Search Log'),(47,'tools_logs','view_email_log','english','can_access_logs','View Email Log'),(48,'tools_utilities','member_import','english','can_access_utilities','Member Import'),(49,'tools_utilities','import_from_mt','english','can_access_utilities','Import From MT'),(50,'tools_utilities','import_from_xml','english','can_access_utilities','Import From XML'),(51,'tools_utilities','translation_tool','english','can_access_utilities','Translation Utility');
/*!40000 ALTER TABLE `exp_cp_search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache`
--

DROP TABLE IF EXISTS `exp_email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL auto_increment,
  `cache_date` int(10) unsigned NOT NULL default '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL default 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL default 'y',
  `priority` char(1) NOT NULL default '3',
  PRIMARY KEY  (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache`
--

LOCK TABLES `exp_email_cache` WRITE;
/*!40000 ALTER TABLE `exp_email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache_mg`
--

DROP TABLE IF EXISTS `exp_email_cache_mg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY  (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache_mg`
--

LOCK TABLES `exp_email_cache_mg` WRITE;
/*!40000 ALTER TABLE `exp_email_cache_mg` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache_mg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache_ml`
--

DROP TABLE IF EXISTS `exp_email_cache_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY  (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache_ml`
--

LOCK TABLES `exp_email_cache_ml` WRITE;
/*!40000 ALTER TABLE `exp_email_cache_ml` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_console_cache`
--

DROP TABLE IF EXISTS `exp_email_console_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL auto_increment,
  `cache_date` int(10) unsigned NOT NULL default '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY  (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_console_cache`
--

LOCK TABLES `exp_email_console_cache` WRITE;
/*!40000 ALTER TABLE `exp_email_console_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_console_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_tracker`
--

DROP TABLE IF EXISTS `exp_email_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL auto_increment,
  `email_date` int(10) unsigned NOT NULL default '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL default '1',
  PRIMARY KEY  (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_tracker`
--

LOCK TABLES `exp_email_tracker` WRITE;
/*!40000 ALTER TABLE `exp_email_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_entry_ping_status`
--

DROP TABLE IF EXISTS `exp_entry_ping_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_entry_ping_status`
--

LOCK TABLES `exp_entry_ping_status` WRITE;
/*!40000 ALTER TABLE `exp_entry_ping_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_entry_ping_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_entry_versioning`
--

DROP TABLE IF EXISTS `exp_entry_versioning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL auto_increment,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY  (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_entry_versioning`
--

LOCK TABLES `exp_entry_versioning` WRITE;
/*!40000 ALTER TABLE `exp_entry_versioning` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_entry_versioning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_extensions`
--

DROP TABLE IF EXISTS `exp_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL auto_increment,
  `class` varchar(50) NOT NULL default '',
  `method` varchar(50) NOT NULL default '',
  `hook` varchar(50) NOT NULL default '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL default '10',
  `version` varchar(10) NOT NULL default '',
  `enabled` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_extensions`
--

LOCK TABLES `exp_extensions` WRITE;
/*!40000 ALTER TABLE `exp_extensions` DISABLE KEYS */;
INSERT INTO `exp_extensions` VALUES (1,'Safecracker_ext','form_declaration_modify_data','form_declaration_modify_data','',10,'2.1','y'),(2,'Matrix_ext','channel_entries_tagdata','channel_entries_tagdata','',10,'2.2.2.1','y');
/*!40000 ALTER TABLE `exp_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_field_formatting`
--

DROP TABLE IF EXISTS `exp_field_formatting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL auto_increment,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY  (`formatting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_field_formatting`
--

LOCK TABLES `exp_field_formatting` WRITE;
/*!40000 ALTER TABLE `exp_field_formatting` DISABLE KEYS */;
INSERT INTO `exp_field_formatting` VALUES (1,1,'none'),(2,1,'br'),(3,1,'xhtml'),(4,2,'none'),(5,2,'br'),(6,2,'xhtml'),(7,3,'none'),(8,3,'br'),(9,3,'xhtml'),(10,4,'none'),(11,4,'br'),(12,4,'xhtml'),(13,5,'none'),(14,5,'br'),(15,5,'xhtml'),(16,6,'none'),(17,6,'br'),(18,6,'xhtml'),(19,7,'none'),(20,7,'br'),(21,7,'xhtml'),(22,8,'none'),(23,8,'br'),(24,8,'xhtml'),(40,14,'none'),(39,13,'xhtml'),(37,13,'none'),(38,13,'br'),(41,14,'br'),(42,14,'xhtml'),(43,15,'none'),(44,15,'br'),(45,15,'xhtml');
/*!40000 ALTER TABLE `exp_field_formatting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_field_groups`
--

DROP TABLE IF EXISTS `exp_field_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_field_groups`
--

LOCK TABLES `exp_field_groups` WRITE;
/*!40000 ALTER TABLE `exp_field_groups` DISABLE KEYS */;
INSERT INTO `exp_field_groups` VALUES (1,1,'Strony'),(2,1,'Banery'),(3,1,'Loga'),(4,1,'Menu'),(5,1,'Mapa');
/*!40000 ALTER TABLE `exp_field_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_fieldtypes`
--

DROP TABLE IF EXISTS `exp_fieldtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) default 'n',
  PRIMARY KEY  (`fieldtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_fieldtypes`
--

LOCK TABLES `exp_fieldtypes` WRITE;
/*!40000 ALTER TABLE `exp_fieldtypes` DISABLE KEYS */;
INSERT INTO `exp_fieldtypes` VALUES (1,'select','1.0','YTowOnt9','n'),(2,'text','1.0','YTowOnt9','n'),(3,'textarea','1.0','YTowOnt9','n'),(4,'date','1.0','YTowOnt9','n'),(5,'file','1.0','YTowOnt9','n'),(6,'multi_select','1.0','YTowOnt9','n'),(7,'checkboxes','1.0','YTowOnt9','n'),(8,'radio','1.0','YTowOnt9','n'),(9,'rel','1.0','YTowOnt9','n'),(10,'matrix','2.2.2.1','YTowOnt9','y'),(11,'nsm_tiny_mce','1.1.0','YTowOnt9','n');
/*!40000 ALTER TABLE `exp_fieldtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_categories`
--

DROP TABLE IF EXISTS `exp_file_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned default NULL,
  `cat_id` int(10) unsigned default NULL,
  `sort` int(10) unsigned default '0',
  `is_cover` char(1) default 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_categories`
--

LOCK TABLES `exp_file_categories` WRITE;
/*!40000 ALTER TABLE `exp_file_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_dimensions`
--

DROP TABLE IF EXISTS `exp_file_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `upload_location_id` int(4) unsigned default NULL,
  `title` varchar(255) default '',
  `short_name` varchar(255) default '',
  `resize_type` varchar(50) default '',
  `width` int(10) default '0',
  `height` int(10) default '0',
  `watermark_id` int(4) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_dimensions`
--

LOCK TABLES `exp_file_dimensions` WRITE;
/*!40000 ALTER TABLE `exp_file_dimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_watermarks`
--

DROP TABLE IF EXISTS `exp_file_watermarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL auto_increment,
  `wm_name` varchar(80) default NULL,
  `wm_type` varchar(10) default 'text',
  `wm_image_path` varchar(100) default NULL,
  `wm_test_image_path` varchar(100) default NULL,
  `wm_use_font` char(1) default 'y',
  `wm_font` varchar(30) default NULL,
  `wm_font_size` int(3) unsigned default NULL,
  `wm_text` varchar(100) default NULL,
  `wm_vrt_alignment` varchar(10) default 'top',
  `wm_hor_alignment` varchar(10) default 'left',
  `wm_padding` int(3) unsigned default NULL,
  `wm_opacity` int(3) unsigned default NULL,
  `wm_x_offset` int(4) unsigned default NULL,
  `wm_y_offset` int(4) unsigned default NULL,
  `wm_x_transp` int(4) default NULL,
  `wm_y_transp` int(4) default NULL,
  `wm_font_color` varchar(7) default NULL,
  `wm_use_drop_shadow` char(1) default 'y',
  `wm_shadow_distance` int(3) unsigned default NULL,
  `wm_shadow_color` varchar(7) default NULL,
  PRIMARY KEY  (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_watermarks`
--

LOCK TABLES `exp_file_watermarks` WRITE;
/*!40000 ALTER TABLE `exp_file_watermarks` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_watermarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_files`
--

DROP TABLE IF EXISTS `exp_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned default '1',
  `title` varchar(255) default NULL,
  `upload_location_id` int(4) unsigned default '0',
  `rel_path` varchar(255) default NULL,
  `status` char(1) default 'o',
  `mime_type` varchar(255) default NULL,
  `file_name` varchar(255) default NULL,
  `file_size` int(10) default '0',
  `caption` text,
  `field_1` text,
  `field_1_fmt` tinytext,
  `field_2` text,
  `field_2_fmt` tinytext,
  `field_3` text,
  `field_3_fmt` tinytext,
  `field_4` text,
  `field_4_fmt` tinytext,
  `field_5` text,
  `field_5_fmt` tinytext,
  `field_6` text,
  `field_6_fmt` tinytext,
  `metadata` mediumtext,
  `uploaded_by_member_id` int(10) unsigned default '0',
  `upload_date` int(10) default NULL,
  `modified_by_member_id` int(10) unsigned default '0',
  `modified_date` int(10) default NULL,
  `file_hw_original` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_files`
--

LOCK TABLES `exp_files` WRITE;
/*!40000 ALTER TABLE `exp_files` DISABLE KEYS */;
INSERT INTO `exp_files` VALUES (8,1,'starsze-malzenstwo.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/starsze-malzenstwo.png','o','image/png','starsze-malzenstwo.png',208773,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1328870515,1,1342174663,'315 430'),(7,1,'lubie_pomagac.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/lubie_pomagac.png','o','image/png','lubie_pomagac.png',10138,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1328870428,1,1342174663,'80 235'),(6,1,'ptpz.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/ptpz.png','o','image/png','ptpz.png',16558,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1328870415,1,1342174663,'80 235'),(5,1,'novartis.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/novartis.png','o','image/png','novartis.png',7250,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1328870384,1,1342174663,'80 235'),(9,1,'Copy_of_KTR.xlsx',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Copy_of_KTR.xlsx','o','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','Copy_of_KTR.xlsx',13978,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1329226344,1,1329226344,' '),(10,1,'Picture1.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture1.png','o','image/png','Picture1.png',219525,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1329295966,1,1342174662,'366 507'),(11,1,'spirala.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/spirala.png','o','image/png','spirala.png',79749,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1330085536,1,1342174663,'336 430'),(12,1,'Vitae_nowe_logo_jpg__.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Vitae_nowe_logo_jpg__.jpg','o','image/jpeg','Vitae_nowe_logo_jpg__.jpg',5038,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1330591205,1,1342174663,'80 137'),(13,1,'Vitae.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Vitae.jpg','o','image/jpeg','Vitae.jpg',5038,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1330591379,1,1342174662,'80 137'),(14,1,'Vitae_logo.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Vitae_logo.jpg','o','image/jpeg','Vitae_logo.jpg',5417,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1330591730,1,1342174662,'80 235'),(15,1,'GUM.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/GUM.jpg','o','image/jpeg','GUM.jpg',12902,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1330698853,1,1342174662,'80 245'),(16,1,'GUM1.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/GUM1.jpg','o','image/jpeg','GUM1.jpg',10240,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,2,1330934345,1,1342174662,'80 245'),(18,1,'Picture2.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture2.png','o','image/png','Picture2.png',109763,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332495774,1,1342174662,'467 828'),(19,1,'Picture3.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture3.png','o','image/png','Picture3.png',93102,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332496181,1,1342174662,'458 637'),(20,1,'Picture1_1.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture1_1.png','o','image/png','Picture1_1.png',219525,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332496368,1,1342174662,'366 507'),(21,1,'mapa.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapa.png','o','image/png','mapa.png',138537,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332498483,1,1342174663,'366 507'),(22,1,'Picture4.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture4.png','o','image/png','Picture4.png',23593,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332499006,1,1342174662,'279 1367'),(23,1,'Picture5.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture5.png','o','image/png','Picture5.png',28549,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332499155,1,1342174662,'100 99'),(24,1,'Picture6.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Picture6.png','o','image/png','Picture6.png',3738,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1332499341,1,1342174662,'204 1386'),(30,1,'Schemat_B.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Schemat_B.jpg','o','image/jpeg','Schemat_B.jpg',15677,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336729217,1,1342174662,'146 872'),(26,1,'ba1rys2.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/ba1rys2.png','o','image/png','ba1rys2.png',28815,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336657356,1,1342174663,'239 775'),(29,1,'Schemat_A.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Schemat_A.jpg','o','image/jpeg','Schemat_A.jpg',20490,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336729167,1,1342174662,'146 872'),(31,1,'mapa.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapa.jpg','o','image/jpeg','mapa.jpg',19026,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336730901,1,1342174663,'343 476'),(32,1,'mapa_-_osrodki.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapa_-_osrodki.jpg','o','image/jpeg','mapa_-_osrodki.jpg',33577,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336732756,1,1342174663,'360 480'),(33,1,'zdrowie_dla_pomorzan.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/zdrowie_dla_pomorzan.jpg','o','image/jpeg','zdrowie_dla_pomorzan.jpg',6154,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336732875,1,1342174663,'157 149'),(34,1,'GUM2.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/GUM2.jpg','o','image/jpeg','GUM2.jpg',6001,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336732909,1,1342174662,'189 160'),(35,1,'PTPZ.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/PTPZ.jpg','o','image/jpeg','PTPZ.jpg',7977,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336732991,1,1342174662,'81 433'),(36,1,'LP.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/LP.jpg','o','image/jpeg','LP.jpg',7619,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336733026,1,1342174662,'78 416'),(37,1,'schemat_a1.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/schemat_a1.jpg','o','image/jpeg','schemat_a1.jpg',21637,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1336995732,1,1342174663,'184 872'),(38,1,'adamed.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/adamed.png','o','image/png','adamed.png',16251,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341564786,1,1342174663,'80 168'),(39,1,'adamed_new.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/adamed_new.png','o','image/png','adamed_new.png',16609,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341564947,1,1342174663,'80 236'),(40,1,'adamed_new2.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/adamed_new2.png','o','image/png','adamed_new2.png',15401,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341565033,1,1342174663,'80 236'),(41,1,'Ksiazki_E_J_.pdf',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Ksiazki_E_J_.pdf','o','application/pdf','Ksiazki_E_J_.pdf',570010,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341834891,1,1341834891,' '),(42,1,'domowe_leczenie_tlenem.pdf',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/domowe_leczenie_tlenem.pdf','o','application/pdf','domowe_leczenie_tlenem.pdf',242401,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341839121,1,1341839121,' '),(43,1,'rehabilitacja_w_opiece_paliatywnej.pdf',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/rehabilitacja_w_opiece_paliatywnej.pdf','o','application/pdf','rehabilitacja_w_opiece_paliatywnej.pdf',296909,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1341839133,1,1341839133,' '),(44,1,'Anna_Janowicz.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Anna_Janowicz.jpg','o','image/jpeg','Anna_Janowicz.jpg',61450,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342003978,1,1342174662,'200 152'),(45,1,'dr_Bogumila_Cynowska.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/dr_Bogumila_Cynowska.jpg','o','image/jpeg','dr_Bogumila_Cynowska.jpg',47995,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342015339,1,1342174663,'200 152'),(46,1,'dr_Iwona_Damps-Konstanska.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/dr_Iwona_Damps-Konstanska.jpg','o','image/jpeg','dr_Iwona_Damps-Konstanska.jpg',45240,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342015349,1,1342174663,'200 152'),(47,1,'lek._med._Malgorzata_Kaczmarek.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/lek._med._Malgorzata_Kaczmarek.jpg','o','image/jpeg','lek._med._Malgorzata_Kaczmarek.jpg',54036,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342015355,1,1342174663,'200 152'),(48,1,'lek._med._Slawomir_Garbicz.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/lek._med._Slawomir_Garbicz.jpg','o','image/jpeg','lek._med._Slawomir_Garbicz.jpg',58798,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342015365,1,1342174663,'200 152'),(49,1,'Rehabilitacja_POChP_1.f4v',1,'/var/apps/ptpz-pochp/current/images/uploads/Rehabilitacja_POChP_1.f4v','o','video/mp4','Rehabilitacja_POChP_1.f4v',99504456,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1342174662,1,1342174662,' '),(50,1,'Rehabilitacja_POChP_1.mp4',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/Rehabilitacja_POChP_1.mp4','o','audio/mp4','Rehabilitacja_POChP_1.mp4',76305736,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1343383838,1,1343383838,' '),(58,1,'zakaz_palenia.jpg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/zakaz_palenia.jpg','o','image/jpeg','zakaz_palenia.jpg',14152,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1350379449,1,1350379449,'93 250'),(53,1,'prof_gorecka_copy.jpeg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/prof_gorecka_copy.jpeg','o','image/jpeg','prof_gorecka_copy.jpeg',33423,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1349856969,1,1349856969,'200 152'),(64,1,'choroby_wspoistniejace.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/choroby_wspoistniejace.png','o','image/png','choroby_wspoistniejace.png',78500,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351523956,1,1351523956,'530 813'),(56,1,'lek.med._Katarzyna_Krejci-Badowicz.jpeg',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/lek.med._Katarzyna_Krejci-Badowicz.jpeg','o','image/jpeg','lek.med._Katarzyna_Krejci-Badowicz.jpeg',642847,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1349857299,1,1349857299,'201 153'),(59,1,'do_banneru_POCHP_430x315.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/do_banneru_POCHP_430x315.png','o','image/png','do_banneru_POCHP_430x315.png',83845,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1350893834,1,1350893834,'315 430'),(61,1,'nowa_mapa.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/nowa_mapa.png','o','image/png','nowa_mapa.png',68178,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351522572,1,1351522572,'315 483'),(62,1,'mapka.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapka.png','o','image/png','mapka.png',73923,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351522717,1,1351522717,'600 920'),(63,1,'mapka_placowki.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapka_placowki.png','o','image/png','mapka_placowki.png',203131,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351522858,1,1351522858,'430 660'),(65,1,'mapka_03.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapka_03.png','o','image/png','mapka_03.png',67799,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351586068,1,1351586068,'425 556'),(66,1,'mapka_small.png',1,'/mnt/fas/apps/ptpz-pochp/shared/images/uploads/mapka_small.png','o','image/png','mapka_small.png',100598,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1351586135,1,1351586135,'310 406'),(69,1,'Logo-Fundacji-ON.jpg',1,'/home/pochp/domains/pochp.eu/public_html/images/uploads/Logo-Fundacji-ON.jpg','o','image/jpeg','Logo-Fundacji-ON.jpg',4741,NULL,NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,'xhtml',NULL,1,1357472401,1,1357472401,'80 235');
/*!40000 ALTER TABLE `exp_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_global_variables`
--

DROP TABLE IF EXISTS `exp_global_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY  (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_global_variables`
--

LOCK TABLES `exp_global_variables` WRITE;
/*!40000 ALTER TABLE `exp_global_variables` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_global_variables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_html_buttons`
--

DROP TABLE IF EXISTS `exp_html_buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL default '1',
  `classname` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_html_buttons`
--

LOCK TABLES `exp_html_buttons` WRITE;
/*!40000 ALTER TABLE `exp_html_buttons` DISABLE KEYS */;
INSERT INTO `exp_html_buttons` VALUES (1,1,0,'b','<strong>','</strong>','b',1,'1','btn_b'),(2,1,0,'i','<em>','</em>','i',2,'1','btn_i'),(3,1,0,'blockquote','<blockquote>','</blockquote>','q',3,'1','btn_blockquote'),(4,1,0,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',4,'1','btn_a'),(5,1,0,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',5,'1','btn_img'),(29,1,1,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',9,'1','btn_a'),(27,1,1,'h5','<h5>','</h5>','',7,'1','btn_h5'),(28,1,1,'h6','<h6>','</h6>','',8,'1','btn_h6'),(26,1,1,'h4','<h4>','</h4>','',6,'1','btn_h4'),(25,1,1,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',5,'1','btn_img'),(24,1,1,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',4,'1','btn_a'),(22,1,1,'i','<em>','</em>','i',2,'1','btn_i'),(23,1,1,'blockquote','<blockquote>','</blockquote>','q',3,'1','btn_blockquote'),(21,1,1,'b','<strong>','</strong>','b',1,'1','btn_b'),(20,1,1,'h3','<h3>','</h3>','',0,'1','btn_h3'),(30,1,1,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',10,'1','btn_img'),(31,1,1,'ul','<ul>','</ul>','u',11,'1','btn_ul'),(32,1,1,'ol','<ol>','</ol>','o',12,'1','btn_ol'),(33,1,1,'li','<li>','</li>','o',13,'1','btn_li');
/*!40000 ALTER TABLE `exp_html_buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_layout_publish`
--

DROP TABLE IF EXISTS `exp_layout_publish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_group` int(4) unsigned NOT NULL default '0',
  `channel_id` int(4) unsigned NOT NULL default '0',
  `field_layout` text,
  PRIMARY KEY  (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_layout_publish`
--

LOCK TABLES `exp_layout_publish` WRITE;
/*!40000 ALTER TABLE `exp_layout_publish` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_layout_publish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_matrix_cols`
--

DROP TABLE IF EXISTS `exp_matrix_cols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned default '1',
  `field_id` int(6) unsigned default NULL,
  `col_name` varchar(32) default NULL,
  `col_label` varchar(50) default NULL,
  `col_instructions` text,
  `col_type` varchar(50) default 'text',
  `col_required` char(1) default 'n',
  `col_search` char(1) default 'n',
  `col_order` int(3) unsigned default NULL,
  `col_width` varchar(4) default NULL,
  `col_settings` text,
  PRIMARY KEY  (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_matrix_cols`
--

LOCK TABLES `exp_matrix_cols` WRITE;
/*!40000 ALTER TABLE `exp_matrix_cols` DISABLE KEYS */;
INSERT INTO `exp_matrix_cols` VALUES (1,1,4,'banner_title','Tytuł','Tytuł Banera','text','y','n',0,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(2,1,4,'banner_text','Treść','Treść pod bannerem','text','y','n',2,'','YTo0OntzOjQ6Im1heGwiO3M6MzoiMjUwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(3,1,4,'banner_adress','Adres przekierowania','Adres podstrony do przekierowania np: \"/pages/zycie-z-pochp','text','y','n',3,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(4,1,4,'banner_photo','Zdjęcie','430px x 315px','file','y','n',4,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9'),(5,1,5,'logo_adress','Link','Adres strony na którą ma przekierowywać logo','text','y','n',0,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(6,1,5,'logo_image','Obrazek','235px x 80px [sprite 2x  (235x40)]','file','y','n',2,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9'),(7,1,4,'banner_short_text','Krótka treść','Króka treść pod bannerem','text','y','n',1,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(8,1,7,'link_title','Tytuł','','text','y','n',0,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(9,1,7,'link_adress','Adress','','text','y','n',1,'','YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),(10,1,5,'logo_title','Nazwa Linku','','text','y','n',1,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9');
/*!40000 ALTER TABLE `exp_matrix_cols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_matrix_data`
--

DROP TABLE IF EXISTS `exp_matrix_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned default '1',
  `entry_id` int(10) unsigned default NULL,
  `field_id` int(6) unsigned default NULL,
  `row_order` int(4) unsigned default NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  PRIMARY KEY  (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_matrix_data`
--

LOCK TABLES `exp_matrix_data` WRITE;
/*!40000 ALTER TABLE `exp_matrix_data` DISABLE KEYS */;
INSERT INTO `exp_matrix_data` VALUES (1,1,3,4,0,'System Zintegrowanej Opieki','  ','/strony/zycie-z-pochp','{filedir_1}starsze-malzenstwo.png',NULL,NULL,'dla chorych na zaawansowaną \nprzewlekłą obturacyjną chorobę płuc - to wspólne przedsięwzięcie szeregu organizacji \ni lekarzy <br/>w celu poprawy jakości życia chorych\n',NULL,NULL,NULL),(2,1,5,7,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Co to jest POChP?','/strony/co_to_jest_pochp',NULL),(3,1,5,7,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Czynniki ryzyka POChP','/strony/czynniki_ryzyka_pochp',NULL),(4,1,5,7,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Jak zapobiegać?','/strony/jak_zapobiegac',NULL),(5,1,2,5,0,NULL,NULL,NULL,NULL,'http://novartis.pl','{filedir_1}novartis.png',NULL,NULL,NULL,'Novartis'),(6,1,3,4,1,'Projekt „POMORZE”\n',' ','/strony/system-opieki','{filedir_1}mapka_small.png',NULL,NULL,'Pilotowy Program w czterech ośrodkach na Pomorzu wprowadzający system zintegrowanej opieki dla chorych <br/>na zaawansowaną POChP\n',NULL,NULL,NULL),(7,1,3,4,2,'Integracja działań <br/>w Pneumonologii ','  ','strony/pacjenci-z-pochp','{filedir_1}do_banneru_POCHP_430x315.png',NULL,NULL,'Polskie Towarzystwo Chorób Płuc (PTCHP) wspiera działania ulepszające system opieki nad chorymi na przewlekłe choroby płuc',NULL,NULL,NULL),(8,1,2,5,1,NULL,NULL,NULL,NULL,'http://www.ptpz.pl/','{filedir_1}ptpz.png',NULL,NULL,NULL,'PTPZ'),(9,1,2,5,2,NULL,NULL,NULL,NULL,'http://lubiepomagac.pl/','{filedir_1}lubie_pomagac.png',NULL,NULL,NULL,'Lubię pomagać'),(10,1,2,5,3,NULL,NULL,NULL,NULL,'http://clinicavitae.pl/','{filedir_1}Vitae_logo.jpg',NULL,NULL,NULL,'Clinica Vitae'),(19,1,2,5,5,NULL,NULL,NULL,NULL,'http://www.adamed.com.pl/pl/','{filedir_1}adamed_new2.png',NULL,NULL,NULL,'Adamed'),(20,1,32,4,0,'tyuuuuuuuuuuuuui','tyuuuuuuuuuuuuui','www.google.pl','{filedir_1}adamed.png',NULL,NULL,'tyuuuuuuuuuuuuui',NULL,NULL,NULL),(11,1,23,7,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Jak rozpoznać POChP?','/strony/jak_rozpoznac_pochp',NULL),(12,1,23,7,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Co to jest spirometria?','/strony/co_to_jest_spirometria',NULL),(13,1,23,7,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Do kogo się zwrócić?','/strony/do_kogo_sie_zwrocic',NULL),(14,1,24,7,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Dlaczego palenie jest złe?','/strony/dlaczego_palenie_jest_zle',NULL),(15,1,24,7,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Jak rzucić?','/strony/jak_rzucic',NULL),(16,1,24,7,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Leczenie zespołu uzależnienia od tytoniu','/strony/leczenie_uzaleznienia',NULL),(17,1,24,7,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Palenie choroba zakaźna','/strony/palenie_choroba_zakazna',NULL),(18,1,2,5,4,NULL,NULL,NULL,NULL,'http://www.gumed.edu.pl/','{filedir_1}GUM1.jpg',NULL,NULL,NULL,'Gdański uniwersystet medyczny'),(21,1,2,5,6,NULL,NULL,NULL,NULL,'http://pochp.eu','{filedir_1}Logo-Fundacji-ON.jpg',NULL,NULL,NULL,'Oddech Nadziei ');
/*!40000 ALTER TABLE `exp_matrix_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_bulletin_board`
--

DROP TABLE IF EXISTS `exp_member_bulletin_board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL default '',
  `bulletin_expires` int(10) unsigned NOT NULL default '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY  (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_bulletin_board`
--

LOCK TABLES `exp_member_bulletin_board` WRITE;
/*!40000 ALTER TABLE `exp_member_bulletin_board` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_bulletin_board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_data`
--

DROP TABLE IF EXISTS `exp_member_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_data`
--

LOCK TABLES `exp_member_data` WRITE;
/*!40000 ALTER TABLE `exp_member_data` DISABLE KEYS */;
INSERT INTO `exp_member_data` VALUES (1),(2),(3),(4),(5),(6);
/*!40000 ALTER TABLE `exp_member_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_fields`
--

DROP TABLE IF EXISTS `exp_member_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL auto_increment,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL default 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) default '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL default 'y',
  `m_field_required` char(1) NOT NULL default 'n',
  `m_field_public` char(1) NOT NULL default 'y',
  `m_field_reg` char(1) NOT NULL default 'n',
  `m_field_cp_reg` char(1) NOT NULL default 'n',
  `m_field_fmt` char(5) NOT NULL default 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_fields`
--

LOCK TABLES `exp_member_fields` WRITE;
/*!40000 ALTER TABLE `exp_member_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_groups`
--

DROP TABLE IF EXISTS `exp_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL default 'y',
  `can_view_offline_system` char(1) NOT NULL default 'n',
  `can_view_online_system` char(1) NOT NULL default 'y',
  `can_access_cp` char(1) NOT NULL default 'y',
  `can_access_content` char(1) NOT NULL default 'n',
  `can_access_publish` char(1) NOT NULL default 'n',
  `can_access_edit` char(1) NOT NULL default 'n',
  `can_access_files` char(1) NOT NULL default 'n',
  `can_access_fieldtypes` char(1) NOT NULL default 'n',
  `can_access_design` char(1) NOT NULL default 'n',
  `can_access_addons` char(1) NOT NULL default 'n',
  `can_access_modules` char(1) NOT NULL default 'n',
  `can_access_extensions` char(1) NOT NULL default 'n',
  `can_access_accessories` char(1) NOT NULL default 'n',
  `can_access_plugins` char(1) NOT NULL default 'n',
  `can_access_members` char(1) NOT NULL default 'n',
  `can_access_admin` char(1) NOT NULL default 'n',
  `can_access_sys_prefs` char(1) NOT NULL default 'n',
  `can_access_content_prefs` char(1) NOT NULL default 'n',
  `can_access_tools` char(1) NOT NULL default 'n',
  `can_access_comm` char(1) NOT NULL default 'n',
  `can_access_utilities` char(1) NOT NULL default 'n',
  `can_access_data` char(1) NOT NULL default 'n',
  `can_access_logs` char(1) NOT NULL default 'n',
  `can_admin_channels` char(1) NOT NULL default 'n',
  `can_admin_upload_prefs` char(1) NOT NULL default 'n',
  `can_admin_design` char(1) NOT NULL default 'n',
  `can_admin_members` char(1) NOT NULL default 'n',
  `can_delete_members` char(1) NOT NULL default 'n',
  `can_admin_mbr_groups` char(1) NOT NULL default 'n',
  `can_admin_mbr_templates` char(1) NOT NULL default 'n',
  `can_ban_users` char(1) NOT NULL default 'n',
  `can_admin_modules` char(1) NOT NULL default 'n',
  `can_admin_templates` char(1) NOT NULL default 'n',
  `can_admin_accessories` char(1) NOT NULL default 'n',
  `can_edit_categories` char(1) NOT NULL default 'n',
  `can_delete_categories` char(1) NOT NULL default 'n',
  `can_view_other_entries` char(1) NOT NULL default 'n',
  `can_edit_other_entries` char(1) NOT NULL default 'n',
  `can_assign_post_authors` char(1) NOT NULL default 'n',
  `can_delete_self_entries` char(1) NOT NULL default 'n',
  `can_delete_all_entries` char(1) NOT NULL default 'n',
  `can_view_other_comments` char(1) NOT NULL default 'n',
  `can_edit_own_comments` char(1) NOT NULL default 'n',
  `can_delete_own_comments` char(1) NOT NULL default 'n',
  `can_edit_all_comments` char(1) NOT NULL default 'n',
  `can_delete_all_comments` char(1) NOT NULL default 'n',
  `can_moderate_comments` char(1) NOT NULL default 'n',
  `can_send_email` char(1) NOT NULL default 'n',
  `can_send_cached_email` char(1) NOT NULL default 'n',
  `can_email_member_groups` char(1) NOT NULL default 'n',
  `can_email_mailinglist` char(1) NOT NULL default 'n',
  `can_email_from_profile` char(1) NOT NULL default 'n',
  `can_view_profiles` char(1) NOT NULL default 'n',
  `can_edit_html_buttons` char(1) NOT NULL default 'n',
  `can_delete_self` char(1) NOT NULL default 'n',
  `mbr_delete_notify_emails` varchar(255) default NULL,
  `can_post_comments` char(1) NOT NULL default 'n',
  `exclude_from_moderation` char(1) NOT NULL default 'n',
  `can_search` char(1) NOT NULL default 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL default 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL default '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL default '60',
  `can_attach_in_private_messages` char(1) NOT NULL default 'n',
  `can_send_bulletins` char(1) NOT NULL default 'n',
  `include_in_authorlist` char(1) NOT NULL default 'n',
  `include_in_memberlist` char(1) NOT NULL default 'y',
  `include_in_mailinglists` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_groups`
--

LOCK TABLES `exp_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_member_groups` DISABLE KEYS */;
INSERT INTO `exp_member_groups` VALUES (1,1,'Super Admins','','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','','y','y','y',0,'y',20,60,'y','y','y','y','y'),(2,1,'Banned','','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','n','n','n',60,'n',20,60,'n','n','n','n','n'),(3,1,'Guests','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),(4,1,'Pending','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),(5,1,'Members','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','','y','n','y',10,'y',20,60,'y','n','n','y','y');
/*!40000 ALTER TABLE `exp_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_homepage`
--

DROP TABLE IF EXISTS `exp_member_homepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL default 'l',
  `recent_entries_order` int(3) unsigned NOT NULL default '0',
  `recent_comments` char(1) NOT NULL default 'l',
  `recent_comments_order` int(3) unsigned NOT NULL default '0',
  `recent_members` char(1) NOT NULL default 'n',
  `recent_members_order` int(3) unsigned NOT NULL default '0',
  `site_statistics` char(1) NOT NULL default 'r',
  `site_statistics_order` int(3) unsigned NOT NULL default '0',
  `member_search_form` char(1) NOT NULL default 'n',
  `member_search_form_order` int(3) unsigned NOT NULL default '0',
  `notepad` char(1) NOT NULL default 'r',
  `notepad_order` int(3) unsigned NOT NULL default '0',
  `bulletin_board` char(1) NOT NULL default 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL default '0',
  `pmachine_news_feed` char(1) NOT NULL default 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_homepage`
--

LOCK TABLES `exp_member_homepage` WRITE;
/*!40000 ALTER TABLE `exp_member_homepage` DISABLE KEYS */;
INSERT INTO `exp_member_homepage` VALUES (1,'l',1,'l',2,'n',0,'r',1,'n',0,'r',2,'r',0,'l',0),(2,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),(3,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),(4,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),(5,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),(6,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0);
/*!40000 ALTER TABLE `exp_member_homepage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_search`
--

DROP TABLE IF EXISTS `exp_member_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY  (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_search`
--

LOCK TABLES `exp_member_search` WRITE;
/*!40000 ALTER TABLE `exp_member_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_members`
--

DROP TABLE IF EXISTS `exp_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL auto_increment,
  `group_id` smallint(4) NOT NULL default '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL default '',
  `unique_id` varchar(40) NOT NULL,
  `remember_me` varchar(32) NOT NULL default '',
  `crypt_key` varchar(40) default NULL,
  `authcode` varchar(10) default NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) default NULL,
  `location` varchar(50) default NULL,
  `occupation` varchar(80) default NULL,
  `interests` varchar(120) default NULL,
  `bday_d` int(2) default NULL,
  `bday_m` int(2) default NULL,
  `bday_y` int(4) default NULL,
  `aol_im` varchar(50) default NULL,
  `yahoo_im` varchar(50) default NULL,
  `msn_im` varchar(50) default NULL,
  `icq` varchar(50) default NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) default NULL,
  `avatar_width` int(4) unsigned default NULL,
  `avatar_height` int(4) unsigned default NULL,
  `photo_filename` varchar(120) default NULL,
  `photo_width` int(4) unsigned default NULL,
  `photo_height` int(4) unsigned default NULL,
  `sig_img_filename` varchar(120) default NULL,
  `sig_img_width` int(4) unsigned default NULL,
  `sig_img_height` int(4) unsigned default NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL default '0',
  `accept_messages` char(1) NOT NULL default 'y',
  `last_view_bulletins` int(10) NOT NULL default '0',
  `last_bulletin_date` int(10) NOT NULL default '0',
  `ip_address` varchar(16) NOT NULL default '0',
  `join_date` int(10) unsigned NOT NULL default '0',
  `last_visit` int(10) unsigned NOT NULL default '0',
  `last_activity` int(10) unsigned NOT NULL default '0',
  `total_entries` smallint(5) unsigned NOT NULL default '0',
  `total_comments` smallint(5) unsigned NOT NULL default '0',
  `total_forum_topics` mediumint(8) NOT NULL default '0',
  `total_forum_posts` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `last_forum_post_date` int(10) unsigned NOT NULL default '0',
  `last_email_date` int(10) unsigned NOT NULL default '0',
  `in_authorlist` char(1) NOT NULL default 'n',
  `accept_admin_email` char(1) NOT NULL default 'y',
  `accept_user_email` char(1) NOT NULL default 'y',
  `notify_by_default` char(1) NOT NULL default 'y',
  `notify_of_pm` char(1) NOT NULL default 'y',
  `display_avatars` char(1) NOT NULL default 'y',
  `display_signatures` char(1) NOT NULL default 'y',
  `parse_smileys` char(1) NOT NULL default 'y',
  `smart_notifications` char(1) NOT NULL default 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL default 'n',
  `localization_is_site_default` char(1) NOT NULL default 'n',
  `time_format` char(2) NOT NULL default 'us',
  `cp_theme` varchar(32) default NULL,
  `profile_theme` varchar(32) default NULL,
  `forum_theme` varchar(32) default NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL default '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL default '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL default 'n',
  `pmember_id` int(10) NOT NULL default '0',
  PRIMARY KEY  (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_members`
--

LOCK TABLES `exp_members` WRITE;
/*!40000 ALTER TABLE `exp_members` DISABLE KEYS */;
INSERT INTO `exp_members` VALUES (1,1,'admin','admin','96e9ff279329b634aee7dfe14a5d5e840b24ecf8cd34ebb4ce4a81b63f75520d81af746375092cc575b1f4f20d4fde5475a667f044fe8e03322ca1350dca5391','0+&[5?,#m*a]Z:w>@nnnkI2L$At(=a1JbwF+-38nf)s2VjOx\'*&^?0eZK?OgvO)F8!9)-E5EN8ugV!jkwX%1VOLY4V<Uy>2*T\\|DV:vK0E#_k3b*pFI>em\'F-J}\"qu:(','47a795bf05c24b5682e6e69cab3a71a956f66cc9','','1f25247fa447f396d494d7476bdabf1181361799',NULL,'helpdesk@connectmedica.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1323086813,1365937935,1370852486,40,0,0,0,1365938020,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','default',NULL,NULL,NULL,'20',NULL,'18','','Pages|index.php?S=202d8698cad0ee193321d8cbe979fa0796b700a8&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=pages|1\nNew Entry: Banery|admin.php?S=969af8435b626ed2a16868fed1cc5e2980a38d59&amp;D=cp&amp;C=content_publish&M=entry_form&channel_id=2|2\nView Entry|admin.php?S=3938edf4ed6961d5e711cc03d45ca5f432cfecfe&amp;D=cp&amp;C=content_publish&M=view_entry&channel_id=1&entry_id=31|3','n',0),(2,1,'jflak','jflak','899f5c9d5dd2e7e45268b2f2395dc776e8edb9a981bbe40846800338b8cefda29582fee8a5a2b8a0f191360998f15c16c4a48472ca8d1d86a8f22e96d6c44cd5','<Qmtt\"q}4=H*7CBQw!ICT(~e#=OJ^\'*3Bc\\\"s%phfEiVw/z5:t;Dw0/c^h1\"jF@.!EkI+r)<Lfx|#8E2n;qcH(nxgfxS)!c8FL>V,:~yIUVd;L|#|{=.(qz/p2^1ztvE','0bec4538d0222296351a70890be0ed8771023124','','da670c223ecaed25ebb828d3d6c718cff75b308e',NULL,'jedrzej.flak@connectmedica.com','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'87.204.0.67',1328715414,1333458381,1334142446,5,0,0,0,1329818795,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,'Loga - Edycja|index.php?S=d092ee61beafd5e769cf6ba8f5036d2e490f0db7&amp;D=cp&amp;C=content_publish&M=entry_form&channel_id=3&entry_id=2|1','n',0),(3,1,'alasocka','alasocka','c42d9d5f2cc04f0abac077dab964a8b6202545d3278e6b5dd719a707cfa130932e9c26fc827974b72a85d1f32cb481be35d31ec46dd532fa72fbd2e787df1e85','Sy{MS?^#(*kNe_Jsjc`m[H>HOQ_BL(${MZwK;&9m5$DMC^/9AEh$tP\'2;t_Rzg1jb(h5i;8c~:~Y)4iWlJvNL~hSog8Y0Ahjb\\x=8It6|M&h9&ME0*\'x\'k7HsA0hHK:4','4930e76652535a76aa1d9bfa8a651ac15cf194b9','','f6c772a82bc36ec44143440235c7717af864d670',NULL,'anna.lasocka@connectmedica.com','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'87.204.0.67',1329396195,1330962394,1331045511,2,0,0,0,1330011264,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0),(4,5,'test','test','b444ac06613fc8d63795be9ad0beaf55011936ac','','aa920a15cf2659f7eaaf4f033b330f1588977410','',NULL,NULL,'h1235638@rtrtr.com','http://www.google.pl','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'87.204.0.67',1343988869,0,0,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0),(5,1,'michal','michal','0c9e14d1bdecf17036cb96f8fc41f5d7850df3534ba01b17198198e2a9e33971f0dd97354b25bdfa2059cdee7e5f77181f5d1d522ca64db8505421da5b22facc','FWZe7pr+xvU{D\';#:7+NKu_ws7[5W__6v\\m8,7Mr&.k|MmPxP\",V2&mU]nm/,{`[q&^G~;}5-!Zdway;1B2z^y-UKxoxX>^d;k5{#VUrzg2/me[\"A\\xp[~-_Kwu7ofM8','9257c32a2a973440c3fb99a6dcfde590c3e889f1','','0f14fd0be832de07de56db39a0b8057a676a7e43',NULL,'m.jankowski@miart.pl','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'153.19.67.43',1354113789,1354113865,1354113865,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0),(6,1,'alulkiewicz','alulkiewicz','a3019600b1f6a8d418c9ca6b713124f4fe045b42e1385ff438486d498a7398d2cd6e2f3895526b3b284f8c64cd6395440d52ca7ea3a4759883818517b4b470bc','dD~zDWxa6f:ucP[%oa/d|F&q/B2Y[|oE&C\'I@!gT}Bpn@Lq?o\"$2*}GO/JI+PR(IHa\\CIPJD,<dpnzD5C$KS`\'r1i;#-{Z-#DLHP<8.7>-a$]^6|^>#yxy-kg_27c,OW','dbd9f78f5769854715cc475092a5db6b1c703502','','d5d19f2903ee67fb585e7e91746249ff00daf541',NULL,'alulkiewicz@gumed.edu.pl','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'153.19.67.43',1354113929,1358331901,1358339926,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'y',0);
/*!40000 ALTER TABLE `exp_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_attachments`
--

DROP TABLE IF EXISTS `exp_message_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL default '0',
  `message_id` int(10) unsigned NOT NULL default '0',
  `attachment_name` varchar(50) NOT NULL default '',
  `attachment_hash` varchar(40) NOT NULL default '',
  `attachment_extension` varchar(20) NOT NULL default '',
  `attachment_location` varchar(150) NOT NULL default '',
  `attachment_date` int(10) unsigned NOT NULL default '0',
  `attachment_size` int(10) unsigned NOT NULL default '0',
  `is_temp` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_attachments`
--

LOCK TABLES `exp_message_attachments` WRITE;
/*!40000 ALTER TABLE `exp_message_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_copies`
--

DROP TABLE IF EXISTS `exp_message_copies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL auto_increment,
  `message_id` int(10) unsigned NOT NULL default '0',
  `sender_id` int(10) unsigned NOT NULL default '0',
  `recipient_id` int(10) unsigned NOT NULL default '0',
  `message_received` char(1) NOT NULL default 'n',
  `message_read` char(1) NOT NULL default 'n',
  `message_time_read` int(10) unsigned NOT NULL default '0',
  `attachment_downloaded` char(1) NOT NULL default 'n',
  `message_folder` int(10) unsigned NOT NULL default '1',
  `message_authcode` varchar(10) NOT NULL default '',
  `message_deleted` char(1) NOT NULL default 'n',
  `message_status` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_copies`
--

LOCK TABLES `exp_message_copies` WRITE;
/*!40000 ALTER TABLE `exp_message_copies` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_copies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_data`
--

DROP TABLE IF EXISTS `exp_message_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL default '0',
  `message_date` int(10) unsigned NOT NULL default '0',
  `message_subject` varchar(255) NOT NULL default '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL default 'y',
  `message_attachments` char(1) NOT NULL default 'n',
  `message_recipients` varchar(200) NOT NULL default '',
  `message_cc` varchar(200) NOT NULL default '',
  `message_hide_cc` char(1) NOT NULL default 'n',
  `message_sent_copy` char(1) NOT NULL default 'n',
  `total_recipients` int(5) unsigned NOT NULL default '0',
  `message_status` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_data`
--

LOCK TABLES `exp_message_data` WRITE;
/*!40000 ALTER TABLE `exp_message_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_folders`
--

DROP TABLE IF EXISTS `exp_message_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL default '0',
  `folder1_name` varchar(50) NOT NULL default 'InBox',
  `folder2_name` varchar(50) NOT NULL default 'Sent',
  `folder3_name` varchar(50) NOT NULL default '',
  `folder4_name` varchar(50) NOT NULL default '',
  `folder5_name` varchar(50) NOT NULL default '',
  `folder6_name` varchar(50) NOT NULL default '',
  `folder7_name` varchar(50) NOT NULL default '',
  `folder8_name` varchar(50) NOT NULL default '',
  `folder9_name` varchar(50) NOT NULL default '',
  `folder10_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_folders`
--

LOCK TABLES `exp_message_folders` WRITE;
/*!40000 ALTER TABLE `exp_message_folders` DISABLE KEYS */;
INSERT INTO `exp_message_folders` VALUES (1,'InBox','Sent','','','','','','','',''),(2,'InBox','Sent','','','','','','','','');
/*!40000 ALTER TABLE `exp_message_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_listed`
--

DROP TABLE IF EXISTS `exp_message_listed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL auto_increment,
  `member_id` int(10) unsigned NOT NULL default '0',
  `listed_member` int(10) unsigned NOT NULL default '0',
  `listed_description` varchar(100) NOT NULL default '',
  `listed_type` varchar(10) NOT NULL default 'blocked',
  PRIMARY KEY  (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_listed`
--

LOCK TABLES `exp_message_listed` WRITE;
/*!40000 ALTER TABLE `exp_message_listed` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_listed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_module_member_groups`
--

DROP TABLE IF EXISTS `exp_module_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_module_member_groups`
--

LOCK TABLES `exp_module_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_module_member_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_module_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_modules`
--

DROP TABLE IF EXISTS `exp_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL auto_increment,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL default 'n',
  `has_publish_fields` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_modules`
--

LOCK TABLES `exp_modules` WRITE;
/*!40000 ALTER TABLE `exp_modules` DISABLE KEYS */;
INSERT INTO `exp_modules` VALUES (1,'Comment','2.2','y','n'),(2,'Email','2.0','n','n'),(3,'Emoticon','2.0','n','n'),(4,'Jquery','1.0','n','n'),(5,'Pages','2.2','y','y'),(6,'Query','2.0','n','n'),(7,'Rss','2.0','n','n'),(8,'Safecracker','2.1','y','n'),(9,'Search','2.1','n','n'),(10,'Channel','2.0.1','n','n'),(11,'Member','2.1','n','n'),(12,'Stats','2.0','n','n'),(13,'Ptpz','0.1','n','n');
/*!40000 ALTER TABLE `exp_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_online_users`
--

DROP TABLE IF EXISTS `exp_online_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `in_forum` char(1) NOT NULL default 'n',
  `name` varchar(50) NOT NULL default '0',
  `ip_address` varchar(16) NOT NULL default '0',
  `date` int(10) unsigned NOT NULL default '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY  (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9652 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_online_users`
--

LOCK TABLES `exp_online_users` WRITE;
/*!40000 ALTER TABLE `exp_online_users` DISABLE KEYS */;
INSERT INTO `exp_online_users` VALUES (9649,1,0,'n','','66.249.75.21',1372453509,''),(9650,1,0,'n','','208.115.111.74',1372454432,''),(9651,1,0,'n','','89.67.201.245',1372455227,''),(9637,1,0,'n','','83.17.1.206',1372427641,''),(9648,1,0,'n','','173.199.116.211',1372452822,''),(9647,1,0,'n','','173.199.116.211',1372445461,''),(9646,1,0,'n','','66.249.75.178',1372445043,''),(9645,1,0,'n','','66.249.78.192',1372445043,''),(9644,1,0,'n','','178.255.215.71',1372444504,''),(9643,1,0,'n','','94.154.26.14',1372443162,''),(9642,1,0,'n','','95.160.126.73',1372436881,''),(9641,1,0,'n','','157.55.32.83',1372431463,''),(9640,1,0,'n','','157.55.32.83',1372431463,''),(9639,1,0,'n','','157.55.32.154',1372430025,''),(9638,1,0,'n','','208.115.111.74',1372429595,'');
/*!40000 ALTER TABLE `exp_online_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_pages_configuration`
--

DROP TABLE IF EXISTS `exp_pages_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_pages_configuration` (
  `configuration_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(8) unsigned NOT NULL default '1',
  `configuration_name` varchar(60) NOT NULL,
  `configuration_value` varchar(100) NOT NULL,
  PRIMARY KEY  (`configuration_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_pages_configuration`
--

LOCK TABLES `exp_pages_configuration` WRITE;
/*!40000 ALTER TABLE `exp_pages_configuration` DISABLE KEYS */;
INSERT INTO `exp_pages_configuration` VALUES (1,1,'homepage_display','not_nested'),(2,1,'default_channel','1'),(3,1,'template_channel_1','7');
/*!40000 ALTER TABLE `exp_pages_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_password_lockout`
--

DROP TABLE IF EXISTS `exp_password_lockout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL auto_increment,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY  (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_password_lockout`
--

LOCK TABLES `exp_password_lockout` WRITE;
/*!40000 ALTER TABLE `exp_password_lockout` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_password_lockout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_ping_servers`
--

DROP TABLE IF EXISTS `exp_ping_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL default '80',
  `ping_protocol` varchar(12) NOT NULL default 'xmlrpc',
  `is_default` char(1) NOT NULL default 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_ping_servers`
--

LOCK TABLES `exp_ping_servers` WRITE;
/*!40000 ALTER TABLE `exp_ping_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_ping_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_relationships`
--

DROP TABLE IF EXISTS `exp_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL auto_increment,
  `rel_parent_id` int(10) NOT NULL default '0',
  `rel_child_id` int(10) NOT NULL default '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY  (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_relationships`
--

LOCK TABLES `exp_relationships` WRITE;
/*!40000 ALTER TABLE `exp_relationships` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_reset_password`
--

DROP TABLE IF EXISTS `exp_reset_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL auto_increment,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY  (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_reset_password`
--

LOCK TABLES `exp_reset_password` WRITE;
/*!40000 ALTER TABLE `exp_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_reset_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_revision_tracker`
--

DROP TABLE IF EXISTS `exp_revision_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL auto_increment,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY  (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_revision_tracker`
--

LOCK TABLES `exp_revision_tracker` WRITE;
/*!40000 ALTER TABLE `exp_revision_tracker` DISABLE KEYS */;
INSERT INTO `exp_revision_tracker` VALUES (44,3,'exp_templates','template_data',1351608003,1,'<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pl\">\n\n<head>\n	<title>SPIRO</title>\n    <link href=\"/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />\n	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n    <script type=\"text/javascript\" src=\"/javascripts/jquery.js\"></script>\n  <link type=\"text/css\" href=\"/stylesheets/style.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <!--[if IE 7]>\n  <link type=\"text/css\" href=\"/stylesheets/style_ie7.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <![endif]-->\n  \n</head>\n<body {if embed:page_id}class=\"home\"{/if}>\n<div id=\"lc\">'),(5,3,'exp_templates','template_data',1323088275,1,''),(6,4,'exp_templates','template_data',1323088314,1,''),(9,7,'exp_templates','template_data',1323091923,1,''),(63,17,'exp_templates','template_data',1360412992,1,'<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"\">Test oceniający POChP</a>\n	</li>\n</ul>\n</div>'),(64,17,'exp_templates','template_data',1360414136,1,'<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"/strony/o-programie\" title=\"O programie\">O programie</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt/a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n	</li>\n	<li>\n		<a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a>\n	</li>\n	<li>\n		<a href=\"/kontakt\" title=\"Kontakt\">Kontakt</a>\n	</li>\n</ul>\n</div>'),(65,17,'exp_templates','template_data',1360414152,1,'<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"/strony/o-programie\" title=\"O programie\">O programie</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n	</li>\n	<li>\n		<a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a>\n	</li>\n	<li>\n		<a href=\"/kontakt\" title=\"Kontakt\">Kontakt</a>\n	</li>\n</ul>\n</div>'),(11,3,'exp_templates','template_data',1328862732,2,'<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pl\">\r\n\r\n<head>\r\n	<title></title>\r\n    <link href=\"/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />\r\n	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\r\n    <script type=\"text/javascript\" src=\"/javascripts/jquery.js\"></script>\r\n  <link type=\"text/css\" href=\"/stylesheets/style.css\" media=\"screen\" rel=\"stylesheet\"/>\r\n</head>\r\n<body {if embed:page_id}class=\"home\"{/if}>\r\n<div id=\"lc\">'),(12,4,'exp_templates','template_data',1328862732,2,'<div id=\"t\">\r\n  <div id=\"t-\">\r\n    <div id=\"tl\">\r\n      <h1><a href=\"/\" title=\"Spiro\">Spiro</a></h1>\r\n    </div>\r\n    <div id=\"tr\">\r\n      <ul>\r\n        <li class=\"search_form\">\r\n          {exp:search:simple_form channel=\"pages\" result_page=\"wyniki\" no_result_page=\"brak_wynikow\" search_in=\"everywhere\"}\r\n            <fieldset>\r\n              <div class=\"input\"><input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"Wpisz szukaną frazę\" title=\"Wpisz szukaną frazę\" /></div>\r\n              <label>Szukaj</label>\r\n            </fieldset>\r\n          {/exp:search:simple_form}\r\n        </li>          \r\n        <li class=\"to_medpass\">\r\n          <form action=\"http://pochp.net/\">\r\n            <fieldset>\r\n              <button type=\"submit\"><span>Rejestr POChP</span></button>\r\n            </fieldset>\r\n          </form>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>'),(14,8,'exp_templates','template_data',1328862732,2,'{if embed:menu_id != 0}\r\n<div id=\"cl\">\r\n    <div id=\"sm\">\r\n      <div id=\"sm-\">\r\n        {exp:channel:entries entry_id=\"{embed:menu_id}\" dynamic=\"no\"}\r\n        {if menu_title}<h3>{menu_title}</h3>{/if}\r\n        <ul>\r\n          {menu_links}\r\n            <li class=\"f\"><a href=\"{link_adress}\" title=\"{link_title}\" class=\"em\"><em class=\"red_gt\">&gt;</em>{link_title}</a></li>\r\n          {/menu_links}\r\n        </ul>\r\n        {/exp:channel:entries}\r\n      </div>\r\n    </div>\r\n</div>\r\n{/if}'),(15,12,'exp_templates','template_data',1328862732,2,'{embed=\"shared/head\"}\r\n{embed=\"shared/header\"}\r\n{embed=\"shared/menu\"}\r\n  <div id=\"c\" class=\"page\">\r\n  <div id=\"c-\">\r\n    <div id=\"breadcrumb\">\r\n      <ul>\r\n        <li class=\"f\">Twoja lokalizacja</li>\r\n        <li><a href=\"/\" title=\"Strona g'),(17,10,'exp_templates','template_data',1328862732,2,'{embed=\"shared/head\"}\r\n{embed=\"shared/header\"}\r\n{embed=\"shared/menu\"}\r\n<div id=\"c\">\r\n  <div id=\"c-\">\r\n    <div id=\"breadcrumb\">\r\n      <ul>\r\n        <li class=\"f\">Twoja lokalizacja</li>\r\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\r\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\r\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\r\n      </ul>\r\n    </div>\r\n    <div class=\"header\">\r\n      <h2>Dla lekarzy i pracowników opieki medycznej</h2>\r\n      <span>Aby dostać się do stron dla Lekarzy, zaloguj się podając swój login Medpass:</span>\r\n    </div>\r\n    <div id=\"cl\">\r\n      <div id=\"login_medpass\">\r\n        <div class=\"input\"><input type=\"text\" /></div>\r\n        <div class=\"login-action\"><a href=\"#\" title=\"Zaloguj się\" class=\"btn\"><i>Zaloguj się</i><em>&gt;</em></a></div> \r\n        <div>Nie masz loginu? <a href=\"#\" title=\"Zarejestruj się\">Zarejestruj się!</a></div>\r\n      </div>\r\n    </div>\r\n    <div id=\"cr\">\r\n      <div id=\"sm\" class=\"login_menu\">\r\n        <div id=\"sm-\">\r\n          <h3>Logując się otrzymujesz:</h3>\r\n          <ul>\r\n            <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\r\n            <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\r\n            <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\r\n              <ul>\r\n                <li><em>&bull;</em>openmedica.pl</li>\r\n                <li><em>&bull;</em>kardiolog.pl</li>\r\n                <li><em>&bull;</em>lekarzonkolog.pl</li>\r\n                <li><em>&bull;</em>lekarzakaznik.pl</li>\r\n                <li>... i wielu innych</li>\r\n              </ul></i>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\r\n  </div>\r\n  \r\n</div>\r\n{embed=\"shared/footer\"}'),(18,11,'exp_templates','template_data',1328862732,2,'{embed=\"shared/head\"}\r\n{embed=\"shared/header\"}\r\n{embed=\"shared/menu\"}\r\n  <div id=\"c\" class=\"page\">\r\n  <div id=\"c-\">\r\n    <div id=\"breadcrumb\">\r\n      <ul>\r\n        <li class=\"f\">Twoja lokalizacja</li>\r\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\r\n        <li><a href=\"dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i opiekunów\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla pacjentów i opiekunów</i></a></li>\r\n        {exp:channel:entries channel=\"pages\" limit=\"1\" sort=\"asc\"}\r\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\r\n      </ul>\r\n    </div>\r\n    {embed=\"shared/menu_pacjenci\" menu_id=\"{site_menu}\"}\r\n    <div id=\"cr\" {if {site_menu} == 0}style=\"width: 920px\"{/if}>\r\n      <div class=\"section article txt\">\r\n        <div class=\"header\">\r\n          <h2>{title}</h2>\r\n          <span>{site_subtitle}</span>\r\n        </div>\r\n        <div class=\"content\">\r\n          {site_abstract}\r\n          {site_content}\r\n        </div>\r\n      </div>\r\n      {/exp:channel:entries}\r\n    </div>\r\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\r\n  </div>\r\n  \r\n</div>\r\n{embed=\"shared/footer\"}'),(19,13,'exp_templates','template_data',1328862732,2,'{embed=\"shared/head\"}\r\n{embed=\"shared/header\"}\r\n{embed=\"shared/menu\"}\r\n  <div id=\"c\" class=\"page\">\r\n  <div id=\"c-\">\r\n    <div id=\"breadcrumb\">\r\n      <ul>\r\n        <li class=\"f\">Twoja lokalizacja</li>\r\n        <li><a href=\"/\" title=\"Strona g'),(51,14,'exp_templates','template_data',1357473253,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n		<h3>Nasz adres:</h3>\n                <p>\nKlinika Alergologii<br/>\nul. Dębinki 7<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}'),(38,11,'exp_templates','template_data',1351516277,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"pages\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        {embed=\"shared/menu_pacjenci\" menu_id=\"{site_menu}\"}\n        <div id=\"cr\" {if {site_menu} == 0}style=\"width: 940px\"{/if}>\n          <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              {if site_abstract}{site_abstract}{/if}\n              {site_content}\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(21,16,'exp_templates','template_data',1330961156,3,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\" style=\"width: 940px\">\n        <div class=\"header\">\n          <h2>Elektroniczna Karta Pacjenta</h2>\n        </div>\n        <div class=\"content\">\n          <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n          <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n          {exp:ptpz:ekp_log_in}\n          <div id=\"login_medpass\">\n          <form method=\"POST\">\n            <div class=\"input\">Nr. Pesel: <input type=\"text\" name=\"pesel\" /></div><br />\n            <div class=\"input\">Kod Pin: <input type=\"text\" name=\"pin\" /></div><br />\n            <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\"  name=\"LogIn\" value=\"Zaloguj się\" /></div> \n          </form>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\">\n        <div id=\"sm\" class=\"login_menu\">\n          <div id=\"sm-\">\n            <h3>Logując się otrzymujesz:</h3>\n            <ul>\n              <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n              <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n              <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n                <ul>\n                  <li><em>&bull;</em>openmedica.pl</li>\n                  <li><em>&bull;</em>kardiolog.pl</li>\n                  <li><em>&bull;</em>lekarzonkolog.pl</li>\n                  <li><em>&bull;</em>lekarzakaznik.pl</li>\n                  <li>... i wielu innych</li>\n                </ul></i>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}'),(22,16,'exp_templates','template_data',1330961217,3,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\" style=\"width: 500px\">\n        <div class=\"header\">\n          <h2>Elektroniczna Karta Pacjenta</h2>\n        </div>\n        <div class=\"content\">\n          <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n          <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n          {exp:ptpz:ekp_log_in}\n          <div id=\"login_medpass\">\n          <form method=\"POST\">\n            <div class=\"input\">Nr. Pesel: <input type=\"text\" name=\"pesel\" /></div><br />\n            <div class=\"input\">Kod Pin: <input type=\"text\" name=\"pin\" /></div><br />\n            <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\"  name=\"LogIn\" value=\"Zaloguj się\" /></div> \n          </form>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\" style=\"width: 400px\">\n        <div id=\"sm\" class=\"login_menu\">\n          <div id=\"sm-\">\n            <h3>Logując się otrzymujesz:</h3>\n            <ul>\n              <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n              <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n              <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n                <ul>\n                  <li><em>&bull;</em>openmedica.pl</li>\n                  <li><em>&bull;</em>kardiolog.pl</li>\n                  <li><em>&bull;</em>lekarzonkolog.pl</li>\n                  <li><em>&bull;</em>lekarzakaznik.pl</li>\n                  <li>... i wielu innych</li>\n                </ul></i>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}'),(23,16,'exp_templates','template_data',1330961968,3,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\" style=\"width: 500px\">\n         <div class=\"section article txt\">\n          <div class=\"header\">\n            <h2>Elektroniczna Karta Pacjenta</h2>\n          </div>\n          <div class=\"content\">\n            <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n            <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n            {exp:ptpz:ekp_log_in}\n            <div id=\"login_medpass\">\n            <form method=\"POST\">\n              <div class=\"input\">Nr. Pesel: <input type=\"text\" name=\"pesel\" /></div><br />\n              <div class=\"input\">Kod Pin: <input type=\"text\" name=\"pin\" /></div><br />\n              <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\"  name=\"LogIn\" value=\"Zaloguj się\" /></div> \n            </form>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\" style=\"width: 400px\">\n        <div id=\"sm\" class=\"login_menu\">\n          <div id=\"sm-\">\n            <h3>Logując się otrzymujesz:</h3>\n            <ul>\n              <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n              <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n              <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n                <ul>\n                  <li><em>&bull;</em>openmedica.pl</li>\n                  <li><em>&bull;</em>kardiolog.pl</li>\n                  <li><em>&bull;</em>lekarzonkolog.pl</li>\n                  <li><em>&bull;</em>lekarzakaznik.pl</li>\n                  <li>... i wielu innych</li>\n                </ul></i>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}'),(24,16,'exp_templates','template_data',1330962614,3,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page ekp_login\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\" style=\"width: 500px\">\n         <div class=\"section article txt\">\n          <div class=\"header\">\n            <h2>Elektroniczna Karta Pacjenta</h2>\n          </div>\n          <div class=\"content\">\n            <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n            <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n            {exp:ptpz:ekp_log_in}\n            <div id=\"login_medpass\">\n            <form method=\"POST\">\n              <div class=\"input\">Nr. Pesel: <input type=\"text\" name=\"pesel\" /></div><br />\n              <div class=\"input\">Kod Pin: <input type=\"text\" name=\"pin\" /></div><br />\n              <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\"  name=\"LogIn\" value=\"Zaloguj się\" /></div> \n            </form>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\" style=\"width: 400px\">\n        <div id=\"sm\" class=\"login_menu\">\n          <div id=\"sm-\">\n            <h3>Logując się otrzymujesz:</h3>\n            <ul>\n              <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n              <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n              <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n                <ul>\n                  <li><em>&bull;</em>openmedica.pl</li>\n                  <li><em>&bull;</em>kardiolog.pl</li>\n                  <li><em>&bull;</em>lekarzonkolog.pl</li>\n                  <li><em>&bull;</em>lekarzakaznik.pl</li>\n                  <li>... i wielu innych</li>\n                </ul></i>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}'),(25,6,'exp_templates','template_data',1351516081,1,'  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n      <div id=\"fb\">\n        <p>Copyright &copy Spiro.pl 2012. Wszelkie prawa zastrzeżone</p>\n        <p>00-078 Warszawa, Pl. Piłsudskiego 3, bud. Metropolitan, wejście nr 3</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>'),(26,6,'exp_templates','template_data',1351516085,1,'  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n      <div id=\"fb\">\n        <p>Copyright &copy Spiro.pl 2012. Wszelkie prawa zastrzeżone</p>\n        <p>00-078 Warszawa, Pl. Piłsudskiego 3, bud. Metropolitan, wejście nr 3</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>'),(27,6,'exp_templates','template_data',1351516219,1,'  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n      <div id=\"fb\">\n        <p>Copyright &copy Spiro.pl 2012. Wszelkie prawa zastrzeżone</p>\n        <p>00-078 Warszawa, Pl. Piłsudskiego 3, bud. Metropolitan, wejście nr 3</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>'),(28,3,'exp_templates','template_data',1351516227,1,'<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pl\">\n\n<head>\n	<title></title>\n    <link href=\"/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />\n	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n    <script type=\"text/javascript\" src=\"/javascripts/jquery.js\"></script>\n  <link type=\"text/css\" href=\"/stylesheets/style.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <!--[if IE 7]>\n  <link type=\"text/css\" href=\"/stylesheets/style_ie7.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <![endif]-->\n  \n</head>\n<body {if embed:page_id}class=\"home\"{/if}>\n<div id=\"lc\">'),(29,4,'exp_templates','template_data',1351516234,1,'<div id=\"t\">\n  <div id=\"t-\">\n    <div id=\"tl\">\n      <h1><a href=\"/\" title=\"Spiro\">Spiro</a></h1>\n    </div>\n    <div id=\"tr\">\n      <ul>\n        <li class=\"search_form\">\n          {exp:search:simple_form channel=\"pages\" result_page=\"wyniki\" no_result_page=\"brak_wynikow\" search_in=\"everywhere\" name=\"simple_search\"}\n            <fieldset>\n              <div class=\"input\"><input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"Wpisz szukaną frazę\" title=\"Wpisz szukaną frazę\" /></div>\n              <input type=\"submit\" value=\"Szukaj\" title=\"Szukaj\" class=\"btn btn_search\" />\n            </fieldset>\n          {/exp:search:simple_form}\n        </li>          \n        <li class=\"to_medpass\">\n          <form action=\"http://pochp.net/\">\n            <fieldset>\n              <button type=\"submit\"><span>Rejestr POChP</span></button>\n            </fieldset>\n          </form>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>'),(55,14,'exp_templates','template_data',1360271963,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n		<h3>Nasz adres:</h3>\n                <p>\nKlinika Alergologii<br/>\nul. Dębinki 7 ( budynek 4 piętro 1 )<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}'),(77,5,'exp_templates','template_data',1361902048,1,'<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"http://pochp.eu/strony/dla-pacjentow-i-opiekunow#domowe_leczenie\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a target=\"_blank\" href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>'),(58,6,'exp_templates','template_data',1360412475,1,'  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n      <div id=\"fb\">\n        <p>Copyright &copy pochp.eu 2013. Wszelkie prawa zastrzeżone</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>'),(32,10,'exp_templates','template_data',1351516263,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div class=\"header\">\n      <h2>Dla lekarzy i pracowników opieki medycznej</h2>\n      <span>Aby dostać się do stron dla Lekarzy, zaloguj się podając swój login Medpass:</span>\n    </div>\n    <div id=\"cl\">\n      <div id=\"login_medpass\">\n        <div class=\"input\"><input type=\"text\" /></div>\n        <div class=\"login-action\"><a href=\"#\" title=\"Zaloguj się\" class=\"btn\"><i>Zaloguj się</i><em>&gt;</em></a></div> \n        <div>Nie masz loginu? <a href=\"#\" title=\"Zarejestruj się\">Zarejestruj się!</a></div>\n      </div>\n    </div>\n    <div id=\"cr\">\n      <div id=\"sm\" class=\"login_menu\">\n        <div id=\"sm-\">\n          <h3>Logując się otrzymujesz:</h3>\n          <ul>\n            <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n            <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n            <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n              <ul>\n                <li><em>&bull;</em>openmedica.pl</li>\n                <li><em>&bull;</em>kardiolog.pl</li>\n                <li><em>&bull;</em>lekarzonkolog.pl</li>\n                <li><em>&bull;</em>lekarzakaznik.pl</li>\n                <li>... i wielu innych</li>\n              </ul></i>\n            </li>\n          </ul>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(33,8,'exp_templates','template_data',1351516277,1,'{if embed:menu_id != 0}\n<div id=\"cl\">\n    <div id=\"sm\">\n      <div id=\"sm-\">\n        {exp:channel:entries entry_id=\"{embed:menu_id}\" dynamic=\"no\"}\n        {if menu_title}<h3>{menu_title}</h3>{/if}\n        <ul>\n          {menu_links}\n            <li class=\"f\"><a href=\"{link_adress}\" title=\"{link_title}\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>{link_title}</i></a></li>\n          {/menu_links}\n        </ul>\n        {/exp:channel:entries}\n      </div>\n    </div>\n</div>\n{/if}'),(34,12,'exp_templates','template_data',1351516277,1,'﻿{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"#\" title=\"Wyniki\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Wyniki wyszukwiania</i></a></li>\n      </ul>\n    </div>\n    <div id=\"cr\" style=\"width: 940px\">\n      <div class=\"section article txt\">\n        <div class=\"header\">\n          <h2>Wyniki wyszukwiania</h2>\n        </div>\n        <div class=\"content\">\n           <h3>Szukana fraza \"{exp:search:keywords}\" nie została znaleziona.</h3>        \n        </div>\n      </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(35,15,'exp_templates','template_data',1351516277,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"ekp\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/elektroniczna_karta_pacjenta\" title=\"Elektroniczna Karta Pacjenta\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Elektroniczna Karta Pacjenta</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n      <div class=\"header\">\n        <h2>Elektroniczna Karta Pacjenta</h2>\n        <span>Twoje dane z Elektronicznej Karty Pacjenta</span>\n      </div>\n        {exp:ptpz:ekp_data}\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(36,16,'exp_templates','template_data',1351516277,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"login ekp_login\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/elektroniczna_karta_pacjenta\" title=\"Elektroniczna Karta Pacjenta\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Elektroniczna Karta Pacjenta</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\">\n        <div class=\"header\">\n          <h2>Elektroniczna Karta Pacjenta</h2>\n        </div>\n        <div class=\"content\">\n          \n          {exp:ptpz:ekp_log_in}\n          <div id=\"login_ekp\">\n          <form method=\"POST\">\n            <div class=\"pesel\"><label>Nr. Pesel:</label> <input type=\"text\" name=\"pesel\" /></div>\n            <div class=\"pin\"><label>Kod Pin: </label> <input type=\"text\" name=\"pin\" class=\"pin\" /></div>\n            <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\" value=\"Zaloguj się\" name=\"LogIn\"></div> \n          </form>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\">\n        <div class=\"login_menu\">\n          <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n          <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}'),(39,13,'exp_templates','template_data',1351516277,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona g'),(78,5,'exp_templates','template_data',1361902608,1,'<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/adresy-placowek-realizujcych-projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"http://pochp.eu/strony/dla-pacjentow-i-opiekunow#domowe_leczenie\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a target=\"_blank\" href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>'),(79,5,'exp_templates','template_data',1361903834,1,'<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/adresy-placowek-realizujcych-projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"/strony/adresy-placowek-prowadzcych-domowe-leczenie-tlenem-dlt\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a target=\"_blank\" href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>'),(80,17,'exp_templates','template_data',1364834186,1,'<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"/strony/o-programie\" title=\"O programie\">O programie</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a>\n	</li>\n\n	<li>\n		<a href=\"/forum\" title=\"Forum\">Forum</a>\n	</li>\n	<li>\n		<a href=\"/kontakt\" title=\"Kontakt\">Kontakt</a>\n	</li>\n	<li>\n		<a href=\"/strony/polityka-prywatnoci-i-wykorzystywania-plikow-cookies-w-serwisach-internetow\" title=\"Polityka prywatności\">Polityka prywatności</a>\n	</li>\n</ul>\n</div>'),(141,4,'exp_templates','template_data',1370852578,1,'<div id=\"t\">\n  <div id=\"t-\">\n    <div id=\"tl\">\n      <h1><a href=\"/\" title=\"Spiro\">Spiro</a></h1>\n    </div>\n    <div id=\"tr\">\n      <ul>\n        <li class=\"search_form\">\n          {exp:search:simple_form channel=\"pages\" result_page=\"wyniki\" no_result_page=\"brak_wynikow\" search_in=\"everywhere\" name=\"simple_search\"}\n            <fieldset>\n              <div class=\"input\"><input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"Wpisz szukaną frazę\" title=\"Wpisz szukaną frazę\" /></div>\n              <input type=\"submit\" value=\"Szukaj\" title=\"Szukaj\" class=\"btn btn_search\" />\n            </fieldset>\n          {/exp:search:simple_form}\n        </li>          \n        <li class=\"to_medpass\">\n          <form action=\"http://rejestr.pochp.eu/\">\n            <fieldset>\n              <button type=\"submit\"><span>Rejestr POChP</span></button>\n            </fieldset>\n          </form>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>'),(41,13,'exp_templates','template_data',1351588201,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona g��wna\" class=\"em gt\"><em class=\"home\">Strona g��wna</em></a></li>\n        <li><a href=\"#\" title=\"Wyniki\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Wyniki wyszukwiania</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n	<div id=\"cr\" style=\"width: 940px\">\n      <div class=\"section article txt\">\n        <div class=\"header\">\n          <h2>Wyniki wyszukwiania</h2>\n        </div>\n        <div class=\"content\">\n            <h3>Wyniki znalezione dla \"{exp:search:keywords}\"</h3>\n          {exp:search:search_results backspace=\"7\"}\n            <b><a href=\"{path=\"strony/{url_title}\"}\">{title}</a><br />\n          {/exp:search:search_results}\n        </div>\n      </div>\n    </div>\n	</div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(42,12,'exp_templates','template_data',1351588234,1,'﻿{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"#\" title=\"Wyniki\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Wyniki wyszukwiania</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n    <div id=\"cr\" style=\"width: 940px\">\n      <div class=\"section article txt\">\n        <div class=\"header\">\n          <h2>Wyniki wyszukwiania</h2>\n        </div>\n        <div class=\"content\">\n           <h3>Szukana fraza \"{exp:search:keywords}\" nie została znaleziona.</h3>        \n        </div>\n      </div>\n    </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(43,3,'exp_templates','template_data',1351607981,1,'<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pl\">\n\n<head>\n	<title>SPIRO</title>\n    <link href=\"/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />\n	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n    <script type=\"text/javascript\" src=\"/javascripts/jquery.js\"></script>\n  <link type=\"text/css\" href=\"/stylesheets/style.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <!--[if IE 7]>\n  <link type=\"text/css\" href=\"/stylesheets/style_ie7.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <![endif]-->\n  \n</head>\n<body {if embed:page_id}class=\"home\"{/if}>\n<div id=\"lc\">'),(76,5,'exp_templates','template_data',1361902025,1,'<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"http://pochp.eu/strony/dla-pacjentow-i-opiekunow#domowe_leczenie\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>'),(62,6,'exp_templates','template_data',1360412739,1,'  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n{embed=\"shared/footermenu\"}\n      <div id=\"fb\">\n        <p>Copyright &copy pochp.eu 2013. Wszelkie prawa zastrzeżone</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>'),(66,17,'exp_templates','template_data',1360414379,1,'<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"/strony/o-programie\" title=\"O programie\">O programie</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a>\n	</li>\n\n	<li>\n		<a href=\"/forum\" title=\"Forum\">Forum</a>\n	</li>\n	<li>\n		<a href=\"/kontakt\" title=\"Kontakt\">Kontakt</a>\n	</li>\n</ul>\n</div>'),(67,1,'exp_templates','template_data',1360414795,1,'{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n<br/>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}'),(48,14,'exp_templates','template_data',1357473110,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"pomoc\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n                <p>Nasz adres: <br/>\nKlinika Alergologii<br/>\nul. Dębinki 7<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}'),(49,14,'exp_templates','template_data',1357473139,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"pomoc\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n		<h3>Nasz adres:</h3>\n                <p>\nKlinika Alergologii<br/>\nul. Dębinki 7<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}'),(50,14,'exp_templates','template_data',1357473193,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"pomoc\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n		<h3>Nasz adres:</h3>\n                <p>\nKlinika Alergologii<br/>\nul. Dębinki 7<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}'),(68,1,'exp_templates','template_data',1360414845,1,'{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n<p></p>\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n<br/>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}'),(69,1,'exp_templates','template_data',1360414911,1,'{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n<p></p>\n<br/>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}'),(70,1,'exp_templates','template_data',1360414977,1,'{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n<br/>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}'),(71,1,'exp_templates','template_data',1360414997,1,'{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n<br/><br/>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}'),(123,18,'exp_templates','template_data',1365935138,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left; clear: none; margin-right:10px\">{opis}</div>\n	      <div style=\"width:605px; float:left; clear: none;\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(124,18,'exp_templates','template_data',1365935703,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left; clear: none; margin-right:10px\">{opis}</div>\n	      <div style=\"width:605px; float:left; clear: none;\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n		{embed=\"shared/menu_dlt\"}	\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(120,18,'exp_templates','template_data',1365934915,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left\">{opis}</div>\n	      <div style=\"width:425px; float:left\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(121,18,'exp_templates','template_data',1365935020,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left; clear: none;\">{opis}</div>\n	      <div style=\"width:625px; float:left; clear: none;\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(122,18,'exp_templates','template_data',1365935099,1,'{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left; clear: none;\">{opis}</div>\n	      <div style=\"width:615px; float:left; clear: none;\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}'),(135,19,'exp_templates','template_data',1365937575,1,'<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n\n\n</ul>'),(136,19,'exp_templates','template_data',1365937678,1,'<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n<li><a href=\"/mapa/samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie\">Olsztyn - Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie</a></li>\n\n</ul>'),(137,19,'exp_templates','template_data',1365937850,1,'<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n<li><a href=\"/mapa/samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie\">Olsztyn - Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie</a></li>\n<li><a href=\"/mapa/zespo-domowego-leczenia-tlenem\">MROZY - ZESPÓŁ DOMOWEGO LECZENIA TLENEM</a></li>\n\n</ul>'),(138,19,'exp_templates','template_data',1365937964,1,'<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n<li><a href=\"/mapa/samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie\">Olsztyn - Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie</a></li>\n<li><a href=\"/mapa/zespo-domowego-leczenia-tlenem\">MROZY - ZESPÓŁ DOMOWEGO LECZENIA TLENEM</a></li>\n<li><a href=\"/mapa/orodek-domowego-leczenia-tlenem\">BARTOSZYCE - OŚRODEK DOMOWEGO LECZENIA TLENEM</a></li>\n</ul>'),(139,19,'exp_templates','template_data',1365938057,1,'<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n<li><a href=\"/mapa/samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie\">Olsztyn - Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie</a></li>\n<li><a href=\"/mapa/zespo-domowego-leczenia-tlenem\">MROZY - ZESPÓŁ DOMOWEGO LECZENIA TLENEM</a></li>\n<li><a href=\"/mapa/orodek-domowego-leczenia-tlenem\">BARTOSZYCE - OŚRODEK DOMOWEGO LECZENIA TLENEM</a></li>\n<li><a href=\"/mapa/orodek-domowego-leczenia-tlenem1\">PRABUTY - OŚRODEK DOMOWEGO LECZENIA TLENEM</a></li>\n</ul>'),(140,5,'exp_templates','template_data',1365938139,1,'<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/adresy-placowek-realizujcych-projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a target=\"_blank\" href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>');
/*!40000 ALTER TABLE `exp_revision_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_search`
--

DROP TABLE IF EXISTS `exp_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL default '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY  (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_search`
--

LOCK TABLES `exp_search` WRITE;
/*!40000 ALTER TABLE `exp_search` DISABLE KEYS */;
INSERT INTO `exp_search` VALUES ('2507b1fa9881f23c9b525f3ab146abfc',1,1359049888,'copd',0,'89.71.196.61',1,50,'s:1427:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n						w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n						m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n						md.*,\n						wd.*\n				FROM MDBMPREFIXchannel_titles		AS t\n				LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n				LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n				LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n				LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n				WHERE t.entry_id IN (13)  ORDER BY entry_date  desc\\\";','a:4:{s:12:\\\"site_content\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"y\\\";}s:13:\\\"site_abstract\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"y\\\";}s:13:\\\"site_subtitle\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"y\\\";}s:9:\\\"site_menu\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}}','wyniki'),('2249b297e7050fd80c19dd732dd8c0e2',1,1359049902,'zintegrowana opiekq',0,'89.71.196.61',0,0,'','',''),('95a06b2009abbde729024380db01851b',1,1360754390,'Wpisz szukaną frazę',0,'178.159.166.112',0,0,'','',''),('a34e3514693b39aa13eb7eaeab127686',1,1367864835,'picie alkocholu jest złe',0,'83.8.88.141',0,0,'','',''),('81a09bd162c34b83cb9d9a940fe64f60',1,1370463394,'astma aspirynowa',0,'84.10.93.104',0,0,'','','');
/*!40000 ALTER TABLE `exp_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_search_log`
--

DROP TABLE IF EXISTS `exp_search_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_search_log`
--

LOCK TABLES `exp_search_log` WRITE;
/*!40000 ALTER TABLE `exp_search_log` DISABLE KEYS */;
INSERT INTO `exp_search_log` VALUES (1,1,2,'jflak','127.0.0.1',1328803253,'site','sdfgsdffgsdf'),(2,1,2,'jflak','127.0.0.1',1328803284,'site','asdfsdtf'),(3,1,0,'','127.0.0.1',1328860818,'site','gfgsdfg'),(4,1,0,'','127.0.0.1',1328860957,'site','sdf'),(5,1,0,'','127.0.0.1',1328860986,'site','opchp'),(6,1,0,'','127.0.0.1',1328861203,'site','dasgesg6e'),(7,1,0,'','127.0.0.1',1328861245,'site','pochp'),(8,1,0,'','127.0.0.1',1328861276,'site','poch'),(9,1,2,'jflak','127.0.0.1',1328861805,'site','aaa'),(10,1,2,'jflak','127.0.0.1',1328861811,'site','test'),(11,1,2,'jflak','127.0.0.1',1328861959,'site','test'),(12,1,2,'jflak','127.0.0.1',1328861963,'site','test'),(13,1,2,'jflak','127.0.0.1',1328862424,'site','test'),(14,1,2,'jflak','87.204.0.67',1328878306,'site','asd'),(15,1,0,'','87.204.0.67',1329125098,'site','rejestr lekarzy'),(16,1,0,'','87.204.0.67',1330005416,'site','ptpz'),(17,1,0,'','87.204.0.67',1330005427,'site','opieki'),(18,1,0,'','87.204.0.67',1330097979,'site','gjc'),(19,1,0,'','87.204.0.67',1331113299,'site','Wpisz szukaną frazę'),(20,1,0,'','87.204.0.67',1331114350,'site','Wpisz szukaną frazę'),(21,1,0,'','87.204.0.67',1331114453,'site','Wpisz szukaną frazę'),(22,1,0,'','87.204.0.67',1331293041,'site','Wpisz szukaną frazę'),(23,1,0,'','87.204.0.67',1331293240,'site','Wpisz szukaną frazę'),(24,1,0,'','87.204.0.67',1331293243,'site','Wpisz szukaną frazę'),(25,1,0,'','87.204.0.67',1332922300,'site','dsdsa'),(26,1,0,'','87.204.0.67',1332922545,'site','dsdsd'),(27,1,0,'','87.204.0.67',1332922627,'site','dsd'),(28,1,0,'','87.204.0.67',1332922634,'site','dsds'),(29,1,0,'','87.204.0.67',1332922638,'site','dsdsd'),(30,1,0,'','87.204.0.67',1332929057,'site','3213'),(31,1,0,'','87.204.0.67',1332929133,'site','fsdfsf'),(32,1,0,'','87.204.0.67',1332935879,'site','j;'),(33,1,0,'','87.204.0.67',1332935896,'site','Wpisz szukaną frazę'),(34,1,0,'','87.204.0.67',1333617808,'site','rtrt'),(35,1,0,'','87.204.0.67',1343985551,'site','choroba'),(36,1,0,'','87.204.0.67',1343986113,'site','POChP'),(37,1,0,'','87.204.0.67',1343986378,'site','44'),(38,1,0,'','87.204.0.67',1343987488,'site','ghklkfd'),(39,1,0,'','87.204.0.67',1343987990,'site','bhjfjgkfjkgkj'),(40,1,0,'','87.204.0.67',1343988000,'site','choroba'),(41,1,0,'','87.204.0.67',1344413813,'site','abc'),(42,1,0,'','10.10.0.1',1344591827,'site','towarzystwo'),(43,1,1,'admin','87.204.0.67',1345114287,'site','&#123;&#125;'),(44,1,0,'','87.204.0.67',1345114328,'site','&#123;&#125;'),(45,1,0,'','87.204.0.67',1345114343,'site','&quot;:'),(46,1,0,'','87.204.0.67',1349777849,'site','papierosy'),(47,1,0,'','10.10.0.1',1350478715,'site','pulmonolog'),(48,1,0,'','10.10.0.1',1351003891,'site','projekt'),(49,1,0,'','10.10.0.1',1351004384,'site','zaawansowanej'),(50,1,0,'','10.10.0.1',1351004406,'site','Wpisz szukaną frazę'),(51,1,0,'','10.10.0.1',1351004420,'site','Wpisz szukaną frazę'),(52,1,0,'','10.10.0.1',1351004429,'site','Wpisz szukaną frazę'),(53,1,0,'','10.10.0.1',1351004445,'site','Wpisz szukaną frazę'),(54,1,0,'','10.10.0.1',1351004571,'site','Wpisz szukaną frazę'),(55,1,0,'','10.10.0.1',1351088519,'site','pacjent'),(56,1,0,'','87.204.0.67',1351512419,'site','asdf'),(57,1,0,'','87.204.0.67',1351512432,'site','dla'),(58,1,1,'admin','87.204.0.67',1351588046,'site','pochp'),(59,1,1,'admin','87.204.0.67',1351588064,'site','asd'),(60,1,0,'','87.204.0.67',1352117900,'site','palenie'),(61,1,0,'','87.204.0.67',1352123677,'site','rejestr'),(62,1,0,'','10.10.0.1',1352284634,'site','Wpisz szukaną frazę'),(63,1,0,'','89.77.222.140',1352844836,'site','Wpisz szukaną frazę'),(64,1,0,'','89.77.222.140',1352845512,'site','Wpisz szukaną frazę'),(65,1,0,'','89.77.222.140',1352845690,'site','Wpisz szukaną frazę'),(66,1,0,'','153.19.67.43',1352885427,'site','pochp'),(67,1,0,'','89.71.59.248',1352916271,'site','cat'),(68,1,0,'','87.204.0.67',1353329450,'site','sss'),(69,1,0,'','91.238.216.1',1353512250,'site','jan zieliński'),(70,1,0,'','153.19.102.13',1353590048,'site','Wpisz szukaną frazę'),(71,1,0,'','94.42.177.36',1354006274,'site','slt'),(72,1,0,'','94.42.177.36',1354006284,'site','dlt'),(73,1,0,'','81.219.157.142',1354302343,'site','Wpisz szukaną frazę'),(74,1,0,'','46.204.48.225',1354310288,'site','rehabilitacja'),(75,1,0,'','46.204.48.225',1354310414,'site','Wpisz szukaną frazę'),(76,1,0,'','79.163.251.249',1356634044,'site','Wpisz szukaną frazę'),(77,1,0,'','89.71.196.61',1359049887,'site','copd'),(78,1,0,'','89.71.196.61',1359049902,'site','zintegrowana opiekq'),(79,1,0,'','178.159.166.112',1360754389,'site','Wpisz szukaną frazę'),(80,1,0,'','83.8.88.141',1367864835,'site','picie alkocholu jest złe'),(81,1,0,'','84.10.93.104',1370463393,'site','astma aspirynowa');
/*!40000 ALTER TABLE `exp_search_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_security_hashes`
--

DROP TABLE IF EXISTS `exp_security_hashes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL auto_increment,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL default '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY  (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM AUTO_INCREMENT=37128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_security_hashes`
--

LOCK TABLES `exp_security_hashes` WRITE;
/*!40000 ALTER TABLE `exp_security_hashes` DISABLE KEYS */;
INSERT INTO `exp_security_hashes` VALUES (36861,1372202373,'208.115.111.74','c65f9854af92bebb39c649a5b72de94cf897f6d1'),(36860,1372199266,'46.229.164.98','b24b9e6aa61bb1e2fc1035f0df72eeca793cc874'),(36859,1372199034,'37.140.141.1','46e82aa5be04174ecfdc286f423970bb7f8895ab'),(36858,1372199031,'37.140.141.1','77272505948a1c991d146f3ae1f212deddbc206d'),(36857,1372198592,'66.249.73.165','8c544723859506283445bafe3d10b69000432842'),(36855,1372198410,'66.249.73.165','3ce49ef77a08792800d7d616f2cb5680ba5d97aa'),(36856,1372198410,'66.249.73.165','799073d768a4a687e212885c0907a206122c8922'),(36854,1372197174,'66.249.73.140','ca6f22666da0c3215fa97fbf235bae6fa337e72d'),(36853,1372197174,'66.249.73.140','8ff5e423e29f2cbcdd603fbcda01784f75994dbe'),(36852,1372195337,'208.115.111.74','b27790a9c3a43f24dd50b4d1cac214cd750e9b49'),(36851,1372195337,'208.115.111.74','597cb1b6edbd4ceac1b16e6ffba77c84c4492141'),(37127,1372455227,'89.67.201.245','7860c355d87050ec50e4d6a96bd25d6cdfd94674'),(37126,1372454432,'208.115.111.74','ad6267bf380236f94439cb308cb6f7ca2c0f0697'),(37125,1372453510,'66.249.75.21','719884a244232e01c1459330acf6c9a33fd47e21'),(37124,1372452823,'173.199.116.211','1ccc0240fb09df9fde6cb8a3f991860c334f3d81'),(37123,1372445461,'173.199.116.211','2c06c97e653cb88809be9d51b21e0ed80ef681a6'),(37122,1372445102,'173.199.116.211','5ba94b0dd64ba83fc1ab76d9d2a8fcb0ff226cad'),(37121,1372445043,'66.249.75.178','e712726387fc32a53fd45e14ad2e91acd780ad6b'),(37120,1372445043,'66.249.78.192','8830cfb7ea544c51f839434c446047bf54757b0e'),(37119,1372444504,'178.255.215.71','f129db54e9d63f7c9a3d0e1c66bfcaeffe8d1529'),(37118,1372443163,'94.154.26.14','3f971b613857c6866a3a7e62be83b8611c364bce'),(37117,1372436881,'95.160.126.73','16583bc5e82beed60bebd4b147cb3514cea58885'),(37116,1372436875,'95.160.126.73','09d765bec22269dc966aad164a21aca3b0bed890'),(37115,1372436831,'95.160.126.73','6ea2da50e6af383f2ae3ebc3bd5cb7a81c6efe4e'),(37114,1372431463,'157.55.32.83','1916ec8dd68ae38a60a2a86cd059ded228f38615'),(37113,1372431463,'157.55.32.83','6f3be83b06b3fd53f39658a1bad96cead2a8f216'),(37112,1372430205,'157.55.32.83','6297e1daf85a09ff3c3afcc8523e7d603526da81'),(37111,1372430025,'157.55.32.154','713a21d66f78b68e00e4e8fd7fd4bf08a934b668'),(37110,1372429960,'157.55.32.154','ea08fb53e0f39d5456b707b5f1d4c0cf6c0fc423'),(37109,1372429595,'208.115.111.74','d848441ce489db0e27d52482586730fdde93d284'),(37108,1372427641,'83.17.1.206','70bf8db5781b77f24e9ac4ab9abcfad98a37417f'),(37107,1372427633,'83.17.1.206','34a8f2ab417374412e38112dab141a6023196c82'),(37106,1372424206,'157.55.32.83','d1cf41f4ee55c1efb9456d86b55af29240944f62'),(37105,1372423778,'173.199.116.211','7bb297559aa7a333882fb8514965c41e089e8c97'),(37104,1372420933,'66.249.78.99','b641372ebd904a6ce3d9dbd366650f723191c8ed'),(37103,1372420931,'66.249.78.99','a693ed22514f18674a0fd07843a67f93e8fcdf96'),(37102,1372419552,'66.249.78.192','a880892823746ac723afce2bf14f0c89c8561af7'),(37101,1372419552,'66.249.78.192','4891282939de178f10ba94a3e618a6125740edd9'),(37100,1372416115,'91.218.218.236','7dc23c8ac70e5ab4fbc6e142e094afbdfc0df582'),(37099,1372415244,'180.76.5.194','3ea051cd7badd6968b9c1fcfe14f535b11e27413'),(37098,1372412731,'66.249.73.140','62b48810d207a6c739029d7f38c7be3a4842b81f'),(37097,1372412730,'66.249.73.140','210ef151e1784aa9ac8687ad40db0599162360ab'),(37096,1372408723,'157.56.229.190','98d6c570b19bb4ced9f3da26bd49427021a78bff'),(37095,1372408597,'157.56.229.190','a85f2b0cf47f350a24ef14dce3d655a29ef96551'),(37094,1372407624,'81.219.156.73','feed90f52aaa27f00a960eacc752cbde54b27ad0'),(37093,1372407121,'157.56.93.63','67a09226e0e0e72d2ca7301004150b7affcb6213'),(37092,1372403615,'194.63.133.2','ccc36c28fa8b78623671672235ac4d42970b8958'),(37091,1372402374,'173.199.116.211','6fe42a948a5e8787368289e37aa19def5886c10a'),(37090,1372401199,'157.56.93.63','321740ef43b43546e5cb56dbb0c68a2f87b0333e'),(37089,1372401033,'173.199.116.211','66b508b5c3a9758b9c33a89f2684fa6290eb2c80'),(37088,1372399559,'173.199.116.211','b27b7168417450174662e45afbcba64a13f56d40'),(37087,1372396288,'208.115.111.74','069e1f6dd13ef7642ce1e0f605331811a85989f0'),(37086,1372392626,'66.249.73.165','bc3e9958fa792143eddb34b230c6aebf6eb3d9c4'),(37085,1372385906,'66.249.73.140','b59cc4cbda8006a1486930ffc251dbbe974948f6'),(37084,1372383923,'173.199.116.211','f98df6ced757ba1670c9fe1fb9316c91e60a4171'),(37083,1372382974,'157.56.93.63','91e4dbf710e3db27b383a61939577798682a2938'),(37082,1372382733,'66.249.73.165','1323ae49c0b135cf93cfa469d536c5cd58cee4f9'),(37081,1372382541,'157.56.93.63','e3041d5452807c92c20d3374bc2ae284af5c5287'),(37080,1372378778,'66.249.73.140','23162b457185738c87479d24bc1544dbea2e40a3'),(37079,1372378778,'66.249.73.140','4ff3999480c5db029084334984dd44bc8bc0aa82'),(37078,1372378616,'157.55.32.154','c923dc911a51dc29e68caa0132f7b6dfd8d7a5b3'),(37077,1372378096,'157.55.32.101','0774485f25ddb6b43e5e85c3139973e161b9414b'),(37076,1372377698,'157.55.32.101','9f43bc4c7da3647f7c0499d37a8d4fec8deab07c'),(37075,1372376941,'157.55.35.114','0f7351fd8b7413adc773471027896630f1427b23'),(37074,1372376886,'157.55.35.114','060dee9aea6c3eca4f7f7684596edecd0617db0b'),(37073,1372376200,'192.95.40.217','c737979e92f56ddd595da5c321a2981f93a27852'),(37072,1372376070,'157.56.93.63','b6712a5bd28dde2fa9dcfbaa017bb0c3c3232b22'),(37071,1372376045,'66.249.73.165','630d8edae902c508bb05c0750391b8af567d390b'),(37070,1372375773,'208.115.113.90','a1c438bc1d87dc3464ce439498d4503d4e473cec'),(37069,1372375023,'208.115.111.74','6f4db5dd874d3054c97e9ff8e86d751e755797ea'),(37068,1372374932,'66.249.73.165','94e0f7f02532c63d2592ee219e5a7713e69f1f7d'),(37067,1372373366,'66.249.73.140','d424638204451f00566e3c497c10f6560c06d5ce'),(37066,1372372865,'83.27.155.51','3fb178dc46836d1f6853f80e2847fbe6550b2849'),(37065,1372372847,'83.27.155.51','ce861e5837659a9fe33ffc98e11e15438e0e4e03'),(37064,1372372823,'83.27.155.51','ddd57ac51a94552a0746798876142a3cbaa69093'),(37063,1372372816,'83.27.155.51','295973747c298801faaf63c3a3c349c851441323'),(37062,1372372804,'83.27.155.51','3fe6170b7220ed20dc1a1bd6171c6cd4d89cabc6'),(37061,1372372160,'66.249.73.165','be2a7283b2adee42089132e5933f19646718fc91'),(37060,1372372159,'66.249.73.165','4e9a8a15693034edd18e3a6a5b5233e107dca4c0'),(37059,1372371081,'66.249.73.140','1b17b4214f88b18451126ea08083933bd6474777'),(37058,1372371080,'66.249.73.140','2e2a12b0534babc6664d1d6d605079b394c9d2ae'),(37057,1372370700,'46.229.164.101','5e0afd752cc92993204c33e6f8635d54807ad7c0'),(37056,1372370449,'77.85.219.105','aac7fd4c8a12d7f142928e61e86c5253a3ee98bb'),(37055,1372368584,'77.85.219.105','b8c2ec713344c7bf13c08684e38fa8fd8ff4ae5a'),(37054,1372368295,'157.56.93.63','4369e93bd07b04360b66b2065a2ca9a9c4eeaab9'),(37053,1372368115,'208.115.111.74','09a6a10b7d9d9e4628043139b41033a006144ba9'),(37052,1372367734,'66.249.73.140','d48a235502967c97f215af832510d8eff897235f'),(37051,1372366124,'66.249.73.140','f4758d41acef1992dbda9a9f491b5451f1c833af'),(37050,1372366124,'66.249.73.140','d7a234772806d82038b2a07f05b0b7a6a485efbc'),(37049,1372365261,'199.30.16.21','3fe750d1038d272375d68d04417ffe81b1ce7fb9'),(37048,1372363825,'157.56.93.63','45780d83e7a8b2dd2de17a16d3ffae863cbdb107'),(37047,1372363825,'157.56.93.63','cd01fa41d0fc7b82a45bc47874154dfd72dc9fd9'),(37046,1372361510,'157.56.93.63','b6bb8957741959c906d8d94df264dfe6e117659d'),(37045,1372357552,'89.67.201.245','3ab7fb2887b2e1f5a975b43040b84a76ed2ad5b9'),(37044,1372357000,'180.76.5.100','01f6f8c67e80d17809d2520f4d3346e28e123db2'),(37043,1372352734,'83.26.249.35','96763bab244b4ec6338c1c7a4fdc3cb5a1158ee4'),(37042,1372352665,'83.26.249.35','49f9237f9f8b2d97ffa67b58dd20511a88897d4e'),(37041,1372352542,'83.26.249.35','dd1af6b089fd9ee715a536771d22e0eecd8979dc'),(37040,1372352534,'83.26.249.35','2600d2aa2ce35abd159dc1aeb6d2d2f0da55b3fd'),(37039,1372349466,'66.249.73.140','d5e5e0a8ef6b79aa914c7f3ded268ac74e2f7a4b'),(37038,1372343498,'81.168.254.14','cf8195c708f7e17b7fe8c1a740a525d7e955be09'),(37037,1372342731,'157.56.93.63','08e79f0c4722875c464bd6e95d78435f44f0aff8'),(37036,1372342433,'157.55.32.88','2142955449e9daed0c0811c39fcc9ee2dabd67df'),(37035,1372341679,'157.55.32.88','9a4f82d2dcf55133393e68f2b5e889d365aada6d'),(37034,1372339729,'77.254.74.131','a7fa9423b294aded3ce44992c2c80f436691b82c'),(37033,1372338926,'157.56.93.63','8c92a8f1241ab87b03d069def82504c7caacc831'),(37032,1372338868,'157.55.32.88','0ac780298436b1b79517e243f3d725b9b4cd6476'),(37031,1372336728,'157.55.32.88','225887b6e2d3e8318a44245a48ace518573d22cd'),(37030,1372335619,'157.56.92.160','501e2ad687c3d71d240a7984cf3f757ce3e85815'),(37029,1372335010,'157.56.92.160','6372d35173f1e3f7a8de856805583d6fd68d2baa'),(37028,1372330339,'66.249.73.165','59b7b523b145bee3db1b2376a184b9e4606e3dcb'),(37027,1372330338,'66.249.73.165','4d99e5d1b2f68ee0762e3e9c746bdf754c4ee460'),(37026,1372327624,'91.188.112.1','8c4179a26f4331c0fbacbeb609f141391c0664c7'),(37025,1372326544,'66.249.73.140','19a608d33ee3017e593b5fec1ee9e788bb8f4ada'),(37024,1372326543,'66.249.73.140','c300fb0f6eec1376c2209ba9d91ad328cbc84afa'),(37023,1372324533,'79.191.238.242','f8b60daf788f1098abfab9c9985f7e7707dbdd4c'),(37022,1372324390,'79.191.238.242','3f1fb85aeffeff46dacb826219c2d57f72cafe7a'),(37021,1372324310,'79.191.238.242','37736624dad7c41e09c3ced80a87bcfe6e011b56'),(37020,1372324190,'79.191.238.242','49af39025a92cd9a038f3581d70f11347097a964'),(37019,1372324070,'79.191.238.242','91a61612d6e2313ce5c7af3926be02ce1a78f892'),(37018,1372324025,'79.191.238.242','b19e475b73aab85091eda3b81b019bbda983521f'),(37017,1372324000,'79.191.238.242','b339243c7ee3c8e8cc8fa77e3817f1549f087b12'),(37016,1372320938,'153.19.67.200','00c77a26fcbaab53ddaabc803ec0355cd1a85878'),(37015,1372319420,'157.56.93.63','103c85b17d5b3f9c485624fabbdcac52a58fbe78'),(37014,1372319374,'157.56.93.63','8a017a09f343fb9bd2cd810629e6bb139fc73968'),(37013,1372313760,'157.56.93.63','4efbde053aef3165991bfcb10471847522a75a5b'),(37012,1372313698,'157.56.93.63','99d900e5b51bcedd8993771f538ed7ab1c231730'),(37011,1372309620,'217.153.57.104','8716e831f75ad3d862941c6511e48750be337446'),(37010,1372309473,'217.153.57.104','240642b21b20ac32ad9f72af0868b65fa85626e5'),(37009,1372303658,'208.115.113.90','4a0179ebcba9bfec9151f5a1ae5788128eeece73'),(37008,1372303268,'37.140.141.1','dc237cc0ba6d566df36e0d1c193dd9df52514ea0'),(37007,1372303235,'37.140.141.1','537115620d56cf0e8fa279336f7fa5311d954f57'),(37006,1372303223,'37.140.141.1','5cf3395888ca7b64273b6240325a75d7dc556f55'),(37005,1372303220,'37.140.141.1','52e76a8b2c3651bccd64ea1fe037f347f04d17d2'),(37004,1372303204,'37.140.141.1','b071d64b796311da33c35bf04164ece727db704e'),(37003,1372303201,'37.140.141.1','d86a3ff77998af3b6a031f30b4f25a0d3adcdca7'),(37002,1372303157,'157.55.32.113','7288d6ca4c6ff871215c3981a26d60cc0449e130'),(37001,1372303054,'157.55.32.113','5e2ea014a04e05049ab5c159bd2acddd2b0c04ac'),(37000,1372302428,'157.55.32.154','9106e2fbe16253e97bc5c699d53ffbf703117175'),(36999,1372301627,'66.249.73.140','3bc2920c22eb0192778e5d8871cc1bed21f76713'),(36998,1372301626,'66.249.73.140','4b5bfda1115a475e48b3c35eb69efeb687a8550a'),(36997,1372301421,'157.55.32.154','313ebd5fa0c262aeb7c5a91e84e8277eba5230b5'),(36996,1372299975,'157.56.93.63','9aeb055b416e2ac27a636472a9962c985d348e4d'),(36995,1372299927,'157.56.93.63','901830879e74547c7e44fcc058073e5a7926c1f5'),(36994,1372299368,'66.249.73.165','0311831e7909b363878e2df93e86f461e072631d'),(36993,1372298870,'208.115.111.74','75f0b20158063baa4a2c46ca20500ab518ec850d'),(36992,1372298817,'66.249.73.140','cff103e9865c7b8648ecbe242aa2020bdd5b01e7'),(36991,1372297063,'208.115.113.90','2a73bf9b8ce2d25a3f0a49d7e34c7571e5e56c6c'),(36990,1372296879,'66.249.73.140','dad1aa1d32c3b7ebcd7b9b5afce6d5c100223794'),(36989,1372296879,'66.249.73.140','f629fff1118e66e80e4136e3bf762fbf2ee4601c'),(36988,1372296751,'157.56.93.63','5dc680ab0d4fc460cab67031a9475c5d78292f72'),(36987,1372296663,'157.56.93.63','09bded8e73c2d855a39dcbd7e5135504586dd4e1'),(36986,1372296154,'108.178.53.226','57e8ec2376f0cb7cf61d6460d69a8f91065d2b35'),(36985,1372296060,'108.178.53.226','c5e3ad3d7751e280bf9864297aa18204d3510c1a'),(36984,1372296031,'108.178.53.226','5876393a70b3390f01f33edaa5e2a8d9204fe253'),(36983,1372296030,'108.178.53.226','f4b8017195d7f16073a980e0215ec6714df146bf'),(36982,1372289233,'208.115.113.90','db806e839117292b5d5dd8e174adf2f6ca69cc10'),(36981,1372287053,'54.226.89.228','e6b76d8bdfa04bd79a59cb8cca8f287ce1dd2e63'),(36980,1372286979,'54.227.7.194','a77aa1dd31a933b268faa170239a727f7251552f'),(36979,1372285249,'157.56.93.63','c5744e95d21edfdac058cc75fbab0303313492a4'),(36978,1372283167,'157.56.93.63','ed136d9e7c88954d189cc1d107131b16baa6aa90'),(36977,1372283100,'157.56.93.63','0ff3a45f7e43ee915e280aebe19479e3b0151292'),(36976,1372282899,'66.249.73.165','f082f452c9adace9a638d6654b65321bd442d798'),(36975,1372282899,'66.249.73.165','be1b3d1da0082bd61773508d9de8753d32b4df89'),(36974,1372279865,'157.55.32.113','8c04cec78eaa813ae26a8545fdb6a842ab237d6e'),(36973,1372279506,'157.56.93.63','14c7bf4d762cea3f5a6284aa902da50e78a1b9b2'),(36972,1372278979,'157.56.93.63','10e56c64c734b501331e1f17882821d905247450'),(36971,1372278923,'157.55.32.113','f74d89a7815b7b013fb52a8e91f1362623681eca'),(36970,1372277211,'46.229.164.97','93918e0e62273270cefd1b99f704bac51be62e20'),(36969,1372275381,'66.249.73.140','1abd03d072a8a07d6695345114d955555c48151b'),(36968,1372275380,'66.249.73.140','f97d26644afb64e78872b13e6f180eb2c0f6fb82'),(36967,1372273953,'66.249.73.165','9fe89ae2b4ee2fd21c1dcf1afd5cf33a169a83d9'),(36966,1372272861,'157.55.32.154','456f0eba4a7805df0a528cf74605155c67951de6'),(36965,1372271571,'157.55.32.154','c914b87ae36d01ca74f8db5f7be607b4d1567e0c'),(36964,1372271390,'157.55.32.154','93a23d3f68697a11528735cd6036133948d8f2c3'),(36963,1372271183,'157.55.32.154','db91c135490d8488df2d01cf63fd379a223e5243'),(36962,1372271114,'157.55.32.154','a32e462bbe3728e10cc669f718e017016e1a3b28'),(36961,1372271008,'66.249.73.165','3cd9366131a3195c5b4f8b367d67a5e15628b03e'),(36960,1372271007,'66.249.73.165','27af61a0a2703f32865da4674e418c4d927f48ae'),(36959,1372270625,'195.205.186.182','86b304f39b65567d66ec890b30a9be679091d3d2'),(36958,1372270605,'195.205.186.182','46f49f3d0c0e7dc807b32cf27c4358efd73ef3aa'),(36957,1372270442,'66.249.73.140','ced62030691421cd36df3a685105e2434986caed'),(36956,1372270441,'66.249.73.140','6b9c5a6caee5bc37727c863eac33c55e6802dbd9'),(36955,1372268242,'208.115.113.90','c1e7d908034c6d62a591a8d6fe470b9558dec3a6'),(36954,1372268239,'208.115.113.90','869694e049315077ed5a60a8b01fde49ef6ce8f9'),(36953,1372267447,'157.55.32.154','2ff58357c79b27640857bfd079b9f7e7788280e3'),(36952,1372266548,'157.56.92.159','ee1df2a644ca0a6ff3e58a61217aed75715a074c'),(36951,1372265776,'81.190.14.196','78cb88236cc8a04cffbcd57937ed26d7d5b2f269'),(36950,1372265220,'81.190.14.196','bdf309d30f2d7dddfe5b6f12279b80e699907d96'),(36949,1372265176,'81.190.14.196','4c34d5525bf57aa93b7c9e771721c87bca38c162'),(36948,1372265139,'81.190.14.196','add328dd3d855be31531e607a4f2cee0bfa8d85d'),(36947,1372263761,'91.218.218.236','da446a02bc0fea31915ee1371460ecf9e5ea0f91'),(36946,1372263656,'157.55.32.113','0cc542d0612493ecb5f522fa85c4619011269926'),(36945,1372263556,'157.55.32.113','4be67f67e2f56252f2cdc57ce2ce88591ac5c959'),(36944,1372260777,'46.204.54.245','29c70d4215f9dd5f1c6619acd4caaf102a8415de'),(36943,1372260486,'208.115.113.90','34c13da5215041fa9e2a10c9126ee2e76c3d8174'),(36942,1372260154,'157.56.92.159','7aaa141aa631e0292cebbe6d2e6495c3ac205be1'),(36941,1372259675,'46.238.103.180','60568334a55409403a34f0b2685fa6ebdb76e646'),(36940,1372259133,'83.242.74.208','665f185ea7b2b05ce3ba9db438e28a0e6c1dd33c'),(36939,1372259127,'83.242.74.208','f73ad62fa45a3ce9314cf94e92c59a5109f8696a'),(36938,1372258927,'83.242.74.208','d8d8157233fb33462de02a655b64d41b064dc1a8'),(36937,1372258591,'5.172.254.33','79041aced8c97dfdb82a44eb485bc4f00bc61818'),(36936,1372258571,'5.172.254.62','0e353c702b3bd652970780651af19df98f9f99bf'),(36935,1372258558,'5.172.254.130','f1d774351c9cddfe720ecdc2fc81fb57c38bc564'),(36934,1372258555,'5.172.254.110','6c89c0f3ad84f521cf6a0a7603e632956cfffc77'),(36933,1372258520,'5.172.254.15','52a4904d676971c59fca343f991a9a84d085a1a0'),(36932,1372257783,'89.228.230.199','6b1f03fc1d9f0d3bfe4e6a7fa12172a21e4c0705'),(36931,1372257771,'89.228.230.199','0088ac18df63fc56a1f79ca272f6705d827a2715'),(36930,1372257127,'46.229.164.97','20ed48999aa86f9be1f5e218b6376c00373b8e48'),(36929,1372256469,'157.56.92.159','09277257aaef046561c8f4bae9fc7ee6f7c12991'),(36928,1372255723,'157.56.93.63','9085e27c1584b6c2bfdfa4b30edcb171cae27f3f'),(36927,1372255659,'157.56.93.63','fbc1bf12c91163eba9a630ed4d583a3ef23ef133'),(36926,1372252721,'213.189.36.102','1d6960d2f83bd5c48092034560327c17caa75dad'),(36925,1372250364,'157.56.93.63','cb8a7b91ce7c5d428f12cc3bf986746e3232395d'),(36924,1372250310,'157.56.93.63','e3f9a053d8be3f751cbba68d7ff8609e3374b1e4'),(36923,1372249412,'66.249.73.165','ad446b0ec9e4be4f63db51c6ac1bbb51b1cf3d3d'),(36922,1372249411,'66.249.73.165','d6fba75932c8931677e7f225170104ee07830c0c'),(36921,1372247802,'66.249.73.140','8ed02bcd6409a28cc2cbfa8e563d7684c0e236b5'),(36920,1372247802,'66.249.73.140','31f4d82b974cbb9491228907c408f0852f5d2c64'),(36919,1372247011,'66.249.73.165','f7033e7bd81c396dcc8561ee6bbde18c7fc684d2'),(36918,1372245530,'208.115.111.74','1c57476ccc71eb572b82c25df3173de19b687b1b'),(36917,1372244383,'66.249.73.165','f90abc686bf45c4fe4c1e474497ea9a932c2f33d'),(36916,1372244383,'66.249.73.165','0b2284a84de1d664c9daf7a96376b2705be55da1'),(36915,1372244103,'194.181.0.38','a9fdf0e620d525411c0976e5a2115c1857e905dd'),(36914,1372243379,'158.66.1.38','9b5e5b685221618899718f37ff8133889836d9dd'),(36913,1372243236,'158.66.1.38','9cd39d729b6ec39cf34d06578a98ea2b965318e1'),(36912,1372243181,'158.66.1.38','c471a975537b897071be8b3fd2373a6413b2bac0'),(36911,1372243155,'158.66.1.38','45d3eeb4ece06d75b22544fb7c1c08f47dd4b35b'),(36910,1372243128,'158.66.1.38','9673992d65296b949f01283c28273669de036a15'),(36909,1372243123,'158.66.1.38','ba326e3d9bb0633732b0787a4951baf8ae38b836'),(36908,1372243119,'158.66.1.38','f12d321ea8489eb879abbac954771bec931067b4'),(36907,1372243110,'158.66.1.38','3a377ec6a49b9b1b3023f9c0b4fca95128414227'),(36906,1372243102,'158.66.1.38','acb32f70a678fb71db07e88ebffd026b8e7c7662'),(36905,1372243053,'158.66.1.38','9497ed10a67fe20ec59fe0541d2e53e2d3c4e564'),(36904,1372243018,'158.66.1.38','1d11fc8ad1e7064f40fa69548e5825974a888e4f'),(36903,1372242996,'158.66.1.38','11a4ed618d72502349cd11704e389cc62cf078ad'),(36902,1372242993,'158.66.1.38','62e5f8efad3a298fbb2dd1047db47b4d72f375fa'),(36901,1372242963,'158.66.1.38','c2a317a953680e308ceb247a43251271d2461592'),(36900,1372242884,'80.50.144.66','69e601795952b8fd25e72199c60de1d556308551'),(36899,1372242831,'80.50.144.66','44c5983f201cdb67cb8667392f17452ed788b04f'),(36898,1372242830,'80.50.144.66','9a53137fc9e0c14569226cf7de09d3f010504b96'),(36897,1372242794,'157.56.93.63','224872062fc175eefbad8d921e4af48fb2a45220'),(36896,1372242738,'80.50.144.66','eca45ccb6415d590ed65ebf1b84883ba3cb77408'),(36895,1372242734,'157.56.93.63','498baaf8de4eda03568b3a83db43726bdca3b6f8'),(36894,1372242732,'80.50.144.66','70002744a2bdc97bf0c2411935cccb81f7214cea'),(36893,1372242705,'80.50.144.66','9c6736360e6500798d94a4c149620e8ca49990e7'),(36892,1372242640,'80.50.144.66','8b37d2e121cd138048c97272c2fdd3eaa36e6138'),(36891,1372242627,'80.50.144.66','98a797471f7b73caaa2b3e108b92480faba248c1'),(36890,1372238549,'208.115.111.74','48e466889fa23ada344c7d07d242b57c536aa3f0'),(36889,1372236866,'157.56.93.63','7f84f54590698e7fc7837376335baf1d944a88e2'),(36888,1372236815,'157.56.93.63','9f0cb18b2ac14138e0e86b5f749d10fffa16f3b4'),(36887,1372236641,'65.55.215.34','8281983e15e99bad1c79e90a8c3c1b535eb4db11'),(36886,1372235943,'79.190.74.10','8b35d364258203176c8028efbeb3a15434283551'),(36885,1372235617,'79.187.241.107','c0826e67b4b9b905a9aef3e0754a6ce0956d4cea'),(36884,1372231734,'66.249.73.140','1852e6b1a8af635f52664dd8899af2802e4be720'),(36883,1372231734,'66.249.73.140','3f606616911e353bd30f2dc4b117329c44db9ffa'),(36882,1372229424,'66.249.73.165','20dc000d8dd93d1516b47ac5286477b38e0abd4c'),(36881,1372229422,'66.249.73.165','b002b9c8fbaccd63c80a024da80a6c346bed593e'),(36880,1372217723,'66.249.73.140','e2a2ae1539b7323e33fe6fdfa214a4e598d72964'),(36879,1372217684,'157.55.33.80','13244eecdb387637f7dffd7b20f501e2f4f2c773'),(36878,1372215763,'157.55.33.80','4970d28c0ea72c61df89fc103cf2f9399776f4bc'),(36877,1372213490,'199.30.16.29','6222aea66e56cb30f20602ca0ab20892d8492184'),(36876,1372213384,'66.249.73.140','6d0e5d7fa2730c5ec63db048152924b89ccee25e'),(36875,1372213193,'66.249.73.140','8323258f08dbfff4ffeb73bb596a63e5471c8577'),(36874,1372211906,'66.249.73.140','af8f60cf66a1d3f5e75b27f697298ad57898a55a'),(36873,1372211804,'66.249.73.140','ca1989209fcb438080697ce8c4f528eb24227af4'),(36872,1372211803,'66.249.73.140','a592f30b87ab2ac87a87807992906809955e6728'),(36871,1372210976,'66.249.73.165','14b65ecf111b35e88428f6204d966573f8633a01'),(36870,1372210753,'208.115.113.90','77053239322be81a8e2fc2c329b059e7ad5b0386'),(36869,1372209625,'66.249.73.165','06f0203b834d9b4ccf062066d8c90663c71f1350'),(36868,1372209624,'208.115.111.74','3e35db905683e03340910f9e11af116d2c24a878'),(36867,1372209624,'66.249.73.165','ae11c6ba3ff5c6d82911f6fb14fb1d8a903eb95a'),(36866,1372202955,'66.249.73.140','bc85320ea8f62d59d91d25ed7f18e4571f1a0a83'),(36865,1372202955,'66.249.73.140','2e0c686f618589962eb5a7a1ba3278b2c4f8bb22'),(36864,1372202826,'208.115.113.90','febe17be97cf3bb3027483bc31e70f15c1bcc6cd'),(36863,1372202434,'46.229.164.97','3e353a0dcba6b85a9000a0e53859866b97873bd3'),(36862,1372202401,'208.115.111.74','064099611a0f4e5336e3fbf64d987471a07f095d');
/*!40000 ALTER TABLE `exp_security_hashes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_sessions`
--

DROP TABLE IF EXISTS `exp_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL default '0',
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `admin_sess` tinyint(1) NOT NULL default '0',
  `ip_address` varchar(16) NOT NULL default '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_sessions`
--

LOCK TABLES `exp_sessions` WRITE;
/*!40000 ALTER TABLE `exp_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_sites`
--

DROP TABLE IF EXISTS `exp_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL auto_increment,
  `site_label` varchar(100) NOT NULL default '',
  `site_name` varchar(50) NOT NULL default '',
  `site_description` text,
  `site_system_preferences` text NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` text NOT NULL,
  PRIMARY KEY  (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_sites`
--

LOCK TABLES `exp_sites` WRITE;
/*!40000 ALTER TABLE `exp_sites` DISABLE KEYS */;
INSERT INTO `exp_sites` VALUES (1,'System Zintegrowanej Opieki pacjentów z POChP','default_site',NULL,'YTo5MDp7czoxMDoic2l0ZV9pbmRleCI7czoxOiIvIjtzOjg6InNpdGVfdXJsIjtzOjE1OiJodHRwOi8vcG9jaHAuZXUiO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6MjM6Imh0dHA6Ly9wb2NocC5ldS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MjY6InBvY2hwLnJlamVzdHJAZ3VtZWQuZWR1LnBsIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czoyMToiQWxla3NhbmRyYSBMdWxraWV3aWN6IjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6MzI6Imh0dHA6Ly9wb2NocC5ldS9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6MToiLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6NzoicmVmcmVzaCI7czo5OiJkZWZ0X2xhbmciO3M6NzoiZW5nbGlzaCI7czo4OiJ4bWxfbGFuZyI7czoyOiJlbiI7czoxMjoic2VuZF9oZWFkZXJzIjtzOjE6InkiO3M6MTE6Imd6aXBfb3V0cHV0IjtzOjE6Im4iO3M6MTM6ImxvZ19yZWZlcnJlcnMiO3M6MToibiI7czoxMzoibWF4X3JlZmVycmVycyI7czozOiI1MDAiO3M6MTE6InRpbWVfZm9ybWF0IjtzOjI6InVzIjtzOjE1OiJzZXJ2ZXJfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJuIjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6MDoiIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjA6IiI7czoxNToiaG9ub3JfZW50cnlfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoic210cCI7czoxMToic210cF9zZXJ2ZXIiO3M6MTM6Im1haWwucG9jaHAuZXUiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MTc6Im5vcmVwbGF5QHBvY2hwLmV1IjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjg6InZlVlE5VXdVIjtzOjExOiJlbWFpbF9kZWJ1ZyI7czoxOiJuIjtzOjEzOiJlbWFpbF9jaGFyc2V0IjtzOjU6InV0Zi04IjtzOjE1OiJlbWFpbF9iYXRjaG1vZGUiO3M6MToibiI7czoxNjoiZW1haWxfYmF0Y2hfc2l6ZSI7czowOiIiO3M6MTE6Im1haWxfZm9ybWF0IjtzOjU6InBsYWluIjtzOjk6IndvcmRfd3JhcCI7czoxOiJ5IjtzOjIyOiJlbWFpbF9jb25zb2xlX3RpbWVsb2NrIjtzOjE6IjUiO3M6MjI6ImxvZ19lbWFpbF9jb25zb2xlX21zZ3MiO3M6MToieSI7czo4OiJjcF90aGVtZSI7czo3OiJkZWZhdWx0IjtzOjIxOiJlbWFpbF9tb2R1bGVfY2FwdGNoYXMiO3M6MToibiI7czoxNjoibG9nX3NlYXJjaF90ZXJtcyI7czoxOiJ5IjtzOjEyOiJzZWN1cmVfZm9ybXMiO3M6MToieSI7czoxOToiZGVueV9kdXBsaWNhdGVfZGF0YSI7czoxOiJ5IjtzOjI0OiJyZWRpcmVjdF9zdWJtaXR0ZWRfbGlua3MiO3M6MToibiI7czoxNjoiZW5hYmxlX2NlbnNvcmluZyI7czoxOiJuIjtzOjE0OiJjZW5zb3JlZF93b3JkcyI7czowOiIiO3M6MTg6ImNlbnNvcl9yZXBsYWNlbWVudCI7czowOiIiO3M6MTA6ImJhbm5lZF9pcHMiO3M6MDoiIjtzOjEzOiJiYW5uZWRfZW1haWxzIjtzOjA6IiI7czoxNjoiYmFubmVkX3VzZXJuYW1lcyI7czowOiIiO3M6MTk6ImJhbm5lZF9zY3JlZW5fbmFtZXMiO3M6MDoiIjtzOjEwOiJiYW5fYWN0aW9uIjtzOjg6InJlc3RyaWN0IjtzOjExOiJiYW5fbWVzc2FnZSI7czozNDoiVGhpcyBzaXRlIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSI7czoxNToiYmFuX2Rlc3RpbmF0aW9uIjtzOjIxOiJodHRwOi8vd3d3LnlhaG9vLmNvbS8iO3M6MTY6ImVuYWJsZV9lbW90aWNvbnMiO3M6MToieSI7czoxMjoiZW1vdGljb25fdXJsIjtzOjMxOiJodHRwOi8vcG9jaHAuZXUvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ4OiIvaG9tZS9wb2NocC9kb21haW5zL3BvY2hwLmV1L3B1YmxpY19odG1sL3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7fQ==','YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==','YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzE6Imh0dHA6Ly9wb2NocC5ldS9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjE6Ii8iO3M6MTY6ImF2YXRhcl9tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE3OiJhdmF0YXJfbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTM6ImF2YXRhcl9tYXhfa2IiO3M6MjoiNTAiO3M6MTM6ImVuYWJsZV9waG90b3MiO3M6MToibiI7czo5OiJwaG90b191cmwiO3M6Mzc6Imh0dHA6Ly9wb2NocC5ldS9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6MToiLyI7czoxNToicGhvdG9fbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNjoicGhvdG9fbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTI6InBob3RvX21heF9rYiI7czoyOiI1MCI7czoxNjoiYWxsb3dfc2lnbmF0dXJlcyI7czoxOiJ5IjtzOjEzOiJzaWdfbWF4bGVuZ3RoIjtzOjM6IjUwMCI7czoyMToic2lnX2FsbG93X2ltZ19ob3RsaW5rIjtzOjE6Im4iO3M6MjA6InNpZ19hbGxvd19pbWdfdXBsb2FkIjtzOjE6Im4iO3M6MTE6InNpZ19pbWdfdXJsIjtzOjQ1OiJodHRwOi8vcG9jaHAuZXUvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTI6InNpZ19pbWdfcGF0aCI7czoxOiIvIjtzOjE3OiJzaWdfaW1nX21heF93aWR0aCI7czozOiI0ODAiO3M6MTg6InNpZ19pbWdfbWF4X2hlaWdodCI7czoyOiI4MCI7czoxNDoic2lnX2ltZ19tYXhfa2IiO3M6MjoiMzAiO3M6MTk6InBydl9tc2dfdXBsb2FkX3BhdGgiO3M6MToiLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToieSI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxMToidG90YWxfcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==','YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJ5IjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo2MzoiL3Zhci9hcHBzL3B0cHotcG9jaHAvY3VycmVudC9zeXN0ZW0vZXhwcmVzc2lvbmVuZ2luZS90ZW1wbGF0ZXMvIjt9','YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjE6ImMiO3M6MjM6ImF1dG9fY29udmVydF9oaWdoX2FzY2lpIjtzOjE6Im4iO3M6MjI6Im5ld19wb3N0c19jbGVhcl9jYWNoZXMiO3M6MToieSI7czoyMzoiYXV0b19hc3NpZ25fY2F0X3BhcmVudHMiO3M6MToieSI7fQ==','YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NTA6Ii9ob21lL3BvY2hwL2RvbWFpbnMvcG9jaHAuZXUvcHVibGljX2h0bWwvaW5kZXgucGhwIjtzOjMyOiJmOTQ4M2I1YjFiMDdlMWQ2MGRhYmE2ZTY5YmFmYmE0OCI7fQ==','YToxOntpOjE7YTozOntzOjM6InVybCI7czoxNjoiaHR0cDovL3BvY2hwLmV1LyI7czo0OiJ1cmlzIjthOjE6e2k6MTtzOjE0OiIvenljaWUtei1wb2NocCI7fXM6OToidGVtcGxhdGVzIjthOjE6e2k6MTtzOjI6IjExIjt9fX0=');
/*!40000 ALTER TABLE `exp_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_snippets`
--

DROP TABLE IF EXISTS `exp_snippets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY  (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_snippets`
--

LOCK TABLES `exp_snippets` WRITE;
/*!40000 ALTER TABLE `exp_snippets` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_snippets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_specialty_templates`
--

DROP TABLE IF EXISTS `exp_specialty_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `enable_template` char(1) NOT NULL default 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY  (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_specialty_templates`
--

LOCK TABLES `exp_specialty_templates` WRITE;
/*!40000 ALTER TABLE `exp_specialty_templates` DISABLE KEYS */;
INSERT INTO `exp_specialty_templates` VALUES (1,1,'y','offline_template','','<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),(2,1,'y','message_template','','<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),(3,1,'y','admin_notify_reg','Notification of new member registration','New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),(4,1,'y','admin_notify_entry','A new channel entry has been posted','A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),(5,1,'y','admin_notify_mailinglist','Someone has subscribed to your mailing list','A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),(6,1,'y','admin_notify_comment','You have just received a comment','You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),(7,1,'y','mbr_activation_instructions','Enclosed is your activation code','Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),(8,1,'y','forgot_password_instructions','Login information','{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),(9,1,'y','reset_password_notification','New Login Information','{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),(10,1,'y','validated_member_notify','Your membership account has been activated','{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),(11,1,'y','decline_member_validation','Your membership account has been declined','{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),(12,1,'y','mailinglist_activation_instructions','Email Confirmation','Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),(13,1,'y','comment_notification','Someone just responded to your comment','{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),(14,1,'y','comments_opened_notification','New comments have been added','Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),(15,1,'y','private_message_notification','Someone has sent you a Private Message','\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),(16,1,'y','pm_inbox_full','Your private message mailbox is full','{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
/*!40000 ALTER TABLE `exp_specialty_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_stats`
--

DROP TABLE IF EXISTS `exp_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `total_members` mediumint(7) NOT NULL default '0',
  `recent_member_id` int(10) NOT NULL default '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL default '0',
  `total_forum_topics` mediumint(8) NOT NULL default '0',
  `total_forum_posts` mediumint(8) NOT NULL default '0',
  `total_comments` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_forum_post_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `last_visitor_date` int(10) unsigned NOT NULL default '0',
  `most_visitors` mediumint(7) NOT NULL default '0',
  `most_visitor_date` int(10) unsigned NOT NULL default '0',
  `last_cache_clear` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_stats`
--

LOCK TABLES `exp_stats` WRITE;
/*!40000 ALTER TABLE `exp_stats` DISABLE KEYS */;
INSERT INTO `exp_stats` VALUES (1,1,6,6,'alukiewicz',46,0,0,0,1365937959,0,0,1372455227,32,1336388196,1372807169);
/*!40000 ALTER TABLE `exp_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_status_groups`
--

DROP TABLE IF EXISTS `exp_status_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_status_groups`
--

LOCK TABLES `exp_status_groups` WRITE;
/*!40000 ALTER TABLE `exp_status_groups` DISABLE KEYS */;
INSERT INTO `exp_status_groups` VALUES (1,1,'Statuses');
/*!40000 ALTER TABLE `exp_status_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_status_no_access`
--

DROP TABLE IF EXISTS `exp_status_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_status_no_access`
--

LOCK TABLES `exp_status_no_access` WRITE;
/*!40000 ALTER TABLE `exp_status_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_status_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_statuses`
--

DROP TABLE IF EXISTS `exp_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY  (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_statuses`
--

LOCK TABLES `exp_statuses` WRITE;
/*!40000 ALTER TABLE `exp_statuses` DISABLE KEYS */;
INSERT INTO `exp_statuses` VALUES (1,1,1,'open',1,'009933'),(2,1,1,'closed',2,'990000');
/*!40000 ALTER TABLE `exp_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_groups`
--

DROP TABLE IF EXISTS `exp_template_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_groups`
--

LOCK TABLES `exp_template_groups` WRITE;
/*!40000 ALTER TABLE `exp_template_groups` DISABLE KEYS */;
INSERT INTO `exp_template_groups` VALUES (1,1,'site',1,'y'),(2,1,'shared',2,'n');
/*!40000 ALTER TABLE `exp_template_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_member_groups`
--

DROP TABLE IF EXISTS `exp_template_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_member_groups`
--

LOCK TABLES `exp_template_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_template_member_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_template_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_no_access`
--

DROP TABLE IF EXISTS `exp_template_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_no_access`
--

LOCK TABLES `exp_template_no_access` WRITE;
/*!40000 ALTER TABLE `exp_template_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_template_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_templates`
--

DROP TABLE IF EXISTS `exp_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL default 'n',
  `template_type` varchar(16) NOT NULL default 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL default '0',
  `last_author_id` int(10) unsigned NOT NULL default '0',
  `cache` char(1) NOT NULL default 'n',
  `refresh` int(6) unsigned NOT NULL default '0',
  `no_auth_bounce` varchar(50) NOT NULL default '',
  `enable_http_auth` char(1) NOT NULL default 'n',
  `allow_php` char(1) NOT NULL default 'n',
  `php_parse_location` char(1) NOT NULL default 'o',
  `hits` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_templates`
--

LOCK TABLES `exp_templates` WRITE;
/*!40000 ALTER TABLE `exp_templates` DISABLE KEYS */;
INSERT INTO `exp_templates` VALUES (1,1,1,'index','y','webpage','{embed=\"shared/head\" page_id=\"homepage\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"a\">\n    <div id=\"a-\">\n      <div class=\"carousel_wrapper\">\n        <span class=\"action_button prev\">&gt;</span>\n        <span class=\"action_button next\">&lt;</span>\n        {exp:channel:entries channel=\"banners\" limit=\"1\"}\n        <div class=\"carousel\">\n            <ul>\n                {banner}\n                <li style=\"background: url(\'{banner_photo}\') no-repeat scroll right top transparent;\">\n                  <h2 class=\"entry-title\">{banner_title}</h2>\n                  <div class=\"entry-summary\">{banner_short_text}</div>\n                  <div class=\"entry-content\"><p>{banner_text}</p></div>\n\n                  <div class=\"entry-action\"><a href=\"{banner_adress}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n                </li>\n                {/banner}\n            </ul>\n        </div>\n    {/exp:channel:entries}\n      </div>\n    </div>\n  </div>\n  <div id=\"b\">\n    <div id=\"b-\">\n      <div class=\"hfeed\">\n        <div class=\"hentry f section-popchp\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/pochp\"}\" title=\"POChP\">POChP</a></h3>\n          \n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/co_to_jest_pochp\"}\" title=\"Co to jest POChP?\">Co to jest POChP?</a><br />\n            <a href=\"{path=\"strony/czynniki_ryzyka_pochp\"}\" title=\"Czynnki ryzyka POChP\">Czynnki ryzyka POChP</a><br />\n            <a href=\"{path=\"strony/jak_zapobiegac\"}\" title=\"Jak zpobiegać?\">Jak zpobiegać?</a><br />\n	    <a href=\"http://www.catestonline.org/english/index_Polish.htm\" target=\"_blank\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor.png\" width=\"137\" height=\"158\"/></div>\n\n          <div class=\"entry-action\"><a href=\"{path=\"strony/pochp\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n        <div class=\"hentry section-spirometria\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Spirometria\">Spirometria</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/jak_rozpoznac_pochp\"}\" title=\"Jak rozpoznać POChP?\">Jak rozpoznać POChP?</a><br />\n            <a href=\"{path=\"strony/co_to_jest_spirometria\"}\" title=\"Co to jest spirometria?\">Co to jest spirometria?</a><br />\n            <a href=\"{path=\"strony/do_kogo_sie_zwrocic\"}\" title=\"Do kogo się zwrócić?\">Do kogo się zwrócić?</a>\n<br/><br/>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/stetoskop.png\" width=\"132\" height=\"110\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/spirometria\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div> \n        <div class=\"hentry section-palenie-papierosow\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Palenie papierosów\">Palenie papierosów</a></h3>\n          <div class=\"entry-content\">\n            <a href=\"{path=\"strony/dlaczego_palenie_jest_zle\"}\" title=\"Dlaczego palenie jest złe?\">Dlaczego palenie jest złe?</a> <br />\n            <a href=\"{path=\"strony/jak_rzucic\"}\" title=\"Jak rzucić?\">Jak rzucić?</a> <br />\n            <a href=\"{path=\"strony/leczenie_uzaleznienia\"}\" title=\"Leczenie uzsleżnienia\">Leczenie uzależnienia</a> \n            <a href=\"{path=\"strony/palenie_choroba_zakazna\"}\" title=\"Palenie choroba zakaźna\">Palenie choroba zakaźna</a>\n          </div>\n          <div class=\"entry-image\"><img src=\"/images/doctor-card.png\" width=\"93\" height=\"140\"/></div>\n          <div class=\"entry-action\"><a href=\"{path=\"strony/palenie_papierosow\"}\" title=\"Dowiedz się więcej\" class=\"btn\"><i>Dowiedz się więcej</i><em></em></a></div>\n        </div>\n       </div>\n     </div>\n     <div id=\"b--\">\n        <div class=\"hfeed\">\n        <div class=\"hentry f\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"strony/rejestr_lekarzy\"}\" title=\"Rejestr Lekarzy\">Rejestr Lekarzy</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Rejestru POChP</p></div>\n          <div class=\"entry-action\">\n              <a href=\"http://pochp.net/\" title=\"zarejestruj się\"><em>&gt;</em>zarejestruj się</a>\n          </div>\n        </div>\n        <div class=\"hentry m\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"Elektorniczna Karta Pacjenta\">Elektorniczna Karta Pacjenta</a></h3>\n          <div class=\"entry-content\"><p>Uzyskanie uprawnień do korzystania z&nbsp;Elektronicznej Karty Pacjenta</p></div>\n          <div class=\"entry-action\">\n              <a href=\"{path=\"elektroniczna_karta_pacjenta\"}\" title=\"zaloguj się\"><em>&gt;</em>zaloguj się</a>\n          </div>\n        </div>    \n        <div class=\"hentry l\">\n          <h3 class=\"entry-title\"><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\">Kontakt</a></h3>\n          <div class=\"entry-content\"><p>Masz jakieś pytania do nas?&nbsp;Chcesz nam przekazać uwagi?</p></div>\n          <div class=\"entry-action\">\n              <a href=\"/kontakt\" title=\"przejdź do działu pomocy\"><em>&gt;</em>Skontaktuj się z nami</a>\n          </div>\n        </div>        \n        </div>\n      </div>\n    </div>\n  </div>  \n{embed=\"shared/footer\"}','',1360414997,1,'n',0,'','n','n','o',13353),(2,1,2,'index','n','webpage','',NULL,1323087971,0,'n',0,'','n','n','o',0),(3,1,2,'head','y','webpage','<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pl\">\n\n<head>\n	<title>SPIRO</title>\n    <link href=\"/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />\n	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n    <script type=\"text/javascript\" src=\"/javascripts/jquery.js\"></script>\n  <link type=\"text/css\" href=\"/stylesheets/style.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <!--[if IE 7]>\n  <link type=\"text/css\" href=\"/stylesheets/style_ie7.css\" media=\"screen\" rel=\"stylesheet\"/>\n  <![endif]-->\n  \n</head>\n<body {if embed:page_id}class=\"home\"{/if}>\n<div id=\"lc\">','',1351608003,1,'n',0,'','n','n','o',0),(4,1,2,'header','y','webpage','<div id=\"t\">\n  <div id=\"t-\">\n    <div id=\"tl\">\n      <h1><a href=\"/\" title=\"Spiro\">Spiro</a></h1>\n    </div>\n    <div id=\"tr\">\n      <ul>\n        <li class=\"search_form\">\n          {exp:search:simple_form channel=\"pages\" result_page=\"wyniki\" no_result_page=\"brak_wynikow\" search_in=\"everywhere\" name=\"simple_search\"}\n            <fieldset>\n              <div class=\"input\"><input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"Wpisz szukaną frazę\" title=\"Wpisz szukaną frazę\" /></div>\n              <input type=\"submit\" value=\"Szukaj\" title=\"Szukaj\" class=\"btn btn_search\" />\n            </fieldset>\n          {/exp:search:simple_form}\n        </li>          \n        <li class=\"to_medpass\">\n          <form action=\"http://rejestr.pochp.eu/\">\n            <fieldset>\n              <button type=\"submit\"><span>Rejestr POChP</span></button>\n            </fieldset>\n          </form>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>','',1370852578,1,'n',0,'','n','n','o',0),(5,1,2,'menu','y','webpage','<div id=\"m\">\n  <div id=\"m-\">\n    <ul>\n      <li id=\"m1\" class=\"f\"><a href=\"/\" title=\"Strona główna\">Strona główna</a></li>\n      <li id=\"m2\"><a href=\"/strony/o-programie\" title=\"O programie\">O programie</a></li>\n      <li id=\"m3\">\n	<a href=\"/strony/dla-pacjentow-i-opiekunow\" title=\"Dla pacjentów i&nbsp;opiekunów\">Dla pacjentów<br />i opiekunów</a>\n        <ul>\n		<li>\n		  <a href=\"/strony/adresy-placowek-realizujcych-projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n		</li>\n		<li>\n		  <a href=\"/strony/dla-pacjentow-i-opiekunow#gabinety\" title=\"Adresy gabinetów pneumonologicznych\">Adresy gabinetów pneumonologicznych</a>\n		</li>\n		<li>\n		  <a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\" title=\"Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)\">Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</a>\n		</li>\n		<li>\n		  <a target=\"_blank\" href=\"http://www.catestonline.org/english/index_Polish.htm\" title=\"Test oceniający POChP\">Test oceniający POChP</a>\n		</li>\n	</ul>\n      </li>\n      <li id=\"m4\"><a href=\"/strony/rehabilitacja\" title=\"Rehabilitacja\">Rehabilitacja</a></li>\n      <li id=\"m5\"><a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a></li>\n      <li id=\"m6\">\n          <a href=\"/strony/edukacja\" title=\"Edukacja\">Edukacja</a>\n          <ul>\n              <li>\n                  <a href=\"/strony/bibioteka\" title=\"Biblioteka\">Biblioteka</a>\n              </li>\n          </ul>\n      </li>\n        <li id=\"m7\">\n          <a href=\"/forum\" title=\"Forum\">Forum</a>\n      </li>\n    </ul>\n  </div>\n</div>','',1365938139,1,'n',0,'','n','n','o',0),(6,1,2,'footer','y','webpage','  <div id=\"f\">\n    <div id=\"f-\">\n      <div id=\"ft\">\n        <ul>\n          <li>Program wspierają</li>\n        </ul>\n        <div class=\"partner_slider\">\n            <ol>\n            {exp:channel:entries dynamic=\"no\" channel=\"partners\" limit=\"1\"}\n                {logo}\n                    <li><a href=\"{logo_adress}\" title=\"{logo_title}\" rel=\"external\" class=\"partner_ico\" style=\"background-image: url(\'{logo_image}\');\">1{logo_title}</a></li>\n                {/logo}\n            {/exp:channel:entries}\n            </ol>    \n        </div>    \n      </div>\n{embed=\"shared/footermenu\"}\n      <div id=\"fb\">\n        <p>Copyright &copy pochp.eu 2013. Wszelkie prawa zastrzeżone</p>\n      </div>\n    </div>\n  </div>\n</div>\n<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/jcarousellite_1.0.1.js\"></script>\n<script type=\"text/javascript\" src=\"/javascripts/connectmedica.js\"></script>\n</body>\n</html>','',1360412739,1,'n',0,'','n','n','o',1),(8,1,2,'menu_pacjenci','y','webpage','{if embed:menu_id != 0}\n<div id=\"cl\">\n    <div id=\"sm\">\n      <div id=\"sm-\">\n        {exp:channel:entries entry_id=\"{embed:menu_id}\" dynamic=\"no\"}\n        {if menu_title}<h3>{menu_title}</h3>{/if}\n        <ul>\n          {menu_links}\n            <li class=\"f\"><a href=\"{link_adress}\" title=\"{link_title}\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>{link_title}</i></a></li>\n          {/menu_links}\n        </ul>\n        {/exp:channel:entries}\n      </div>\n    </div>\n</div>\n{/if}',NULL,1351516277,1,'n',0,'','n','n','o',0),(10,1,1,'login','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/dla-lekarzy\" title=\"Dla lekarzy\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Dla lekarzy</i></a></li>\n        <li><a href=\"/login\" title=\"Logowanie Medpass\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Logowanie Medpass</i></a></li>\n      </ul>\n    </div>\n    <div class=\"header\">\n      <h2>Dla lekarzy i pracowników opieki medycznej</h2>\n      <span>Aby dostać się do stron dla Lekarzy, zaloguj się podając swój login Medpass:</span>\n    </div>\n    <div id=\"cl\">\n      <div id=\"login_medpass\">\n        <div class=\"input\"><input type=\"text\" /></div>\n        <div class=\"login-action\"><a href=\"#\" title=\"Zaloguj się\" class=\"btn\"><i>Zaloguj się</i><em>&gt;</em></a></div> \n        <div>Nie masz loginu? <a href=\"#\" title=\"Zarejestruj się\">Zarejestruj się!</a></div>\n      </div>\n    </div>\n    <div id=\"cr\">\n      <div id=\"sm\" class=\"login_menu\">\n        <div id=\"sm-\">\n          <h3>Logując się otrzymujesz:</h3>\n          <ul>\n            <li id=\"sm1\" class=\"f em\"><em class=\"red_gt\">&gt;</em><i>Dostęp do artykułów i materiałów szkoleniowych,</i></li>\n            <li id=\"sm2\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Możliwość korzystania z Rejestru Pacjentów POChP,</i></li>\n            <li id=\"sm3\" class=\"em\"><em class=\"red_gt\">&gt;</em><i>Konto Medpass uprawniające do dostępu do wielu polskich portali o tematyce medycznej, m.in:\n              <ul>\n                <li><em>&bull;</em>openmedica.pl</li>\n                <li><em>&bull;</em>kardiolog.pl</li>\n                <li><em>&bull;</em>lekarzonkolog.pl</li>\n                <li><em>&bull;</em>lekarzakaznik.pl</li>\n                <li>... i wielu innych</li>\n              </ul></i>\n            </li>\n          </ul>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}','',1351516263,1,'n',0,'','n','n','o',7),(11,1,1,'strony','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"pages\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        {embed=\"shared/menu_pacjenci\" menu_id=\"{site_menu}\"}\n        <div id=\"cr\" {if {site_menu} == 0}style=\"width: 940px\"{/if}>\n          <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              {if site_abstract}{site_abstract}{/if}\n              {site_content}\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}',NULL,1351516277,1,'n',0,'','n','n','o',11576),(12,1,1,'brak_wynikow','y','webpage','﻿{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"#\" title=\"Wyniki\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Wyniki wyszukwiania</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n    <div id=\"cr\" style=\"width: 940px\">\n      <div class=\"section article txt\">\n        <div class=\"header\">\n          <h2>Wyniki wyszukwiania</h2>\n        </div>\n        <div class=\"content\">\n           <h3>Szukana fraza \"{exp:search:keywords}\" nie została znaleziona.</h3>        \n        </div>\n      </div>\n    </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}','',1351588234,1,'n',0,'','n','n','o',76),(13,1,1,'wyniki','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona g��wna\" class=\"em gt\"><em class=\"home\">Strona g��wna</em></a></li>\n        <li><a href=\"#\" title=\"Wyniki\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Wyniki wyszukwiania</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n	<div id=\"cr\" style=\"width: 940px\">\n      <div class=\"section article txt\">\n        <div class=\"header\">\n          <h2>Wyniki wyszukwiania</h2>\n        </div>\n        <div class=\"content\">\n            <h3>Wyniki znalezione dla \"{exp:search:keywords}\"</h3>\n          {exp:search:search_results backspace=\"7\"}\n            <b><a href=\"{path=\"strony/{url_title}\"}\">{title}</a><br />\n          {/exp:search:search_results}\n        </div>\n      </div>\n    </div>\n	</div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}','',1351588201,1,'n',0,'','n','n','o',42),(14,1,1,'kontakt','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n  <div id=\"c\" class=\"page pomoc\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"{path=\"kontakt\"}\" title=\"Kontakt\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Kontakt</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cl\" style=\"width:380px; margin-left:18px;\">\n            <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>Kontakt</h2>\n            </div>\n            <div class=\"content contact\">\n              {exp:ptpz:create_contact_form}\n            </div>\n          </div>\n        </div>\n        <div id=\"cr\" style=\"width: 450px\">\n            <div class=\"section article txt\">\n		<h3>Nasz adres:</h3>\n                <p>\nKlinika Alergologii<br/>\nul. Dębinki 7 ( budynek 4 piętro 1 )<br/>\n80-211 Gdańsk<br/>\ntel. (58) 349 16 25<br/>\n                </p>\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n</div>\n{embed=\"shared/footer\"}','',1360271963,1,'n',0,'','n','n','o',577),(15,1,1,'elektroniczna_karta_pacjenta','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"ekp\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/elektroniczna_karta_pacjenta\" title=\"Elektroniczna Karta Pacjenta\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Elektroniczna Karta Pacjenta</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n      <div class=\"header\">\n        <h2>Elektroniczna Karta Pacjenta</h2>\n        <span>Twoje dane z Elektronicznej Karty Pacjenta</span>\n      </div>\n        {exp:ptpz:ekp_data}\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}',NULL,1351516277,1,'n',0,'','n','n','o',522),(16,1,1,'elektroniczna_karta_pacjenta_logowanie','y','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"login ekp_login\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        <li><a href=\"/elektroniczna_karta_pacjenta\" title=\"Elektroniczna Karta Pacjenta\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>Elektroniczna Karta Pacjenta</i></a></li>\n      </ul>\n    </div>\n    <div id=\"c--\">\n       <div id=\"cl\">\n        <div class=\"header\">\n          <h2>Elektroniczna Karta Pacjenta</h2>\n        </div>\n        <div class=\"content\">\n          \n          {exp:ptpz:ekp_log_in}\n          <div id=\"login_ekp\">\n          <form method=\"POST\">\n            <div class=\"pesel\"><label>Nr. Pesel:</label> <input type=\"text\" name=\"pesel\" /></div>\n            <div class=\"pin\"><label>Kod Pin: </label> <input type=\"text\" name=\"pin\" class=\"pin\" /></div>\n            <div class=\"login-action\"><input type=\"submit\" title=\"Zaloguj się\" class=\"btn\" value=\"Zaloguj się\" name=\"LogIn\"></div> \n          </form>\n          </div>\n        </div>\n      </div>\n      <div id=\"cr\">\n        <div class=\"login_menu\">\n          <p>Jeśli na wizycie u lekarza pulmonologa, zostałeś włączony do Rejestru Pacjentów z POChP, wpisz poniżej swój PESEL oraz otrzymany na wizycie unikalny KOD.</p>\n          <p>Uzyskasz dzięki temu dostęp do Twoich aktualnych danych i historii Twojej choroby.</p>\n        </div>\n      </div>\n    </div>\n    <div id=\"footer_doctor_bg\"><img src=\"/images/bg_doctor.png\" /></div>\n  </div>\n</div>\n{embed=\"shared/footer\"}','',1351516277,1,'n',0,'','n','n','o',520),(17,1,2,'footermenu','n','webpage','<div class=\"footer-menu\">\n<ul>\n	<li>\n		<a href=\"/strony/o-programie\" title=\"O programie\">O programie</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-pacjentow-i-opiekunow#projekt\" title=\"Adresy placówek realizujących Projekt\">Adresy placówek realizujących Projekt</a>\n	</li>\n\n	<li>\n		<a href=\"/strony/dla-wolontariuszy\" title=\"Dla wolontariuszy\">Dla wolontariuszy</a>\n	</li>\n\n	<li>\n		<a href=\"/forum\" title=\"Forum\">Forum</a>\n	</li>\n	<li>\n		<a href=\"/kontakt\" title=\"Kontakt\">Kontakt</a>\n	</li>\n	<li>\n		<a href=\"/strony/polityka-prywatnoci-i-wykorzystywania-plikow-cookies-w-serwisach-internetow\" title=\"Polityka prywatności\">Polityka prywatności</a>\n	</li>\n</ul>\n</div>','',1364834186,1,'n',0,'','n','n','o',0),(18,1,1,'Mapa','n','webpage','{embed=\"shared/head\"}\n{embed=\"shared/header\"}\n{embed=\"shared/menu\"}\n<div id=\"c\" class=\"page\">\n  <div id=\"c-\">\n    <div id=\"breadcrumb\">\n      <ul>\n        <li class=\"f\">Twoja lokalizacja</li>\n        <li><a href=\"/\" title=\"Strona główna\" class=\"em gt\"><em class=\"home\">Strona główna</em></a></li>\n        {if embed:menu_id != 0}\n            <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{menu_title}</i></a></li>\n        {/if}\n        {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n        <li><a href=\"#\" title=\"{title}\" class=\"em gt\"><em class=\"gt\">&gt;</em><i>{title}</i></a></li>\n	{/exp:channel:entries}\n      </ul>\n    </div>\n    <div id=\"c--\">\n        <div id=\"cr\" style=\"width: 940px\">\n          {exp:channel:entries channel=\"mapa\" limit=\"1\" sort=\"asc\"}\n	  <div class=\"section article txt\">\n            <div class=\"header\">\n              <h2>{title}</h2>\n              {if site_subtitle}<span class=\"subtitle\">{site_subtitle}</span>{/if}\n            </div>\n            <div class=\"content\">\n              <div style=\"width:300px; float:left; clear: none; margin-right:10px\">{opis}</div>\n	      <div style=\"width:605px; float:left; clear: none;\">{iframe_map}</div>\n	      <div style=\"clear:both\"></div>\n		{embed=\"shared/menu_dlt\"}	\n            </div>\n          </div>\n          {/exp:channel:entries}\n        </div>\n    </div>\n    <div id=\"footer_page_bg\"><img src=\"/images/bg_people.png\" /></div>\n  </div>\n  \n</div>\n{embed=\"shared/footer\"}','',1365935703,1,'n',0,'','n','n','o',684),(19,1,2,'menu_dlt','n','webpage','<h4>Adresy placówek prowadzących DOMOWE LECZENIE TLENEM (DLT)</h4>\n<ul>\n <li><a href=\"/mapa/sp-csk\">Warszawa - SP CSK - Klinika Chorób Wewnętrznych, Pneumonologii i Alergologii</a></li>\n <li><a href=\"/mapa/sp-zoz\">Tomaszów Lubelski - SP ZOZ - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/uniwersyteckie-centrum-kliniczne-w-gdasku\">Gdańsk - Uniwersyteckie Centrum Kliniczne w Gdańsku</a></li>\n <li><a href=\"/mapa/poradnia-domowego-leczenia-tlenem\">Ostrów Wielkopolski - Poradnia Domowego Leczenia Tlenem</a></li>\n <li><a href=\"/mapa/samodzielny-publiczny-zakad-opieki-zdrowotnej-w-tarnowie\">Tarnów - Samodzielny Publiczny Zakład Opieki Zdrowotnej w Tarnowie</a></li>\n <li><a href=\"/mapa/niepubliczny-zakad-opieki-zdrowotnej-medyk-sp.-z-o.o\">Chełm - Niepubliczny Zakład Opieki Zdrowotnej \"MEDYK\" Sp. z o.o.</a></li>\n<li><a href=\"/mapa/wojewodzki-zespo-zakadow-opieki-zdrowotnej\">Łódź - Wojewódzki Zespół Zakładów Opieki Zdrowotnej</a></li>\n<li><a href=\"/mapa/samodzielny-publiczny-zespo-grulicy-i-chorob-puc-w-olsztynie\">Olsztyn - Samodzielny Publiczny Zespół Grużlicy i Chorób Płuc w Olsztynie</a></li>\n<li><a href=\"/mapa/zespo-domowego-leczenia-tlenem\">MROZY - ZESPÓŁ DOMOWEGO LECZENIA TLENEM</a></li>\n<li><a href=\"/mapa/orodek-domowego-leczenia-tlenem\">BARTOSZYCE - OŚRODEK DOMOWEGO LECZENIA TLENEM</a></li>\n<li><a href=\"/mapa/orodek-domowego-leczenia-tlenem1\">PRABUTY - OŚRODEK DOMOWEGO LECZENIA TLENEM</a></li>\n</ul>','',1365938057,1,'n',0,'','n','n','o',0);
/*!40000 ALTER TABLE `exp_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_throttle`
--

DROP TABLE IF EXISTS `exp_throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL auto_increment,
  `ip_address` varchar(16) NOT NULL default '0',
  `last_activity` int(10) unsigned NOT NULL default '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_throttle`
--

LOCK TABLES `exp_throttle` WRITE;
/*!40000 ALTER TABLE `exp_throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_upload_no_access`
--

DROP TABLE IF EXISTS `exp_upload_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_upload_no_access`
--

LOCK TABLES `exp_upload_no_access` WRITE;
/*!40000 ALTER TABLE `exp_upload_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_upload_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_upload_prefs`
--

DROP TABLE IF EXISTS `exp_upload_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL default '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL default 'img',
  `max_size` varchar(16) default NULL,
  `max_height` varchar(6) default NULL,
  `max_width` varchar(6) default NULL,
  `properties` varchar(120) default NULL,
  `pre_format` varchar(120) default NULL,
  `post_format` varchar(120) default NULL,
  `file_properties` varchar(120) default NULL,
  `file_pre_format` varchar(120) default NULL,
  `file_post_format` varchar(120) default NULL,
  `cat_group` varchar(255) default NULL,
  `batch_location` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_upload_prefs`
--

LOCK TABLES `exp_upload_prefs` WRITE;
/*!40000 ALTER TABLE `exp_upload_prefs` DISABLE KEYS */;
INSERT INTO `exp_upload_prefs` VALUES (1,1,'Upload','/home/pochp/domains/pochp.eu/public_html/images/uploads/','http://pochp.eu/images/uploads/','all','','','','','','','','','','',NULL);
/*!40000 ALTER TABLE `exp_upload_prefs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-28 23:44:07
